import { defineConfig, Options } from 'tsup';

export default defineConfig((options: Options) => ({
  banner: {
    js: "'use client'",
  },
  external: ['react'],
  name: 'NERVE',
  clean: true,
  // dts: true,
  entry: ['src/index.tsx'],
  format: ['cjs', 'esm'],
  shims: true,
  ...options,
}));
