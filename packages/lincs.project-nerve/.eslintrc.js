module.exports = {
  root: true,
  extends: ['custom'],
  ignorePatterns: [
    // 'README.md',
    '.eslintrc.js',
    '**/studies/**/*.*',
    '**/src-copyy/**/*.*',
    '**/src-changed/**/*.*',
  ],
  // overrides: [
  //   {
  //     env: { node: true },
  //     files: ['**/*.ts', '**/*.tsx'],
  //     extends: [
  //       'next/core-web-vitals',
  //       'turbo',
  //       //* For more relaxed TS rules, uncommend next 2 lines and comment the following 2.
  //       'plugin:@typescript-eslint/recommended',
  //       'plugin:@typescript-eslint/stylistic-type-checked',
  //       // "plugin:@typescript-eslint/recommended-type-checked",
  //       // "plugin:@typescript-eslint/stylistic-type-checked",
  //       'prettier',
  //     ],
  //     parser: '@typescript-eslint/parser',
  //     parserOptions: {
  //       ecmaVersion: 'latest',
  //       sourceType: 'module',
  //       project: ['./tsconfig.json'],
  //     },
  //     plugins: ['@typescript-eslint'],
  //     rules: {
  //       '@typescript-eslint/ban-ts-comment': 1,
  //       '@typescript-eslint/no-explicit-any': 1,
  //       '@typescript-eslint/require-await': 0,
  //     },
  //   },
  // ],
};
