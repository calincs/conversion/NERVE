import {
  Skeleton,
  Stack,
  Typography,
  type SkeletonProps,
  type StackProps,
  type TypographyProps,
} from '@mui/joy';

export interface Props {
  animation?: SkeletonProps['animation'];
  descriptionLevel?: TypographyProps['level'];
  descriptionPlaceholder?: string;
  quantity?: number;
  titlePlaceholder?: string;
  titleLevel?: TypographyProps['level'];
  width?: number;
  slotProps?: {
    stack?: StackProps;
  };
}

export const Skeletons = ({
  animation,
  descriptionLevel = 'body-xs',
  descriptionPlaceholder = 'A place holder for text that describes the entity and its content.',
  quantity = 7,
  titleLevel = 'title-lg',
  titlePlaceholder = 'A place holder for the Entity Label',
  width = 350,
  slotProps,
}: Props) => (
  <>
    {Array(quantity)
      .fill('')
      .map((_, index) => (
        <Stack key={index} width={width} gap={0.5} m={1} px={1} {...slotProps?.stack}>
          <Typography level={titleLevel}>
            <Skeleton animation={animation} loading={true}>
              {titlePlaceholder}
            </Skeleton>
          </Typography>
          <Typography level={descriptionLevel}>
            <Skeleton animation={animation} loading={true}>
              {descriptionPlaceholder}
            </Skeleton>
          </Typography>
        </Stack>
      ))}
  </>
);
