import type { Body } from '@lincs.project/webannotation-schema';
import { List, type ListProps, Sheet, type SheetProps } from '@mui/joy';
import { useAtom, useAtomValue } from 'jotai';
import { getAuthorityFromUri } from '../../../annotation';
import {
  authoritiesAtom,
  authoritiesVisibleAtom,
  candidatesAtom,
  selectedBodyAtom,
  store,
} from '../store';
import { Group } from './group';
import { Item } from './item';
import { ManualEntry } from './manual-entry';
import { Skeletons } from './skeletons';

export type BodyValue = Pick<Body, 'id' | 'label' | 'description' | 'type'>;

interface CollectionProps {
  maxHeight?: number;
  onSelect: (body?: Body) => void;
  onDoubleClick: (body: Body) => void;
  slotProps?: {
    root?: Omit<SheetProps, 'children'>;
    list?: Omit<ListProps, 'children'>;
  };
}

export const Collection = ({
  maxHeight = 500,
  onSelect,
  onDoubleClick,
  slotProps,
}: CollectionProps) => {
  const authorities = useAtomValue(authoritiesAtom);
  const candidates = useAtomValue(candidatesAtom);
  const [selectedBody, setSelectedBody] = useAtom(selectedBodyAtom);

  const handleSetGroupInView = (id: string, isVisible: boolean) => {
    const visible = new Set(store.get(authoritiesVisibleAtom));
    isVisible ? visible.add(id) : visible.delete(id);
    store.set(authoritiesVisibleAtom, [...visible]);
  };

  const changeSelectedBody = (body: Body, isValid = true) => {
    if (selectedBody?.id === body.id) return;

    const newBody = !isValid || body.id === '' ? undefined : body;
    setSelectedBody(newBody);
    onSelect(newBody);
  };

  const doubleClickHandle = (body: Body) => {
    setSelectedBody(body);
    onDoubleClick(body);
  };

  return (
    <Sheet sx={{ flexGrow: 1 }} {...slotProps?.root}>
      <List
        size="sm"
        sx={{
          width: '100%',
          maxHeight,
          overflow: 'auto',
          scrollBehavior: 'smooth',
          p: 0,
          '& ul': {
            p: 0,
          },
        }}
        {...slotProps?.list}
      >
        {!candidates ? (
          <Skeletons />
        ) : (
          authorities?.map((name, index) => (
            <Group key={name} name={name} index={index} setGroupInView={handleSetGroupInView}>
              {name === 'other' && (
                <ManualEntry
                  onBlur={(_event, value) => changeSelectedBody(value.body, value.isValid)}
                  onFocus={(_event, value) => changeSelectedBody(value.body, value.isValid)}
                />
              )}
              {candidates
                ?.filter((candidate) => {
                  const authority = getAuthorityFromUri(candidate.id);
                  return authority === name;
                })
                ?.map((item, i) => (
                  <Item
                    key={`${item.id}-${i}`}
                    body={item}
                    onPointerDown={(_event, value) => changeSelectedBody(value)}
                    onDoubleClick={(_event, value) => doubleClickHandle(value)}
                  />
                ))}
            </Group>
          ))
        )}
      </List>
    </Sheet>
  );
};
