import {
  List,
  ListItem,
  ListProps,
  ListSubheader,
  type ListItemProps,
  type ListSubheaderProps,
} from '@mui/joy';
import chroma from 'chroma-js';
import { useAtomValue } from 'jotai';
import { useEffect } from 'react';
import { useInView } from 'react-intersection-observer';
import { entityTypeAtom } from '../store';

interface GroupProps {
  name: string;
  children: ListItemProps['children'];
  index: number;
  setGroupInView: (id: string, isVisible: boolean) => void;
  slotProps?: {
    root?: Omit<ListItemProps, 'children'>;
    header?: Omit<ListSubheaderProps, 'children'>;
    list?: Omit<ListProps, 'children'>;
  };
}

export const Group = ({ children, index, name, setGroupInView, slotProps }: GroupProps) => {
  const entityType = useAtomValue(entityTypeAtom);

  const { ref, inView, entry } = useInView({ threshold: 0, initialInView: index === 0 });

  useEffect(() => {
    if (entry) setGroupInView(entry.target.id, inView);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inView]);

  return (
    <ListItem key={name} ref={ref} id={name} nested {...slotProps?.root}>
      <ListSubheader
        sx={{
          my: 0.5,
          backgroundColor: ({ palette }) =>
            chroma(palette[entityType][800])
              .luminance(palette.mode === 'dark' ? 0.01 : 0.9)
              .css(),
          fontWeight: 700,
          letterSpacing: 2,
        }}
        variant="soft"
        sticky
        {...slotProps?.header}
      >
        {name}
      </ListSubheader>
      <List {...slotProps?.list}>{children}</List>
    </ListItem>
  );
};
