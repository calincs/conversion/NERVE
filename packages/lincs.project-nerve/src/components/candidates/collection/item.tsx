import {
  IconButton,
  ListItem,
  ListItemButton,
  ListItemContent,
  Typography,
  useTheme,
  type IconButtonProps,
  type ListItemButtonProps,
  type ListItemProps,
  type TypographyProps,
} from '@mui/joy';
import chroma from 'chroma-js';
import { AnimatePresence, motion } from 'framer-motion';
import { useAtomValue } from 'jotai';
import {
  useMemo,
  useState,
  type MouseEvent,
  type PointerEvent,
  type PointerEventHandler,
} from 'react';
import { HiOutlineExternalLink } from 'react-icons/hi';
import type { BodyValue } from '.';
import { entityTypeAtom, selectedBodyAtom } from '../store';

interface ItemProps {
  body: BodyValue;
  onPointerDown: (event: PointerEvent, body: BodyValue) => void;
  onDoubleClick: (event: MouseEvent, body: BodyValue) => void;
  slotProps?: {
    root?: Omit<ListItemProps, 'children' | 'endAction'>;
    iconButtonEndAction?: IconButtonProps;
    listItemButton?: Omit<ListItemButtonProps, 'children' | 'onPointerDown' | 'onDoubleClick'>;
    label?: TypographyProps;
    description?: TypographyProps;
  };
}

export const Item = ({ body, onPointerDown, onDoubleClick, slotProps }: ItemProps) => {
  const { description, id, label } = body;

  const { palette } = useTheme();

  const entityType = useAtomValue(entityTypeAtom);
  const selectedBody = useAtomValue(selectedBodyAtom);

  const [hover, setHover] = useState(false);

  const isSelected = selectedBody?.id === id;

  const accentColor = useMemo(() => palette[entityType][500], [entityType, palette]);

  const handlePointerDown: PointerEventHandler = (event) => {
    if (selectedBody?.id === body.id) return;
    onPointerDown(event, body);
  };

  return (
    <ListItem
      color={entityType}
      endAction={
        <AnimatePresence>
          {hover && (
            <IconButton
              aria-label="Open in new tab"
              component={motion.a}
              initial={{ x: 40 }}
              animate={{ x: 0 }}
              exit={{ x: 40 }}
              href={id}
              size="sm"
              target="_blank"
              sx={{
                borderRadius: 4,
                ':hover': { backgroundColor: chroma(accentColor).alpha(0.15).css() },
              }}
              {...slotProps?.iconButtonEndAction}
            >
              <HiOutlineExternalLink />
            </IconButton>
          )}
        </AnimatePresence>
      }
      sx={{ mx: 0.5, py: 0.25, overflow: 'hidden', scrollpaddingTop: '20px' }}
      {...slotProps?.root}
      onPointerOver={(event) => {
        setHover(true);
        slotProps?.root?.onPointerOver?.(event);
      }}
      onPointerOut={(event) => {
        setHover(false);
        slotProps?.root?.onPointerOut?.(event);
      }}
    >
      <ListItemButton
        onPointerDown={handlePointerDown}
        onDoubleClick={(event) => onDoubleClick(event, body)}
        sx={{
          borderRadius: 8,
          backgroundColor: isSelected ? chroma(accentColor).alpha(0.25).css() : 'inherit',
          ':hover': {
            backgroundColor: isSelected
              ? `${chroma(accentColor).alpha(0.35).css()} !important`
              : chroma(accentColor).alpha(0.15).css(),
          },
        }}
        {...slotProps?.listItemButton}
      >
        <ListItemContent>
          <Typography level="title-sm" fontWeight={700} {...slotProps?.label}>
            {label}
          </Typography>
          {description && (
            <Typography
              fontSize="0.775rem"
              level="body-sm"
              sx={{
                display: isSelected ? 'block' : '-webkit-box',
                // * WebkitBoxOrient is deprecated, but still works. See: https://developer.mozilla.org/en-US/docs/Web/CSS/box-orient
                // TODO: Replace WebkitBoxOrient for another solution.
                WebkitBoxOrient: 'vertical',
                WebkitLineClamp: '3',
                overflow: isSelected ? 'auto' : 'hidden',
                transition: '0.4s',
              }}
              {...slotProps?.description}
            >
              {description}
            </Typography>
          )}
        </ListItemContent>
      </ListItemButton>
    </ListItem>
  );
};
