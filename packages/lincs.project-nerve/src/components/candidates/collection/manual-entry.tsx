import {
  Box,
  FormControl,
  FormHelperText,
  FormLabel,
  IconButton,
  Input,
  Sheet,
  Stack,
  type SheetProps,
  type StackProps,
} from '@mui/joy';
import { AnimatePresence, motion } from 'framer-motion';
import { useAtomValue } from 'jotai';
import {
  useState,
  type ChangeEvent,
  type ChangeEventHandler,
  type FocusEvent,
  type FocusEventHandler,
} from 'react';
import { useTranslation } from 'react-i18next';
import { HiOutlineExternalLink } from 'react-icons/hi';
import type { BodyValue } from '.';
import { getBodyTypeforEntityType } from '../../../annotation';
import { isUriValid } from '../../../utilities';
import { entityTypeAtom } from '../store';

interface ManualEntryProps {
  onBlur?: (event: FocusEvent, value: { body: BodyValue; isValid: boolean }) => void;
  onChange?: (event: ChangeEvent, value: { body: BodyValue }) => void;
  onFocus?: (event: FocusEvent, value: { body: BodyValue; isValid: boolean }) => void;
  slotProps?: {
    root?: Omit<SheetProps, 'children'>;
    stack?: Omit<StackProps, 'children'>;
  };
}

export const ManualEntry = ({ onBlur, onChange, onFocus, slotProps }: ManualEntryProps) => {
  const { t } = useTranslation();

  const entityType = useAtomValue(entityTypeAtom);

  const [input, setInput] = useState<BodyValue>({
    id: '',
    label: '',
    type: getBodyTypeforEntityType(entityType),
  });
  const [isValid, setIsValid] = useState(true);

  const handleChange: ChangeEventHandler<HTMLInputElement> = (event) => {
    const id = event.target.name === 'id' ? event.target.value : input.id;
    const label = event.target.name === 'label' ? event.target.value : input.label;

    const newValue = {
      ...input,
      label,
      id,
      entityType: getBodyTypeforEntityType(entityType),
    };

    setInput(newValue);

    onChange?.(event, { body: newValue });
  };

  const handleBlur: FocusEventHandler<HTMLInputElement> = (event) => {
    const id = event.target.name === 'id' ? event.target.value : input.id;
    setIsValid(isUriValid(id));
    onBlur?.(event, { body: input, isValid });
  };

  const handleFocus: FocusEventHandler<HTMLInputElement> = (event) => {
    onFocus?.(event, { body: input, isValid });
  };

  return (
    <Sheet sx={{ m: 1, p: 1 }} {...slotProps?.root}>
      <Stack spacing={1} {...slotProps?.stack}>
        <FormControl error={!isValid} size="sm">
          <FormLabel>URI</FormLabel>
          <Input
            endDecorator={
              <Box minHeight={32}>
                <AnimatePresence>
                  {isUriValid(input.id) && (
                    <IconButton
                      aria-label="Open in new tab"
                      href={input.id}
                      target="_blank"
                      size="sm"
                      component={motion.a}
                      initial={{ scale: 0 }}
                      animate={{ scale: 1, transition: { delay: 0.4 } }}
                      exit={{ scale: 0 }}
                    >
                      <HiOutlineExternalLink />
                    </IconButton>
                  )}
                </AnimatePresence>
              </Box>
            }
            id="id"
            name="id"
            onBlur={handleBlur}
            onChange={handleChange}
            onFocus={handleFocus}
            required
            size="sm"
            value={input.id}
          />
          <FormHelperText sx={{ textTransform: 'capitalize' }}>
            {t('nerve.common.required')}
          </FormHelperText>
        </FormControl>
        <FormControl size="sm">
          <FormLabel sx={{ textTransform: 'capitalize' }}>{t('nerve.common.label')}</FormLabel>
          <Input
            id="label"
            name="label"
            onBlur={handleBlur}
            onChange={handleChange}
            onFocus={handleFocus}
            size="sm"
            value={input.label}
          />
        </FormControl>
      </Stack>
    </Sheet>
  );
};
