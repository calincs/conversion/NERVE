import { Box, Button, useTheme, type ButtonProps } from '@mui/joy';
import chroma from 'chroma-js';
import { motion } from 'framer-motion';
import { useAtomValue } from 'jotai';
import { useMemo, type PointerEvent } from 'react';
import { entityTypeAtom, menuPositionAtom } from '../store';

interface AuthoritysProps {
  inView?: boolean;
  name: string;
  onPointerDown: (event: PointerEvent, value: string) => void;
  slotProps?: {
    buton?: Omit<ButtonProps, 'onPointerDown'>;
  };
}

export const Authority = ({
  name,
  inView: selected,
  onPointerDown,
  slotProps,
}: AuthoritysProps) => {
  const { palette } = useTheme();

  const entityType = useAtomValue(entityTypeAtom);
  const menuPosition = useAtomValue(menuPositionAtom);

  const accentColor = useMemo(() => palette[entityType][500], [entityType, palette]);
  const selectedTextColor = useMemo(
    () =>
      palette.mode === 'dark'
        ? chroma(accentColor).luminance(0.6).css()
        : chroma(accentColor).luminance(0.3).css(),
    [accentColor, palette.mode],
  );

  const indicatorVariantsVertical = {
    initial: { width: 0, opacity: 0 },
    selected: { width: '100%', opacity: 1 },
  };

  const indicatorVariantsHorizontal = {
    initial: { height: 0, opacity: 0 },
    selected: { height: '100%', opacity: 1 },
  };

  return (
    <Button
      id={`authority-button-${name}`}
      key={name}
      color={entityType}
      onPointerDown={(event) => onPointerDown(event, name)}
      size="sm"
      sx={{
        justifyContent: ['top', 'bottom'].includes(menuPosition) ? 'center' : 'flex-start',
        color: selected ? selectedTextColor : palette.text.primary,
        textTransform: 'capitalize',
        '&:hover': {
          backgroundColor: chroma(accentColor).alpha(0.2).css(),
        },
      }}
      variant="plain"
      {...slotProps?.buton}
    >
      <Box
        position="absolute"
        bottom={['top', 'bottom'].includes(menuPosition) ? 0 : 'inherit'}
        left={['top', 'bottom'].includes(menuPosition) ? 'inherit' : -4}
        height={['top', 'bottom'].includes(menuPosition) ? 2 : 0}
        width={['top', 'bottom'].includes(menuPosition) ? 0 : 2}
        bgcolor={accentColor}
        component={motion.div}
        variants={
          ['top', 'bottom'].includes(menuPosition)
            ? indicatorVariantsVertical
            : indicatorVariantsHorizontal
        }
        initial="initial"
        animate={selected ? 'selected' : 'initial'}
        transition={{ type: 'spring', duration: 0.8 }}
      />
      {name}
    </Button>
  );
};
