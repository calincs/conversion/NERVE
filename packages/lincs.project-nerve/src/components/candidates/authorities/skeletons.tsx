import { Skeleton, Typography, type SkeletonProps, type TypographyProps } from '@mui/joy';

export interface Props {
  animation?: SkeletonProps['animation'];
  direction?: 'row' | 'column';
  quantity?: number;
  level?: TypographyProps['level'];
  placeholder?: string;
}

export const Skeletons = ({
  animation,
  level = 'title-lg',
  placeholder = 'Authority',
  quantity = 4,
}: Props) => (
  <>
    {Array(quantity)
      .fill('')
      .map((_, index) => (
        <Typography key={index} level={level} my={0.5}>
          <Skeleton animation={animation} loading={true}>
            {placeholder}
          </Skeleton>
        </Typography>
      ))}
  </>
);
