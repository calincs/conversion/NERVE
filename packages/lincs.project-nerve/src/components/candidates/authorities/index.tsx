import { Sheet, Stack, type SheetProps, type StackProps } from '@mui/joy';
import chroma from 'chroma-js';
import { useAtomValue } from 'jotai';
import { forwardRef, useLayoutEffect, useRef } from 'react';
import {
  authoritiesAtom,
  authoritiesVisibleAtom,
  entityTypeAtom,
  menuPositionAtom,
} from '../store';
import { Authority } from './authority';
import { Skeletons } from './skeletons';

interface AuthoritiesProps {
  onChange: (name: string) => void;
  slotProps?: {
    root?: Omit<SheetProps, 'children'>;
    stack?: Omit<StackProps, 'children'>;
  };
}

export const Authorities = forwardRef<HTMLDivElement, AuthoritiesProps>(function Authorities(
  { onChange, slotProps }: AuthoritiesProps,
  ref,
) {
  const authorities = useAtomValue(authoritiesAtom);
  const entityType = useAtomValue(entityTypeAtom);
  const authoritiesVisible = useAtomValue(authoritiesVisibleAtom);
  const menuPosition = useAtomValue(menuPositionAtom);

  const refContainer = useRef<HTMLDivElement | null>(null);

  useLayoutEffect(() => {
    if (!refContainer.current || !authoritiesVisible[0]) return;
    refContainer.current
      .querySelector(`#authority-button-${authoritiesVisible[0]}`)
      ?.scrollIntoView({ inline: 'center' });
  }, [authoritiesVisible]);

  return (
    <Sheet invertedColors ref={refContainer} {...slotProps?.root}>
      <Stack
        direction={['top', 'bottom'].includes(menuPosition) ? 'row' : 'column'}
        overflow="auto"
        px={1}
        py={0.25}
        ref={ref}
        gap={0.5}
        sx={{
          backgroundColor: ({ palette }) =>
            ['top', 'bottom'].includes(menuPosition)
              ? chroma(palette[entityType][500]).alpha(0.2).css()
              : 'transparent',
        }}
        minHeight={36}
        {...slotProps?.stack}
      >
        {!authorities || (authorities && authorities.length === 0) ? (
          <Skeletons />
        ) : (
          authorities?.map((name) => (
            <Authority
              key={name}
              inView={authoritiesVisible.includes(name)}
              name={name}
              onPointerDown={(_event, name) => onChange(name)}
            />
          ))
        )}
      </Stack>
    </Sheet>
  );
});
