import type { Body } from '@lincs.project/webannotation-schema';
import { Stack, type StackProps } from '@mui/joy';
import { Provider } from 'jotai';
import { useEffect, useMemo, useRef, useState } from 'react';
import { Annotation, getAuthorityFromUri } from '../../annotation';
import { Authorities } from './authorities';
import { Collection } from './collection';
import {
  authoritiesAtom,
  authoritiesVisibleAtom,
  candidatesAtom,
  entityTypeAtom,
  menuPositionAtom,
  selectedBodyAtom,
  store,
} from './store';

export * from './store';

export type MenuPosition = 'top' | 'bottom' | 'left' | 'right';

const menuFlexPosition: Record<MenuPosition, 'column' | 'column-reverse' | 'row' | 'row-reverse'> =
  {
    top: 'column',
    bottom: 'column-reverse',
    left: 'row',
    right: 'row-reverse',
  };

interface BodyCandidatesProps {
  annotation: Annotation;
  disabled?: boolean;
  maxHeight?: number;
  menuPosition?: MenuPosition;
  onCandidateDoubleClick?: (body: Body) => void;
  onChange?: (selectedBody?: Body) => void;
  setSelectedBody?: (selectedBody?: Body) => void;
  slotProps?: {
    root: Omit<StackProps, 'children' | 'direction' | 'ref'>;
  };
}

export const BodyCandidates = ({
  annotation,
  disabled = false,
  maxHeight = 500,
  menuPosition = 'right',
  onCandidateDoubleClick,
  onChange,
  setSelectedBody,
  slotProps,
}: BodyCandidatesProps) => {
  const refContainer = useRef<HTMLDivElement | null>(null);
  const refAuthoritiesComponent = useRef<HTMLDivElement | null>(null);

  const [collectionMaxHeight, setCollectionMaxHeight] = useState(maxHeight);

  const authorities = useMemo(() => {
    const auths = new Set<string>();

    annotation.bodyCandidates?.forEach(({ id }) => {
      const authority = getAuthorityFromUri(id);
      if (authority) auths.add(authority);
    });

    if (annotation.bodyCandidates) auths.add('other');

    return [...auths];
  }, [annotation?.bodyCandidates]);

  useEffect(() => {
    store.set(candidatesAtom, annotation.bodyCandidates);
    store.set(entityTypeAtom, annotation.entityType ?? 'person');
    store.set(menuPositionAtom, menuPosition);
    store.set(selectedBodyAtom, annotation.bodyCandidates?.[0]);
    store.set(authoritiesAtom, authorities);
    store.set(authoritiesVisibleAtom, [authorities[0] ?? 'other']);
    setSelectedBody?.(annotation.bodyCandidates?.[0]);
  }, [
    annotation?.bodyCandidates,
    annotation?.entityType,
    authorities,
    menuPosition,
    setSelectedBody,
  ]);

  useEffect(() => {
    if (refAuthoritiesComponent.current && ['bottom', 'top'].includes(menuPosition)) {
      setCollectionMaxHeight(maxHeight - refAuthoritiesComponent.current.offsetHeight);
    }
  }, [collectionMaxHeight, maxHeight, menuPosition]);

  useEffect(() => {
    onChange?.(annotation?.bodyCandidates?.[0]);
  }, [annotation?.bodyCandidates, onChange]);

  const changeSelectedGroup = (authority: string) => {
    if (!refContainer.current) return;
    refContainer.current.querySelector(`#${authority}`)?.scrollIntoView({ block: 'start' });
  };

  const changeSelectedBody = (body?: Body) => {
    onChange?.(body);
    setSelectedBody?.(body);
  };

  const handleItemDoubleClick = (body: Body) => {
    setSelectedBody?.(body);
    onCandidateDoubleClick?.(body);
  };

  return (
    <Stack
      ref={refContainer}
      direction={menuFlexPosition[menuPosition]}
      sx={{
        width: '100%',
        pointerEvents: disabled ? 'none' : 'auto',
        filter: disabled ? 'blur(10px)' : 'none',
        transition: 'all 0.4s',
      }}
      {...slotProps?.root}
    >
      <Provider store={store}>
        <Authorities ref={refAuthoritiesComponent} onChange={changeSelectedGroup} />
        <Collection
          maxHeight={collectionMaxHeight}
          onSelect={changeSelectedBody}
          onDoubleClick={handleItemDoubleClick}
        />
      </Provider>
    </Stack>
  );
};
