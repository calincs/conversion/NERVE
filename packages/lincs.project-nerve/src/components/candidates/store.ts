import type { Body } from '@lincs.project/webannotation-schema';
import { atom, createStore } from 'jotai';
import type { MenuPosition } from '.';
import type { EntityType } from '../../types';

export const authoritiesAtom = atom<string[] | null>(null);
authoritiesAtom.debugLabel = 'authoritiesAtom';

export const authoritiesVisibleAtom = atom<string[]>([]);
authoritiesVisibleAtom.debugLabel = 'authoritiesVisibleAtom';

export const entityTypeAtom = atom<EntityType>('person');
entityTypeAtom.debugLabel = 'entityTypeAtom';

export const candidatesAtom = atom<Body[] | undefined>(undefined);
candidatesAtom.debugLabel = 'candidatesAtom';

export const menuPositionAtom = atom<MenuPosition>('top');
menuPositionAtom.debugLabel = 'menuPositionAtom';

export const selectedBodyAtom = atom<Body | undefined>(undefined);
selectedBodyAtom.debugLabel = 'selectedBodyAtom';

export const isSubmittingAtom = atom(false);
isSubmittingAtom.debugLabel = 'isSubmittingAtom';

export const store = createStore();
