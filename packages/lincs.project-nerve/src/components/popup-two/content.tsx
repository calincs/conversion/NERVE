import {
  Sheet,
  Stack,
  Typography,
  useTheme,
  type SheetProps,
  type StackProps,
  type TypographyProps,
} from '@mui/joy';
import chroma from 'chroma-js';
import { useMemo } from 'react';
import type { IconType } from 'react-icons';
import { MdHelpCenter } from 'react-icons/md';
import { Icon, type IconName } from '../../icons';
import type { EntityType } from '../../types';

export interface Props {
  isDraft?: boolean;
  entityType: EntityType;
  label?: string;
  quote: string;
  slotProps?: {
    root?: Omit<SheetProps, 'children'>;
    stack?: Omit<StackProps, 'children'>;
    quote?: {
      root: Omit<StackProps, 'children'>;
      icon?: Omit<typeof Icon, 'name'>;
      typography?: Omit<TypographyProps, 'children'>;
    };
    label?: {
      root: Omit<StackProps, 'children'>;
      icon?: IconType;
      typography?: Omit<TypographyProps, 'children'>;
    };
  };
}

export const Content = ({
  isDraft = true,
  entityType = 'conceptualObject',
  label = 'unknwon',
  quote,
  slotProps,
}: Props) => {
  const theme = useTheme();

  const icon = useMemo(
    () => (isDraft ? `${entityType}Draft` : entityType) as IconName,
    [entityType, isDraft],
  );

  return (
    <Sheet color={entityType} variant="outlined" {...slotProps}>
      <Stack
        borderRadius={1}
        boxShadow={theme.vars.shadow.md}
        p={isDraft ? 0 : 0.25}
        {...slotProps?.stack}
      >
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          gap={1}
          px={0.5}
          sx={{
            borderTopLeftRadius: 2,
            borderTopRightRadius: 2,
            backgroundColor: isDraft ? 'inherit' : theme.vars.palette[entityType][500],
          }}
          {...slotProps?.quote?.root}
        >
          <Typography
            fontWeight={700}
            sx={{ color: isDraft ? 'inherit' : theme.vars.palette.common.white }}
            {...slotProps?.quote?.typography}
          >
            {quote}
          </Typography>
          <Icon
            name={icon}
            style={{
              height: 18,
              width: 18,
              color: isDraft
                ? theme.vars.palette[entityType][500]
                : theme.vars.palette.common.white,
            }}
            {...slotProps?.quote?.icon}
          />
        </Stack>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          gap={1}
          px={0.5}
          sx={{
            borderBottomLeftRadius: 2,
            borderBottomRightRadius: 2,
            backgroundColor: isDraft
              ? chroma(theme.vars.palette[entityType][500]).alpha(0.2).css()
              : chroma(theme.vars.palette[entityType][500]).darken(1).css(),
          }}
          {...slotProps?.label?.root}
        >
          <Typography
            sx={{ color: isDraft ? 'inherit' : theme.vars.palette.common.white }}
            {...slotProps?.label?.typography}
          >
            {label}
          </Typography>
          {isDraft && (
            <MdHelpCenter
              style={{ height: 18, width: 18, opacity: 0.5 }}
              {...slotProps?.label?.icon}
            />
          )}
        </Stack>
      </Stack>
    </Sheet>
  );
};
