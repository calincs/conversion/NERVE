import { Popper } from '@mui/base';
import { useEffect, useState } from 'react';
import { getTagByAttribute } from '../../viewer/helper';
import { Content, type Props as ContentProps } from './content';

export interface PopupProps extends ContentProps {
  id: string;
}

export const Popup = ({
  isDraft = true,
  entityType = 'person',
  label = 'unknwon',
  quote,
  id,
  ...props
}: Partial<PopupProps>) => {
  const [anchor, setAnchor] = useState<Element | null>(null);

  useEffect(() => {
    if (!id) {
      setAnchor(null);
    } else {
      const element = getTagByAttribute('xml:id', id);
      element ? setAnchor(element) : setAnchor(null);
    }
  }, [id]);

  return (
    <Popper
      open={Boolean(anchor)}
      anchorEl={anchor}
      role={undefined}
      placement="bottom-start"
      transition
      disablePortal
      style={{
        left: `${400 + (anchor?.getBoundingClientRect().left ?? 0)}px !important`,
        top: `${48 + (anchor?.getBoundingClientRect().top ?? 0)}px !important`,
        // translate: `${400 + (anchor?.getBoundingClientRect().left ?? 0)}px ${
        //   48 + (anchor?.getBoundingClientRect().top ?? 0)
        // }px`,
      }}
      // disablePortal={false}
      modifiers={[
        {
          name: 'flip',
          enabled: true,
          options: {
            altBoundary: true,
            rootBoundary: 'document',
            padding: 8,
          },
        },
        {
          name: 'preventOverflow',
          enabled: true,
          options: {
            altAxis: true,
            altBoundary: true,
            tether: true,
            rootBoundary: 'document',
            padding: 8,
          },
        },
        // {
        //   name: 'arrow',
        //   enabled: true,
        //   options: {
        //     element: arrowRef,
        //   },
        // },
      ]}
    >
      <Content
        entityType={entityType}
        isDraft={true}
        label={label ?? 'unknown'}
        quote={quote ?? ''}
        {...props}
      />
    </Popper>
  );
};
