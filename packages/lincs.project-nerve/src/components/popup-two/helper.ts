import { PopupProps } from '.';
import { popupPropsAtom, store } from '../../jotai/store';
import { getTagByAttribute } from '../../viewer/helper';

export const openPopup = (popupProps: Omit<PopupProps, 'anchor'>) => {
  const anchor = getTagByAttribute('xml:id', popupProps.id);
  if (!anchor) return;

  store.set(popupPropsAtom, popupProps);
};

export const closePopup = () => {
  store.set(popupPropsAtom, {});
};
