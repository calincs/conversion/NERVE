import {
  Dropdown,
  IconButton,
  Menu,
  MenuButton,
  Tooltip,
  type MenuButtonProps,
  type MenuProps,
} from '@mui/joy';
import { useAtomValue, useSetAtom } from 'jotai';
import { useCallback, useEffect, useState, type PointerEvent } from 'react';
import { useTranslation } from 'react-i18next';
import { IoMdAdd } from 'react-icons/io';
// import { getTargetString } from '../../annotation';
import { useDocument } from '../../hooks';
import { domSelectionAtom, setEntityLookupDialogAtom } from '../../jotai/store';
import type { EntityType } from '../../types';
import { EntityButton, Props as EntityButtonProps } from './entity-button';

export interface Props {
  slotProps?: {
    menuButton?: MenuButtonProps;
    menu?: Omit<MenuProps, 'children'>;
  };
}

export const ManualIdentiyEntityMenu = ({ slotProps }: Props) => {
  const { t } = useTranslation();

  const { getAnnotationTarget } = useDocument();

  const domSelection = useAtomValue(domSelectionAtom);
  const setEntityLookupDialog = useSetAtom(setEntityLookupDialogAtom);

  const [open, setOpen] = useState(false);

  const options: Pick<EntityButtonProps, 'label' | 'value'>[] = [
    { label: t('nerve.entity.person'), value: 'person' },
    { label: t('nerve.entity.place'), value: 'place' },
    { label: t('nerve.entity.organization'), value: 'organization' },
    { label: t('nerve.entity.work'), value: 'work' },
    { label: t('nerve.entity.event'), value: 'event' },
    { label: t('nerve.entity.physicalThing'), value: 'physicalThing' },
    { label: t('nerve.entity.conceptualObject'), value: 'conceptualObject' },
    { label: t('nerve.entity.citation'), value: 'citation' },
  ];

  const handleOpenChange = useCallback(() => {
    setOpen(open ? false : domSelection ? true : false);
  }, [domSelection, open]);

  // const handlePointerLeave: PointerEventHandler = useCallback(() => {
  //   setOpen(false);
  // }, []);

  const handlPointerDown = useCallback(
    (_event: PointerEvent<Element>, value: EntityType) => {
      const target = getAnnotationTarget();
      console.log(target);
      // setOpen(false);
      // if (!target) return;

      // //TODO: Get Textquote exact
      // const quote = getTargetString(target);
      // setEntityLookupDialog({ entityType: value, target, textSelected: quote });
    },
    [getAnnotationTarget, setEntityLookupDialog],
  );

  useEffect(() => {
    setOpen(domSelection ? true : false);
  }, [domSelection]);

  return (
    <Dropdown open={open} onOpenChange={handleOpenChange}>
      <Tooltip
        enterDelay={700}
        size="sm"
        sx={{ textTransform: 'capitalize' }}
        title={domSelection ? 'Identify Entity' : 'Select Text to Identify Entity'}
        variant="solid"
      >
        <span>
          <MenuButton
            disabled={domSelection ? false : true}
            slots={{ root: IconButton }}
            slotProps={{
              root: {
                'aria-label': 'Manual Add Entity',
                size: 'sm',
              },
            }}
            sx={{
              ':hover': {
                backgroundColor: 'background.level3',
              },
            }}
          >
            <IoMdAdd style={{ rotate: open ? '45deg' : '0deg', transition: 'all 400ms' }} />
          </MenuButton>
        </span>
      </Tooltip>
      <Menu
        // onPointerLeave={handlePointerLeave}
        placement="left"
        size="sm"
        sx={{
          '--List-padding': '0.25rem',
          flexDirection: 'row',
          overflow: 'visible',
          background: 'transparent',
          boxShadow: 'none',
        }}
        variant="plain"
        {...slotProps?.menu}
      >
        {options.map(({ label, value }, i) => (
          <EntityButton
            key={value}
            animationIndex={10 - i}
            label={label}
            onPointerDown={handlPointerDown}
            value={value}
          />
        ))}
      </Menu>
    </Dropdown>
  );
};
