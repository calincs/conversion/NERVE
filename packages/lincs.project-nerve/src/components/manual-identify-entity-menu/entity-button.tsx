import { MenuItem, Tooltip, type MenuItemProps, type TooltipProps } from '@mui/joy';
import { motion } from 'framer-motion';
import { useState, type PointerEvent } from 'react';
import { Icon } from '../../icons';
import type { EntityType } from '../../types';

export interface Props {
  animationDelay?: number;
  animationIndex?: number;
  label: string;
  onPointerDown?: (event: PointerEvent, value: EntityType) => void;
  value: EntityType;
  slopProps?: {
    root?: Omit<MenuItemProps, 'onPointerDown'>;
    tooltip?: Omit<TooltipProps, 'children'>;
  };
}

export const EntityButton = ({
  animationDelay = 0.05,
  animationIndex = 0,
  label,
  onPointerDown,
  value,
  slopProps,
}: Props) => {
  const [hover, setHover] = useState(false);
  return (
    <Tooltip
      enterDelay={700}
      size="sm"
      sx={{ textTransform: 'capitalize' }}
      title={label}
      variant="solid"
      {...slopProps?.tooltip}
    >
      <MenuItem
        color={value}
        onPointerEnter={() => setHover(true)}
        onPointerLeave={() => setHover(false)}
        onPointerDown={(event) => onPointerDown?.(event, value)}
        sx={{ borderRadius: 4, backgroundColor: 'transparent' }}
        component={motion.div}
        animate={{
          opacity: 1,
          transition: { delay: animationIndex * animationDelay },
        }}
        exit={{
          opacity: 0,
          transition: { delay: animationIndex * animationDelay },
        }}
        initial={{ opacity: 0 }}
        {...slopProps?.root}
      >
        <motion.div
          initial={{ scale: 1, x: 0 }}
          animate={{ scale: hover ? 2 : 1, y: hover ? -5 : 0 }}
        >
          <Icon name={value} color={value} />
        </motion.div>
      </MenuItem>
    </Tooltip>
  );
};
