import { Button, Tooltip, useTheme, type IconButtonProps } from '@mui/joy';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { NerveIcon } from '../icons/svg/nerve-icon';

export const NERButton = ({ onPointerDown, ...props }: IconButtonProps) => {
  const theme = useTheme();
  const { t } = useTranslation();

  const [hover, setHover] = useState(false);

  return (
    <Tooltip
      enterDelay={700}
      size="sm"
      title={t('nerve.ner.Run Named Entity Recognition')}
      variant="solid"
    >
      <Button
        aria-label="run ner"
        color="neutral"
        size="sm"
        startDecorator={
          <NerveIcon
            style={{
              padding: 2,
              fill: hover ? 'white' : theme.vars.palette.text.primary,
              opacity: hover ? 0.8 : 1,
              scale: hover ? 1.4 : 1,
              rotate: '-90deg',
              transform: hover ? 'translate(0px, 0px)' : 'translate(0px, 3px)',
              transition: '0.8s all',
            }}
          />
        }
        sx={{
          minWidth: 36,
          overflow: 'hidden',
          '--Button-gap': '0.15rem',
          transition: '0.4s all',

          ':hover': {
            backgroundImage:
              'linear-gradient(to right top, #73c9c0, #00b9ce, #00a5e3, #008bf2, #1268eb)',
            boxShadow: '0px 0px 10px 2px rgba(115, 201, 192, 0.8)',
            color: 'white',
          },
        }}
        {...props}
        onPointerEnter={(event) => {
          setHover(true);
          props.onPointerEnter?.(event);
        }}
        onPointerLeave={(event) => {
          setHover(false);
          props.onPointerLeave?.(event);
        }}
        onPointerDown={(event) => {
          setHover(false);
          onPointerDown?.(event);
        }}
        variant="plain"
      >
        NER
      </Button>
    </Tooltip>
  );
};
