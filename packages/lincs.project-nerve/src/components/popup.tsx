import { Sheet, Stack, Typography, useTheme } from '@mui/joy';
import chroma from 'chroma-js';
import { useMemo } from 'react';
import { MdHelpCenter } from 'react-icons/md';
import { Icon, type IconName } from '../icons';
import type { EntityType } from '../types';

interface PopupProps {
  isDraft: boolean;
  entityType: EntityType;
  id?: string;
  label?: string;
  quote: string;
}

export const Popup = ({ isDraft, entityType, label = 'unknwon', quote }: PopupProps) => {
  const theme = useTheme();

  const icon = useMemo(
    () => (isDraft ? `${entityType}Draft` : entityType) as IconName,
    [entityType, isDraft],
  );

  return (
    <Sheet color={entityType} variant="outlined">
      <Stack borderRadius={1} boxShadow={theme.vars.shadow.sm} p={isDraft ? 0 : 0.25}>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          gap={1}
          px={0.5}
          sx={{
            borderTopLeftRadius: 2,
            borderTopRightRadius: 2,
            backgroundColor: isDraft ? 'inherit' : theme.vars.palette[entityType][500],
          }}
        >
          <Typography
            fontWeight={700}
            sx={{ color: isDraft ? 'inherit' : theme.vars.palette.common.white }}
          >
            {quote}
          </Typography>
          <Icon
            name={icon}
            style={{
              height: 18,
              width: 18,
              color: isDraft
                ? theme.vars.palette[entityType][500]
                : theme.vars.palette.common.white,
            }}
          />
        </Stack>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          gap={1}
          px={0.5}
          sx={{
            borderBottomLeftRadius: 2,
            borderBottomRightRadius: 2,
            backgroundColor: isDraft
              ? chroma(theme.vars.palette[entityType][500]).alpha(0.2).css()
              : chroma(theme.vars.palette[entityType][500]).darken(1).css(),
          }}
        >
          <Typography sx={{ color: isDraft ? 'inherit' : theme.vars.palette.common.white }}>
            {label}
          </Typography>
          {isDraft && <MdHelpCenter style={{ height: 18, width: 18, opacity: 0.5 }} />}
        </Stack>
      </Stack>
    </Sheet>
  );
};
