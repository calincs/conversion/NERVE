import {
  IconButton,
  Input,
  Sheet,
  type IconButtonProps,
  type InputProps,
  type SheetProps,
} from '@mui/joy';
import chroma from 'chroma-js';
import type { ChangeEvent, KeyboardEventHandler } from 'react';
import { forwardRef, useState } from 'react';
import { IoSearchSharp } from 'react-icons/io5';
import type { EntityType } from '../types';

interface Props {
  disabled?: boolean;
  entityType: EntityType;
  onQuery: (value: string) => void;
  selection: string;
  slotProps?: {
    root?: Omit<SheetProps, 'children' | 'ref'>;
    input?: InputProps;
    endDecoratorButton?: Omit<IconButtonProps, 'disabled' | 'color' | 'onPointerDown'>;
  };
}

export const QueryField = forwardRef<HTMLDivElement, Props>(function QueryField(
  { disabled = false, entityType, onQuery, selection, slotProps }: Props,
  ref,
) {
  const [query, setQuery] = useState(selection);

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => setQuery(event.target.value);

  const handleKeyDown: KeyboardEventHandler<HTMLInputElement> = (event) => {
    if (disabled) return;
    if (event.code === 'Enter') handleSearch();
  };

  const handleSearch = () => {
    if (query !== '') onQuery(query);
  };

  return (
    <Sheet
      ref={ref}
      sx={{ p: 0.5, backgroundColor: ({ vars }) => vars.palette[entityType][800] }}
      {...slotProps?.root}
    >
      <Input
        autoComplete="off"
        color="neutral"
        endDecorator={
          <IconButton
            aria-label="trigger-search"
            disabled={disabled}
            color={entityType}
            onPointerDown={() => handleSearch()}
            sx={{ color: 'white' }}
          >
            <IoSearchSharp />
          </IconButton>
        }
        id="query"
        onChange={handleChange}
        onKeyDown={handleKeyDown}
        size="sm"
        value={query}
        variant="solid"
        sx={{
          color: 'white',
          backgroundColor: 'transparent !important',
          '--Input-focusedHighlight': ({ palette }) =>
            `${chroma(palette[entityType][500]).alpha(0.8).css()} !important`,
        }}
        {...slotProps?.input}
      />
    </Sheet>
  );
});
