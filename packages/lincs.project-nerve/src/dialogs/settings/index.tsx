import {
  DialogContent,
  DialogTitle,
  Modal,
  ModalClose,
  ModalDialog,
  type DialogTitleProps,
  type ModalDialogProps,
} from '@mui/joy';
import { useAtom } from 'jotai';
import { PointerEvent } from 'react';
import { useTranslation } from 'react-i18next';
import { settingsDialogOpenAtom } from '../../jotai/store';
import { Main } from './main';

export interface Props {
  slotProps?: {
    root?: Omit<ModalDialogProps, 'chidlren' | 'id'>;
    title: Omit<DialogTitleProps, 'chidlren' | 'id'>;
  };
}

export const SettingsDialog = ({ slotProps }: Props) => {
  const [settingsDialogOpen, setSettingsDialogOpen] = useAtom(settingsDialogOpenAtom);
  const { t } = useTranslation();

  const handleModalClose = async (_event: PointerEvent, _reason: string) => {
    setSettingsDialogOpen(false);
  };

  return (
    <Modal open={settingsDialogOpen} onClose={handleModalClose}>
      <ModalDialog
        aria-labelledby="alert-dialog-title"
        minWidth={600}
        maxWidth={800}
        sx={{ height: 750 }}
      >
        <ModalClose />
        <DialogTitle
          id="alert-dialog-title"
          sx={{ mb: 2, textTransform: 'capitalize' }}
          {...slotProps?.title}
        >
          {t('nerve.common.settings')}
        </DialogTitle>
        <DialogContent>
          <Main />
        </DialogContent>
      </ModalDialog>
    </Modal>
  );
};
