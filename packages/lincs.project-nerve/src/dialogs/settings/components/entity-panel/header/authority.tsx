import { Chip, type ChipProps } from '@mui/joy';
import chroma from 'chroma-js';
import { getAuthorityById } from '../../../../../config/lookups';
import type { EntityType } from '../../../../../types';

interface Props extends ChipProps {
  entityType: EntityType;
  highlight?: boolean;
  id: string;
}

export const Authority = ({ entityType, highlight, id, ...props }: Props) => {
  const name = getAuthorityById(id)?.name ?? id;
  return (
    <Chip
      color={highlight ? entityType : 'neutral'}
      id={id}
      sx={{
        color: ({ palette }) => palette.text.secondary,
        backgroundColor: ({ palette }) => {
          return highlight ? chroma(palette[entityType][500]).alpha(0.2).css() : 'inherit';
        },
      }}
      size="sm"
      variant="outlined"
      {...props}
    >
      {name}
    </Chip>
  );
};
