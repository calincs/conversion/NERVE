import {
  Chip,
  IconButton,
  Stack,
  Typography,
  useTheme,
  type ChipProps,
  type IconButtonProps,
  type StackProps,
  type TypographyProps,
} from '@mui/joy';
import { useMemo, type PointerEventHandler, type PropsWithChildren } from 'react';
import type { IconBaseProps } from 'react-icons';
import { GoChevronDown } from 'react-icons/go';
import { Icon, type IconProps } from '../../../../../icons';
import type { EntityType } from '../../../../../types';
import { useAuthorityServices } from '../useAuthorityService';
import { Authority } from './authority';

interface Props extends PropsWithChildren {
  entityType: EntityType;
  hover?: boolean;
  onPointerDown: PointerEventHandler<HTMLDivElement | HTMLButtonElement>;
  open: boolean;
  showMax?: number;
  slotProps?: {
    root?: Omit<StackProps, 'children' | 'onPointerDown'>;
    label?: {
      root?: Omit<StackProps, 'children'>;
      icon?: Omit<IconProps, 'name'>;
      typography?: Omit<TypographyProps, 'children'>;
    };
    authorities?: {
      root?: Omit<StackProps, 'children'>;
      stack?: Omit<StackProps, 'children'>;
      chip?: Omit<ChipProps, 'children'>;
    };
    openButon?: {
      root?: Omit<IconButtonProps, 'children' | 'onPointerDown'>;
      icon?: IconBaseProps;
    };
  };
}

export const Header = ({
  children,
  entityType,
  hover,
  onPointerDown,
  open,
  showMax = 3,
  slotProps,
}: Props) => {
  const theme = useTheme();
  const { services } = useAuthorityServices(entityType);

  const displayAuthorities = useMemo(
    () => services?.filter((service) => service.active).slice(0, showMax) ?? [],
    [services],
  );

  const numActiveAuthorities = useMemo(
    () => services?.filter((service) => service.active).length ?? 0,
    [services],
  );

  return (
    <Stack
      direction="row"
      alignItems="center"
      justifyContent="space-between"
      onPointerDown={(event) => !open && onPointerDown(event)}
      sx={{ cursor: open ? 'default' : 'pointer' }}
      {...slotProps?.root}
    >
      <Stack direction="row" alignItems="center" gap={1} {...slotProps?.label?.root}>
        <Icon
          name={entityType}
          {...slotProps?.label?.icon}
          style={{
            color:
              hover || open ? theme.vars.palette[entityType][500] : theme.vars.palette.text.icon,
          }}
        />
        <Typography
          color={hover || open ? entityType : 'neutral'}
          fontWeight={open ? 700 : 500}
          level="title-md"
          sx={{ textTransform: 'capitalize' }}
          {...slotProps?.label?.typography}
        >
          {children}
        </Typography>
      </Stack>
      <Stack direction="row" alignItems="center" gap={1} {...slotProps?.authorities?.root}>
        {!open && (
          <Stack direction="row" alignItems="center" gap={1} {...slotProps?.authorities?.stack}>
            {displayAuthorities.map((authority) => (
              <Authority
                key={authority.authorityId}
                entityType={entityType}
                highlight={hover}
                id={authority.authorityId}
              />
            ))}
            {numActiveAuthorities > showMax && (
              <Chip
                color={hover ? entityType : 'neutral'}
                sx={{ backgroundColor: 'transparent' }}
                variant={'outlined'}
                {...slotProps?.authorities?.chip}
              >
                +{numActiveAuthorities - showMax}
              </Chip>
            )}
          </Stack>
        )}
        <IconButton
          onPointerDown={(event) => onPointerDown(event)}
          size="sm"
          {...slotProps?.openButon?.root}
        >
          <GoChevronDown
            style={{ transition: '0.2s', rotate: open ? '0deg' : '90deg' }}
            {...slotProps?.openButon?.icon}
          />
        </IconButton>
      </Stack>
    </Stack>
  );
};
