import { DndContext, PointerSensor, closestCenter, useSensor, useSensors } from '@dnd-kit/core';
import { restrictToFirstScrollableAncestor, restrictToVerticalAxis } from '@dnd-kit/modifiers';
import { SortableContext, verticalListSortingStrategy } from '@dnd-kit/sortable';
import { List, type ListProps } from '@mui/joy';
import type { EntityType } from '../../../../types';
import { Item } from './item';
import { useAuthorityServices } from './useAuthorityService';

interface Props {
  entityType: EntityType;
  slotProps?: {
    root?: Omit<ListProps, 'children'>;
  };
}

export const Collection = ({ entityType, slotProps }: Props) => {
  const sensors = useSensors(useSensor(PointerSensor));

  const { handleDragEnd, handleSelect, services } = useAuthorityServices(entityType);

  return (
    <List size="sm" sx={{ '--List-gap': '2px' }} {...slotProps}>
      {services && (
        <DndContext
          collisionDetection={closestCenter}
          onDragEnd={handleDragEnd}
          modifiers={[restrictToVerticalAxis, restrictToFirstScrollableAncestor]}
          sensors={sensors}
        >
          <SortableContext items={services} strategy={verticalListSortingStrategy}>
            {services.map(({ id, authorityId, active }) => (
              <Item
                key={id}
                authorityId={authorityId}
                entityType={entityType}
                id={id}
                onSelect={(_event, id, active) => handleSelect(id, active)}
                active={active}
              />
            ))}
          </SortableContext>
        </DndContext>
      )}
    </List>
  );
};
