import type { DragEndEvent } from '@dnd-kit/core';
import { arrayMove } from '@dnd-kit/sortable';
import { useLiveQuery } from 'dexie-react-hooks';
import { db } from '../../../../db';
import type { EntityType } from '../../../../types';

export const useAuthorityServices = (entityType: EntityType) => {
  const services = useLiveQuery(
    () =>
      db.entityLookup
        .where({ entityType })
        .filter((service) => !service.disabled)
        .sortBy('order'),
    [],
  );

  const handleSelect = async (id: string, active: boolean) => {
    if (!services) return;

    const oldIndex = services.findIndex((item) => item.id === id);
    const lastActiveIndex = services.findLastIndex((item) => item.active === true);
    const firstInactiveIndex = services.findIndex((item) => item.active === false);

    const newIndex = active
      ? lastActiveIndex + 1
      : firstInactiveIndex > 0
        ? firstInactiveIndex - 1
        : services.length;

    const newOrder = arrayMove(services, oldIndex, newIndex);

    await db.entityLookup.where({ entityType }).modify((value, ref) => {
      const itemIndex = newOrder.findIndex((auth) => auth.id === value.id) ?? 0;
      const item = newOrder.at(itemIndex) ?? value;
      item.order = itemIndex;
      if (item.id === id) item.active = active;
      ref.value = item;
    });
  };

  const handleDragEnd = async (event: DragEndEvent) => {
    const { active, over } = event;

    if (!services) return;
    if (!over) return;
    if (active.id === over.id) return;

    const oldIndex = services.findIndex((item) => item.id === active.id);
    const overItem = services.find((item) => item.id === over.id)!;

    const newIndex =
      overItem.active === false
        ? services.findLastIndex((item) => item.active === true)
        : services.findIndex((item) => item.id === over.id);

    const newOrder = arrayMove(services, oldIndex, newIndex);

    await db.entityLookup.where({ entityType }).modify((value, ref) => {
      const itemIndex = newOrder.findIndex((auth) => auth.id === value.id) ?? 0;
      const item = newOrder.at(itemIndex) ?? value;
      item.order = itemIndex;
      ref.value = item;
    });
  };

  return {
    handleSelect,
    handleDragEnd,
    services,
  };
};
