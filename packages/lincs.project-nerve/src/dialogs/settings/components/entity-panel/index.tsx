import { Sheet, Stack, type SheetProps, type StackProps } from '@mui/joy';
import chroma from 'chroma-js';
import { useState } from 'react';
import type { EntityType } from '../../../../types';
import { getEntityTypeLabelLocalized } from '../../../../utilities';
import { Collection } from './collection';
import { Header } from './header';

interface Props {
  entityType: EntityType;
  slotProps?: {
    root?: Omit<SheetProps, 'children'>;
    stack?: Omit<StackProps, 'children'>;
  };
}

export const EntityPanel = ({ entityType, slotProps }: Props) => {
  const [hover, serHover] = useState(false);
  const [open, setOpen] = useState(false);

  const label = getEntityTypeLabelLocalized(entityType);

  return (
    <Sheet
      color={open ? entityType : 'neutral'}
      onPointerEnter={() => serHover(true)}
      onPointerLeave={() => serHover(false)}
      sx={{
        mx: 1,
        pl: 2,
        pr: 0.5,
        py: 0.5,
        borderRadius: 4,
        backgroundColor: ({ palette }) => {
          return open ? chroma(palette[entityType][500]).alpha(0.1).css() : 'inherit';
        },
        ':hover': {
          backgroundColor: ({ palette }) => {
            return open ? 'inherent' : chroma(palette[entityType][500]).alpha(0.2).css();
          },
        },
      }}
      // variant="soft"
      {...slotProps?.root}
    >
      <Stack {...slotProps?.stack}>
        <Header
          entityType={entityType}
          hover={hover}
          onPointerDown={() => setOpen(!open)}
          open={open}
        >
          {label}
        </Header>
        {open && <Collection entityType={entityType} />}
      </Stack>
    </Sheet>
  );
};
