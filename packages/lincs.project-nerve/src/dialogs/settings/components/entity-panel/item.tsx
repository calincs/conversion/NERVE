import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';
import {
  IconButton,
  ListItem,
  ListItemButton,
  ListItemContent,
  ListItemProps,
  type IconButtonProps,
  type ListItemContentProps,
} from '@mui/joy';
import chroma from 'chroma-js';
import { motion } from 'framer-motion';
import { useState, type PointerEvent } from 'react';
import { CgBlock } from 'react-icons/cg';
import { MdRadioButtonChecked } from 'react-icons/md';
import { getAuthorityById } from '../../../../config/lookups';
import type { EntityType } from '../../../../types';

interface Props {
  active: boolean;
  authorityId: string;
  entityType: EntityType;
  id: string;
  onSelect: (event: PointerEvent<HTMLAnchorElement>, id: string, active: boolean) => void;
  slotProps?: {
    root?: Omit<ListItemProps, 'children' | 'ref'>;
    icon?: Omit<IconButtonProps, 'children' | 'onPointerDown'>;
    content: Omit<ListItemContentProps, 'children'>;
  };
}

export const Item = ({ active, authorityId, id, entityType, onSelect, slotProps }: Props) => {
  const { attributes, listeners, setNodeRef, transform, transition } = useSortable({ id });

  const [isDragging, setIsDragging] = useState(false);

  const name = getAuthorityById(authorityId)?.name ?? authorityId;

  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
  };

  return (
    <ListItem
      component={motion.li}
      ref={setNodeRef}
      endAction={
        <IconButton
          color={entityType}
          onPointerDown={(event) => onSelect(event, id, !active)}
          size="sm"
          variant="plain"
        >
          {active ? <MdRadioButtonChecked /> : <CgBlock style={{ opacity: active ? 1 : 0.6 }} />}
        </IconButton>
      }
      style={style}
      {...slotProps?.root}
    >
      <ListItemButton
        {...attributes}
        {...listeners}
        color={active ? entityType : 'neutral'}
        disabled={!active}
        onMouseDown={() => (active ? setIsDragging(true) : undefined)}
        onMouseUp={() => setIsDragging(false)}
        selected={active}
        sx={{
          borderRadius: 4,
          cursor: isDragging ? 'grabbing' : 'grab',
          backgroundColor: ({ palette }) =>
            active ? chroma(palette[entityType][500]).alpha(0.2).css() : 'transparent',
          ':hover': {
            backgroundColor: ({ palette }) => chroma(palette[entityType][500]).alpha(0.1).css(),
          },
        }}
      >
        <ListItemContent
          sx={{ color: ({ palette }) => (active ? palette.text.primary : palette.text.tertiary) }}
          {...slotProps?.content}
        >
          {name}
        </ListItemContent>
      </ListItemButton>
    </ListItem>
  );
};
