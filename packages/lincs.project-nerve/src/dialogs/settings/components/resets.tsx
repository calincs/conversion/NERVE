import { Button, Stack, type ButtonProps, type StackProps } from '@mui/joy';
import { useTranslation } from 'react-i18next';
import { Icon } from '../../../icons';
import { enqueueSnackbar } from 'notistack';
import { db } from '../../../db';

interface Props {
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    button?: Omit<ButtonProps, 'children'>;
  };
}

export const Resets = ({ slotProps }: Props) => {
  const { t } = useTranslation();

  const handleReserDialogWarnings = async () => {
    await db.doNotDisplayDialogs.clear();
    enqueueSnackbar(t('nerve.dialogs.settings.Confirmation dialog preferences have been reset'), {
      variant: 'info',
    });
  };

  return (
    <Stack alignItems="flex-start" {...slotProps?.root}>
      <Button
        color="neutral"
        startDecorator={<Icon name="reset" />}
        onPointerDown={handleReserDialogWarnings}
        variant="soft"
        {...slotProps?.button}
      >
        {t('nerve.dialogs.settings.Reset Dialog Warnings')}
      </Button>
    </Stack>
  );
};
