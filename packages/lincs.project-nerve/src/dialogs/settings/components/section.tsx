import {
  Divider,
  Stack,
  Typography,
  type DividerProps,
  type StackProps,
  type TypographyProps,
} from '@mui/joy';
import type { PropsWithChildren } from 'react';

interface SectionProps extends PropsWithChildren {
  label: string;
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    typography?: Omit<TypographyProps, 'children'>;
    divider?: DividerProps;
  };
}

export const Section = ({ children, label, slotProps }: SectionProps) => {
  return (
    <Stack {...slotProps?.root}>
      <Typography level="title-md" sx={{ textTransform: 'capitalize' }} {...slotProps?.typography}>
        {label}
      </Typography>
      <Divider sx={{ my: 1 }} {...slotProps?.divider} />
      {children}
    </Stack>
  );
};
