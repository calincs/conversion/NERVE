import { FormControl, FormLabel, Stack, Switch, type StackProps } from '@mui/joy';
import { useAtom } from 'jotai';
import { useTranslation } from 'react-i18next';
import { db } from '../../../db';
import { viewerShowEntityIconAtom } from '../../../jotai/store';

interface Props {
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
  };
}

export const Viewer = ({ slotProps }: Props) => {
  const { t } = useTranslation();

  const [viewerShowEntityIcon, setViewerShowEntityIcon] = useAtom(viewerShowEntityIconAtom);

  const handleReserDialogWarnings = async (value: boolean) => {
    setViewerShowEntityIcon(value);
    await db.settings.put({ id: 'viewerShowEntityIcon', value });
  };

  return (
    <Stack alignItems="flex-start" gap={1} py={1} {...slotProps?.root}>
      <FormControl orientation="horizontal" sx={{ width: '100%', justifyContent: 'space-between' }}>
        <FormLabel>{t('nerve.dialogs.settings.Show icon next to entities')}</FormLabel>
        <Switch
          checked={viewerShowEntityIcon}
          onChange={(event) => handleReserDialogWarnings(event.target.checked)}
        />
      </FormControl>
    </Stack>
  );
};
