import { Stack, StackProps } from '@mui/joy';
import { useTranslation } from 'react-i18next';
import { EntityTypes } from '../../types';
import { EntityPanel, Resets, Section } from './components';
import { Viewer } from './components/viewer';

export interface Props {
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
  };
}

export const Main = ({ slotProps }: Props) => {
  const { t } = useTranslation();

  return (
    <Stack gap={2} {...slotProps?.root}>
      <Section label={t('nerve.common.viewer')}>
        <Viewer />
      </Section>
      <Section label={t('nerve.common.entity lookups')}>
        <Stack gap={1}>
          {EntityTypes.map((entityType) => (
            <EntityPanel key={entityType} entityType={entityType} />
          ))}
        </Stack>
      </Section>
      <Section label={t('nerve.common.reset')}>
        <Resets />
      </Section>
    </Stack>
  );
};
