import { Alert, Grid, Sheet, Stack, Typography, useTheme, type StackProps } from '@mui/joy';
import { useTranslation } from 'react-i18next';
import { IoWarningOutline } from 'react-icons/io5';
import { Icon } from '../../../icons';
import type { EntityType } from '../../../types';
import type { ResultsGroups } from '../hooks';

export interface ResultsProps {
  resultsGroups?: ResultsGroups;
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
  };
}

export const Results = ({ resultsGroups = new Map(), slotProps }: ResultsProps) => {
  const { t } = useTranslation();

  const sumOfAllGroups = Array.from(resultsGroups.values()).reduce((acc, group) => acc + group, 0);

  return (
    <Stack sx={{ gap: 2 }} {...slotProps?.root}>
      <Typography level="body-md" textAlign="center">
        {t(`nerve.ner.found new entities`, { count: sumOfAllGroups })}
      </Typography>
      {sumOfAllGroups > 0 && <EntityTableResult resultsGroups={resultsGroups} />}
      <Alert
        size="sm"
        startDecorator={<IoWarningOutline style={{ width: 16, height: 16 }} />}
        sx={{ alignItems: 'flex-start' }}
        variant="plain"
      >
        {t('nerve.ner.NERVE uses natural language processing and can make mistakes')}.{' '}
        {t('nerve.ner.Please double-check the annotations')}.
      </Alert>
    </Stack>
  );
};

const EntityTableResult = ({ resultsGroups }: { resultsGroups: ResultsGroups }) => (
  <Sheet color="neutral" sx={{ p: 1, borderRadius: 4 }} variant="soft">
    <Grid
      container
      columns={2}
      columnSpacing={3}
      fontWeight={600}
      rowSpacing={1}
      sx={{ flexGrow: 1 }}
    >
      {[...resultsGroups.entries()].map(([entityType, count]) => (
        <EntityTypeResut key={entityType} name={entityType} count={count} />
      ))}
    </Grid>
  </Sheet>
);

const EntityTypeResut = ({ name, count }: { name: EntityType; count: number }) => {
  const { palette } = useTheme();
  const { t } = useTranslation();

  return (
    <Grid xs={1} key={name} borderRight={{ xs: `1px solid ${palette.neutral.outlinedBorder}` }}>
      <Stack direction="row" justifyContent="space-between">
        <Stack direction="row" alignItems="center" gap={0.5}>
          <Icon name={`${name}Draft`} />
          <Typography level="body-sm" sx={{ textTransform: 'capitalize' }}>
            {t(`nerve.entity._plural.${name}`, { count })}
          </Typography>
        </Stack>
        <Typography level="body-sm" textAlign="right">
          {count}
        </Typography>
      </Stack>
    </Grid>
  );
};
