import {
  CircularProgress,
  ListItem,
  Option,
  Select,
  selectClasses,
  Stack,
  Tooltip,
  Typography,
  type CircularProgressProps,
  type ListItemProps,
  type TypographyProps,
} from '@mui/joy';
import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { GoChevronDown } from 'react-icons/go';
import { IoLanguageOutline } from 'react-icons/io5';
import { locales, type Locales } from '../../../i18n';
import { useNerLanguageSupport } from '../hooks';

export type LanguageCode = Locales | 'auto' | (string & {});

interface LanguageOption {
  code: LanguageCode;
  label: string;
}

interface SelectorProps {
  lang: string;
  options: LanguageOption[];
  setLang: (value: string) => void;
}

interface LanguageSwitcherProps {
  lang: SelectorProps['lang'];
  setLang: SelectorProps['setLang'];
  slotProps?: {
    root?: Omit<ListItemProps, 'children'>;
    label?: Omit<TypographyProps, 'children'>;
    progress?: CircularProgressProps;
  };
}

export const LanguageSwitcher = ({ lang, setLang, slotProps }: LanguageSwitcherProps) => {
  const { t } = useTranslation();
  const { data: nerLanguageSupport, isLoading } = useNerLanguageSupport();

  const [options, setOptions] = useState<LanguageOption[]>([
    { code: 'auto', label: t('nerve.common.auto') },
  ]);

  useEffect(() => {
    if (nerLanguageSupport) {
      const nerOptions = nerLanguageSupport.map((code) => ({
        code,
        label: locales.some((c) => c === code) ? t(`nerve.language.${code}`) : code,
      }));
      setOptions([options[0], ...nerOptions]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoading]);

  return (
    <Stack>
      <ListItem
        sx={{
          alignItems: 'center',
          justifyContent: 'space-between',
          gap: 1,
          px: 1.5,
          borderRadius: 4,
          ':hover': { backgroundColor: 'neutral.plainHoverBg' },
        }}
        {...slotProps?.root}
      >
        <Typography
          level="title-sm"
          startDecorator={<IoLanguageOutline style={{ marginRight: 8 }} />}
          sx={{ textTransform: 'capitalize' }}
          {...slotProps?.label}
        >
          {t('nerve.common.language')}
        </Typography>
        {!nerLanguageSupport ? (
          <CircularProgress size="sm" {...slotProps?.progress} />
        ) : (
          <LanguageSelector lang={lang} options={options} setLang={setLang} />
        )}
      </ListItem>
    </Stack>
  );
};

const LanguageSelector = ({ lang, options, setLang }: SelectorProps) => {
  const { t } = useTranslation();

  return (
    <Tooltip
      arrow
      enterDelay={2000}
      placement="top"
      size="sm"
      sx={{ width: 240 }}
      title={
        lang === 'auto' &&
        t(
          'nerve.message.Fallback to the websites language if the language is not defined in the document',
        )
      }
      variant="soft"
    >
      <Select
        defaultValue={lang}
        indicator={<GoChevronDown />}
        onChange={(_event, value) => value && setLang(value)}
        size="sm"
        slotProps={{
          button: {
            sx: {
              justifyContent: 'flex-end',
              textTransform: 'capitalize',
            },
          },
        }}
        sx={{
          backgroundColor: 'neutral.plainHoverBg',
          [`& .${selectClasses.indicator}`]: {
            transition: '0.2s',
            [`&.${selectClasses.expanded}`]: {
              transform: 'rotate(-180deg)',
            },
          },
        }}
        value={lang}
        variant="plain"
      >
        {options.map(({ code, label }) => (
          <Option
            key={code}
            sx={{
              minWidth: 100,
              mx: 0.5,
              my: 0.25,
              borderRadius: 4,
              textTransform: 'capitalize',
            }}
            value={code}
          >
            {label}
          </Option>
        ))}
      </Select>
    </Tooltip>
  );
};
