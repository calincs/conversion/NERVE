import { LinearProgress, Stack, type LinearProgressProps, type StackProps } from '@mui/joy';

export interface ProgressBarProps {
  size?: LinearProgressProps['size'];
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    progressBar?: LinearProgressProps;
  };
}

export const ProgressBar = ({ size = 'lg', slotProps }: ProgressBarProps) => (
  <Stack justifyContent="center" gap={2} height="100%" {...slotProps?.root}>
    <Stack sx={{ width: '100%' }}>
      <LinearProgress size={size} {...slotProps?.progressBar} />
    </Stack>
  </Stack>
);
