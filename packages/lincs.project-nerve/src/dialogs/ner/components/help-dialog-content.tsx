import { Checkbox, Stack, Step, StepIndicator, Stepper, Typography } from '@mui/joy';
import { useTranslation } from 'react-i18next';
import { AiOutlineBulb } from 'react-icons/ai';
import { RiInformationLine } from 'react-icons/ri';
import { db } from '../../../db';
import { GoChevronRight } from 'react-icons/go';

export const HELP_AFTER_NER_DIALOG_ID = 'HELP_AFTER_NER_DIALOG_ID';

export const HelpDialogContent = () => {
  const { t } = useTranslation();

  const handleCheckBoxChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      await db.doNotDisplayDialogs.put({ id: HELP_AFTER_NER_DIALOG_ID });
    } else {
      await db.doNotDisplayDialogs.delete(HELP_AFTER_NER_DIALOG_ID);
    }
  };

  return (
    <Stack gap={2}>
      <Stepper orientation="vertical" size="sm" sx={{ my: 2 }}>
        <Step
          indicator={
            <StepIndicator color="primary" variant="solid">
              <GoChevronRight />
            </StepIndicator>
          }
        >
          <Typography level="title-md">
            {t('nerve.dialogs.after-ner-hint.Draft Annotations')}
          </Typography>
          <Stack spacing={1}>
            <Typography level="body-sm" my={1}>
              {t(
                'nerve.dialogs.after-ner-hint.The newly found named entities will be listed as draft annotations in the side panel and outlined in the document',
              )}
              .{' '}
            </Typography>
            <Typography level="body-sm" my={1}>
              {t(
                'nerve.dialogs.after-ner-hint.Draft annotations are identified on the sidebar with a dotted line around an outlined icon identifying its entity type',
              )}
              .{' '}
              {t(
                'nerve.dialogs.after-ner-hint.In the document panel draft annotations are highlighted with a colour identifying their entity type and a dotted line around them when mouse over them',
              )}
              .
            </Typography>
          </Stack>
        </Step>
        <Step indicator={<AiOutlineBulb />}>
          <Typography color="primary" level="body-sm" variant="plain">
            {t('nerve.dialogs.after-ner-hint.Draft annotations are not saved with your document')}.{' '}
            {t(
              'nerve.dialogs.after-ner-hint.You must link the entity to an authority to approve it',
            )}
            .
          </Typography>
        </Step>
        <Step
          indicator={
            <StepIndicator color="primary" variant="solid">
              <GoChevronRight />
            </StepIndicator>
          }
        >
          <Typography level="title-md">
            {t('nerve.dialogs.after-ner-hint.Approving Annotations')}
          </Typography>
          <Stack spacing={1}>
            <Typography level="body-sm" my={1}>
              {t(
                'nerve.dialogs.after-ner-hint.Click on draft annotation in the side panel or in the document to open a card with linking options',
              )}
              .{' '}
              {t(
                'nerve.dialogs.after-ner-hint.This card allows you to approve reclassify or reject an annotation',
              )}
              .
            </Typography>
            <Typography level="body-sm" my={1}>
              {t(
                'nerve.dialogs.after-ner-hint.Use the group view in the side panel to bulk approve link or reject references to the same entity',
              )}
              .
            </Typography>
            <Typography level="body-sm" my={1}>
              {t(
                'nerve.dialogs.after-ner-hint.Approved annotations are identified on the sidebar with a solid icon identifying its entity type',
              )}
              .{' '}
              {t(
                'nerve.dialogs.after-ner-hint. In the document panel approved annotations are highlighted with a coloured box identifying their entity type and a solid line around them when mouse over them',
              )}
              .
            </Typography>
          </Stack>
        </Step>
        <Step indicator={<AiOutlineBulb />}>
          <Typography color="primary" level="body-sm" variant="plain">
            {t(
              'nerve.dialogs.after-ner-hint.Approving an annotation creates tags around the found named entities and stores the annotation in the document as JSON-LD',
            )}
            .
          </Typography>
        </Step>
        <Step
          indicator={
            <StepIndicator color="primary" variant="solid">
              <GoChevronRight />
            </StepIndicator>
          }
        >
          <Typography level="title-md">
            {t('nerve.dialogs.after-ner-hint.Editing Annotations and Adding Extra Metadata')}
          </Typography>
          <Stack spacing={1}>
            <Typography level="body-sm" my={1}>
              {t(
                'nerve.dialogs.after-ner-hint.To edit or add extra metadata click on an approved annotation to open the card then click the pencil icon',
              )}
              .{' '}
              {t(
                'nerve.dialogs.after-ner-hint.You can also relink reclassify or reject an approved',
              )}
              .
            </Typography>
          </Stack>
        </Step>
        <Step indicator={<AiOutlineBulb />}>
          <Typography color="primary" level="body-sm" variant="plain">
            {t(
              'nerve.dialogs.after-ner-hint.Changes to approved annotations are automatically saved to the document',
            )}
            .
          </Typography>
        </Step>
      </Stepper>
      <Checkbox
        label={t(`nerve.dialogs.Dont show this again`)}
        onChange={handleCheckBoxChange}
        size="sm"
      />
    </Stack>
  );
};
