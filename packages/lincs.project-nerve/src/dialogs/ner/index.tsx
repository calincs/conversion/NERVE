import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Modal,
  ModalDialog,
  type ButtonProps,
  type DialogActionsProps,
  type DialogContentProps,
  type DialogTitleProps,
  type ModalDialogProps,
} from '@mui/joy';
import { motion } from 'framer-motion';
import { useSetAtom } from 'jotai';
import { useResetAtom } from 'jotai/utils';
import { useTranslation } from 'react-i18next';
import { alertDialogAtom, setAlertDialogAtom } from '../../jotai/store';
import {
  HELP_AFTER_NER_DIALOG_ID,
  HelpDialogContent,
  LanguageSwitcher,
  ProgressBar,
  Results,
} from './components';
import { useNer } from './hooks';
import { useLiveQuery } from 'dexie-react-hooks';
import { db } from '../../db';

export type DialogState = 'initial' | 'processing' | 'results';
type CloseReason = 'backdropClick' | 'escapeKeyDown' | 'closeClick' | (string & {});

interface NerDialogProps {
  open: boolean;
  onClose?: (reason?: CloseReason) => void;
  slotProps?: {
    root?: Omit<ModalDialogProps, 'children'>;
    title?: Omit<DialogTitleProps, 'id'>;
    content?: Omit<DialogContentProps, 'children'>;
    actions?: {
      root?: Omit<DialogActionsProps, 'children'>;
      submit?: Omit<ButtonProps, 'children' | 'onPointerDown' | 'onKeyDown'>;
      close?: Omit<ButtonProps, 'children' | 'onPointerDown' | 'onKeyDown'>;
    };
  };
}

export const NerDialog = ({ onClose, open, slotProps }: NerDialogProps) => {
  const { t } = useTranslation();

  const setAlertDialog = useSetAtom(setAlertDialogAtom);
  const resetAlertDialog = useResetAtom(alertDialogAtom);

  const { documentLanguage, dialogState, resultsGroups, setDocumentLanguage, submit } = useNer();

  const shouldShowHelpAfterNer = !useLiveQuery(async () => {
    return await db.doNotDisplayDialogs.where('id').equals(HELP_AFTER_NER_DIALOG_ID).count();
  });

  const handleClose = (reason?: CloseReason) => {
    onClose?.(reason);

    if (reason === 'close' && shouldShowHelpAfterNer) {
      setAlertDialog({
        actions: [
          {
            label: t('nerve.common.close'),
            onPointerDown: () => resetAlertDialog(),
            variant: 'outlined',
          },
        ],
        children: <HelpDialogContent />,
        onClose: () => resetAlertDialog(),
        severity: 'info',
        title: t('nerve.common.whats next'),
        variant: 'soft',
      });
    }
  };

  return (
    <Modal open={open} onClose={(_event, reason) => handleClose(reason)}>
      <ModalDialog
        aria-labelledby="alert-dialog-title"
        sx={{ width: 440, transition: '200ms' }}
        {...slotProps?.root}
      >
        <DialogTitle
          id="alert-dialog-title"
          sx={{ mb: 2, justifyContent: 'center' }}
          {...slotProps?.title}
        >
          {t('nerve.common.named entity recognition')}
        </DialogTitle>
        <DialogContent
          component={motion.div}
          initial={{ height: 'auto' }}
          animate={{ height: dialogState === 'processing' ? 200 : 'auto' }}
          sx={{ flex: 'auto' }}
        >
          {dialogState === 'results' ? (
            <Results resultsGroups={resultsGroups} />
          ) : dialogState === 'processing' ? (
            <ProgressBar />
          ) : (
            <LanguageSwitcher lang={documentLanguage} setLang={setDocumentLanguage} />
          )}
        </DialogContent>
        {dialogState !== 'processing' && (
          <DialogActions
            buttonFlex="none"
            sx={{ justifyContent: 'space-between' }}
            {...slotProps?.actions?.root}
          >
            {dialogState === 'initial' && (
              <Button
                onKeyDown={(event) => event.key === 'Enter' && submit()}
                onPointerDown={submit}
                sx={{ textTransform: 'capitalize' }}
                {...slotProps?.actions?.submit}
              >
                {t('nerve.common.submit')}
              </Button>
            )}
            <Button
              onKeyDown={(event) =>
                event.key === 'Enter' && handleClose(dialogState === 'initial' ? 'cancel' : 'close')
              }
              onPointerDown={() => handleClose(dialogState === 'initial' ? 'cancel' : 'close')}
              sx={{ textTransform: 'capitalize' }}
              variant="plain"
              {...slotProps?.actions?.close}
            >
              {dialogState === 'initial' ? t('nerve.common.cancel') : t('nerve.common.close')}
            </Button>
          </DialogActions>
        )}
      </ModalDialog>
    </Modal>
  );
};
