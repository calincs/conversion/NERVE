import { useQuery } from '@tanstack/react-query';

export const useNerLanguageSupport = () => {
  const { data, isLoading, error } = useQuery({
    queryKey: ['language'],
    queryFn: (): Promise<string[]> =>
      fetch('https://lincs-api.lincsproject.ca/api/language').then((res) => res.json()),
  });

  return {
    data,
    error,
    isLoading,
  };
};
