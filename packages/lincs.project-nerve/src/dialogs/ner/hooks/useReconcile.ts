import { useQuery } from '@tanstack/react-query';
import { reconcile } from '../../../service/lincs-api';
import { ReconcileResult } from '../../../service/lincs-api/types';

export const useLincsReconcile = (query: string, authorities: string[]) => {
  const { data, isLoading, error } = useQuery<ReconcileResult | undefined>({
    queryKey: [query, ...authorities],
    queryFn: async () => await reconcile(query, authorities),
  });

  return {
    data,
    error,
    isLoading,
  };
};
