import i18next from 'i18next';
import { useAtom, useAtomValue } from 'jotai';
import { useState } from 'react';
import type { DialogState } from '..';
import {
  Annotation,
  createTarget,
  createTextPositionSelector,
  createTextQuoteSelector,
} from '../../../annotation';
import { useDocument } from '../../../hooks';
import {
  annotationsAtom,
  documentMetadataAtom,
  documentStringAtom,
  userAtom,
} from '../../../jotai/store';
import { NER_LINCS_ENTITY_MAPPING, dectectLanguage, ner } from '../../../service/lincs-api';
import type { EntityType } from '../../../types';
import { getTextQuotePrefix, getTextQuoteSufix } from '../../../utilities';
import * as utilities from '../../../viewer/utilities';
import { useNerLanguageSupport } from './useNerLanguageSupport';

export type ResultsGroups = Map<EntityType, number>;

export const useNer = () => {
  const [annotations, setAnnotations] = useAtom(annotationsAtom);
  const documentMetadata = useAtomValue(documentMetadataAtom);
  const documentString = useAtomValue(documentStringAtom);
  const user = useAtomValue(userAtom);

  const { renderDocument } = useDocument();
  const nerLanguageSupport = useNerLanguageSupport();

  const [dialogState, setDialogState] = useState<DialogState>('initial');
  const [documentLanguage, setDocumentLanguage] = useState('auto');
  const [resultsGroups, setResultsGroups] = useState<ResultsGroups>(new Map());

  const submit = async () => {
    if (!documentString) return;

    //TODO: GET LANGUAGE FROM XML DOCUMENT

    const contentHelper = utilities.extractContentFromRendered();
    // console.log(contentHelper);
    // if (!content) return;

    setDialogState('processing');

    const contentLanguage =
      documentLanguage === 'auto'
        ? ((await dectectLanguage(contentHelper.content)) ?? documentLanguage)
        : documentLanguage;

    const isDetectedLanguageSupported = nerLanguageSupport.data?.includes(contentLanguage);

    const language = isDetectedLanguageSupported ? contentLanguage : i18next.language.slice(0, 2);

    const entities = await ner({ text: contentHelper.content, language });

    //TODO: HANDLE NER Failure
    if (!entities) {
      setDialogState('initial');
      return;
    }

    const filtredLabels = entities.filter((entity) => NER_LINCS_ENTITY_MAPPING.has(entity.label));
    const flatResults = [];

    for (const entity of filtredLabels) {
      for (const match of entity.matches) {
        flatResults.push({
          name: entity.name,
          label: entity.label,
          text: match.text,
          start: match.start,
          end: match.end,
        });
      }
    }

    const newEntities = flatResults.filter((entity) => {
      console.group(`[${entity.label}] ${entity.text}: ${entity.start} - ${entity.end}`);
      const entityType = NER_LINCS_ENTITY_MAPPING.get(entity.label);

      const matchEntityTypeAnnotations = [...annotations.values()].filter(
        (anno) => anno.entityType === entityType,
      );

      if (matchEntityTypeAnnotations.length === 0) {
        console.groupEnd();
        return true;
      }

      const matchEntity = matchEntityTypeAnnotations.some((anno) => {
        if (entity.text !== anno.quote) return false;

        const annoTargetOffsetPosition = anno.getTargetOffsetPosition();
        if (!annoTargetOffsetPosition) return false;

        const matchQuote =
          entityType === anno.entityType &&
          entity.text === anno.quote &&
          entity.start === annoTargetOffsetPosition.start &&
          entity.end === annoTargetOffsetPosition.end;

        console.table({
          anno: { start: annoTargetOffsetPosition.start, end: annoTargetOffsetPosition.end },
          entity: { start: entity.start, end: entity.end },
        });

        return matchQuote;
      });

      console.groupEnd();
      return !matchEntity;
    });

    const newAnnotations: Annotation[] = [];

    for (const entity of newEntities) {
      const entityType = NER_LINCS_ENTITY_MAPPING.get(entity.label)!;

      // for (const match of entity.matches) {
      const textQuoteSelector = createTextQuoteSelector({
        exact: entity.text,
        prefix: getTextQuotePrefix(contentHelper.content, entity),
        suffix: getTextQuoteSufix(contentHelper.content, entity),
        isDraft: true,
      });

      const textPositionSelector = createTextPositionSelector({
        start: entity.start,
        end: entity.end,
        isDraft: true,
        refinedBy: textQuoteSelector,
      });

      const target = createTarget({
        fileFormat: documentMetadata?.mimetype ?? 'application/xml',
        documentUrl: documentMetadata?.url,
        selector: textPositionSelector,
      });

      const annotation = new Annotation({
        target,
        body: { type: entityType, label: entity.name },
        user,
      });

      newAnnotations.push(annotation);

      //? TAG THE DOCUMENT BASED ON NEW ANNOTATION USING TEXT OFFSET

      contentHelper.addTag({ annotation, entity, entityType });
    }

    setAnnotations(newAnnotations);

    const newContent = contentHelper.reassembleDocument();
    renderDocument(newContent);

    //
    groupAnnotationsByEntityType(newAnnotations);

    setDialogState('results');
  };

  const groupAnnotationsByEntityType = (newAnnotations: Annotation[]) => {
    const annoGroupedByEntityType = Map.groupBy(newAnnotations, (item) => item.entityType);
    const groupsLength = Object.fromEntries(
      Array.from(annoGroupedByEntityType, ([k, v]) => [k, v.length]),
    );
    const groupsLengthMap = new Map(Object.entries(groupsLength)) as Map<EntityType, number>;
    setResultsGroups(groupsLengthMap);
  };

  return {
    dialogState,
    documentLanguage,
    resultsGroups,
    setDocumentLanguage,
    submit,
  };
};
