import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Modal,
  ModalDialog,
  type ButtonProps,
  type DialogContentProps,
  type DialogTitleProps,
  type ModalDialogProps,
} from '@mui/joy';
import type { PointerEvent } from 'react';
import { BiError } from 'react-icons/bi';
import { RiErrorWarningLine, RiInformationLine } from 'react-icons/ri';

export type SeverityType = 'error' | 'info' | 'success' | 'warning';

export interface DialogActionProps extends ButtonProps {
  canShowLoading?: boolean;
  label: string;
  onPointerDown: <T>(data?: T) => Promise<void> | void;
  value?: string;
}

export interface AlertDialogProps extends ModalDialogProps {
  actions?: DialogActionProps[];
  isProcessing?: boolean;
  onClose?: <T>(action: string, data?: T) => Promise<void> | void;
  open: boolean;
  severity?: SeverityType;
  slotProps?: {
    root?: Omit<ModalDialogProps, 'children'>;
    title?: Omit<DialogTitleProps, 'children' | 'id'>;
    content?: Omit<DialogContentProps, 'children'>;
  };
}

export const AlertDialog = ({
  actions,
  children,
  isProcessing = false,
  maxWidth = 'sm',
  onClose,
  open = false,
  severity,
  title,
  variant,
  slotProps,
}: AlertDialogProps) => {
  const handleClose = async (_event: PointerEvent, reason: string) => {
    if (isProcessing) return;
    onClose?.(reason);
  };

  return (
    <Modal open={open} onClose={handleClose}>
      <ModalDialog
        aria-labelledby="alert-dialog-title"
        maxWidth={maxWidth}
        variant={variant}
        {...slotProps?.root}
      >
        <DialogTitle
          id="alert-dialog-title"
          sx={{ display: 'flex', alignItems: 'center', gap: 1, textTransform: 'capitalize' }}
          {...slotProps?.title}
        >
          {severity === 'error' && <RiErrorWarningLine />}
          {severity === 'warning' && <BiError />}
          {severity === 'info' && <RiInformationLine />}
          {title}
        </DialogTitle>
        <DialogContent sx={{ pt: 0.5 }} {...slotProps?.content}>
          {children}
        </DialogContent>
        {actions && (
          <DialogActions buttonFlex="none">
            {actions.reverse().map(({ canShowLoading, label, value, ...rest }) => (
              <Button
                key={value ?? label}
                color="neutral"
                disabled={isProcessing}
                loading={isProcessing && canShowLoading}
                sx={{ textTransform: 'capitalize' }}
                variant="plain"
                {...rest}
              >
                {label}
              </Button>
            ))}
          </DialogActions>
        )}
      </ModalDialog>
    </Modal>
  );
};
