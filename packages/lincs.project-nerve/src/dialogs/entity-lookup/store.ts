import type { Body } from '@lincs.project/webannotation-schema';
import { atom, createStore } from 'jotai';
import { atomWithReset } from 'jotai/utils';
import { Annotation } from '../../annotation';

export const annotationAtom = atom<Annotation | undefined>(undefined);
annotationAtom.debugLabel = 'annotationAtom';

export const textSelectionAtom = atomWithReset<string>('');
textSelectionAtom.debugLabel = 'textSelectionAtom';

export const entityTypeAtom = atom((get) => get(annotationAtom)?.entityType ?? 'person');
entityTypeAtom.debugLabel = 'entityTypeAtom';

export const selectedBodyAtom = atom<Body | undefined>(undefined);
selectedBodyAtom.debugLabel = 'selectedBodyAtom';

export const isSubmittingAtom = atom(false);
isSubmittingAtom.debugLabel = 'isSubmittingAtom';

export const isFetchingAtom = atom(false);
isFetchingAtom.debugLabel = 'isFetchingAtom';

export const store = createStore();
