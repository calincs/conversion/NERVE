/* eslint-disable @typescript-eslint/prefer-nullish-coalescing */
import type { Body } from '@lincs.project/webannotation-schema';
import { Button, DialogActions, type ButtonProps, type DialogActionsProps } from '@mui/joy';
import { useAtomValue } from 'jotai';
import type { PointerEvent, PointerEventHandler } from 'react';
import { useTranslation } from 'react-i18next';
import { entityTypeAtom, selectedBodyAtom } from '../store';

interface FooterProps {
  disabled?: boolean;
  isSubmitting?: boolean;
  onCancel: PointerEventHandler<HTMLButtonElement>;
  onSelect: (event: PointerEvent<HTMLButtonElement>, selectedBody?: Body) => void;
  slotProps?: {
    root?: Omit<DialogActionsProps, 'children'>;
    cancelButton?: Omit<
      ButtonProps,
      'children' | 'onPointerDown' | 'disabled' | 'loading' | 'color'
    >;
    selectButton?: Omit<ButtonProps, 'children' | 'onPointerDown' | 'disabled'>;
  };
}

export const Footer = ({ disabled, isSubmitting, onCancel, onSelect, slotProps }: FooterProps) => {
  const { t } = useTranslation();

  const entityType = useAtomValue(entityTypeAtom);
  const selectedBody = useAtomValue(selectedBodyAtom);

  return (
    <DialogActions sx={{ justifyContent: 'space-between', px: 2, py: 1 }} {...slotProps?.root}>
      <Button
        color={disabled ? 'neutral' : entityType}
        disabled={disabled || isSubmitting || !selectedBody}
        loading={isSubmitting}
        loadingPosition="start"
        onPointerDown={(event) => onSelect(event, selectedBody)}
        sx={{ borderRadius: 4, textTransform: 'capitalize' }}
        variant={selectedBody ? 'solid' : 'plain'}
        {...slotProps?.selectButton}
      >
        <span>{t('nerve.common.select')}</span>
      </Button>
      <Button
        disabled={isSubmitting}
        onPointerDown={onCancel}
        sx={{ borderRadius: 4, textTransform: 'capitalize' }}
        variant="plain"
        {...slotProps?.cancelButton}
      >
        {t('nerve.common.cancel')}
      </Button>
    </DialogActions>
  );
};
