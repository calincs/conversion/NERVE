import {
  Sheet,
  Stack,
  Typography,
  useTheme,
  type SheetProps,
  type StackProps,
  type TypographyProps,
} from '@mui/joy';
import { useAtomValue } from 'jotai';
import type { PropsWithChildren } from 'react';
import { Icon, type IconProps } from '../../../icons';
import { entityTypeAtom, textSelectionAtom } from '../store';

export interface Props extends PropsWithChildren {
  slotProps?: {
    root?: Omit<SheetProps, 'children'>;
    infoHeader?: Omit<StackProps, 'children'>;
    icon?: Omit<IconProps, 'name'>;
    typography?: Omit<TypographyProps, 'children'>;
  };
}

export const Header = ({ children, slotProps }: Props) => {
  const theme = useTheme();

  const entityType = useAtomValue(entityTypeAtom);
  const selection = useAtomValue(textSelectionAtom);

  return (
    <Sheet color={entityType} invertedColors variant="solid" {...slotProps?.root}>
      <Stack py={1} gap={1}>
        <Stack
          direction="row"
          justifyContent="left"
          alignItems="center"
          height={36}
          px={3}
          gap={1}
          {...slotProps?.infoHeader}
        >
          <Icon
            name={entityType}
            size={32}
            style={{ color: theme.vars.palette.text.icon }}
            {...slotProps?.icon}
          />
          <Typography
            level="title-lg"
            sx={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}
            {...slotProps?.typography}
          >
            {selection}
          </Typography>
        </Stack>
        {children}
      </Stack>
    </Sheet>
  );
};
