import type { Target } from '@lincs.project/webannotation-schema';
import { Modal, ModalDialogProps } from '@mui/joy';
import { Provider, useAtomValue } from 'jotai';
import { useResetAtom } from 'jotai/utils';
import { PointerEvent, useCallback, useEffect } from 'react';
import { Annotation } from '../../annotation';
import { entityLookupDialogAtom, userAtom } from '../../jotai/store';
import type { EntityType } from '../../types';
import { Dialog } from './dialog';
import { annotationAtom, isSubmittingAtom, store, textSelectionAtom } from './store';

export interface EntityLookupsDialogProps extends ModalDialogProps {
  entityType?: EntityType;
  onClose?: <T>(action?: string, data?: T) => Promise<void> | void;
  open: boolean;
  target?: Target;
  textSelected?: string;
}

export const EntityLookupDialog = ({
  entityType = 'person',
  onClose,
  open = false,
  target,
  textSelected = '',
}: EntityLookupsDialogProps) => {
  const user = useAtomValue(userAtom);
  const resetEntityLookupDialog = useResetAtom(entityLookupDialogAtom);

  const init = useCallback(() => {
    const annotation = new Annotation({
      target: target!,
      body: { type: entityType, label: '' },
      user,
    });
    store.set(annotationAtom, annotation);
    store.set(textSelectionAtom, textSelected);
  }, [entityType, target, textSelected, user]);

  useEffect(() => {
    if (open) init();
  }, [init, open]);

  const handleOnClose = (_event: PointerEvent, reason: string) => {
    if (!store.get(isSubmittingAtom)) return;
    onClose?.(reason);
  };

  const handleClose = () => {
    onClose?.();
    resetEntityLookupDialog();
  };

  return (
    <Modal open={open} onClose={handleOnClose}>
      <Provider store={store}>
        <Dialog onClose={handleClose} />
      </Provider>
    </Modal>
  );
};
