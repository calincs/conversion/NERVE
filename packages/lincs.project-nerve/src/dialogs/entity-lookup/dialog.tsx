import type { Body } from '@lincs.project/webannotation-schema';
import { DialogContent, ModalDialog } from '@mui/joy';
import { useAtom, useAtomValue, useSetAtom } from 'jotai';
import { useCallback, useEffect } from 'react';
import { useDebounceCallback } from 'usehooks-ts';
import type { EntityLookupsDialogProps } from '.';
import { getEntityCandidates } from '../../annotation';
import { BodyCandidates, QueryField } from '../../components';
import { useAnnotations } from '../../hooks';
import { Footer } from './components/footer';
import { Header } from './components/header';
import {
  annotationAtom,
  entityTypeAtom,
  isFetchingAtom,
  isSubmittingAtom,
  selectedBodyAtom,
  textSelectionAtom,
} from './store';

export const Dialog = ({ onClose }: Required<Pick<EntityLookupsDialogProps, 'onClose'>>) => {
  const { approveAnnotation } = useAnnotations();

  const [isFetching, setIsFetching] = useAtom(isFetchingAtom);
  const [isSubmitting, setIsSubmitting] = useAtom(isSubmittingAtom);
  const setSelectedBody = useSetAtom(selectedBodyAtom);
  const annotation = useAtomValue(annotationAtom)!;
  const entityType = useAtomValue(entityTypeAtom);
  const textSelection = useAtomValue(textSelectionAtom);

  const handleQuery = useCallback(
    async (query: string) => {
      if (!annotation) return;

      if (isFetching && query === textSelection) return;

      setIsFetching(true);
      const bodyCandidates = await getEntityCandidates(query, entityType);
      setIsFetching(false);
      if (!bodyCandidates) return;

      annotation.bodyCandidates = bodyCandidates;
    },
    [annotation, entityType, isFetching, setIsFetching, textSelection],
  );

  const debounced = useDebounceCallback(handleQuery, 500);

  useEffect(() => {
    if (textSelection !== '') debounced(textSelection);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleChange = (value?: Body) => setSelectedBody(value);

  const handleApproveAction = async (selectedBody?: Body) => {
    console.log(selectedBody);
    if (!selectedBody) return;

    const bodyCandidate = annotation.getBodyCandidateById(selectedBody.id);
    if (!bodyCandidate) return;

    setIsSubmitting(true);
    await approveAnnotation(annotation, bodyCandidate);
    setIsSubmitting(false);
    onClose();
  };

  const handleCancel = () => onClose();

  return (
    <ModalDialog minWidth={600} maxWidth={800} size="sm" sx={{ p: 0, gap: 0 }} variant="soft">
      {annotation && (
        <>
          <Header>
            <QueryField
              disabled={isSubmitting}
              entityType={entityType}
              onQuery={handleQuery}
              selection={textSelection}
            />
          </Header>
          <DialogContent>
            <BodyCandidates
              annotation={annotation}
              disabled={isFetching}
              onCandidateDoubleClick={handleApproveAction}
              onChange={handleChange}
            />
          </DialogContent>
        </>
      )}
      <Footer
        disabled={isFetching}
        isSubmitting={isSubmitting}
        onCancel={handleCancel}
        onSelect={(_event, value) => handleApproveAction(value)}
      />
    </ModalDialog>
  );
};
