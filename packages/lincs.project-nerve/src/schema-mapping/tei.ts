import type { Certainty, ModeExistence, Precision } from '@lincs.project/webannotation-schema';
import type { EntityType } from '../types';

export const tei = {
  tagMapping: new Map<EntityType, string>([
    ['person', 'persName'],
    ['place', 'placeName'],
    ['organization', 'orgName'],
    ['work', 'title'],
    ['event', 'event'],
    ['physicalThing', 'object'],
    ['conceptualObject', 'rs'],
    ['citation', 'bibl'],
  ]),

  certaintyValuesMapping: new Map<Certainty, string>([
    ['edit:certaintyHigh', 'high'],
    ['edit:certaintyMedium', 'medium'],
    ['edit:certaintyLow', 'low'],
    ['edit:certaintyUnknown', 'unknown'],
  ]),

  precisionValuesMapping: new Map<Precision, string>([
    ['edit:precisionHigh', 'high'],
    ['edit:precisionMedium', 'medium'],
    ['edit:precisionLow', 'low'],
    ['edit:precisionUnknown', 'unknown'],
  ]),

  modeExistanceValuesMapping: new Map<ModeExistence, string>([
    ['edit:modeReal', 'real'],
    ['edit:modeFictional', 'fictional'],
    ['edit:modeFictionalized', 'fictionalized'],
  ]),
};
