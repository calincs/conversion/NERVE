import type { Certainty, ModeExistence, Precision } from '@lincs.project/webannotation-schema';
import type { EntityType } from '../types';
import { tei } from './tei';

const schemaMapping = new Map<string, typeof tei>([['tei', tei]]);

export const getSchemaEntityTagsMap = (schema: string = 'tei') => {
  const mapping = schemaMapping.get(schema);
  return mapping?.tagMapping;
};

export const getTagNameForEntityType = (entityType: EntityType, schema: string = 'tei') => {
  const mapping = schemaMapping.get(schema);
  return mapping?.tagMapping.get(entityType) ?? entityType;
};

export const getEntityTypeFromTagName = (tag: string, schema: string) => {
  const mapping = schemaMapping.get(schema);
  if (!mapping) return undefined;

  const reverseMapping: Map<string, EntityType> = Object.fromEntries(
    Object.entries(mapping.tagMapping).map(([key, value]) => [value, key]),
  );

  return reverseMapping.get(tag);
};

export const getCertaintyValue = (value: Certainty, schema: string) => {
  const mapping = schemaMapping.get(schema);
  return mapping?.certaintyValuesMapping.get(value) ?? value;
};

export const getPrecisionValue = (value: Precision, schema: string) => {
  const mapping = schemaMapping.get(schema);
  return mapping?.precisionValuesMapping.get(value) ?? value;
};

export const getModeExistenceValue = (value: ModeExistence, schema: string) => {
  const mapping = schemaMapping.get(schema);
  return mapping?.modeExistanceValuesMapping.get(value) ?? value;
};
