import Dexie, { Table } from 'dexie';
import type { AuthorityService } from '../types';

export interface Setting {
  id: 'viewerShowEntityIcon';
  value: boolean;
}

export interface DoNotDisplayDialogs {
  id: string;
}

export class DexieDB extends Dexie {
  entityLookup!: Table<AuthorityService>;
  doNotDisplayDialogs!: Table<DoNotDisplayDialogs>;
  settings!: Table<Setting>;

  constructor() {
    super('NERVE');
    this.version(1).stores({
      entityLookup: '&id, &[authorityId+entityType], authorityId, entityType', // '&' means 'unique'
      doNotDisplayDialogs: 'id',
      settings: '&id',
    });
  }
}

export const db = new DexieDB();

export const deleteDb = async () => {
  await db.delete().catch(() => new Error('Something went wrong.'));
};
