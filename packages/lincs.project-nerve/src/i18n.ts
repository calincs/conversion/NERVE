import { createInstance } from 'i18next';
import z from 'zod';
import en from './locales/en.json';
import fr from './locales/fr.json';
import { log } from './utilities';

//https://luxiyalu.com/how-to-have-multiple-instances-of-i18next-for-component-library/

export const resources = { en, fr } as const;

export const locales = ['en', 'fr'] as const;
export const localesSchema = z.enum(locales);
export type Locales = z.infer<typeof localesSchema>;

const i18n = createInstance(
  {
    // debug: true,
    defaultNS: 'nerve',
    fallbackLng: ['en', 'fr'],
    lng: 'en',
    ns: ['nerve'],
    nsSeparator: '.',
    react: { useSuspense: false },
    resources,
    returnEmptyString: false,
    supportedLngs: locales,
  },
  // We must provide a function as second parameter, otherwise i18next errors
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (error, _t) => {
    if (error) return log.error(error);
  },
);

export default i18n;
