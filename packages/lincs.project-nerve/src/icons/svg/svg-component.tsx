import type { PropsWithChildren, SVGProps } from 'react';
interface SVGRProps extends PropsWithChildren, SVGProps<SVGSVGElement> {
  title?: string;
  titleId?: string;
}
const SvgComponent = ({ children, title, titleId, ...props }: SVGRProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="1em"
    height="1em"
    aria-labelledby={titleId}
    {...props}
  >
    {title && <title>{title}</title>}
    {children}
  </svg>
);
export default SvgComponent;
