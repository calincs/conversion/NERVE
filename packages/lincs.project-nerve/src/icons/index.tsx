import type { IconBaseProps, IconType } from 'react-icons';
import { BiBook, BiSolidBook } from 'react-icons/bi';
import {
  BsBox,
  BsBoxFill,
  BsChatSquareQuote,
  BsFillChatSquareQuoteFill,
  BsFillPersonFill,
  BsPerson,
} from 'react-icons/bs';
import { FiChevronUp } from 'react-icons/fi';
import { GoCheckCircle, GoIssueDraft } from 'react-icons/go';
import { IoMdMore } from 'react-icons/io';
import { LuCheck } from 'react-icons/lu';
import { MdClear, MdOutlinePlace, MdPlace, MdReplay } from 'react-icons/md';
import { PiLightning, PiLightningFill, PiUsersThree, PiUsersThreeFill } from 'react-icons/pi';
import { TbChartBubble, TbChartBubbleFilled } from 'react-icons/tb';

const entityIconsComplete = {
  person: BsFillPersonFill,
  place: MdPlace,
  organization: PiUsersThreeFill,
  work: BiSolidBook,
  event: PiLightningFill,
  physicalThing: BsBoxFill,
  conceptualObject: TbChartBubbleFilled,
  citation: BsFillChatSquareQuoteFill,
};

const entityIconsDraft = {
  personDraft: BsPerson,
  placeDraft: MdOutlinePlace,
  organizationDraft: PiUsersThree,
  workDraft: BiBook,
  eventDraft: PiLightning,
  physicalThingDraft: BsBox,
  conceptualObjectDraft: TbChartBubble,
  citationDraft: BsChatSquareQuote,
};

const statuslIcons = {
  approved: GoCheckCircle,
  draft: GoIssueDraft,
};

const generalIcons = {
  approve: LuCheck,
  change: MdReplay,
  chevronUp: FiChevronUp,
  clear: MdClear,
  delete: MdClear,
  moreMenu: IoMdMore,
  reclassify: MdReplay,
  reset: MdReplay,
  reject: MdClear,
  remove: MdClear,
};

const icons = {
  ...entityIconsComplete,
  ...entityIconsDraft,
  ...generalIcons,
  ...statuslIcons,
};

export type IconName = typeof icons extends Record<infer I, IconType> ? I : never;

export interface IconProps extends IconBaseProps {
  name: IconName;
}

export const getIcon = (name: IconName) => icons[name];

export const Icon = (props: IconProps) => {
  const { name, ...rest } = props;
  const Icon = icons[name];
  return <Icon {...rest} />;
};
