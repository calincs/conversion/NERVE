import { forwardRef, useImperativeHandle, useRef } from 'react';
import App from './app';
import { NerveActions } from './nerve-actions';
import { I18nextProvider } from './providers/I18nextProvider';
import { JotaiProvider } from './providers/jotai-provider';
import { ReactQueryProvider } from './providers/react-query-provider';
import type { NerveProps } from './types';

export { deleteDb } from './db';
export type { NerveActionsType } from './nerve-actions';
export { theme } from './providers/theme-registry/theme';
export type { NerveProps } from './types';

export const Nerve = forwardRef(function Nerve(props: NerveProps, ref) {
  const imperative = useRef(NerveActions);

  useImperativeHandle(ref, () => ({ ...imperative.current }), []);

  return (
    <ReactQueryProvider>
      <I18nextProvider>
        <JotaiProvider>
          <App {...props} />
        </JotaiProvider>
      </I18nextProvider>
    </ReactQueryProvider>
  );
});
