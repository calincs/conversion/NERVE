import { EntityType } from '../../types';

export const NER_LINCS_ENTITY_MAPPING = new Map<string, EntityType>([
  ['EVENT', 'event'],
  ['FAC', 'place'],
  ['GPE', 'place'],
  ['LAW', 'work'],
  ['LOC', 'place'],
  ['NORP', 'conceptualObject'],
  ['ORG', 'organization'],
  ['PERSON', 'person'],
  ['PRODUCT', 'physicalThing'],
  ['WORK_OF_ART', 'work'],
]);
