import { DectectLanguageResultSchema, NerResultSchema, ReconcileResultSchema } from './types';

export { NER_LINCS_ENTITY_MAPPING } from './mappings';

export const dectectLanguage = async (text: string) => {
  const response = await fetch('https://lincs-api.lincsproject.ca/api/language', {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ text }),
  });

  if (!response.ok) {
    //TODO: HANDLE SERVER 400 - 500
    console.error(response);
    return;
  }

  const data = await response.json();
  const validatedData = await DectectLanguageResultSchema.spa(data);
  if (!validatedData.success) {
    //TODO: HANDLE INVALID DATA
    return;
  }

  return validatedData.data.language;
};

// *******************

export const ner = async (payload: { text: string; language: string }) => {
  const response = await fetch('https://lincs-api.lincsproject.ca/api/ner', {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
  });

  //TODO: HANDLE SERVER 400 - 500
  if (!response.ok) {
    //TODO: HANDLE SERVER 400 - 500
    console.error(response);
    return;
  }

  const data = await response.json();
  const validatedData = await NerResultSchema.spa(data);
  if (!validatedData.success) {
    //TODO: HANDLE INVALID DATA
    return;
  }

  //TODO: HANDLE DATA ERRORS AND WARNINGS
  if (validatedData.data.warning) {
    console.warn(validatedData.data.warning);
    console.warn(validatedData.data.errors);
  }
  return validatedData.data.entities;
};

/// ***

export const reconcile = async (query: string, authorities: string[]) => {
  const response = await fetch('https://lincs-api.lincsproject.ca/api/link/reconcile', {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      entity: query,
      authorities,
    }),
  });

  if (!response.ok) {
    //TODO: HANDLE SERVER 400 - 500
    console.error(response);
    return;
  }

  const data = await response.json();
  const validatedData = ReconcileResultSchema.safeParse(data);
  if (!validatedData.success) {
    //TODO: HANDLE INVALID DATA
    return;
  }

  return validatedData.data;
};
