import { z } from 'zod';

export const DectectLanguageResultSchema = z.object({
  language: z.string(),
});

export const NerEntityResultSchema = z.object({
  name: z.string(),
  label: z.string(),
  matches: z.array(
    z.object({
      start: z.number().min(0),
      end: z.number().min(0),
      text: z.string(),
    }),
  ),
});

export const NerResultSchema = z.object({
  entities: z.array(NerEntityResultSchema),
  errors: z
    .array(
      z.object({
        message: z.string(),
      }),
    )
    .nullable()
    .optional(),
  warning: z
    .object({
      message: z.string(),
    })
    .nullable()
    .optional(),
});

export const ReconcileResultSchema = z.array(
  z.object({
    authority: z.string(),
    matches: z.array(
      z.object({
        description: z.string().optional(),
        label: z.string(),
        uri: z.string().url(),
      }),
    ),
  }),
);

export type ReconcileResult = z.infer<typeof ReconcileResultSchema>;
