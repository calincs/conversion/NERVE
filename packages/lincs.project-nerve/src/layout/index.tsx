import { Box, Stack, useTheme, type StackProps } from '@mui/joy';
import { useAtomValue } from 'jotai';
import { type PropsWithChildren } from 'react';
import Resizable from 'react-resizable-layout';
import { Popup } from '../components/popup-two';
import { AlertDialog, EntityLookupDialog, SettingsDialog } from '../dialogs';
import {
  alertDialogAtom,
  alertDialogIsProcessingAtom,
  entityLookupDialogAtom,
  popupPropsAtom,
  settingsDialogOpenAtom,
} from '../jotai/store';
import { AnnotationsPanel } from '../panels/annotations';
import { Viewer } from '../viewer';
import { CompactSideBar } from './components/compact-side-bar';
import { Separator } from './components/separator';

const DEFAULT_WIDTH = 400;
const MIN_WIDTH = 300;
const MAX_WIDTH = 650;
const COMPACT_WIDTH = 70;

export interface Props extends PropsWithChildren {
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
  };
}

export const Layout = ({ slotProps }: StackProps) => {
  const { palette } = useTheme();

  const popupProps = useAtomValue(popupPropsAtom);
  const alertDialog = useAtomValue(alertDialogAtom);
  const alertDialogIsProcessing = useAtomValue(alertDialogIsProcessingAtom);
  const entityLookupDialog = useAtomValue(entityLookupDialogAtom);
  const settingsDialogOpen = useAtomValue(settingsDialogOpenAtom);

  return (
    <Stack
      direction="row"
      height="100%"
      width="100%"
      overflow="clip"
      sx={{
        backgroundColor: palette.background.surface,
        backgroundImage:
          palette.mode === 'dark'
            ? 'linear-gradient(rgba(255, 255, 255, 0.05), rgba(255, 255, 255, 0.05))'
            : 'initial',
      }}
      {...slotProps?.root}
    >
      <Box sx={{ display: 'flex', flexGrow: 1 }}>
        <Resizable axis="x" initial={DEFAULT_WIDTH} min={MIN_WIDTH} max={MAX_WIDTH}>
          {({ position: x, separatorProps, isDragging, setPosition }) => {
            const tooNarrow = x <= MIN_WIDTH;
            return (
              <Stack direction="row" id="wrapper" style={{ overflow: 'hidden', flexGrow: 1 }}>
                <Box
                  sx={{ pointerEvents: isDragging ? 'none' : 'auto' }}
                  width={tooNarrow ? COMPACT_WIDTH : x}
                >
                  {tooNarrow ? (
                    <CompactSideBar onPointerDown={() => setPosition(DEFAULT_WIDTH)} />
                  ) : (
                    <AnnotationsPanel />
                  )}
                </Box>
                <Separator id="splitter" isDragging={isDragging} {...separatorProps} />
                <Box
                  display="flex"
                  sx={{ pointerEvents: isDragging ? 'none' : 'auto' }}
                  width={tooNarrow ? `calc(100% - ${COMPACT_WIDTH}px)` : `calc(100% - ${x}px)`}
                >
                  <Viewer />
                </Box>
              </Stack>
            );
          }}
        </Resizable>
      </Box>

      <Popup {...popupProps} />
      <AlertDialog isProcessing={alertDialogIsProcessing} {...alertDialog} />
      <EntityLookupDialog {...entityLookupDialog} />
      {settingsDialogOpen && <SettingsDialog />}
    </Stack>
  );
};
