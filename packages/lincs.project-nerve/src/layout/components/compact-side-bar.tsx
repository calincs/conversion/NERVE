import { IconButton, Stack, type IconButtonProps, type StackProps } from '@mui/joy';
import type { PointerEventHandler } from 'react';
import { HiOutlineAnnotation } from 'react-icons/hi';

interface Props {
  onPointerDown?: PointerEventHandler<HTMLButtonElement>;
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    iconButton?: Omit<IconButtonProps, 'onPointerDown'>;
  };
}

export const CompactSideBar = ({ onPointerDown, slotProps }: Props) => {
  return (
    <Stack height={'100%'} alignItems="center" p={0.5} {...slotProps?.root}>
      <IconButton onPointerDown={onPointerDown} size="md" {...slotProps?.iconButton}>
        <HiOutlineAnnotation />
      </IconButton>
    </Stack>
  );
};
