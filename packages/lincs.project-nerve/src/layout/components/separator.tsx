import { Box, type BoxProps } from '@mui/joy';
import { useState, type PointerEvent } from 'react';

interface Props extends BoxProps {
  direction?: 'vertical' | 'horizontal';
  disabled?: boolean;
  isDragging?: boolean;
  slotProps?: {
    handle?: BoxProps;
  };
}

export const Separator = ({
  id = 'drag-bar',
  direction = 'vertical',
  disabled,
  isDragging,
  slotProps,
  ...props
}: Props) => {
  const [hover, setHover] = useState(false);

  const { onPointerDown, onKeyDown, onDoubleClick, ...ariaProps } = props;

  return (
    <Box
      {...ariaProps}
      data-testid={id}
      onPointerDown={(event: PointerEvent<HTMLDivElement>) => !disabled && onPointerDown?.(event)}
      id={id}
      sx={{
        flex: '0 0 .4rem',
        width: direction === 'horizontal' ? '100%' : '5px',
        height: direction === 'horizontal' ? '5px' : '100%',
        cursor: disabled ? 'auto' : direction === 'horizontal' ? 'row-resize' : 'col-resize',
        position: 'relative',
      }}
      tabIndex={disabled ? -1 : 0}
      {...props}
      onPointerOver={(event) => {
        !disabled && setHover(true);
        props.onPointerOver?.(event);
      }}
      onPointerOut={(event) => {
        !disabled && setHover(false);
        props.onPointerOut?.(event);
      }}
    >
      <Box
        sx={{
          position: 'absolute',
          top: isDragging ? '5%' : hover ? '47%' : '49%',
          bottom: isDragging ? '5%' : hover ? '47%' : '49%',
          left: '0.1rem',
          right: '0.1rem',
          borderRadius: 4,
          backgroundColor: ({ palette }) =>
            isDragging ? palette.primary[500] : palette.neutral[500],
          transition: 'all 0.4s linear',
          opacity: hover ? 1 : 0.1,
        }}
        {...slotProps?.handle}
      />
    </Box>
  );
};
