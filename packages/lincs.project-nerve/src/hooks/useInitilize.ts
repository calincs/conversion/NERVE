import { useSetAtom } from 'jotai';
import { db } from '../db';
import { initialLookupSettings } from '../config/lookups';
import { viewerShowEntityIconAtom } from '../jotai/store';

export const useInitilize = () => {
  const setViewerShowEntityIcon = useSetAtom(viewerShowEntityIconAtom);

  const initializeNerve = async () => {
    await setupLookups();
    await setupSettings();
  };

  const setupLookups = async () => {
    const isLookUpSetup = await db.entityLookup.count();
    if (isLookUpSetup === 0) {
      for (const [, authorityServices] of initialLookupSettings) {
        await db.entityLookup.bulkPut(authorityServices).catch((err) => console.log(err));
      }
    }
  };

  const setupSettings = async () => {
    const viewerShowEntityIcon = await db.settings.get('viewerShowEntityIcon');
    if (viewerShowEntityIcon?.value) setViewerShowEntityIcon(viewerShowEntityIcon.value);
  };

  return {
    initializeNerve,
  };
};
