import { useTheme } from '@mui/joy';
import chroma from 'chroma-js';
import { useAtomValue } from 'jotai';
import jss from 'jss';
import preset from 'jss-preset-default';
import { viewerShowEntityIconAtom } from '../jotai/store';
import type { EntityType } from '../types';
import { entityIcon } from '../viewer/components/icons';

jss.setup({ ...preset(), createGenerateId: () => (rule) => rule.key });

//* min contrast according to W3C: https://www.w3.org/TR/WCAG20-TECHS/G18.html
const MINIMUM_CONTRAST_RATIO = 7;

export const useCss = () => {
  const theme = useTheme();
  const { palette } = theme;

  const viewerShowEntityIcon = useAtomValue(viewerShowEntityIconAtom);

  const entityTagBefore = ({
    entityType,
    annotationStatus = 'draft',
  }: {
    entityType: EntityType;
    annotationStatus?: 'draft' | 'approved';
  }) => {
    if (!viewerShowEntityIcon) return {};

    const icon = entityIcon[annotationStatus][entityType];
    const color =
      annotationStatus === 'draft'
        ? palette[entityType][500]
        : chroma.contrast(palette[entityType][500], palette.neutral[800]) > MINIMUM_CONTRAST_RATIO
          ? `${palette.neutral[800]} !important`
          : `${palette.text.primary} !important`;

    // * Need to escape "#"
    const content = `url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' viewBox='0 0 24 24' fill='%23${color.slice(1, 7)}'>${icon}</svg>")`;

    return {
      content,
      marginRight: 4,
    };
  };

  const entityTag = (entityType: EntityType) => ({
    display: 'inline-flex',
    paddingInline: 4,
    borderRadius: 4,
    outlineWidth: 1,
    outlineStyle: 'solid',
    outlineColor: 'transparent',
    outlineOffset: 1,
    backgroundColor: palette[entityType][500],
    color:
      chroma.contrast(palette[entityType][500], palette.neutral[800]) > MINIMUM_CONTRAST_RATIO
        ? `${palette.neutral[800]} !important`
        : `${palette.text.primary} !important`,
    cursor: 'pointer',
    textIndent: 'initial',
    scrollMarginTop: '50px',
    transitionDuration: 300,
    transitionTimingFunction: 'cubic-bezier(0.4, 0, 0.2, 1)',
    '&[data-nerve-status="draft"]': {
      paddingInline: 1,
      outlineStyle: 'dashed',
      backgroundColor: 'transparent',
      color: `${palette[entityType][500]} !important`,
      '&[data-nerve-selected=true]': {
        outlineStyle: 'solid',
      },
      '&:before': entityTagBefore({ entityType, annotationStatus: 'draft' }),
    },
    '&[data-nerve-selected=true]': {
      boxShadow: theme.shadow.md,
    },
    '&:hover, &[data-nerve-action="hover"], &[data-nerve-selected=true]': {
      outlineColor: palette[entityType][500],
    },
    '&:hover, &[data-nerve-action="hover"]': {
      boxShadow: palette.mode === 'dark' ? 'unset' : theme.shadow.lg,
      transform: palette.mode === 'dark' ? 'translate(0px, -2px)' : 'unset',

      '&[data-nerve-status="draft"]': {
        transform: ['translate(0px, -2px)'],
        boxShadow: 'unset',
      },
    },
    '&:before': entityTagBefore({ entityType, annotationStatus: 'approved' }),
  });

  const popupEntityType = (entityType: EntityType) => ({
    color: palette[entityType][500],
    fill: palette[entityType][500],
    '&[data-nerve-status="approved"]': {
      backgroundColor: palette[entityType][500],
    },
    '& .decorator': {
      backgroundColor: `color-mix(in srgb, ${palette[entityType][500]} 20%, ${palette.background.surface})`,
      color: palette[entityType][500],
      fill: palette[entityType][500],
    },
  });

  const pb = {
    position: 'relative',
    height: 32,
    marginBlock: 24, // theme.spacing(3),
    marginInline: -18, // theme.spacing(-2.25),
    backgroundColor:
      palette.mode === 'dark' ? palette.background.level1 : palette.background.surface,
    fontFamily: 'Lato',
    fontSize: '0.8em',
    textIndent: 0,
    '&:before, &:after': {
      display: ['inline-flex', '!important'],
      alignItems: 'center',
      justifyContent: 'center',
    },
    '&:before': {
      content: '',
      position: ['absolute', '!important'],
      top: theme.spacing(-2),
      left: [0, '!important'],
      width: ['100%', '!important'],
      height: [16, '!important'],
      borderBottomLeftRadius: 8,
      borderBottomRightRadius: 8,
      borderBottom: `1px solid ${palette.mode === 'dark' ? palette.neutral[700] : palette.neutral[300]}`,
      boxShadow: `0 8px 0 0 ${palette.mode === 'dark' ? palette.background.level1 : palette.background.surface} !important`,
    },
    '&:after': {
      // content: 'attr(n)',
      top: 16,
      left: [0, '!important'],
      width: ['100%', '!important'],
      height: ['1.5rem', '!important'],
      borderTopLeftRadius: 8,
      borderTopRightRadius: 8,
      background: palette.mode === 'dark' ? palette.neutral[700] : palette.neutral[100],
      boxShadow: `0 -8px 0 0 ${palette.mode === 'dark' ? palette.background.level1 : palette.background.surface} !important`,
      textIndent: ['unset', '!important'],
    },
  };

  const styles = {
    '@global': {
      '.frame-content': {
        boxShadow: palette.mode === 'dark' ? theme.shadow.xl : theme.shadow.sm,
        borderRadius: 4,
        borderStyle: palette.mode === 'dark' ? 'solid' : 'none',
        borderWidth: 1,
        borderColor: 'rgb(255 255 255 / 12%)',
      },
      body: {
        margin: '0 auto !important',
        padding: '16px !important', //`${theme.spacing(2)} !important`,
      },
      'html > body': {
        maxWidth: 900,
        color: palette.mode === 'dark' ? palette.neutral[200] : palette.neutral[800],
        backgroundColor: palette.background.surface,
        backgroundImage:
          palette.mode === 'dark'
            ? 'linear-gradient(rgba(255, 255, 255, 0.05), rgba(255, 255, 255, 0.05))'
            : 'initial',
      },
      teiHeader: {
        backgroundColor: palette.mode === 'dark' ? 'rgb(0 0 0 / 14%) !important' : 'inherit',
      },
      xenoData: { display: ['none', '!important'] },
      p: {
        marginBottom: '2rem !important',
        wordSpacing: '0.16em',
      },
      pb,
      'div > pb:first-child, body > pb:first-child': {
        marginTop: -48,
        '&:before': { borderBottom: 0 },
      },
      '[data-nerve-entity-type="person"]': entityTag('person'),
      '[data-nerve-entity-type="place"]': entityTag('place'),
      '[data-nerve-entity-type="organization"]': entityTag('organization'),
      '[data-nerve-entity-type="work"]': entityTag('work'),
      '[data-nerve-entity-type="physicalThing"]': entityTag('physicalThing'),
      '[data-nerve-entity-type="conceptualObject"]': entityTag('conceptualObject'),
      '[data-nerve-entity-type="event"]': entityTag('event'),
      '[data-nerve-entity-type="citation"]': entityTag('citation'),
      '.tippy-box[data-animation="fadeInOut"][data-state="hidden"]': {
        transform: 'scale(.9) translate(0px, 4px)',
        opacity: 0,
      },
    },
    'popup-entity': {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'stretch',
      gap: theme.spacing(1),
      paddingInline: theme.spacing(0.5),
      overflow: 'hidden',
      boxShadow: theme.shadow.md,
      borderRadius: 4,
      backgroundColor: palette.mode === 'dark' ? palette.neutral[900] : palette.background.surface,
      fontFamily: 'Lato, Helvetica, Arial, sans-serif',
      fontWeight: 700,
      lineHeight: 1.2,
      textIndent: 'initial',
      '&[data-nerve-status="draft"]': {
        paddingRight: 0,
      },
      '& span': {
        paddingBlock: theme.spacing(0.5),
      },
      '& div': {
        display: 'flex',
        justifyContent: 'center',
        minWidth: 24,
      },
      '& .icon': {
        alignItems: 'start',
        marginTop: theme.spacing(0.65),
      },
      '& .decorator': {
        alignItems: 'center',
      },
      '& svg': {
        height: 18,
        width: 18,
      },
      '&[type="person"]': popupEntityType('person'),
      '&[type="place"]': popupEntityType('place'),
      '&[type="organization"]': popupEntityType('organization'),
      '&[type="work"]': popupEntityType('work'),
      '&[type="physicalThing"]': popupEntityType('physicalThing'),
      '&[type="conceptualObject"]': popupEntityType('conceptualObject'),
      '&[type="event"]': popupEntityType('event'),
      '&[type="citation"]': popupEntityType('citation'),
      '&[data-nerve-status="approved"]': {
        color: palette.common.white,
        fill: palette.common.white,
      },
    },
  };

  const stylesheet = jss.createStyleSheet(styles);
  const css = stylesheet.toString();

  return {
    css,
  };
};
