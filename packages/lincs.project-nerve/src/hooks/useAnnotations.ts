import type { Body } from '@lincs.project/webannotation-schema';
import { useAtom, useAtomValue, useSetAtom } from 'jotai';
import { nanoid } from 'nanoid';
import { Annotation } from '../annotation';
import {
  annotationsAtom,
  deleteAnnotationAtom,
  documentHasChangedAtom,
  domSelectionAtom,
  store,
  userAtom,
} from '../jotai/store';
import { getTagNameForEntityType } from '../schema-mapping';
import type { EntityType } from '../types';
import { getTagByAttribute } from '../viewer/helper';

export const useAnnotations = () => {
  const deleteAnnotation = useSetAtom(deleteAnnotationAtom);
  const [annotations, setAnnotations] = useAtom(annotationsAtom);
  const user = useAtomValue(userAtom);

  const approveAnnotation = async (annotation: Annotation, bodyCandidate: Body) => {
    console.log(annotation);
    //* 1. Change WA
    annotation.approveBody(bodyCandidate, user);

    // Must used the store directly becasuse the actions might have been triggered from outside react (selection in the iframe)
    store.set(annotationsAtom, annotation);

    //* 2. Change TAG
    const tag = getTagByAttribute('data-nerve-wa-uuid', annotation.uuid);
    if (!tag && store.get(domSelectionAtom)) createTag(annotation, store.get(domSelectionAtom)!);
    if (!tag) return;

    tag.setAttribute('xml:id', annotation.id);
    tag.setAttribute('data-nerve-status', 'approved');

    if (annotation.bodyLabel) tag.setAttribute('key', annotation.bodyLabel);
    if (annotation.body) tag.setAttribute('ref', annotation.body.id);

    store.set(documentHasChangedAtom, nanoid(11));
  };

  const createTag = (annotation: Annotation, selection: Selection) => {
    const tagName = getTagNameForEntityType(annotation.entityType);

    const tag = Object.assign(document.createElement(tagName), {
      'data-nerve-wa-uuid': annotation.uuid,
      'data-nerve-entity-type': annotation.entityType,
    });
    tag.classList.add('nerve-entity');

    selection.getRangeAt(0).surroundContents(tag);
  };

  const approveAnnotationsByTarget = async (
    target: string,
    bodyCandidate: Body,
    options?: { entityType?: EntityType; isDraft?: boolean },
  ) => {
    const entityType = options?.entityType;
    const isDraft = options?.isDraft;

    for (const [, anno] of annotations.entries()) {
      if (anno.quote !== target) continue;
      if (entityType !== undefined && anno.entityType !== entityType) continue;
      if (isDraft !== undefined && anno.isDraft !== isDraft) continue;

      await approveAnnotation(anno, bodyCandidate);
    }
  };

  const relinkAnnotationByUuid = async (uuid: string) => {
    //* 1. Change WA
    const annotation = annotations.get(uuid);
    if (!annotation) return;

    annotation.resetLinking();

    setAnnotations(annotation);

    //* 2. Change TAG
    const tag = getTagByAttribute('data-nerve-wa-uuid', annotation.uuid);
    if (!tag) return;

    tag.setAttribute('data-nerve-status', 'draft');

    tag.removeAttribute('ref');
    tag.removeAttribute('cert');
    tag.removeAttribute('type');
    tag.removeAttribute('precision');

    store.set(documentHasChangedAtom, nanoid(11));
  };

  const relinkAnnotationsByTarget = async (
    target: string,
    options?: { entityType?: EntityType; isDraft?: boolean },
  ) => {
    const entityType = options?.entityType;
    const isDraft = options?.isDraft;

    for (const [uuid, annotation] of annotations.entries()) {
      if (annotation.quote !== target) continue;
      if (entityType !== undefined && entityType !== annotation.entityType) continue;
      if (isDraft !== undefined && isDraft !== annotation.isDraft) continue;

      await relinkAnnotationByUuid(uuid);
    }
  };

  const reclassifyAnnotationByUuid = async (uuid: string, type: EntityType) => {
    //* 1. Change WA
    const annotation = annotations.get(uuid);
    if (!annotation) return;

    annotation.entityType = type;

    setAnnotations(annotation);

    //* 2. Change TAG
    const tag = getTagByAttribute('data-nerve-wa-uuid', annotation.uuid);
    if (!tag) return;

    tag.setAttribute('data-nerve-entity-type', type);
    tag.setAttribute('data-nerve-status', 'draft');

    tag.removeAttribute('ref');
    tag.removeAttribute('cert');
    tag.removeAttribute('type');
    tag.removeAttribute('precision');

    store.set(documentHasChangedAtom, nanoid(11));
  };

  const reclassifyAnnotationsByTarget = async (
    target: string,
    newEntityType: EntityType,
    options?: { entityType?: EntityType; isDraft?: boolean },
  ) => {
    const entityType = options?.entityType;
    const isDraft = options?.isDraft;

    for (const [uuid, annotation] of annotations.entries()) {
      if (annotation.quote !== target) continue;
      if (entityType !== undefined && entityType !== annotation.entityType) continue;
      if (isDraft !== undefined && isDraft !== annotation.isDraft) continue;

      await reclassifyAnnotationByUuid(uuid, newEntityType);
    }
  };

  const removeAnnotationByUuid = async (uuid: string) => {
    const annotation = annotations.get(uuid);
    if (!annotation) return;

    //* 1. Remove WA
    deleteAnnotation(uuid);

    //* 2. Remove TAG
    const tag = getTagByAttribute('data-nerve-wa-uuid', annotation.uuid);
    if (!tag) return;

    tag.replaceWith(...tag.childNodes);
    store.set(documentHasChangedAtom, nanoid(11));
  };

  const removeAnnotationsByTarget = async (
    target: string,
    options?: { entityType?: EntityType; isDraft?: boolean },
  ) => {
    const entityType = options?.entityType;
    const isDraft = options?.isDraft;

    for (const [uuid, annotation] of annotations.entries()) {
      if (annotation.quote !== target) continue;
      if (entityType !== undefined && entityType !== annotation.entityType) continue;
      if (isDraft !== undefined && isDraft !== annotation.isDraft) continue;

      await removeAnnotationByUuid(uuid);
    }
  };

  return {
    approveAnnotationsByTarget,
    approveAnnotation,
    reclassifyAnnotationByUuid,
    reclassifyAnnotationsByTarget,
    relinkAnnotationByUuid,
    relinkAnnotationsByTarget,
    removeAnnotationByUuid,
    removeAnnotationsByTarget,
  };
};
