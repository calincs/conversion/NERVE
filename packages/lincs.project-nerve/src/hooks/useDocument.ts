import type { Source, Target, WebAnnotation } from '@lincs.project/webannotation-schema';
import { useAtom, useSetAtom } from 'jotai';
import { Annotation } from '../annotation';
import {
  annotationsAtom,
  annotationsBulkInitAtom,
  documentMetadataAtom,
  documentSourceP1UUIDAtom,
  documentStringAtom,
  documentXmlAtom,
  domSelectionAtom,
  selectedAnnotationUuidAtom,
  store,
} from '../jotai/store';
import { useAnnotation } from '../panels/annotations/annotation-card/hooks';
import { getSchemaEntityTagsMap } from '../schema-mapping';
import { DocumentMetadata } from '../types';
import * as utils from '../utilities';
import { createPopup } from '../viewer/components/popup';
import { removeTagAttribute, setTagAttribute } from '../viewer/helper';
import * as utilities from '../viewer/utilities';

// * references
// https://javascript.info/selection-range

export const useDocument = () => {
  const { loadCandidates } = useAnnotation();
  const annotationsBulkInit = useSetAtom(annotationsBulkInitAtom);
  const [documentMetadata, setDocumentMetadata] = useAtom(documentMetadataAtom);
  const [documentSourceP1UUID, setDocumentSourceP1UUID] = useAtom(documentSourceP1UUIDAtom);

  const setDocumentString = useSetAtom(documentStringAtom);
  const setDocumentXml = useSetAtom(documentXmlAtom);
  const setDomSelection = useSetAtom(domSelectionAtom);

  const initialize = async (content = '', url?: string) => {
    let cleanedContent = utils.cleanUp(content);

    let documentXml = new DOMParser().parseFromString(cleanedContent, 'application/xml');
    setDocumentXml(documentXml);

    const metadata = getDocumentMetadata(documentXml, url);
    const annotations = retrieveAnnotationsFromStandOffTags(documentXml);

    if (annotations) {
      if (annotations.length > 0) setSourceP1UUID(annotations, metadata);
      documentXml = connectTagsWithAnnotations(documentXml, annotations);
    }

    cleanedContent = new XMLSerializer().serializeToString(documentXml);
    setDocumentString(cleanedContent);

    //render
    setTimeout(() => {
      renderDocument(cleanedContent);
    }, 1500);
  };

  const connectTagsWithAnnotations = (documentXml: XMLDocument, annotations: Annotation[]) => {
    const tagsMapping = getSchemaEntityTagsMap('tei');
    if (!tagsMapping) return documentXml;

    const textTag = documentXml.querySelector('text');
    if (!textTag) return documentXml;

    for (const tag of tagsMapping.values()) {
      const elements = textTag.querySelectorAll(tag);
      if (elements.length === 0) continue;

      for (const element of elements) {
        const id = element.getAttribute('xml:id');
        if (!id) continue;

        const annotation = annotations.find((a) => a.id === id);
        if (!annotation) continue;

        element.setAttribute('data-nerve-status', 'approved');
        element.setAttribute('data-nerve-entity-type', annotation.entityType);
        element.setAttribute('data-nerve-wa-uuid', annotation.uuid);

        element.classList.add('nerve-entity');
      }
    }

    return documentXml;
  };

  const getDocumentMetadata = (documentXml: XMLDocument, url?: string) => {
    //MIME Type
    let mimetype = documentXml.documentElement?.getAttribute('mimetype') ?? 'application/xml';
    const firstChild = documentXml.firstElementChild;
    if (firstChild?.nodeName === 'TEI') mimetype = 'application/tei+xml';

    //Title
    const title =
      documentXml.querySelector('teiHeader > fileDesc > titleStmt > title')?.textContent ??
      undefined;

    //Language
    const language = firstChild ? utilities.getLanguage({ node: firstChild }) : undefined;

    const metadata: DocumentMetadata = {
      language,
      mimetype,
      schemaFamily: 'tei',
      title,
      url,
    };

    setDocumentMetadata(metadata);

    setDocumentSourceP1UUID(utils.generateUriId());

    return metadata;
  };

  const setSourceP1UUID = ([firstAnnotation]: Annotation[], { mimetype }: DocumentMetadata) => {
    const P1UUID = firstAnnotation.getTargetByFormat(mimetype)?.source.P1_is_identified_by?.id;
    if (P1UUID) setDocumentSourceP1UUID(P1UUID);
  };

  const retrieveAnnotationsFromStandOffTags = (docXML: XMLDocument) => {
    const standOff = docXML.querySelector('standOff[type="annotation"]');
    if (!standOff) return;

    const rdfRDF = standOff.getElementsByTagName('rdf:RDF')[0];
    if (!rdfRDF) return;

    const rdfDescription = rdfRDF.getElementsByTagName('rdf:Description')[0];
    if (!rdfDescription) return;

    //check if the content is a CDATA and not empty
    if (
      // rdfDescription.firstChild?.nodeType !== Node.CDATA_SECTION_NODE ||
      !rdfDescription.firstChild?.textContent
    ) {
      return;
    }

    //parse the content
    try {
      const annos = JSON.parse(rdfDescription.firstChild.textContent) as WebAnnotation[];
      const annoCollection: Annotation[] = [];

      for (const anno of annos) {
        annoCollection.push(new Annotation(anno));
      }

      annotationsBulkInit(annoCollection);

      return annoCollection;
    } catch (e) {
      console.error(e);
      return;
    }
  };

  const renderDocument = (xmlString: string) => {
    const documentXML = new DOMParser().parseFromString(xmlString, 'application/xml');

    // Get content div
    const iframeContent = utilities.getIframeContent();
    if (!iframeContent) return;

    // Stylesheets
    const styleSheets = utilities.getDocumentStyleSheets(documentXML);
    for (const styleSheet of styleSheets) {
      const isStyleSheetApplied = !!iframeContent.head.querySelector(`link[href="${styleSheet}"]`);
      if (!isStyleSheetApplied) {
        const linkStylesheet = Object.assign(document.createElement('link'), {
          rel: 'stylesheet',
          href: styleSheet,
        });
        iframeContent.head.append(linkStylesheet);
      }
    }

    //* --- Isolate text tag and keep the root tag as is
    //* The goal here is to get rid of auxiliary metadata tags (e.g., teiheader, standOff) that are not part of the text nd focous on the text itself
    //* This is important to:
    //* - Not feed the render with auxiliary metadata
    //* - Not feed NER render with auxiliary metadata
    //* - Be consistent with text offsets

    //Clone document root
    const rootElement = documentXML.firstElementChild?.cloneNode();
    if (!rootElement) return;

    //Deep Clone text tag
    const textNode = documentXML.querySelector('text')?.cloneNode(true) as Element;

    //Insert text tag into root tag
    rootElement.appendChild(textNode);

    //----

    iframeContent.body.replaceChildren();

    iframeContent.body.append(rootElement);

    const iframe = utilities.getIframe()!;

    iframe.onselectionchange = () => {
      const iframe = utilities.getIframe()!;
      const selection = iframe.getSelection();
      if (selection) {
        setDomSelection(selection.isCollapsed ? null : selection);
      } else {
        setDomSelection(null);
      }
    };

    setTagInteration();
  };

  const setTagInteration = () => {
    const iframe = utilities.getIframe();
    if (!iframe) return;

    const entities = iframe.querySelectorAll('.nerve-entity');

    entities.forEach((entity) => {
      entity.addEventListener('pointerover', handleTagPointerOver);
      entity.addEventListener('pointerdown', handleTagPointerDown);
    });
  };

  const handleTagPointerOver = (event: Event) => {
    const element = event.target as Element;
    if (!event.target) return;

    const uuid = element.getAttribute('data-nerve-wa-uuid');
    if (!uuid) return;

    const annotation = store.get(annotationsAtom).get(uuid);
    if (!annotation) return;

    if (annotation.isDraft && !annotation.bodyCandidates) {
      loadCandidates(annotation);
    }

    createPopup(element, annotation);
  };

  const handleTagPointerDown = (event: Event) => {
    const element = event.target as Element;
    if (!element) return;

    const uuid = element.getAttribute('data-nerve-wa-uuid');
    if (!uuid) return;

    //prevent select the same annotation
    if (store.get(selectedAnnotationUuidAtom) === uuid) return;

    // deselect any selected instance in the document
    if (store.get(selectedAnnotationUuidAtom)) {
      removeTagAttribute(uuid, 'data-nerve-selected');
      store.set(selectedAnnotationUuidAtom, undefined);
    }

    //select
    store.set(selectedAnnotationUuidAtom, uuid);

    //select in the document
    setTagAttribute(uuid, 'data-nerve-selected', 'true');
  };

  const getElementsByTextQuote = ({
    exact,
    prefix,
    suffix,
  }: {
    exact: string;
    prefix: string;
    suffix: string;
  }) => {
    //Get iframe
    const iframe = utilities.getIframe();
    if (!iframe) return;
    const body = iframe.querySelector('TEI');
    if (!body) return;

    const clone = body.cloneNode(true) as Element;
    //* We should not send xenoData to NER
    clone.getElementsByTagName('xenoData')[0]?.remove();

    //* range: Select the whole document
    const range = new Range();
    range.selectNodeContents(clone);

    const all = Array.from(clone.querySelectorAll('*'));
    const elements = all.filter((el) => {
      console.group(el);
      const textContent = utils.cleanUp(el.textContent ?? '');
      // console.log(textContent);
      // console.log(`${prefix}${exact}${suffix}`);
      // console.log(textContent.includes(`${prefix}${exact}${suffix}`));
      // console.log(textContent.indexOf(`${prefix}${exact}${suffix}`));
      if (textContent.includes(`${prefix}${exact}${suffix}`)) {
        console.log('yay');
        // console.log(el.textContent.indexOf(exact));
        el.normalize();
        const regex = new RegExp(`${exact}`, 'g');
        const matches = [textContent.matchAll(regex)];
        console.log(matches);

        console.groupEnd();
        return el.textContent?.includes(exact);
      }
      console.groupEnd();
    });

    console.log(elements);

    // const element = elements[12];
    // if (element.textContent) {
    //   const regex = new RegExp(`${exact}`, 'g');
    //   const matches = [...element.textContent.matchAll(regex)];
    //   const [match, group, index] = matches[0];
    //   console.log(matches[0].index);

    //   const range = iframe.createRange();
    //   range.setStart(element, matches[0].index);
    //   range.setEnd(element, matches[0].index + 4);

    //   console.log(range);
    // }

    // const test = utilities.evaluateXPath(iframe, "/p");

    const evalResult = iframe.evaluate(
      "/html/body//*[contains(.,'Oslo')]",
      iframe,
      null,
      XPathResult.ANY_TYPE,
      null,
    );

    console.log(evalResult);

    let result: number | string | boolean | Node | null = null;

    switch (evalResult.resultType) {
      case XPathResult.NUMBER_TYPE:
        result = evalResult.numberValue;
        break;
      case XPathResult.STRING_TYPE:
        result = evalResult.stringValue;
        break;
      case XPathResult.BOOLEAN_TYPE:
        result = evalResult.booleanValue;
        break;
      case XPathResult.UNORDERED_NODE_ITERATOR_TYPE:
      case XPathResult.ORDERED_NODE_ITERATOR_TYPE:
        result = evalResult.iterateNext();
        while (result) {
          console.log(result);
          result = evalResult.iterateNext();
        }
        break;
      case XPathResult.ANY_UNORDERED_NODE_TYPE:
      case XPathResult.FIRST_ORDERED_NODE_TYPE:
        result = evalResult.singleNodeValue;
        break;
    }

    console.log(result);
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const getAnnotationTarget = () => {
    //Get iframe
    const iframe = utilities.getIframe();
    if (!iframe) return;

    //Get selection
    const selection = iframe.getSelection();
    if (!selection) return;

    //Selectors
    const textQuoteSelector = utilities.createTextQuoteSelector(selection);
    const xPathSelector = utilities.createXpathSelector(selection);
    const textPositionSelector = utilities.createTextPositionSelector(selection);

    // getCharacterOffsetFromXPath(xPathSelector.value);

    // const test = getOffsettt(xPathSelector.value);
    // console.log(test);

    const test2 = getOffset2(selection);
    console.log('offset', test2);

    //Append Selectors
    // xPathSelector.refinedBy = textPositionSelector;
    // textQuoteSelector.refinedBy = xPathSelector;

    xPathSelector.refinedBy = textQuoteSelector;

    const selector = xPathSelector;

    const language = selection.anchorNode
      ? utilities.getLanguage({ node: selection.anchorNode })
      : undefined;

    const source: Source = {
      id: documentMetadata?.url ?? utils.generateUriId(),
      type: 'crm:D1_Digital_Object',
      format: documentMetadata?.mimetype ?? 'application/xml',
      language,
      P1_is_identified_by: {
        id: documentSourceP1UUID, // This ID is unique for the whole document not for each annotation
        type: 'crm:E33_E41_Linguistic_Appellation',
        title: documentMetadata?.title ?? documentMetadata?.url ?? '',
      },
    };

    const target: Target = {
      id: utils.generateUriId('draft'),
      type: ['SpecificResource', 'crm:E73_Information_Object'],
      source,
      selector,
    };

    return target;
  };

  return {
    getAnnotationTarget,
    handleTagPointerDown,
    handleTagPointerOver,
    initialize,
    getElementsByTextQuote,
    renderDocument,
  };
};

// Function to recursively traverse the nodes and count characters
const getCharacterOffsetFromXPath = (xpath: string) => {
  let offset = 0;
  const nodeStack = []; // Stack to hold nodes for traversal

  // Function to traverse and count characters
  const traverse = (node: Node | Element) => {
    // If the node is a text node, count its characters
    if (node.nodeType === Node.TEXT_NODE) {
      let textLength = node.nodeValue?.length ?? 0;

      // Check if this is the target node specified by the XPath
      if (node === targetNode) return true; // Found the target node, stop further traversal

      // Add the length of the text node to the total offset
      offset += textLength;
    } else if (node.nodeType === Node.ELEMENT_NODE) {
      // If the node is an element, push its children onto the stack
      for (let i = node.childNodes.length - 1; i >= 0; i--) {
        nodeStack.push(node.childNodes[i]);
      }
    }

    return false; // Continue traversal
  };

  const iframe = utilities.getIframe()!;

  const context = utilities.getIframeContent()!;

  // Get the target node using XPath
  console.log(xpath);
  // const result = iframe.evaluate(xpath, iframe, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);

  const result = iframe.evaluate(xpath, context.body, null, XPathResult.ANY_TYPE, null);

  // const targetNode = result.singleNodeValue;

  console.log(context.body);
  console.dir(context.body);
  const targetNode = utilities.evaluateXPath(iframe, '//div', context.body);
  console.log(targetNode);

  if (targetNode) {
    // Start traversal from the root node of the document
    nodeStack.push(document.documentElement);

    while (nodeStack.length > 0) {
      const currentNode = nodeStack.pop()!;

      // Traverse current node and check if we found the target node
      if (traverse(currentNode)) {
        break;
      }
    }

    console.log('Character offset relative to the start of the document: ' + offset);
  } else {
    console.log('Node not found for the given XPath.');
  }
};

const getOffsettt = (xpath: string) => {
  console.log(xpath);
  // xpath = '/html/body/div/div/tei'
  xpath =
    "/html/body/div/div/*[translate(name(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz') = 'tei']/text";
  console.log(xpath);

  const iframe = utilities.getIframe()!;
  const context = utilities.getIframeContent()!;

  const evaluator = new XPathEvaluator();
  // const result = evaluator.evaluate(xpath, document, null, XPathResult.ANY_TYPE, null);
  const result = evaluator.evaluate(xpath, context.body, null, XPathResult.ANY_TYPE, null);
  const targetNode = utilities.evaluateXPath(iframe, xpath, context.body);
  console.log(targetNode);

  console.log(context.body);
  console.log(result);

  const offsets = [];
  let node;
  while ((node = result.iterateNext())) {
    console.group('node');
    console.log(node);

    const offset = node.compareDocumentPosition(context.body);
    console.log(
      'offset',
      offset,
      Node.DOCUMENT_POSITION_PRECEDING,
      offset & Node.DOCUMENT_POSITION_PRECEDING,
    );

    if (offset & Node.DOCUMENT_POSITION_PRECEDING) {
      console.log('cond. a');
      const precedingText = node.previousSibling?.textContent;
      const precedingOffset = precedingText ? precedingText.length : 0;
      offsets.push(precedingOffset);
    } else {
      console.log('cond. b');
      // const offset = node.compareDocumentPosition(document.documentElement);
      // offsets.push(offset);
    }
    console.groupEnd();
  }

  console.log(offsets);
  console.log(offsets.reduce((a, b) => a + b, 0));
  return offsets.reduce((a, b) => a + b, 0);
};

const getOffset2 = (selection: Selection) => {
  console.log(selection);
  const anchorNode = selection.anchorNode;
  let node = anchorNode;

  if (anchorNode === null) throw new Error('missing required parameter "node"');

  const root = utilities.getDocumentRoot();
  console.log(root);

  let offset = selection.anchorOffset;

  // console.log(anchorNode.nodeName, root.nodeName);
  console.log('initial offset', offset);

  if (anchorNode.nodeName === root.nodeName) return offset;

  while (node !== root) {
    // console.group('node');

    if (!node) {
      throw new DOMException(
        'The supplied node is not contained by the root node.',
        'InvalidNodeTypeError',
      );
    }

    if (node.nodeName === 'text') {
      // node = node.parentNode;
    } else if (node.previousSibling) {
      // console.dir(node.previousSibling);

      const content = node.previousSibling.textContent;
      let contentLength = content ? content.length : 0;
      // contentLength = content === ' ' ? 0 : contentLength;

      offset += contentLength;

      console.table({
        node: node.previousSibling,
        nodeName: node.previousSibling.nodeName,
        content: content,
        contentLength: content?.length,
        contentLengthUsed: contentLength,
        offset: offset,
      });

      node = node.previousSibling;
    } else {
      node = node.parentNode;
    }

    // console.log(offset);
    // console.groupEnd();
  }

  console.log(offset);

  return offset;
};
