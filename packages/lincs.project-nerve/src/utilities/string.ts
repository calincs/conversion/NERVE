import { nanoid } from 'nanoid';
import z from 'zod';
import i18n from '../i18n';
import type { EntityType } from '../types';

export const generateUriId = (addPrefix = '') => {
  const id = addPrefix ? `${addPrefix}/${nanoid(11)}` : nanoid(11);
  return [window.location.origin, id].join('/');
};

/**
 * Take a string, capitalize the first letter, and lowercase the rest.
 * @param {string} w - string - the word to capitalize
 */
export const capitalizeWord = (w: string) => w.charAt(0).toUpperCase() + w.slice(1).toLowerCase();

export const getEntityTypeLabelLocalized = (entity: EntityType) => {
  return i18n.t(`nerve.entity.${entity}`);
};

export const cleanUp = (
  content: string,
  { escapeDoubleQuotes = false, removeTags = false } = {},
) => {
  let cleanedContent = content;
  // .replaceAll(/(\r\n|\n|\r)/gm, ''); //* Remove new line
  // .replaceAll(/\t+/g, '') //* Remove tabs space (recursevelly)
  // .replaceAll(/\s\s+/g, ' '); //* Remove double space (recursevelly)

  if (removeTags) {
    cleanedContent = cleanedContent.replaceAll(/<[^>]*>/g, ''); //* Remove tags
  }

  if (escapeDoubleQuotes) {
    cleanedContent = cleanedContent.replaceAll('"', '\\"'); //* Escape `"` [double quotes]
  }

  return cleanedContent;
};

export const extractContentFromRaw = (documentString: string) => {
  const content = cleanUp(documentString, { escapeDoubleQuotes: true }).replaceAll(/<[^>]*>/g, ''); //* Remove tags
  return content;
};

/// ** TextQuote Prefix and suffix
interface Match {
  label: string;
  start: number;
  end: number;
  text: string;
}

const MAX_CHARACTERS = 1000;

export const getTextQuotePrefix = (content: string, match: Match) => {
  const textBeforeMatch = content.slice(0, match.start - 1);
  const prefix = textBeforeMatch
    .substring(textBeforeMatch.lastIndexOf('.') + 1, textBeforeMatch.length)
    .slice(-MAX_CHARACTERS);
  return prefix;
};

export const getTextQuoteSufix = (content: string, match: Match) => {
  const textAfterMatch = content.slice(match.end + 1, content.length);
  const suffix = textAfterMatch.substring(0, textAfterMatch.indexOf('.')).slice(0, MAX_CHARACTERS);
  return suffix;
};

export const isUriValid = (value: string) => z.string().url().safeParse(value).success;
