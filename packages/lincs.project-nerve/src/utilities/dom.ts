import { annotationsCollectionAtom, store } from '../jotai/store';

export const removeDraftTags = (docXML: XMLDocument) => {
  const draftEntityTags = docXML.querySelectorAll('.nerve-entity[data-nerve-status="draft"]');
  draftEntityTags.forEach((tag) => {
    const children = Array.from(tag.childNodes);
    tag.replaceWith(...children);
  });
  return docXML;
};

export const cleanNerveTags = (docXML: XMLDocument) => {
  const tags = docXML.querySelectorAll('.nerve-entity[data-nerve-status="approved"]');
  tags.forEach((tag) => {
    const updatedTag = tag.cloneNode(true) as Element;

    updatedTag.removeAttribute('aria-expanded');
    updatedTag.removeAttribute('data-nerve-status');
    updatedTag.removeAttribute('data-nerve-entity-type');
    updatedTag.removeAttribute('data-nerve-selected');
    updatedTag.removeAttribute('data-nerve-action');
    updatedTag.removeAttribute('data-nerve-wa-uuid');

    updatedTag.classList.remove('nerve-entity');
    if (updatedTag.classList.length === 0) updatedTag.removeAttribute('class');

    tag.replaceWith(updatedTag);
  });
  return docXML;
};

export const createStandOffTag = (
  docXML: XMLDocument,
  { attributes }: { attributes?: { type?: string } } = {},
) => {
  const selector = attributes?.type ? `standOff[type="${attributes.type}"]` : 'standOff';
  if (docXML.querySelector(selector)) return docXML;

  const standOff = docXML.createElement('standOff');
  if (attributes?.type) standOff.setAttribute('type', attributes.type);
  docXML.documentElement.append(standOff);

  return docXML;
};

export const createRdfRDFTag = (docXML: XMLDocument, parent: Element) => {
  if (parent.getElementsByTagName('rdf:RDF')[0]) return docXML;

  const rdf = docXML.createElement('rdf:RDF');
  rdf.setAttribute('xmlns:rdf', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#');
  parent.append(rdf);

  return docXML;
};

export const createRdfDescriptionTag = (docXML: XMLDocument, parent: Element) => {
  if (parent.getElementsByTagName('rdf:Description')[0]) return docXML;

  const rdf = docXML.createElement('rdf:Description');
  rdf.setAttribute('rdf:datatype', 'http://www.w3.org/TR/json-ld/');
  parent.append(rdf);

  return docXML;
};

export const updateAnnotationToStandoffTag = (docXML: XMLDocument) => {
  const annotations = Array.from(store.get(annotationsCollectionAtom).values());
  const approvedAnnotations = annotations
    .filter((anno) => !anno.isDraft)
    .map((anno) => anno.webAnnotation);

  if (approvedAnnotations.length === 0) {
    const standOff = docXML.querySelector('standOff[type="annotation"]');
    if (standOff) docXML.removeChild(standOff);
    return docXML;
  }

  let standOff = docXML.querySelector('standOff[type="annotation"]');
  if (!standOff) {
    docXML = createStandOffTag(docXML, { attributes: { type: 'annotation' } });
    standOff = docXML.querySelector('standOff[type="annotation"]');
    if (!standOff) return docXML;
  }

  let rdfRDF = standOff.getElementsByTagName('rdf:RDF')[0];
  if (!rdfRDF) {
    docXML = createRdfRDFTag(docXML, standOff);
    rdfRDF = standOff.getElementsByTagName('rdf:RDF')[0];
    if (!rdfRDF) return docXML;
  }

  let rdfDescription = rdfRDF.getElementsByTagName('rdf:Description')[0];
  if (!rdfDescription) {
    docXML = createRdfDescriptionTag(docXML, rdfRDF);
    rdfDescription = rdfRDF.getElementsByTagName('rdf:Description')[0];
    if (!rdfDescription) return docXML;
  }

  const CDATA = docXML.createCDATASection(JSON.stringify(approvedAnnotations, null, 4));
  if (rdfDescription.firstChild) {
    rdfDescription.firstChild.replaceWith(CDATA);
  } else {
    rdfDescription.appendChild(CDATA);
  }

  return docXML;
};
