import { atom, createStore } from 'jotai';
import { atomWithReset } from 'jotai/utils';
import { nanoid } from 'nanoid';
import { Annotation, type AnnotationStatus } from '../annotation';
import type { PopupProps } from '../components/popup-two';
import type { AlertDialogProps, EntityLookupsDialogProps } from '../dialogs';
import type {
  AnnotationCollectionModel,
  DocumentMetadata,
  EntityType,
  SortView,
  User,
} from '../types';

// SESSION

export const nerveSessionIdAtom = atomWithReset<string | undefined>(undefined);
nerveSessionIdAtom.debugLabel = 'nerveSessionId.Atom';

// USER

export const userAtom = atomWithReset<User | undefined>(undefined);
userAtom.debugLabel = 'user.Atom';

// DOCUMENT

export const documentStringAtom = atomWithReset<string | undefined>(undefined);
documentStringAtom.debugLabel = 'documentString.Atom';

export const documentXmlAtom = atomWithReset<XMLDocument | undefined>(undefined);
documentXmlAtom.debugLabel = 'documentXml.Atom';

export const documentMetadataAtom = atomWithReset<DocumentMetadata | undefined>(undefined);
documentMetadataAtom.debugLabel = 'documentMetadata.Atom';

export const documentSourceP1UUIDAtom = atomWithReset<string>(nanoid());
documentSourceP1UUIDAtom.debugLabel = 'documentSourceP1UUID.Atom';

export const domSelectionAtom = atomWithReset<Selection | null>(null);
domSelectionAtom.debugLabel = 'domSelection.Atom';

export const documentHeaderAtom = atomWithReset<Element | undefined>(undefined);
documentHeaderAtom.debugLabel = 'documentHeader.Atom';

export const documentHasChangedAtom = atomWithReset<string | null>(null);
documentHasChangedAtom.debugLabel = 'documentHasChanged.Atom';

// ANNOTATION

export const annotationsCollectionAtom = atomWithReset<Map<string, Annotation>>(new Map());
annotationsCollectionAtom.debugLabel = 'annotations.Collection.Atom';

export const annotationsAtom = atom<Map<string, Annotation>, [Annotation | Annotation[]], void>(
  (get) => get(annotationsCollectionAtom),
  (get, set, update) => {
    const annotations = new Map(get(annotationsCollectionAtom));
    if (Array.isArray(update)) {
      update.map((anno) => annotations.set(anno.uuid, anno));
    } else {
      annotations.set(update.uuid, update);
    }
    set(annotationsCollectionAtom, annotations);
  },
);
annotationsAtom.debugLabel = 'annotations.Atom';

export const annotationsBulkInitAtom = atom<null, [Annotation[]], void>(
  null,
  (_get, set, update) => {
    const collection = new Map(update.map((anno) => [anno.uuid, anno]));
    set(annotationsCollectionAtom, collection);
  },
);
annotationsBulkInitAtom.debugLabel = 'annotationsBulkInit.Atom';

export const selectedAnnotationAtom = atom((get) => {
  const selectedAnnotationUuid = get(selectedAnnotationUuidAtom);
  if (!selectedAnnotationUuid) return;
  return get(annotationsCollectionAtom).get(selectedAnnotationUuid);
});
selectedAnnotationAtom.debugLabel = 'selectedAnnotationAtom';

export const deleteAnnotationAtom = atom(null, (get, set, uuid: string) => {
  const annotations = new Map(get(annotationsCollectionAtom));
  annotations.delete(uuid);
  set(annotationsCollectionAtom, annotations);
});
deleteAnnotationAtom.debugLabel = 'deleteAnnotationAtom';

export const selectedAnnotationUuidAtom = atomWithReset<string | undefined>(undefined);
selectedAnnotationUuidAtom.debugLabel = 'selectedAnnotationUuidAtom';

export const collectionMetaAtom = atomWithReset<
  Omit<AnnotationCollectionModel, 'first'> | undefined
>(undefined);
collectionMetaAtom.debugLabel = 'collectionMetaAtom';

/// UI

export const stackedAnnotationsAtom = atomWithReset(false);
stackedAnnotationsAtom.debugLabel = 'stackedAnnotationsAtom';

export const sortAnnotationsAtom = atomWithReset<SortView>('linear');
sortAnnotationsAtom.debugLabel = 'sortAnnotationsAtom';

export const filterViewAtom = atomWithReset<(EntityType | AnnotationStatus)[]>([]);
filterViewAtom.debugLabel = 'filterViewAtom';

export const popupPropsAtom = atomWithReset<Partial<PopupProps>>({});
popupPropsAtom.debugLabel = 'popupPropsAtom';

export const alertDialogAtom = atomWithReset<AlertDialogProps>({ open: false });
alertDialogAtom.debugLabel = 'alertDialogAtom';

export const setAlertDialogAtom = atom(
  null,
  (_get, set, update: Omit<AlertDialogProps, 'open'>) => {
    set(alertDialogAtom, { ...update, open: true });
    set(alertDialogIsProcessingAtom, false);
  },
);

export const alertDialogIsProcessingAtom = atomWithReset(false);
alertDialogIsProcessingAtom.debugLabel = 'alertDialogIsProcessingAtom';

export const entityLookupDialogAtom = atomWithReset<EntityLookupsDialogProps>({ open: false });
entityLookupDialogAtom.debugLabel = 'entityLookupDialogAtom';

export const setEntityLookupDialogAtom = atom(
  null,
  (_get, set, update: Omit<EntityLookupsDialogProps, 'open'>) => {
    set(entityLookupDialogAtom, { ...update, open: true });
  },
);

//

export const settingsDialogOpenAtom = atomWithReset(false);
settingsDialogOpenAtom.debugLabel = 'settingsDialogOpenAtom';

//VIEWER UI
export const viewerShowEntityIconAtom = atom(false);
viewerShowEntityIconAtom.debugLabel = 'viewerShowEntityIcon.atom';

//STORE

export const store = createStore();
store.set(nerveSessionIdAtom, nanoid(7));
