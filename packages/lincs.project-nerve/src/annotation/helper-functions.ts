import type {
  Body,
  CssSelectorExtended,
  ModeExistence,
  Motivation,
  RangeSelector,
  Selector,
  Source,
  Target,
  TextPositionSelectorExtended,
  TextQuoteSelectorExtended,
  TypeBody,
  WebAnnotation,
  XpathSelector,
  XpathSelectorExtended,
} from '@lincs.project/webannotation-schema';
import { db } from '../db';
import { documentMetadataAtom, documentSourceP1UUIDAtom, store } from '../jotai/store';
import { reconcile } from '../service/lincs-api';
import { AuthorityTypes, type EntityType, type MimeType } from '../types';
import { generateUriId } from '../utilities';

export const getEntityCandidates = async (query: string, entityType: EntityType) => {
  const services = await db.entityLookup
    .where({ entityType })
    .filter((service) => !service.disabled && service.active)
    .sortBy('order');

  const servicesNames = services
    .filter(({ lincsApiCode }) => lincsApiCode !== undefined)
    .flatMap(({ lincsApiCode }) => lincsApiCode)
    .map((code) => code ?? '')
    .filter((code) => code !== '');

  const candidates = await reconcile(query, servicesNames);
  if (!candidates) return;

  const candidatesArray = candidates.flatMap((candidate) => candidate.matches);

  //remove duplicates based on uri
  const candidatesArrayUnique: typeof candidatesArray = [];
  for (const candidate of candidatesArray) {
    if (!candidatesArrayUnique.some((item) => item.uri === candidate.uri)) {
      candidatesArrayUnique.push(candidate);
    }
  }

  const bodyType = getBodyTypeforEntityType(entityType);

  const bodyCandidates: Body[] = candidatesArrayUnique.map(({ uri, label, description }) => ({
    id: uri,
    type: bodyType,
    label,
    description,
  }));

  return bodyCandidates;
};

export const getMotivationByEntityType = (entityType: EntityType): Motivation => {
  if (entityType === 'conceptualObject') return 'oa:tagging';
  if (entityType === 'citation') return 'citing';
  return 'oa:identifying';
};

export const getMotivationByBodyType = (entityType: TypeBody): Motivation => {
  if (entityType[0] === 'cito:Citation') return 'citing';
  return 'oa:identifying';
};

export const getBodyTypeforEntityType = (
  entityType: EntityType,
  modeExistence: ModeExistence = 'edit:modeReal',
): TypeBody => {
  if (entityType === 'person') {
    return modeExistence === 'edit:modeReal'
      ? 'crm:E21_Person'
      : ['foaf:Person', 'crm:E89_Propositional_Object'];
  }

  if (entityType === 'place') {
    return modeExistence === 'edit:modeReal'
      ? 'crm:E53_Place'
      : ['biography:fictionalPlace', 'crm:E89_Propositional_Object'];
  }

  if (entityType === 'organization') {
    return modeExistence === 'edit:modeReal'
      ? 'crm:E74_Group'
      : ['foaf:Organization', 'crm:E89_Propositional_Object'];
  }

  if (entityType === 'work') {
    return modeExistence === 'edit:modeReal' ? 'frbroo:F1' : ['frbroo:F1', 'wikidata:Q15306849'];
  }

  if (entityType === 'physicalThing') {
    return modeExistence === 'edit:modeReal'
      ? 'crm:E18_Physical_Thing'
      : ['wikidata:Q15831596', 'crm:E89_Propositional_Object'];
  }

  if (entityType === 'conceptualObject') return 'crm:E28_Conceptual_Object';

  if (entityType === 'event') {
    return modeExistence === 'edit:modeReal'
      ? 'crm:E5_Event'
      : ['event:Event', 'crm:E89_Propositional_Object'];
  }

  if (entityType === 'citation') {
    return ['cito:Citation', 'crm:E73_Information_Object'];
  }

  return 'crm:E28_Conceptual_Object';
};

export const getEntityTypeFromBodyType = (bodyType: TypeBody): EntityType => {
  if (bodyType === 'crm:E21_Person' || bodyType[0] === 'foaf:Person') return 'person';
  if (bodyType === 'crm:E53_Place' || bodyType[0] === 'biography:fictionalPlace') return 'place';
  if (bodyType === 'crm:E74_Group' || bodyType[0] === 'foaf:Organization') return 'organization';
  if (bodyType === 'frbroo:F1' || bodyType[0] === 'frbroo:F1') return 'work';
  if (bodyType === 'crm:E18_Physical_Thing' || bodyType[0] === 'wikidata:Q15831596') {
    return 'physicalThing';
  }
  if (bodyType === 'crm:E28_Conceptual_Object') return 'conceptualObject';
  if (bodyType === 'crm:E5_Event' || bodyType[0] === 'event:Event') return 'event';
  if (bodyType[0] === 'cito:Citation') return 'citation';

  return 'conceptualObject';
};

export const getAuthorityNameFromUrl = (url: string) => {
  if (url.toLowerCase().includes('dbpedia')) return 'DBPedia';
  if (url.toLowerCase().includes('geonames')) return 'Geonames';
  if (url.toLowerCase().includes('getty')) return 'Getty';
  if (url.toLowerCase().includes('lincs')) return 'LINCS';
  if (url.toLowerCase().includes('viaf')) return 'VIAF';
  if (url.toLowerCase().includes('wikidata')) return 'Wikidata';
  return url;
};

export function isTextPositionSelector(param: Selector): param is TextPositionSelectorExtended {
  return (param as TextPositionSelectorExtended).type.includes('TextPositionSelector');
}

export function isTextQuoteSelector(param: Selector): param is TextQuoteSelectorExtended {
  return (param as TextQuoteSelectorExtended).type.includes('TextQuoteSelector');
}

export function isRangeSelector(param: Selector): param is RangeSelector {
  return (param as RangeSelector).type.includes('RangeSelector');
}

export function isXPathSelector(param: Selector): param is XpathSelectorExtended {
  return (param as XpathSelectorExtended).type.includes('XPathSelector');
}

export const getTextPositionRecursivelly = (
  selector: Target['selector'],
): TextPositionSelectorExtended | null => {
  if (!selector) return null;
  if (!Array.isArray(selector)) selector = [selector];

  for (const selectorInstance of selector) {
    if (isTextPositionSelector(selectorInstance)) {
      return selectorInstance;
    }

    if ('refinedBy' in selectorInstance && selectorInstance.refinedBy) {
      return getTextPositionRecursivelly(selectorInstance.refinedBy);
    }
  }
  return null;
};

const getTextQuoteSelectorRecursivelly = (
  selector: Target['selector'],
): TextQuoteSelectorExtended | null => {
  if (!selector) return null;
  if (!Array.isArray(selector)) selector = [selector];

  for (const selectorInstance of selector) {
    if (isTextQuoteSelector(selectorInstance)) {
      return selectorInstance;
    }

    if ('refinedBy' in selectorInstance && selectorInstance.refinedBy) {
      return getTextQuoteSelectorRecursivelly(selectorInstance.refinedBy);
    }
  }
  return null;
};

export const getTargetString = (target: Target) => {
  if (!target.selector) {
    console.warn(`textQuoteSelector not present in annotation`);
    return '';
  }
  const textQuoteSelector = getTextQuoteSelectorRecursivelly(target.selector);
  if (!textQuoteSelector) {
    console.warn(`textQuoteSelector not present in annotation`);
    return '';
  }

  return textQuoteSelector.exact;
};

export const getTargetByFormat = (
  target: WebAnnotation['target'],
  format: MimeType,
): Target | undefined => {
  if (!target) return;

  if (Array.isArray(target)) {
    let _target: Target | undefined = undefined;

    for (const tg of target) {
      if (Array.isArray(tg.source.format)) {
        const match = tg.source.format.some((f) => f === format);
        if (match) {
          _target = tg;
          break;
        }
      }

      if (tg.source.format === format) {
        _target = tg;
        break;
      }
    }
    return _target;
  }

  if (Array.isArray(target.source.format)) {
    const match = target.source.format.some((f) => f === format);
    if (match) return target;
  }

  if (target.source.format === format) return target;
};

interface CreateTargetProps {
  documentUrl?: string;
  fileFormat?: string;
  isDraft?: boolean;
  selector?: Target['selector'];
}
export const createTarget = ({
  documentUrl,
  fileFormat,
  isDraft = false,
  selector,
}: CreateTargetProps) => {
  const source: Source = {
    id: documentUrl ?? store.get(documentMetadataAtom)?.url ?? generateUriId('draft'),
    type: 'crm:D1_Digital_Object',
    format: fileFormat ?? store.get(documentMetadataAtom)?.mimetype ?? 'application/xml',
    P1_is_identified_by: {
      id: store.get(documentSourceP1UUIDAtom), // This ID is unique for the whole document not for each annotation
      type: 'crm:E33_E41_Linguistic_Appellation',
      title: store.get(documentMetadataAtom)?.title ?? store.get(documentMetadataAtom)?.url ?? '',
    },
  };

  const target: Target = {
    id: generateUriId(isDraft ? 'draft' : ''),
    type: ['SpecificResource', 'crm:E73_Information_Object'],
    source,
    selector,
  };
  return target;
};

interface CreateTextPositionSelectorProps {
  start: number;
  end: number;
  isDraft?: boolean;
  refinedBy?: TextPositionSelectorExtended['refinedBy'];
}
export const createTextPositionSelector = ({
  start,
  end,
  isDraft = false,
  refinedBy,
}: CreateTextPositionSelectorProps) => {
  const selector: TextPositionSelectorExtended = {
    id: generateUriId(isDraft ? 'draft' : ''),
    type: ['TextPositionSelector', 'crm:E73_Information_Object'],
    start,
    end,
    refinedBy,
  };
  return selector;
};

interface CreateTextQuoteSelectorProps {
  exact: string;
  prefix: string;
  suffix: string;
  isDraft?: boolean;
  refinedBy?: TextQuoteSelectorExtended['refinedBy'];
}
export const createTextQuoteSelector = ({
  exact,
  prefix,
  suffix,
  isDraft = false,
  refinedBy,
}: CreateTextQuoteSelectorProps) => {
  const selector: TextQuoteSelectorExtended = {
    id: generateUriId(isDraft ? 'draft' : ''),
    type: ['TextQuoteSelector', 'crm:E33_Linguistic_Object'],
    exact,
    prefix,
    suffix,
    refinedBy,
  };
  return selector;
};

interface CreateXpathSelectorProps {
  value: string;
  isDraft?: boolean;
  refinedBy?: XpathSelectorExtended['refinedBy'];
}
export const createXpathSelector = ({
  value,
  isDraft = false,
  refinedBy,
}: CreateXpathSelectorProps) => {
  const selector: XpathSelectorExtended = {
    id: generateUriId(isDraft ? 'draft' : ''),
    type: ['XPathSelector', 'crm:E73_Information_Object'],
    value,
    refinedBy,
  };
  return selector;
};

interface CreateRangeSelectorProps {
  startSelector: XpathSelector;
  endSelector: XpathSelector;
  isDraft?: boolean;
}
export const createRangeSelector = ({
  startSelector,
  endSelector,
  isDraft = false,
}: CreateRangeSelectorProps) => {
  const selector: RangeSelector = {
    id: generateUriId(isDraft ? 'draft' : ''),
    type: ['RangeSelector', 'crm:E73_Information_Object'],
    startSelector,
    endSelector,
  };
  return selector;
};

interface CreateCssSelectorProps {
  value: string;
  isDraft?: boolean;
  refinedBy?: CssSelectorExtended['refinedBy'];
}
export const createCssSelector = ({
  value,
  isDraft = false,
  refinedBy,
}: CreateCssSelectorProps) => {
  const selector: CssSelectorExtended = {
    id: generateUriId(isDraft ? 'draft' : ''),
    type: ['CssSelector', 'crm:E73_Information_Object'],
    value,
    refinedBy,
  };
  return selector;
};

export const getAuthorityFromUri = (url: string) => {
  const _URL = new URL(url);
  const parts = _URL.hostname.split('.');
  const domainName = parts.at(-2); //Domain name is usually the second to the last part of Hostname
  if (domainName) return domainName;

  //fall back to compare to supported list
  for (const authority of AuthorityTypes) {
    if (url.includes(authority.toLowerCase())) return authority;
  }
};
