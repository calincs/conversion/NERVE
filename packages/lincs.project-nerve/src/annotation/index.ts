import {
  Certainties,
  ModeExistence,
  ModesExistence,
  Precisions,
  type Body,
  type Certainty,
  type Precision,
  type Target,
  type TextPositionSelectorExtended,
  type TextQuoteSelector,
  type TypeBody,
  type WebAnnotation,
} from '@lincs.project/webannotation-schema';
import { nanoid } from 'nanoid';
import pkg from '../../package.json' with { type: 'json' };
import { documentMetadataAtom, store } from '../jotai/store';
import type { EntityType, MimeType, User } from '../types';
import { generateUriId } from '../utilities';
import {
  getAuthorityNameFromUrl,
  getBodyTypeforEntityType,
  getEntityTypeFromBodyType,
  getMotivationByEntityType,
  getTargetByFormat,
  getTargetString,
  getTextPositionRecursivelly,
} from './helper-functions';

export type AnnotationStatus = 'draft' | 'approved';
export * from './helper-functions';

interface CreateAnnotationParams {
  target: Target;
  body: {
    id?: Body['id'];
    type: EntityType;
    label: Body['label'];
    description?: Body['description'];
  };
  user?: User;
}

export class Annotation {
  readonly uuid: string;
  private _webAnnotation: WebAnnotation;

  private _locked = false;

  private _target?: Target | Target[];
  private _quote: TextQuoteSelector['exact'];

  private _query?: string;

  private _body: Body;
  private _bodyCandidates?: Body[];

  private _bodyId: Body['id'];
  private _bodyType: EntityType;
  private _bodyLabel?: string;

  private _isDraft = false;

  constructor(anno: WebAnnotation | CreateAnnotationParams) {
    this.uuid = nanoid(11);

    anno = 'id' in anno ? anno : this.createWebAnnotationDraft(anno);

    this._locked = true;

    this._webAnnotation = anno;

    const fileFormat = store.get(documentMetadataAtom)?.mimetype ?? 'application/xml';
    this._target = anno.target;

    const currentTarget = getTargetByFormat(anno.target, fileFormat);

    this._quote = currentTarget ? getTargetString(currentTarget) : '';

    //Body
    this._body = Array.isArray(anno.body) ? (anno.body[0] as Body) : (anno.body as Body);

    this._bodyId = this._body.id;
    this._bodyType = getEntityTypeFromBodyType(this._body.type);
    this._bodyLabel = this._body.label;

    this._isDraft = this._webAnnotation.id.includes('draft') ? true : false;
  }

  private createWebAnnotationDraft({ target, body, user }: CreateAnnotationParams) {
    const _user: User = user ?? {
      url: 'http://anonymous.com',
      name: 'anonymous',
    };

    const motivation = getMotivationByEntityType(body.type);

    const anno: WebAnnotation = {
      '@context': [
        'http://www.w3.org/ns/anno.jsonld',
        'https://www.cidoc-crm.org/cidoc-crm/json-ld_context.jsonld',
        'https://wa.lincsproject.ca/v1/ns/anno.jsonld',
      ],
      id: generateUriId('draft'),
      type: ['Annotation', 'crm:E33_Linguistic_Object'],
      motivation: motivation,
      created: new Date().toDateString(),
      creator: {
        id: _user?.url!,
        type: 'crm:E21_Person',
        name: _user?.name!,
      },
      generator: {
        id: window.location.origin,
        type: ['Software', 'crm:E73_Information_Object'],
        P16_used_specific_object: window.location.origin,
        name: 'NERVE',
        softwareVersion: pkg.version,
      },
      target,
      body: {
        id: generateUriId('draft'),
        type: getBodyTypeforEntityType(body.type),
        label: body.label,
        description: body.description,
      },
      P2_has_type: [motivation],
    };

    return anno;
  }

  toJson({ space }: { space?: number } = { space: 4 }) {
    return JSON.stringify(this.webAnnotation, null, space);
  }

  public get webAnnotation() {
    return this._webAnnotation;
  }

  public get id() {
    return this._webAnnotation.id;
  }

  public get locked() {
    return this._locked;
  }

  public setLocked(value: boolean) {
    this._locked = value;
  }

  public get isDraft() {
    return this._isDraft;
  }

  public get creator() {
    return this._webAnnotation.creator;
  }
  public get createdAt() {
    return this._webAnnotation.created;
  }

  public get modifiedAt() {
    return this._webAnnotation.modified;
  }

  public set modifiedAt(_modifiedAt: WebAnnotation['modified']) {
    this._webAnnotation.modified = _modifiedAt;
  }

  public get quote() {
    return this._quote;
  }

  public get target() {
    return this._target;
  }

  public getTargetOffsetPosition():
    | { start: TextPositionSelectorExtended['start']; end: TextPositionSelectorExtended['end'] }
    | undefined {
    if (!this._target) return;

    const fileFormat = store.get(documentMetadataAtom)?.mimetype ?? 'application/xml';
    const currentTarget = getTargetByFormat(this._target, fileFormat);
    if (!currentTarget) return;

    const selector = getTextPositionRecursivelly(currentTarget.selector);
    if (!selector) return;

    return {
      start: selector.start,
      end: selector.end,
    };
  }

  public getTargetByFormat = (format: MimeType): Target | undefined => {
    const target = this._webAnnotation.target;
    if (!target) return;

    if (Array.isArray(target)) {
      let _target: Target | undefined = undefined;

      for (const tg of target) {
        if (Array.isArray(tg.source.format)) {
          const match = tg.source.format.some((f) => f === format);
          if (match) {
            _target = tg;
            break;
          }
        }

        if (tg.source.format === format) {
          _target = tg;
          break;
        }
      }
      return _target;
    }

    if (Array.isArray(target.source.format)) {
      const match = target.source.format.some((f) => f === format);
      if (match) return target;
    }

    if (target.source.format === format) return target;
  };

  public set quote(quote: TextQuoteSelector['exact']) {
    //TODO: Update Selector
    // this._quote = quote;
    // this._webAnnotation.target.
  }

  public get query() {
    return this._query ?? this._quote;
  }

  public set query(query: string) {
    this._query = query;
  }

  public get body() {
    return this._body;
  }

  public get bodyCandidates(): Body[] | undefined {
    return this._bodyCandidates;
  }

  public set bodyCandidates(candidates: Body[]) {
    this._bodyCandidates = candidates;
  }

  public getBodyCandidateById(id: string): Body | undefined {
    return this.bodyCandidates?.find((body) => 'id' in body && body.id === id);
  }

  public get entityUri() {
    return this._bodyId;
  }

  public get entityType() {
    return this._bodyType;
  }

  public set entityType(entityType: EntityType) {
    this._bodyType = entityType;
    this.resetLinking(entityType);
  }

  public resetLinking(entityType?: EntityType) {
    this._bodyType = entityType ?? this._bodyType;
    const type = getBodyTypeforEntityType(entityType ?? this.entityType);

    if (this._isDraft) {
      this._bodyCandidates = undefined;
      this._webAnnotation.body = {
        ...this._webAnnotation.body,
        type,
      };
    } else {
      this._webAnnotation.body = {
        id: generateUriId('draft'),
        type,
        label: this._quote,
      };
    }

    this._query = undefined;

    this._body = this._webAnnotation.body as Body;
    this._bodyId = this._body.id;
    this._bodyLabel = this._body.label;

    this._isDraft = true;
  }

  public get bodyLabel() {
    return this._bodyLabel;
  }

  public get authorityName() {
    const body = Array.isArray(this.body) ? this.body[0] : this.body;
    return getAuthorityNameFromUrl(body.id);
  }

  private removeRootP2Prop(propValues: string[]) {
    //hold to motivation
    const motivation = this._webAnnotation.P2_has_type[0];

    //desassemble
    const workableP2 = this._webAnnotation.P2_has_type;
    workableP2.shift();

    //filter
    const filteredP2 = workableP2.filter((prop) => prop && !propValues.includes(prop));

    // reassemble
    const newP2: WebAnnotation['P2_has_type'] = [motivation];
    filteredP2.forEach((prop, i) => {
      if (prop) newP2[i + 1] = prop;
    });

    this._webAnnotation.P2_has_type = newP2;
  }

  public get certainty() {
    return this._webAnnotation.P2_has_type.find(
      (prop) => prop && (Certainties.options as string[]).includes(prop),
    ) as Certainty | undefined;
  }

  public setCertainty(value?: Certainty) {
    //Allowed entities
    if (
      this.entityType !== 'person' &&
      this.entityType !== 'place' &&
      this.entityType !== 'organization' &&
      this.entityType !== 'work' &&
      this.entityType !== 'physicalThing' &&
      this.entityType !== 'conceptualObject' &&
      this.entityType !== 'event'
    ) {
      return;
    }

    // Remove if no value is provided
    if (!value) {
      this.removeRootP2Prop(Certainties.options);
      return;
    }

    //Do not add value if it alrready exists
    if (this._webAnnotation.P2_has_type.includes(value)) return;

    const index = this._webAnnotation.P2_has_type.findIndex(
      (prop) => prop && (Certainties.options as string[]).includes(prop),
    );

    // ** replace
    if (index !== -1) {
      this._webAnnotation.P2_has_type.splice(index, 1, value);
      return;
    }

    // ** add
    this._webAnnotation.P2_has_type.push(value);
  }

  public get precision() {
    return this._webAnnotation.P2_has_type.find(
      (prop) => prop && (Precisions.options as string[]).includes(prop),
    ) as Precision | undefined;
  }

  public setPrecision(value?: Precision) {
    if (this.entityType !== 'place') return;

    // Remove if no value is provided
    if (!value) {
      this.removeRootP2Prop(Precisions.options);
      return;
    }

    //Do not add value if it already exists
    if (this._webAnnotation.P2_has_type.includes(value)) return;

    const index = this._webAnnotation.P2_has_type.findIndex(
      (prop) => prop && (Precisions.options as string[]).includes(prop),
    );

    // ** replace
    if (index !== -1) {
      this._webAnnotation.P2_has_type.splice(index, 1, value);
      return;
    }

    // ** add
    this._webAnnotation.P2_has_type.push(value);
  }

  public get modeExistence() {
    return this._body.P2_has_type?.find((prop) =>
      (ModesExistence.options as string[]).includes(prop),
    ) as ModeExistence | undefined;
  }

  private getBodyTypeForModeExistence(existence: ModeExistence = 'edit:modeReal') {
    let type: TypeBody | null = null;

    if (this._body.type === 'crm:E21_Person' || this._body.type[0] === 'foaf:Person') {
      type =
        existence === 'edit:modeReal'
          ? 'crm:E21_Person'
          : ['foaf:Person', 'crm:E89_Propositional_Object'];
    }

    if (this._body.type === 'crm:E53_Place' || this._body.type[0] === 'biography:fictionalPlace') {
      type =
        existence === 'edit:modeReal'
          ? 'crm:E53_Place'
          : ['biography:fictionalPlace', 'crm:E89_Propositional_Object'];
    }

    if (this._body.type === 'crm:E74_Group' || this._body.type[0] === 'foaf:Organization') {
      type =
        existence === 'edit:modeReal'
          ? 'crm:E74_Group'
          : ['foaf:Organization', 'crm:E89_Propositional_Object'];
    }

    if (this._body.type === 'frbroo:F1' || this._body.type[0] === 'frbroo:F1') {
      type = existence === 'edit:modeReal' ? 'frbroo:F1' : ['frbroo:F1', 'wikidata:Q15306849'];
    }

    if (
      this._body.type === 'crm:E18_Physical_Thing' ||
      this._body.type[0] === 'crm:E18_Physical_Thing'
    ) {
      type =
        existence === 'edit:modeReal'
          ? 'crm:E18_Physical_Thing'
          : ['wikidata:Q15831596', 'crm:E89_Propositional_Object'];
    }

    if (this._body.type[0] === 'crm:E5_Event') {
      type =
        existence === 'edit:modeReal'
          ? 'crm:E5_Event'
          : ['event:Event', 'crm:E89_Propositional_Object'];
    }

    return type;
  }

  public setModeExistence(value?: ModeExistence) {
    // return is the value is same as current
    const currentValue = this.modeExistence;
    if (currentValue === value) return;

    // remove if no value is provided
    if (!value) {
      //remove from p2
      this.removeBodyP2Prop(ModesExistence.options);

      // update type to default
      const updatedBodyType = this.getBodyTypeForModeExistence(value);
      if (!updatedBodyType) return;
      this._body.type = updatedBodyType;
      this._webAnnotation.body = this._body;
      return;
    }

    const updatedBodyType = this.getBodyTypeForModeExistence(value);
    if (!updatedBodyType) return;

    // update internal body
    this._body.type = updatedBodyType;

    //* create p2 if not set yet
    if (!this._body.P2_has_type) this._body.P2_has_type = [value];

    if (currentValue) {
      const currentIndex = this._body.P2_has_type.findIndex((prop) =>
        (ModesExistence.options as string[]).includes(prop),
      );

      // ** replace
      if (currentIndex !== -1) {
        this._body.P2_has_type.splice(currentIndex, 1, value);
      } else {
        // ** add
        this._body.P2_has_type.push(value);
      }
    }

    //deduplicate
    this._body.P2_has_type = [...new Set([...this._body.P2_has_type, value])];

    // update web annotations
    this._webAnnotation.body = this._body;
  }

  private removeBodyP2Prop(propValues: string[]) {
    if (!this._body.P2_has_type) return;

    const newP2 = this._body.P2_has_type.filter((prop) => !propValues.includes(prop));

    if (newP2.length === 0) {
      delete this._body.P2_has_type;
    } else {
      this._body.P2_has_type = newP2;
    }

    this._webAnnotation.body = this._body;
  }

  public get bodyDescription() {
    return this._body.description;
  }

  public get bodyValue() {
    return this._body.value;
  }

  public approveBody(body: Body, user?: User) {
    user = user ?? {
      url: 'http://anonymous.com',
      name: 'anonymous',
    };

    //reset

    this._query = undefined;
    this._bodyCandidates = undefined;

    //web annotation

    this._webAnnotation = {
      ...this._webAnnotation,
      id: generateUriId(),
      created: new Date().toISOString(),
      creator: {
        id: user.url,
        type: 'crm:E21_Person',
        name: user.name,
      },
      generator: {
        id: window.location.origin,
        type: ['Software', 'crm:E73_Information_Object'],
        P16_used_specific_object: window.location.origin,
        name: 'NERVE',
        softwareVersion: pkg.version,
      },
      body,
    };

    //internals

    this._body = body;

    this._bodyId = body.id;
    this._bodyType = getEntityTypeFromBodyType(body.type);
    this._bodyLabel = body.label;

    this._locked = false;

    this._isDraft = false;
  }

  public static allowsAttribute(
    entityType: EntityType,
    attribute: 'certainty' | 'precision' | 'modeExistence',
  ) {
    if (entityType === 'person') {
      if (attribute === 'certainty') return true;
      if (attribute === 'precision') return false;
      if (attribute === 'modeExistence') return true;
    }

    if (entityType === 'place') {
      if (attribute === 'certainty') return true;
      if (attribute === 'precision') return true;
      if (attribute === 'modeExistence') return true;
    }

    if (entityType === 'organization') {
      if (attribute === 'certainty') return true;
      if (attribute === 'precision') return false;
      if (attribute === 'modeExistence') return true;
    }

    if (entityType === 'work') {
      if (attribute === 'certainty') return true;
      if (attribute === 'precision') return false;
      if (attribute === 'modeExistence') return true;
    }

    if (entityType === 'event') {
      if (attribute === 'certainty') return true;
      if (attribute === 'precision') return false;
      if (attribute === 'modeExistence') return true;
    }

    if (entityType === 'physicalThing') {
      if (attribute === 'certainty') return true;
      if (attribute === 'precision') return false;
      if (attribute === 'modeExistence') return true;
    }

    if (entityType === 'conceptualObject') {
      if (attribute === 'certainty') return true;
      if (attribute === 'precision') return false;
      if (attribute === 'modeExistence') return false;
    }

    if (entityType === 'citation') {
      if (attribute === 'certainty') return true;
      if (attribute === 'precision') return false;
      if (attribute === 'modeExistence') return false;
    }

    return false;
  }
}
