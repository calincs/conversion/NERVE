import { RESET } from 'jotai/utils';
// import xmlFormat from 'xml-formatter';
import {
  alertDialogAtom,
  alertDialogIsProcessingAtom,
  annotationsCollectionAtom,
  collectionMetaAtom,
  documentHasChangedAtom,
  domSelectionAtom,
  documentHeaderAtom,
  documentMetadataAtom,
  documentStringAtom,
  documentXmlAtom,
  entityLookupDialogAtom,
  filterViewAtom,
  popupPropsAtom,
  selectedAnnotationUuidAtom,
  settingsDialogOpenAtom,
  sortAnnotationsAtom,
  stackedAnnotationsAtom,
  store,
  userAtom,
} from './jotai/store';
import { cleanNerveTags, removeDraftTags, updateAnnotationToStandoffTag } from './utilities/dom';
import { getContent } from './viewer/helper';

export const NerveActions = {
  openSettingsPanel: (value = true) => {
    store.set(settingsDialogOpenAtom, value);
  },
  getContentHasChanged: () => {
    const hasChanged = store.get(documentHasChangedAtom);
    return !!hasChanged;
  },
  resetContentHasChanged: () => {
    store.set(documentHasChangedAtom, null);
  },
  unload: () => {
    store.set(userAtom, RESET);
    store.set(documentMetadataAtom, RESET);
    store.set(documentStringAtom, RESET);
    store.set(documentXmlAtom, RESET);
    store.set(domSelectionAtom, RESET);
    store.set(documentHeaderAtom, RESET);
    store.set(documentHasChangedAtom, RESET);
    store.set(annotationsCollectionAtom, RESET);
    store.set(selectedAnnotationUuidAtom, RESET);
    store.set(collectionMetaAtom, RESET);
    store.set(stackedAnnotationsAtom, RESET);
    store.set(sortAnnotationsAtom, RESET);
    store.set(filterViewAtom, RESET);
    store.set(popupPropsAtom, RESET);
    store.set(alertDialogAtom, RESET);
    store.set(alertDialogIsProcessingAtom, RESET);
    store.set(entityLookupDialogAtom, RESET);
    store.set(settingsDialogOpenAtom, RESET);
  },
  getAnnotations: () => {
    const annotations = Array.from(store.get(annotationsCollectionAtom).values());
    const approvedAnnotations = annotations
      .filter((anno) => !anno.isDraft)
      .map((anno) => anno.webAnnotation);
    return approvedAnnotations;
  },
  getContent: ({ includeStandoff = true }: { includeStandoff?: boolean } = {}) => {
    const documentXml = store.get(documentXmlAtom);
    if (!documentXml) return null;

    const content = getContent();
    if (!content) return null;

    //Copy
    let docXML = new DOMParser().parseFromString(
      new XMLSerializer().serializeToString(documentXml),
      'application/xml',
    );

    //*  replace original XLM with the new content
    docXML
      .getElementsByTagName('text')[0]
      .replaceWith(content.getElementsByTagName('text')[0].cloneNode(true));

    docXML = removeDraftTags(docXML);
    docXML = cleanNerveTags(docXML);

    if (includeStandoff) docXML = updateAnnotationToStandoffTag(docXML);

    const xmlString = new XMLSerializer().serializeToString(docXML);

    // const formated = xmlFormat(xmlString, {
    //   collapseContent: true,
    //   // ignoredPaths: ['standOff'],
    //   // whiteSpaceAtEndOfSelfclosingTag: true,
    // });

    // console.log(formated);

    // const formated = formatXml(xmlString);

    return xmlString;
  },
};

export type NerveActionsType = typeof NerveActions;

const xsl1 = `<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:output indent="yes"/>

 <xsl:template match="node()|@*">
  <xsl:copy>
   <xsl:apply-templates select="node()|@*"/>
  </xsl:copy>
 </xsl:template>
</xsl:stylesheet>`;

const xsl = `<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:output method="xml" indent="yes"/>
 <xsl:strip-space elements="*"/>
 <xsl:template match="/">
  <xsl:copy-of select="."/>
 </xsl:template>
</xsl:stylesheet>`;

const formatXml = (string: string) => {
  const xslNode = new DOMParser().parseFromString(xsl, 'text/xml');
  const xml = new DOMParser().parseFromString(string, 'text/xml');

  const xsltProcessor = new XSLTProcessor();
  xsltProcessor.importStylesheet(xslNode);
  const resultDocument = xsltProcessor.transformToFragment(xml, document);

  const xmlString = new XMLSerializer().serializeToString(resultDocument);

  return xmlString;
};
