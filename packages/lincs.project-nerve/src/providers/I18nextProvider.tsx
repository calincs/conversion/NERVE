'use client';

import { I18nextProvider as ReactI18nextProvider } from 'react-i18next';
import i18next from '../i18n';

export const I18nextProvider = ({ children }: React.PropsWithChildren) => {
  return <ReactI18nextProvider i18n={i18next}>{children}</ReactI18nextProvider>;
};
