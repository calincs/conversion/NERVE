import { extendTheme, type PaletteRange } from '@mui/joy/styles';
import chroma from 'chroma-js';

declare module '@mui/joy/styles' {
  interface ColorPalettePropOverrides {
    // apply to all Joy UI components that support `color` prop
    person: true;
    place: true;
    organization: true;
    work: true;
    event: true;
    physicalThing: true;
    conceptualObject: true;
    citation: true;
  }

  interface Palette {
    // this will make these nodes configurable in `extendTheme`
    // and add the node to the theme's palette.
    person: PaletteRange;
    place: PaletteRange;
    organization: PaletteRange;
    work: PaletteRange;
    event: PaletteRange;
    physicalThing: PaletteRange;
    conceptualObject: PaletteRange;
    citation: PaletteRange;
  }
}

//* https://uxdesign.cc/defining-colors-in-your-design-system-828148e6210a
// * https://colorbox.io/

const cssVarPrefix = 'nerve';

const createPalette = (name: string, color: `#${string}`) => {
  return {
    50: chroma(color).brighten(1.6).hex(),
    100: chroma(color).brighten(1.2).hex(),
    200: chroma(color).brighten(0.9).hex(),
    300: chroma(color).brighten(0.6).hex(),
    400: chroma(color).brighten(0.3).hex(),
    500: color,
    600: chroma(color).darken(0.3).hex(),
    700: chroma(color).darken(0.6).hex(),
    800: chroma(color).darken(0.9).hex(),
    900: chroma(color).darken(1.2).hex(),
    plainColor: `var(--${cssVarPrefix}-palette-${name}-900)`,
    plainHoverBg: `var(--${cssVarPrefix}-palette-${name}-500)`,
    solidBg: `var(--${cssVarPrefix}-palette-${name}-700)`,
    solidActiveBg: `var(--${cssVarPrefix}-palette-${name}-900)`,
    solidHoverBg: `var(--${cssVarPrefix}-palette-${name}-800)`,
    solidDisabledBg: `var(--${cssVarPrefix}-palette-${name}-50)`,
    solidDisabledColor: `var(--${cssVarPrefix}-palette-neutral-400)`,
    outlinedColor: `var(--${cssVarPrefix}-palette-${name}-800)`,
    outlinedBorder: `var(--${cssVarPrefix}-palette-${name}-700)`,
    outlinedHoverBg: `var(--${cssVarPrefix}-palette-${name}-50)`,
    softColor: `var(--${cssVarPrefix}-palette-${name}-800)`,
    softBg: `var(--${cssVarPrefix}-palette-${name}-200)`,
    softHoverBg: `var(--${cssVarPrefix}-palette-${name}-300)`,
    softActiveBg: `var(--${cssVarPrefix}-palette-${name}-400)`,
  };
};

export const theme = extendTheme({
  cssVarPrefix,
  colorSchemes: {
    light: {
      palette: {
        primary: {
          '50': '#c4efff',
          '100': '#a8e5fb',
          '200': '#8cdbf7',
          '300': '#72d1f3',
          '400': '#59c7ee',
          '500': '#38b8e6',
          '600': '#1fa8d9',
          '700': '#0c8ebd',
          '800': '#00516e',
          '900': '#0d2933',
          plainColor: 'var(--nerve-palette-primary-900)',
          plainHoverBg: 'var(--nerve-palette-primary-500)',
          solidBg: 'var(--nerve-palette-primary-700)',
          solidActiveBg: 'var(--nerve-palette-primary-900)',
          solidHoverBg: 'var(--nerve-palette-primary-800)',
          solidDisabledBg: 'var(--nerve-palette-primary-50)',
          solidDisabledColor: 'var(--nerve-palette-neutral-400)',
          outlinedColor: 'var(--nerve-palette-primary-800)',
          outlinedBorder: 'var(--nerve-palette-primary-700)',
          outlinedHoverBg: 'var(--nerve-palette-primary-50)',
          softColor: 'var(--nerve-palette-primary-800)',
          softBg: 'var(--nerve-palette-primary-200)',
          softHoverBg: 'var(--nerve-palette-primary-300)',
          softActiveBg: 'var(--nerve-palette-primary-400)',
        },
        person: createPalette('person', '#2E86DE'),
        place: createPalette('place', '#FF9F43'),
        organization: createPalette('organization', '#B0B97A'),
        work: createPalette('work', '#AF46F0'),
        event: createPalette('event', '#46DCF0'),
        physicalThing: createPalette('physicalThing', '#8395A7'),
        conceptualObject: createPalette('conceptualObject', '#FF6B00'),
        citation: createPalette('citation', '#008040'),
      },
    },
    dark: {
      palette: {
        primary: {
          solidBg: 'var(--nerve-palette-primary-400)',
        },
        person: createPalette('person', '#2E86DE'),
        place: createPalette('place', '#FF9F43'),
        organization: createPalette('organization', '#B0B97A'),
        work: createPalette('work', '#AF46F0'),
        event: createPalette('event', '#46DCF0'),
        physicalThing: createPalette('physicalThing', '#8395A7'),
        conceptualObject: createPalette('conceptualObject', '#FF6B00'),
        citation: createPalette('citation', '#008040'),
      },
    },
  },
});
