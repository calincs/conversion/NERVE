'use client';

import { GlobalStyles } from '@mui/joy';
import CssBaseline from '@mui/joy/CssBaseline';
import { CssVarsProvider as JoyCssVarsProvider } from '@mui/joy/styles';
import { SnackbarProvider } from 'notistack';
import { type PropsWithChildren } from 'react';
import { theme } from './theme';

interface ThemeRegistry extends PropsWithChildren {
  darkMode?: boolean;
}

export const ThemeRegistry = ({ children, darkMode = false }: ThemeRegistry) => {
  return (
    <JoyCssVarsProvider defaultMode={darkMode ? 'dark' : 'system'} theme={theme}>
      <CssBaseline />
      <GlobalStyles
        styles={{
          // The {selector} is the CSS selector to target the icon.
          // We recommend using a class over a tag if possible.
          '{selector}': {
            color: 'var(--Icon-color)',
            margin: 'var(--Icon-margin)',
            fontSize: 'var(--Icon-fontSize, 20px)',
            width: '0.75em',
            height: '0.75em',
          },
        }}
      />
      <SnackbarProvider>{children}</SnackbarProvider>
    </JoyCssVarsProvider>
  );
};
