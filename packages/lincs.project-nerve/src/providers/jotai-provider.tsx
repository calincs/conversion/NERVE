'use client';

import { Provider } from 'jotai';
import { DevTools } from 'jotai-devtools';
import 'jotai-devtools/styles.css';
import { store } from '../jotai/store';

export const JotaiProvider = ({ children }: React.PropsWithChildren) => {
  return (
    <Provider store={store}>
      {children}
      <DevTools position="bottom-right" theme="dark" />
    </Provider>
  );
};
