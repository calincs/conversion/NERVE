'use client';

import { useAtom, useSetAtom } from 'jotai';
import { nanoid } from 'nanoid';
import { useEffect } from 'react';
import { useDocument, useInitilize } from './hooks';
import i18next from './i18n';
import {
  annotationsAtom,
  documentHasChangedAtom,
  nerveSessionIdAtom,
  store,
  userAtom,
} from './jotai/store';
import { Layout } from './layout';
import { SnackbarProvider } from './providers/snackbar-provider';
import { ThemeRegistry } from './providers/theme-registry';
import type { NerveProps } from './types';

const App = ({ annotations, document, onDocumentOnchange, preferences, user }: NerveProps) => {
  const { initializeNerve } = useInitilize();
  const { initialize } = useDocument();

  const setAnnotationsAtom = useSetAtom(annotationsAtom);
  const [documentHasChanged, setDocumentHasChanged] = useAtom(documentHasChangedAtom);
  const setUser = useSetAtom(userAtom);

  // const setNerveSessionId = useSetAtom(nerveSessionIdAtom);

  const init = async () => {
    store.set(nerveSessionIdAtom, nanoid(7));
    // setNerveSessionId(nanoid(7));
    setUser(user);

    await initializeNerve();
    initialize(document.content, document.url);

    // if (annotations) {
    //   const annos = annotations.map((anno) => new Annotation(anno));
    //   setAnnotationsAtom(annos);
    // }
  };

  useEffect(() => {
    init();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setUser(user);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user.url]);

  useEffect(() => {
    if (preferences?.locale) i18next.changeLanguage(preferences.locale);
  }, [preferences?.locale]);

  useEffect(() => {
    onDocumentOnchange?.(!!documentHasChanged);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [documentHasChanged]);

  useEffect(() => {
    setDocumentHasChanged(null);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [document.content, document.url]);

  return (
    <ThemeRegistry darkMode={preferences?.darkTheme}>
      <SnackbarProvider>
        <Layout />
      </SnackbarProvider>
    </ThemeRegistry>
  );
};

export default App;
