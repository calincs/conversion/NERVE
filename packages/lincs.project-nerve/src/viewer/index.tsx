import { Sheet, Stack, type SheetProps } from '@mui/joy';
import { motion } from 'framer-motion';
import Frame, { type FrameComponentProps } from 'react-frame-component';
import { useCss } from '../hooks/useCss';
import { Topbar } from './topbar';

export interface Props {
  slotProps?: {
    root?: Omit<SheetProps, 'children'>;
    iframe?: Omit<FrameComponentProps, 'children'>;
  };
}

export const Viewer = () => {
  const { css } = useCss();

  return (
    <Stack sx={{ display: 'flex', flex: 'auto' }} px={1} py={0.5}>
      <Topbar />
      <Sheet
        sx={{ display: 'flex', flex: 'auto' }}
        component={motion.div}
        initial={{ y: 10, opacity: 0 }}
        animate={{ y: 0, opacity: 1 }}
        transition={{ duration: 1, delay: 1 }}
      >
        <Frame
          head={
            <>
              <style>{css}</style>
            </>
          }
          style={{ width: '100%', border: 0 }}
        >
          {null}
        </Frame>
      </Sheet>
    </Stack>
  );
};
