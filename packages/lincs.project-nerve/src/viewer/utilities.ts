import type {
  TextPositionSelector,
  TextQuoteSelector,
  XpathSelectorExtended,
} from '@lincs.project/webannotation-schema';
import { nanoid } from 'nanoid';
import { Annotation } from '../annotation';
import { documentMetadataAtom, documentStringAtom, store } from '../jotai/store';
import { getTagNameForEntityType } from '../schema-mapping';
import type { EntityType } from '../types';

/**
 * Returns the result of the specified xpath on the specified context node.
 * Can detect and convert an XML xpath for use with the leaf-writer format.
 * Adds support for default namespace.
 * @param {Document} document
 * @param {String} xpath
 * @param {Document|Element} context
 * @returns {XPathResult|null} The result or null
 */
export const evaluateXPath = (document: Document, xpath: string, context: Element) => {
  // grouped matches: 1 separator, 2 axis, 3 namespace, 4 element name or attribute name or function, 5 predicate
  const regex = /(\/{0,2})([\w-]+::|@)?(\w+?:)?([\w-(\.\*)]+)(\[.+?\])?/g;

  const defaultNamespace = document.documentElement.getAttribute('xmlns');
  console.log({ document, defaultNamespace });

  const nsr = document.createNSResolver(document.documentElement);

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  //@ts-ignore
  const nsResolver = (prefix) => nsr.lookupNamespaceURI(prefix) ?? defaultNamespace;

  // remove all namespaces from the xpath
  if (defaultNamespace === null) {
    xpath = xpath.replace(regex, (_match, p1, p2, _p3, p4, p5) => [p1, p2, p4, p5].join(''));
  } else {
    // default namespace hack (http://stackoverflothis.writer.com/questions/9621679/javascript-xpath-and-default-namespaces)
    // add foo namespace to the element name
    xpath = xpath.replace(regex, (match, p1, p2, p3, p4, p5) => {
      if (p3 !== undefined) {
        // already has a namespace
        return match;
      } else {
        if (
          // it's an attribute and therefore doesn't need a default namespace
          (p2 !== undefined && (p2.indexOf('attribute') === 0 || p2.indexOf('@') === 0)) ||
          // it's a function not an element name
          p4.match(/\(.*?\)/) !== null
        ) {
          return [p1, p2, p3, p4, p5].join('');
        } else {
          return [p1, p2, 'foo:', p4, p5].join('');
        }
      }
    });
  }

  console.log({ xpath, context });

  try {
    const evalResult = document.evaluate(
      xpath,
      context ?? document,
      nsResolver,
      XPathResult.ANY_TYPE,
      null,
    );

    console.log(evalResult);

    let result: number | string | boolean | Node | null = null;

    switch (evalResult.resultType) {
      case XPathResult.NUMBER_TYPE:
        result = evalResult.numberValue;
        break;
      case XPathResult.STRING_TYPE:
        result = evalResult.stringValue;
        break;
      case XPathResult.BOOLEAN_TYPE:
        result = evalResult.booleanValue;
        break;
      case XPathResult.UNORDERED_NODE_ITERATOR_TYPE:
      case XPathResult.ORDERED_NODE_ITERATOR_TYPE:
        result = evalResult.iterateNext();
        break;
      case XPathResult.ANY_UNORDERED_NODE_TYPE:
      case XPathResult.FIRST_ORDERED_NODE_TYPE:
        result = evalResult.singleNodeValue;
        break;
    }

    return result;
  } catch (error) {
    console.warn(`evaluateXPath: there was an error evaluating the xpath ${error}`);
    return null;
  }
};

export const getIframe = () => {
  // eslint-disable-next-line @typescript-eslint/prefer-optional-chain
  if (typeof window === 'undefined' || !window.document) return;
  const iframe = document.getElementsByTagName('iframe')[0];
  if (!iframe) return null;

  const frameContent = iframe.contentDocument;
  return frameContent;
};

export const getCurrentDocument = (format: 'string' | 'xml' = 'xml') => {
  const iframeContent = getIframeContent();
  if (!iframeContent) return document.implementation.createDocument(null, 'doc');

  const TEI = iframeContent.body.getElementsByTagName('TEI')[0];
  if (!TEI) return null;

  const xmlString = new XMLSerializer().serializeToString(TEI);
  if (format === 'string') return xmlString;

  const xml = new DOMParser().parseFromString(xmlString, 'application/xml');
  return xml;
};

export const getIframeContent = () => {
  const frame = getIframe();
  if (!frame) return;

  const head = frame.getElementsByTagName('head')[0];
  if (!head) return;

  const body = frame.getElementsByClassName('frame-content')[0];
  if (!body) return;

  return { head, body };
};

export const getDocumentStyleSheets = (xml: XMLDocument) => {
  const hrefRegex = new RegExp(`href=('|")([^('|")]*)('|")`);
  let stylesheets: string[] = [];

  xml.childNodes.forEach((node) => {
    if (node.nodeName === 'xml-stylesheet') {
      const xmlStylesheetData = node.nodeValue!;
      const hrefMatch = xmlStylesheetData.match(hrefRegex);
      const url = hrefMatch?.[2];
      if (url) stylesheets.push(url);
    }
  });

  return stylesheets;
};

export const createTextQuoteSelector = (selection: Selection) => {
  const quote = selection.toString();
  const prefix = selection.anchorNode?.textContent?.slice(0, selection.anchorOffset) ?? '';
  const suffix = selection.focusNode?.textContent?.slice(selection.focusOffset) ?? '';

  const selector: TextQuoteSelector = {
    id: 'ID',
    type: ['TextQuoteSelector', 'crm:E33_Linguistic_Object'],
    exact: quote,
    prefix,
    suffix,
  };

  return selector;
};

export const createXpathSelector = (selection: Selection) => {
  const xpath = getElementXPath(selection.anchorNode);

  const selector: XpathSelectorExtended = {
    id: 'ID',
    type: ['XPathSelector', 'crm:E73_Information_Object'],
    value: xpath,
  };

  return selector;
};

export const createTextPositionSelector = (selection: Selection) => {
  const start = selection.anchorOffset;
  const end = selection.focusOffset;

  const selector: TextPositionSelector = {
    id: 'ID',
    type: ['TextPositionSelector', 'crm:E73_Information_Object'],
    start,
    end,
  };

  return selector;
};

export const getDocumentRoot = () => {
  const iframeContent = getIframeContent();
  if (!iframeContent) return document;

  const { body } = iframeContent;

  const rootElement = body.firstElementChild;
  if (!rootElement) return document;
  return rootElement;
};

export const getLanguage = ({
  node,
  root,
  inherit = true,
}: {
  node: Node;
  root?: Document | Element;
  inherit?: boolean;
}) => {
  root = root ?? getDocumentRoot();

  let element = node.parentElement;
  if (!element) return;

  let language = element.getAttribute('lang');
  if (!inherit) return language ?? undefined;

  if (!language) {
    while (element !== root && !language) {
      if (!element) {
        const message = 'The supplied element is not contained by the root node.';
        const name = 'InvalidNodeTypeError';
        throw new DOMException(message, name);
      }

      language = element.getAttribute('lang');

      element = element.parentElement;
    }
  }

  return language ?? undefined;
};

export const getElementXPath = (node: Node | null, root?: Document | Element) => {
  if (node === null) throw new Error('missing required parameter "node"');

  root = root ?? getDocumentRoot();

  const xPathNodes: string[] = [];

  if (node.nodeName !== root.nodeName) {
    while (node !== root) {
      if (!node) {
        const message = 'The supplied node is not contained by the root node.';
        const name = 'InvalidNodeTypeError';
        throw new DOMException(message, name);
      }

      if (node.nodeType !== 1) {
        node = node.parentNode;
        continue;
      }

      const name = nodeName(node);
      const position = nodePosition(node);

      xPathNodes.unshift(`/${name}[${position}]`);

      node = node.parentNode;
    }
  }

  xPathNodes.unshift(`/${root.nodeName}`);

  return xPathNodes
    .join('')
    .replaceAll(/\[[1]\]/g, '') //remove element position if it is the first of its kind
    .replace(/\/$/, ''); //remove trailing '/'
};

// Get the XPath node name.
export const nodeName = (node: Node) => {
  switch (node.nodeName) {
    case '#text':
      return 'text()';
    case '#comment':
      return 'comment()';
    case '#cdata-section':
      return 'cdata-section()';
    default:
      return node.nodeName;
  }
};

// Get the ordinal position of this node among its siblings of the same name.
export const nodePosition = (node: Node | null) => {
  if (!node) return 1;

  const name = node.nodeName;
  let position = 1;
  while ((node = node.previousSibling)) {
    if (node.nodeName === name) position += 1;
  }
  return position;
};

export const extractContentFromRendered = () => {
  const iframe = getIframe()!;
  // if (!iframe) return;

  const rootContent = iframe.getElementsByClassName('frame-content')[0].firstChild;
  const clone = rootContent?.cloneNode(true) as Element;

  const xmlString = new XMLSerializer().serializeToString(clone);
  const { content, tagCollecion: collection } = extractTags(xmlString);

  // return { content, tagCollecion };

  return {
    content,
    tagCollecion: collection,

    addTag({
      annotation,
      entity,
      entityType,
    }: {
      annotation: Annotation;
      entity: { label: string; start: number; end: number; text: string };
      entityType: EntityType;
    }) {
      //? TAG THE DOCUMENT BASED ON NEW ANNOTATION USING TEXT OFFSET

      // console.group(entityType);
      const tagName = getTagNameForEntityType(
        entityType,
        store.get(documentMetadataAtom)?.schemaFamily,
      );

      const nearestPossibleTag = this.tagCollecion.filter((tag) => {
        if (tag.role !== 'open') false;
        return tag.offset <= entity.start && (tag.closeOffset ?? 0) >= entity.end;
      });

      const nearestPostionForOpenTagIndex = this.tagCollecion.findIndex(
        (tag) => nearestPossibleTag.at(-1)?.id === tag.id,
      );

      // console.log(nearestPossibleTag);

      const nearestPostionForOpenTag = this.tagCollecion.findIndex((tag, index) => {
        return index > nearestPostionForOpenTagIndex && tag.offset >= entity.start;
      });
      // console.log(nearestPostionForOpenTag);

      this.tagCollecion.splice(nearestPostionForOpenTag, 0, {
        id: `${annotation.id}-${entity.text}`,
        tag: `<${tagName}
                xml:id="${annotation.id}"
                class="nerve-entity"
                data-nerve-entity-type="${entityType}"
                data-nerve-status="draft"
                data-nerve-wa-uuid="${annotation.uuid}"
              >`,
        offset: entity.start,
        tagName,
        role: 'open',
      });

      //-----

      const nearestPostionForCloseTagIndex = this.tagCollecion.findIndex(
        (tag) => nearestPossibleTag.at(-1)?.id === tag.openTagId,
      );

      // const nearestPostionForCloseTag = tagCollecion.findIndex((tag) => tag.offset >= match.end);
      const nearestPostionForCloseTag = this.tagCollecion.findLastIndex((tag, index) => {
        return (
          index < nearestPostionForCloseTagIndex &&
          tag.offset <= entity.end && // &&
          // tag.offset <= (nearestPossibleTag.at(-1)?.closeOffset ?? 0)
          index <= nearestPostionForCloseTagIndex
        );
      });

      // console.log(nearestPostionForCloseTag);

      this.tagCollecion.splice(nearestPostionForCloseTag + 1, 0, {
        id: `${annotation.id}-${entity.text}-close`,
        tag: `</${tagName}>`,
        offset: entity.end,
        tagName,
        role: 'close',
      });

      // console.groupEnd();
    },
    reassembleDocument() {
      const reverseTagCollection = this.tagCollecion.toReversed();
      const newString = reverseTagCollection.reduce((str, ins) => {
        return str.slice(0, ins.offset) + ins.tag + str.slice(ins.offset);
      }, this.content);

      this.content = newString;
      store.set(documentStringAtom, newString);

      return newString;
    },
  };
};

// export const extractContentFromRenderedCopy = () => {
//   const iframe = getIframe()!;
//   // if (!iframe) return;

//   const rootContent = iframe.getElementsByClassName('frame-content')[0].firstChild;
//   const clone = rootContent?.cloneNode(true) as Element;

//   // console.log(clone);

//   const header = clone.querySelector('teiHeader');
//   // console.log(header);
//   if (header) store.set(documentHeaderAtom, header);

//   //* We should not send teiHeader to NER
//   clone.getElementsByTagName('teiHeader')[0]?.remove();

//   const xmlString = new XMLSerializer().serializeToString(clone);
//   const { content, tagCollecion: collection } = extractTags(xmlString);

//   // return { content, tagCollecion };

//   return {
//     content,
//     tagCollecion: collection,

//     addTag({
//       annotation,
//       match,
//       entityType,
//     }: {
//       annotation: Annotation;
//       match: { start: number; end: number; text: string };
//       entityType: EntityType;
//     }) {
//       //? TAG THE DOCUMENT BASED ON NEW ANNOTATION USING TEXT OFFSET

//       // console.group(entityType);

//       const nearestPossibleTag = this.tagCollecion.filter((tag) => {
//         if (tag.role !== 'open') false;
//         return tag.offset <= match.start && (tag.closeOffset ?? 0) >= match.end;
//       });

//       const nearestPostionForOpenTagIndex = this.tagCollecion.findIndex(
//         (tag) => nearestPossibleTag.at(-1)?.id === tag.id,
//       );

//       // console.log(nearestPossibleTag);

//       const nearestPostionForOpenTag = this.tagCollecion.findIndex((tag, index) => {
//         return index > nearestPostionForOpenTagIndex && tag.offset >= match.start;
//       });
//       // console.log(nearestPostionForOpenTag);

//       this.tagCollecion.splice(nearestPostionForOpenTag, 0, {
//         id: `${annotation.id}-${match.text}`,
//         tag: `<${entityType} id="${annotation.id}" data-nerve-status="draft" class="nerve-entity" data-nerve-wa-uuid="${annotation.uuid}">`,
//         offset: match.start,
//         tagName: entityType,
//         role: 'open',
//       });

//       //-----

//       const nearestPostionForCloseTagIndex = this.tagCollecion.findIndex(
//         (tag) => nearestPossibleTag.at(-1)?.id === tag.openTagId,
//       );

//       // const nearestPostionForCloseTag = tagCollecion.findIndex((tag) => tag.offset >= match.end);
//       const nearestPostionForCloseTag = this.tagCollecion.findLastIndex((tag, index) => {
//         return (
//           index < nearestPostionForCloseTagIndex &&
//           tag.offset <= match.end && // &&
//           // tag.offset <= (nearestPossibleTag.at(-1)?.closeOffset ?? 0)
//           index <= nearestPostionForCloseTagIndex
//         );
//       });

//       // console.log(nearestPostionForCloseTag);

//       this.tagCollecion.splice(nearestPostionForCloseTag + 1, 0, {
//         id: `${annotation.id}-${match.text}-close`,
//         tag: `</${entityType}>`,
//         offset: match.end,
//         tagName: entityType,
//         role: 'close',
//       });

//       // console.groupEnd();
//     },
//     reassembleDocument() {
//       const reverseTagCollection = this.tagCollecion.toReversed();
//       const newString = reverseTagCollection.reduce((str, ins) => {
//         return str.slice(0, ins.offset) + ins.tag + str.slice(ins.offset);
//       }, this.content);

//       this.content = newString;
//       store.set(documentStringAtom, newString);

//       return newString;
//     },
//   };
// };

/// --------

interface TagExtraction {
  id: string;
  tag: string;
  tagName: string;
  offset: number;
  closeOffset?: number;
  openTagId?: string;
  role: 'open' | 'close' | 'autoClose';
}

// const _extractTags = (content: string) => {
//   const tagCollecion: TagExtraction[] = [];

//   function replacer(match: string, offset: number) {
//     tagCollecion.push({ tag: match, offset });
//     return ``;
//   }

//   const newString = content.replaceAll(/<[^>]*>/g, replacer);

//   return {
//     content: newString,
//     tagCollecion,
//   };
// };

const extractTags = (content: string) => {
  const tagCollecion: TagExtraction[] = [];
  const hasMatch = true;
  let newString = content;

  const tagStack: TagExtraction[] = [];

  while (hasMatch) {
    // const match = newString.match(/<[^>]*>/);
    const match = newString.match(/<\/?([a-z][^>\s]*)[^>]*>/i);
    if (!match) break;

    const role = match[0].startsWith('</') ? 'close' : 'open';
    const isAutoCloseTag = match[0].endsWith('/>');

    const tag: TagExtraction = {
      id: nanoid(11),
      tag: match[0],
      tagName: match[1],
      offset: match.index!,
      role: isAutoCloseTag ? 'autoClose' : role,
      closeOffset: isAutoCloseTag ? match.index! : undefined,
    };

    if (tag.role === 'open') {
      tagStack.push(tag);
      // console.log(
      //   tagStack.length,
      //   '-'.repeat(tagStack.length * 2),
      //   tagStack.at(-1)?.tagName,
      //   tagStack.at(-1)?.offset,
      // );
    }

    const lastOnStack = tagStack.at(-1);
    if (tag.role === 'close' && lastOnStack?.tagName === match[1]) {
      const tagOnCollection = tagCollecion.find(({ id }) => id === lastOnStack.id);
      if (tagOnCollection) {
        tagOnCollection.closeOffset = match.index!;
        tag.openTagId = tagOnCollection.id;
      }
      tagStack.pop();
    }

    tagCollecion.push(tag);

    newString = newString.replace(/<\/?([a-z][^>\s]*)[^>]*>/i, '');
  }

  return {
    content: newString,
    tagCollecion,
  };
};

export const reassembleTags = (content: string, tagCollecion: TagExtraction[]) => {
  const reverseTagCollection = tagCollecion.toReversed();
  const newString = reverseTagCollection
    // .sort((a, b) => b.offset - a.offset) // Start from the end of the string
    .reduce((str, ins) => {
      return str.slice(0, ins.offset) + ins.tag + str.slice(ins.offset);
    }, content);

  return newString;
};
