'use client';

import { Sheet, Stack, type SheetProps } from '@mui/joy';
import chroma from 'chroma-js';
import { useState } from 'react';
// import { ManualIdentiyEntityMenu } from '../components';
import { NERButton } from '../components/ner-button';
import { NerDialog } from '../dialogs/ner';

export interface Props {
  slotProps?: {
    root?: Omit<SheetProps, 'children'>;
  };
}

export const Topbar = ({ slotProps }: Props) => {
  const [openDialog, setOpenDialog] = useState(false);
  return (
    <Sheet
      invertedColors
      sx={{
        px: 1,
        py: 0.75,
        borderRadius: 4,
        backgroundColor: ({ palette }) => `light-dark(
          ${chroma(palette.neutral[200]).alpha(0.5).css()},
          ${chroma(palette.neutral[700]).alpha(0.5).css()}
        )`,
      }}
      {...slotProps?.root}
    >
      <Stack direction="row" gap={1} justifyContent="space-between">
        <Stack direction="row" gap={1}>
          <NERButton onPointerDown={() => setOpenDialog(true)} />
        </Stack>
        {/* <ManualIdentiyEntityMenu /> */}
      </Stack>
      {openDialog && <NerDialog open={openDialog} onClose={() => setOpenDialog(false)} />}
    </Sheet>
  );
};
