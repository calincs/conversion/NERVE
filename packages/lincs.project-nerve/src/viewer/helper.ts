import * as utilities from './utilities';

export const getContent = () => {
  const iframeContent = utilities.getIframeContent();
  if (!iframeContent) return null;
  return iframeContent.body;
};

export const getTagByAttribute = (attribute: string, value: string | number) => {
  const iframeContent = utilities.getIframeContent();
  if (!iframeContent) return null;

  const escapedAttr = CSS.escape(attribute);
  const element = iframeContent.body.querySelector(`[${escapedAttr}="${value}"]`);
  if (!element) return;

  //FIXME - This two lines should be move to another method... one that explicitly deals with tag selection.
  removeAttributeFromAllTags('data-nerve-action');
  element.setAttribute('data-nerve-action', 'hover');

  return element;
};

export const setTagAttribute = (tagUuid: string, attribute: string, value: string | undefined) => {
  const tag = getTagByAttribute('data-nerve-wa-uuid', tagUuid);
  if (!tag) return;

  value ? tag.setAttribute(attribute, value) : tag.removeAttribute(attribute);
  return tag;
};

export const removeTagAttribute = (tagUuid: string, attribute: string) => {
  const tag = getTagByAttribute('data-nerve-wa-uuid', tagUuid);
  if (!tag) return;

  tag.removeAttribute(attribute);
  return tag;
};

export const removeAttributeFromAllTags = (attribute: string) => {
  const body = utilities.getIframeContent()?.body;
  if (!body) return null;

  const elements = body.querySelectorAll('.nerve-entity');
  if (!elements) return;

  elements.forEach((element) => element.removeAttribute(attribute));
};

export const scrollToElement = (id: string) => {
  const element = getTagByAttribute('xml:id', id);
  if (!element) return;

  // * scrollIntoView
  // scrollIntoView is a better alternative,
  // but Goolge Chome has a bug and cannot handle multiple `scrollIntoView`
  // from differnt element simultaeusly (both on the document and on the sidebar)
  // element.scrollIntoView({ behavior: 'smooth' })

  // Alternative
  const value = element.getBoundingClientRect().top;
  element.ownerDocument.scrollingElement?.scrollTo({ top: value, behavior: 'smooth' });
};
