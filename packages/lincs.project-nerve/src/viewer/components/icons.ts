import type { EntityType } from '../../types';

const entityIconDraft: Record<EntityType, string> = {
  person:
    "<path d='M12 5.9c1.16 0 2.1.94 2.1 2.1s-.94 2.1-2.1 2.1S9.9 9.16 9.9 8s.94-2.1 2.1-2.1m0 9c2.97 0 6.1 1.46 6.1 2.1v1.1H5.9V17c0-.64 3.13-2.1 6.1-2.1M12 4C9.79 4 8 5.79 8 8s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 9c-2.67 0-8 1.34-8 4v3h16v-3c0-2.66-5.33-4-8-4z' />",
  place:
    "<path d='M12 12c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2zm6-1.8C18 6.57 15.35 4 12 4s-6 2.57-6 6.2c0 2.34 1.95 5.44 6 9.14 4.05-3.7 6-6.8 6-9.14zM12 2c4.2 0 8 3.22 8 8.2 0 3.32-2.67 7.25-8 11.8-5.33-4.55-8-8.48-8-11.8C4 5.22 7.8 2 12 2z' />",
  organization:
    "<path d='M22.4,14.3c-.3.2-.8.2-1-.1-.9-1.2-2.2-1.9-3.7-1.8-.3,0-.5-.2-.7-.4,0-.2,0-.4,0-.6.1-.3.4-.4.7-.4,1.2,0,2.1-1,2.1-2.1s-1-2.1-2.1-2.1c-1,0-1.8.7-2.1,1.6,0,.4-.5.6-.9.5s-.6-.5-.5-.9c.5-1.9,2.4-3,4.3-2.6,1.9.5,3,2.4,2.6,4.3-.2.7-.5,1.3-1,1.7,1,.4,1.8,1.1,2.4,1.9.2.3.2.8-.1,1,0,0,0,0,0,0ZM17.6,19.8c.2.3.1.8-.2,1-.3.2-.8.1-1-.2,0,0,0,0,0,0-1.4-2.4-4.5-3.2-6.9-1.8-.7.4-1.4,1.1-1.8,1.8-.2.3-.6.5-1,.3-.3-.2-.5-.6-.3-1,0,0,0,0,0,0,.7-1.2,1.7-2.1,3-2.7-1.9-1.4-2.2-4.1-.8-6,1.4-1.9,4.1-2.2,6-.8s2.2,4.1.8,6c-.2.3-.5.6-.8.8,1.3.5,2.3,1.5,3,2.7ZM12,16.6c1.6,0,2.8-1.3,2.8-2.8s-1.3-2.8-2.8-2.8-2.8,1.3-2.8,2.8,1.3,2.8,2.8,2.8ZM7,11.6c0-.4-.3-.7-.7-.7-1.2,0-2.1-1-2.1-2.1s1-2.1,2.1-2.1c1,0,1.8.7,2.1,1.6,0,.4.5.6.9.5s.6-.5.5-.9h0c-.5-1.9-2.4-3-4.3-2.6-1.9.5-3,2.4-2.6,4.3.2.7.5,1.3,1,1.7-1,.4-1.8,1.1-2.4,1.9-.2.3-.2.8.1,1,.3.2.8.2,1-.1.9-1.2,2.2-1.9,3.7-1.8.4,0,.7-.3.7-.7Z' />",
  work: "<g><path d='M5.81,0C3.25,0,1.18,2.08,1.18,4.64v15.46c0,2.56,2.08,3.9,4.64,3.9h15.46c.86,0,1.55-.29,1.55-1.15s-.69-1.2-1.55-1.2v-3.09c.86,0,1.55-.69,1.55-1.55V1.55c0-.86-.69-1.55-1.55-1.55H5.81Z'/><path d='M5.93,18.56h13.31v3.09H5.93c-.92,0-1.66-.69-1.66-1.55s.74-1.55,1.66-1.55Z' fill='#fff'/><path d='M8.62,1.6h11.33c.75,0,1.36,.61,1.36,1.36V15.62c0,.75-.61,1.36-1.36,1.36H8.62V1.6h0Z' fill='#fff'/><path d='M7.14,16.95c-.83,0-4.34-.21-4.3,2.13V5.6c0-2.2,1.92-3.98,4.3-3.98h0v15.32Z' fill='#fff'/><path d='M10.12,6.96c0-.43,.3-.77,.67-.77h8.07c.37,0,.67,.35,.67,.77s-.3,.77-.67,.77H10.79c-.37,0-.67-.35-.67-.77Z'/><path d='M10.79,9.28h8.07c.37,0,.67,.35,.67,.77s-.3,.77-.67,.77H10.79c-.37,0-.67-.35-.67-.77s.3-.77,.67-.77Z'/></g>",
  physicalThing:
    "<g><path d='M2.24,4.21c.11-.23,.36-.36,.61-.33l9.15,1.14,9.15-1.14c.25-.03,.5,.1,.61,.33l1.56,3.12c.34,.67-.02,1.48-.74,1.69l-6.11,1.75c-.52,.15-1.08-.07-1.35-.53l-3.12-5.19-3.12,5.19c-.28,.46-.83,.68-1.35,.53l-6.1-1.75c-.72-.21-1.08-1.02-.74-1.69l1.55-3.12Z' strokeLinejoin='round' /><path d='M20.78,10.76v5.48c0,.77-.51,1.45-1.25,1.64l-7,1.8c-.35,.09-.72,.09-1.06,0l-7-1.8c-.73-.19-1.25-.87-1.25-1.64v-5.48l-.78-.22v6.24c0,.82,.56,1.54,1.36,1.75l7.63,1.91c.38,.1,.78,.1,1.16,0l7.63-1.91c.8-.2,1.36-.92,1.36-1.74v-6.24l-.78,.22Z'/><line x1='12.07' y1='19.6' x2='12.07' y2='9.54' strokeLinecap='round' strokeLinejoin='round'/></g>",
  conceptualObject:
    "<g><path stroke='none' d='M0 0h24v24H0z' fill='none'></path><path d='M6 16m-3 0a3 3 0 1 0 6 0a3 3 0 1 0 -6 0'></path><path d='M16 19m-2 0a2 2 0 1 0 4 0a2 2 0 1 0 -4 0'></path><path d='M14.5 7.5m-4.5 0a4.5 4.5 0 1 0 9 0a4.5 4.5 0 1 0 -9 0'></path></g>",
  event: "<path d='M6 17h3l2-4V7H5v6h3zm8 0h3l2-4V7h-6v6h3z' />",
  citation: "<path d='M6 17h3l2-4V7H5v6h3zm8 0h3l2-4V7h-6v6h3z' />",
};

const entityIconApproved: Record<EntityType, string> = {
  person:
    "<path d='M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z' />",
  place:
    "<path d='M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z' />",
  organization:
    "<path d='M64.12,147.8a4,4,0,0,1-4,4.2H16a8,8,0,0,1-7.8-6.17,8.35,8.35,0,0,1,1.62-6.93A67.79,67.79,0,0,1,37,117.51a40,40,0,1,1,66.46-35.8,3.94,3.94,0,0,1-2.27,4.18A64.08,64.08,0,0,0,64,144C64,145.28,64,146.54,64.12,147.8Zm182-8.91A67.76,67.76,0,0,0,219,117.51a40,40,0,1,0-66.46-35.8,3.94,3.94,0,0,0,2.27,4.18A64.08,64.08,0,0,1,192,144c0,1.28,0,2.54-.12,3.8a4,4,0,0,0,4,4.2H240a8,8,0,0,0,7.8-6.17A8.33,8.33,0,0,0,246.17,138.89Zm-89,43.18a48,48,0,1,0-58.37,0A72.13,72.13,0,0,0,65.07,212,8,8,0,0,0,72,224H184a8,8,0,0,0,6.93-12A72.15,72.15,0,0,0,157.19,182.07Z' />",
  work: "<path d='M6,.5C3.52,.5,1.5,2.52,1.5,5v15c0,2.48,2.02,4.5,4.5,4.5h15c.83,0,1.5-.67,1.5-1.5s-.67-1.5-1.5-1.5v-3c.83,0,1.5-.67,1.5-1.5V2c0-.83-.67-1.5-1.5-1.5H6Zm0,18h12v3H6c-.83,0-1.5-.67-1.5-1.5s.67-1.5,1.5-1.5Zm1.5-11.25c0-.41,.34-.75,.75-.75h9c.41,0,.75,.34,.75,.75s-.34,.75-.75,.75H8.25c-.41,0-.75-.34-.75-.75Zm.75,2.25h9c.41,0,.75,.34,.75,.75s-.34,.75-.75,.75H8.25c-.41,0-.75-.34-.75-.75s.34-.75,.75-.75Z' />",
  physicalThing:
    "<path d='M1.76,3.63c.12-.24,.38-.38,.64-.34l9.59,1.2,9.59-1.2c.26-.03,.52,.11,.64,.34l1.63,3.27c.35,.7-.02,1.55-.78,1.77l-6.4,1.83c-.54,.16-1.13-.07-1.42-.56l-3.27-5.45-3.27,5.45c-.29,.49-.87,.72-1.42,.56L.91,8.66c-.76-.22-1.13-1.07-.78-1.77L1.76,3.63Zm10.28,3.37l2.15,3.58c.58,.97,1.75,1.43,2.84,1.12l5-1.43v6.55c0,.86-.59,1.61-1.43,1.83l-8,2c-.4,.1-.82,.1-1.22,0l-8-2c-.84-.22-1.43-.97-1.43-1.83v-6.55l5,1.43c1.09,.31,2.26-.15,2.84-1.12l2.15-3.58h.09Z' />",
  conceptualObject:
    "<path d='M1.76,3.63c.12-.24,.38-.38,.64-.34l9.59,1.2,9.59-1.2c.26-.03,.52,.11,.64,.34l1.63,3.27c.35,.7-.02,1.55-.78,1.77l-6.4,1.83c-.54,.16-1.13-.07-1.42-.56l-3.27-5.45-3.27,5.45c-.29,.49-.87,.72-1.42,.56L.91,8.66c-.76-.22-1.13-1.07-.78-1.77L1.76,3.63Zm10.28,3.37l2.15,3.58c.58,.97,1.75,1.43,2.84,1.12l5-1.43v6.55c0,.86-.59,1.61-1.43,1.83l-8,2c-.4,.1-.82,.1-1.22,0l-8-2c-.84-.22-1.43-.97-1.43-1.83v-6.55l5,1.43c1.09,.31,2.26-.15,2.84-1.12l2.15-3.58h.09Z' />",
  event: "<path d='M6 17h3l2-4V7H5v6h3zm8 0h3l2-4V7h-6v6h3z' />",
  citation: "<path d='M6 17h3l2-4V7H5v6h3zm8 0h3l2-4V7h-6v6h3z' />",
};

export const entityIcon = {
  draft: entityIconDraft,
  approved: entityIconApproved,
};
