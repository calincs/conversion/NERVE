import type { EntityType } from '../../../types';
import { entityIcon } from '../icons';

interface Props {
  isDraft: boolean;
  type: EntityType;
  label: string;
}

const SvgIconComponent = (icon: string, className = '') => `
<svg focusable="false" aria-hidden="true" viewBox="0 0 24 24" class="${className}">
  ${icon}
</svg>`;

export const PopUpContainerComponent = ({ isDraft, type, label }: Props) => {
  const icon = SvgIconComponent(entityIcon[isDraft ? 'draft' : 'approved'][type], type);

  const content = `
  <div class="popup-entity" type="${type}" data-nerve-status="${isDraft ? 'draft' : 'approved'}">
    <div class="icon">${icon}</div>
    <span>${label}</span>
    ${isDraft ? '<div class="decorator">?</div>' : ''}
  </div>`;

  return content;
};
