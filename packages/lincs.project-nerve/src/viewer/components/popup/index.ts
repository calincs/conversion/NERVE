import tippy, { Instance as TippyInstance, Props as TippyProps } from 'tippy.js';
import { Annotation } from '../../../annotation';
import { PopUpContainerComponent } from './template';

const createTippy = (element: Element, content: string) => {
  return tippy(element, {
    allowHTML: true,
    appendTo: element.parentElement!.parentElement!,
    animation: 'fadeInOut',
    content,
    delay: [1250, null],
    interactive: true,
    // trigger: 'click',
  });
};

export const createPopup = (element: Element, annotation: Annotation) => {
  const _element = element as Element & { _tippy?: TippyInstance<TippyProps> };

  const body =
    annotation.isDraft && annotation.bodyCandidates
      ? annotation.bodyCandidates[0]
      : Array.isArray(annotation.body)
        ? annotation.body[0]
        : annotation.body;

  const template = PopUpContainerComponent({
    isDraft: annotation.isDraft,
    type: annotation.entityType,
    label: body.label,
  });

  const popup = _element._tippy ?? createTippy(_element, template);
  popup.setContent(template);
};
