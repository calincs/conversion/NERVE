import { useAtomValue } from 'jotai';
import { useCallback, useEffect, useMemo, useState } from 'react';
import type { Annotation } from '../../../annotation';
import {
  annotationsAtom,
  filterViewAtom,
  sortAnnotationsAtom,
  stackedAnnotationsAtom,
} from '../../../jotai/store';

const sortByQuoteAlphabetiacally = (annotations: Annotation[]) => {
  return annotations.toSorted((a, b) => {
    if (a.quote.toUpperCase() < b.quote.toUpperCase()) return -1;
    if (a.quote.toUpperCase() > b.quote.toUpperCase()) return 1;
    return 0;
  });
};

const sortLinear = (annotations: Annotation[]) => {
  return annotations.toSorted((a, b) => {
    if ((a.getTargetOffsetPosition()?.start ?? 0) < (b.getTargetOffsetPosition()?.start ?? 0)) {
      return -1;
    }
    if ((a.getTargetOffsetPosition()?.start ?? 0) > (b.getTargetOffsetPosition()?.start ?? 0)) {
      return 1;
    }
    return 0;
  });
};

export const useCollection = () => {
  const annotations = useAtomValue(annotationsAtom);
  const filterView = useAtomValue(filterViewAtom);
  const sortView = useAtomValue(sortAnnotationsAtom);
  const stackedView = useAtomValue(stackedAnnotationsAtom);

  const [annotationsList, setAnnotationList] = useState(annotations);

  const filter = useCallback(
    (annotations: Annotation[]) => {
      const filterSet = new Set(filterView);
      if (filterSet.size === 0) return annotations;

      const filterList = annotations.filter((annotation) => {
        let match = false;
        if (filterSet.has('draft') && annotation.isDraft) {
          match = true;
        }

        if (filterSet.has('approved') && !annotation.isDraft) {
          match = true;
        }
        if (filterSet.has(annotation.entityType)) {
          match = true;
        }

        if (match) return annotation;
      });

      return filterList;
    },
    [filterView],
  );

  const stackedAnnotations = useMemo(() => {
    if (!stackedView) return;

    let filteredList = filter([...annotations.values()]);

    filteredList =
      sortView !== 'linear' ? sortByQuoteAlphabetiacally(filteredList) : sortLinear(filteredList);

    const uniqueEntities = filteredList.reduce((stack, item) => {
      const key = item.isDraft
        ? `${item.quote}-${item.entityType}-${item.isDraft}`
        : `${item.bodyLabel}-${item.entityType}-${item.isDraft}`;

      stack.has(key) ? stack.set(key, [...stack.get(key)!, item]) : stack.set(key, [item]);

      return stack;
    }, new Map<string, Annotation[]>());

    return uniqueEntities;
  }, [stackedView, filter, annotations, sortView]);

  useEffect(() => {
    let filteredList = filter([...annotations.values()]);

    filteredList =
      sortView !== 'linear' ? sortByQuoteAlphabetiacally(filteredList) : sortLinear(filteredList);

    const listMap = new Map<string, Annotation>();
    filteredList.forEach((anno) => listMap.set(anno.uuid, anno));

    setAnnotationList(listMap);
  }, [filterView, sortView, annotations, filter]);

  return {
    annotationsList,
    stackedAnnotations,
  };
};
