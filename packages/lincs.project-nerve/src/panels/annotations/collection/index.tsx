import { Box, Stack, type BoxProps, type StackProps } from '@mui/joy';
import { AnimatePresence } from 'framer-motion';
import { useAtomValue } from 'jotai';
import { selectedAnnotationUuidAtom, stackedAnnotationsAtom } from '../../../jotai/store';
import { AnnotationCard } from '../annotation-card';
import { NoAnnotationsCard } from '../no-annotation-card';
import { cardMaxHeightAtom } from '../store';
import { useCollection } from './useCollection';

export interface Props {
  slotProps?: {
    root?: Omit<BoxProps, 'children'>;
    stack?: Omit<StackProps, 'children'>;
  };
}

export const Collection = ({ slotProps }: Props) => {
  const maxCardHeight = useAtomValue(cardMaxHeightAtom);
  const selectedAnnotationUuid = useAtomValue(selectedAnnotationUuidAtom);
  const stackedView = useAtomValue(stackedAnnotationsAtom);

  const { annotationsList, stackedAnnotations } = useCollection();

  return (
    <Box height={maxCardHeight} overflow="auto" {...slotProps?.root}>
      <Stack alignItems="center" gap={0.25} py={1} px={0.75} {...slotProps?.stack}>
        {annotationsList.size === 0 && <NoAnnotationsCard />}
        <AnimatePresence>
          {stackedView && stackedAnnotations
            ? [...stackedAnnotations.entries()].map(([key, items]) => {
                const annoIndex = items.findIndex((item) => item.uuid === selectedAnnotationUuid);
                const annotationSelected = items[annoIndex] ?? items[0];
                if (!annotationSelected) return null;
                return (
                  <AnnotationCard key={key} annotation={annotationSelected} collection={items} />
                );
              })
            : [...annotationsList.values()].map((item) => (
                <AnnotationCard key={item.uuid} annotation={item} />
              ))}
        </AnimatePresence>
      </Stack>
    </Box>
  );
};
