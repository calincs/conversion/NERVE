import { Sheet, Stack, type SheetProps } from '@mui/joy';
import chroma from 'chroma-js';
import { useSetAtom } from 'jotai';
import { useEffect, useRef } from 'react';
import { optionsComponentHeightAtom } from '../store';
import { Filter } from './filter';
import { Sort } from './sort';
import { Stacked } from './stacked';

export interface Props {
  slotProps?: {
    root?: Omit<SheetProps, 'children' | 'ref'>;
  };
}

export const OptionsBar = ({ slotProps }: Props) => {
  const setOptionComponentHeight = useSetAtom(optionsComponentHeightAtom);

  const ref = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    if (ref.current) setOptionComponentHeight(ref.current.getBoundingClientRect().height);
    return () => setOptionComponentHeight(0);
  }, []);

  return (
    <Sheet
      ref={ref}
      sx={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        px: 1,
        py: 0.75,
        borderRadius: 4,
        backgroundColor: ({ palette }) => `light-dark(
          ${chroma(palette.neutral[200]).alpha(0.5).css()},
          ${chroma(palette.neutral[700]).alpha(0.5).css()}
        )`,
      }}
      {...slotProps?.root}
    >
      <Stack direction="row" gap={1}>
        <Stacked />
        <Sort />
      </Stack>
      <Stack direction="row" gap={1}>
        <Filter />
      </Stack>
    </Sheet>
  );
};
