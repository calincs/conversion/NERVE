import {
  Dropdown,
  ListItemContent,
  ListItemDecorator,
  Menu,
  MenuButton,
  MenuItem,
  type MenuButtonProps,
  type MenuProps,
} from '@mui/joy';
import { useAtom } from 'jotai';
import { useTranslation } from 'react-i18next';
import { IconType } from 'react-icons';
import { LiaSortAlphaDownSolid } from 'react-icons/lia';
import { MdLinearScale, MdSort } from 'react-icons/md';
import { sortAnnotationsAtom } from '../../../jotai/store';
import { SortView } from '../../../types';

interface Option {
  Icon: IconType;
  label: string;
  value: SortView;
}

interface SortrProps {
  slotProps?: {
    menuButtonProps?: MenuButtonProps;
    menuProps?: MenuProps;
  };
}

export const Sort = ({ slotProps }: SortrProps) => {
  const { t } = useTranslation();

  const [sortView, setSortView] = useAtom(sortAnnotationsAtom);

  const options: Option[] = [
    { Icon: MdLinearScale, label: t('nerve.common.linear'), value: 'linear' },
    {
      Icon: LiaSortAlphaDownSolid,
      label: t('nerve.common.Alphabetically'),
      value: 'alphabetically',
    },
  ];

  return (
    <Dropdown>
      <MenuButton
        size="sm"
        startDecorator={<MdSort />}
        sx={{
          textTransform: 'capitalize',
          ':hover': { backgroundColor: 'background.level3' },
        }}
        variant="plain"
        {...slotProps?.menuButtonProps}
      >
        {options.find((option) => option.value === sortView)?.label}
      </MenuButton>
      <Menu
        placement="bottom-end"
        size="sm"
        sx={{ gap: 0.5 }}
        variant="soft"
        {...slotProps?.menuProps}
      >
        {options.map(({ Icon, label, value }) => (
          <MenuItem
            key={label}
            onPointerDown={() => setSortView(value)}
            selected={value === sortView}
            sx={{ mx: 0.5, borderRadius: 4 }}
          >
            <ListItemDecorator>
              <Icon />
            </ListItemDecorator>
            <ListItemContent>{label}</ListItemContent>
          </MenuItem>
        ))}
      </Menu>
    </Dropdown>
  );
};
