import {
  ListItemContent,
  ListItemDecorator,
  MenuItem,
  type ListItemContentProps,
  type ListItemDecoratorProps,
  type MenuItemProps,
} from '@mui/joy';
import { useTranslation } from 'react-i18next';
import type { IconBaseProps } from 'react-icons';
import { CgBlock } from 'react-icons/cg';

export interface Props extends Omit<MenuItemProps, 'children'> {
  slotPtops?: {
    decorator?: ListItemDecoratorProps;
    icon?: IconBaseProps;
    content?: Omit<ListItemContentProps, 'children'>;
  };
}

export const ClearButton = ({ slotPtops, ...props }: Props) => {
  const { t } = useTranslation();

  return (
    <MenuItem sx={{ borderRadius: 4 }} {...props}>
      <ListItemDecorator sx={{ color: 'danger.plainColor' }} {...slotPtops?.decorator}>
        <CgBlock {...slotPtops?.icon} />
      </ListItemDecorator>
      <ListItemContent sx={{ textTransform: 'capitalize' }} {...slotPtops?.content}>
        {t('nerve.common.clear')}
      </ListItemContent>
    </MenuItem>
  );
};
