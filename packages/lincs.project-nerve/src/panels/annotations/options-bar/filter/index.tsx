import {
  Dropdown,
  IconButton,
  List,
  ListItem,
  ListSubheader,
  Menu,
  MenuButton,
  type ListItemProps,
  type ListSubheaderProps,
  type MenuButtonProps,
  type MenuProps,
} from '@mui/joy';
import { useAtom } from 'jotai';
import { RESET } from 'jotai/utils';
import { useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { BsFilter } from 'react-icons/bs';
import type { AnnotationStatus } from '../../../../annotation';
import { filterViewAtom } from '../../../../jotai/store';
import type { EntityType } from '../../../../types';
import { ClearButton } from './clear-button';
import { Item } from './item';

interface Category {
  label: string;
  options: Option[];
}

interface Option {
  label: string;
  type: 'entity' | 'status';
  value: EntityType | AnnotationStatus;
}

interface FilterProps {
  slotProps?: {
    menuButtonProps?: MenuButtonProps;
    menuProps?: MenuProps;
    listItem?: Omit<ListItemProps, 'children' | 'nested'>;
    listSubheader?: Omit<ListSubheaderProps, 'children'>;
  };
}

export const Filter = ({ slotProps }: FilterProps) => {
  const { t } = useTranslation();

  const [filterView, setFilterView] = useAtom(filterViewAtom);

  const optionsEntities: Option[] = [
    { label: t('nerve.entity.person'), value: 'person', type: 'entity' },
    { label: t('nerve.entity.place'), value: 'place', type: 'entity' },
    { label: t('nerve.entity.organization'), value: 'organization', type: 'entity' },
    { label: t('nerve.entity.work'), value: 'work', type: 'entity' },
    { label: t('nerve.entity.physicalThing'), value: 'physicalThing', type: 'entity' },
    { label: t('nerve.entity.conceptualObject'), value: 'conceptualObject', type: 'entity' },
    { label: t('nerve.entity.event'), value: 'event', type: 'entity' },
    { label: t('nerve.entity.citation'), value: 'citation', type: 'entity' },
  ];

  const optionsStatus: Option[] = [
    { label: t('nerve.common.draft'), value: 'draft', type: 'status' },
    { label: t('nerve.common.approved'), value: 'approved', type: 'status' },
  ];

  const categories: Category[] = [
    { label: t('nerve.common.by entity type'), options: optionsEntities },
    { label: t('nerve.common.by status'), options: optionsStatus },
  ];

  const [open, setOpen] = useState(false);

  const handleOpenChange = useCallback((_event: React.SyntheticEvent | null, isOpen: boolean) => {
    setOpen(isOpen);
  }, []);

  const handleSelect = (value: EntityType | AnnotationStatus) => {
    const filterSet = new Set(filterView);

    //make status exclsuive selection
    if (value === 'draft' && filterSet.has('approved')) filterSet.delete('approved');
    if (value === 'approved' && filterSet.has('draft')) filterSet.delete('draft');

    //Add selection
    filterSet.has(value) ? filterSet.delete(value) : filterSet.add(value);

    setFilterView([...filterSet]);

    setOpen(false);
  };

  const handleReset = () => {
    setFilterView(RESET);
    setOpen(false);
  };

  return (
    <Dropdown open={open} onOpenChange={handleOpenChange}>
      <MenuButton
        slots={{ root: IconButton }}
        slotProps={{
          root: {
            'aria-label': 'filter annotations',
            'aria-pressed': filterView.length > 0 ? 'true' : 'false',
            color: 'neutral',
            size: 'sm',
          },
        }}
        size="sm"
        sx={{
          ':hover': { backgroundColor: 'background.level3' },
        }}
        variant={filterView.length > 0 ? 'soft' : 'plain'}
        {...slotProps?.menuButtonProps}
      >
        <BsFilter />
      </MenuButton>

      <Menu
        placement="bottom-end"
        size="sm"
        sx={{ gap: 0.5 }}
        variant="soft"
        {...slotProps?.menuProps}
      >
        {filterView.length > 0 && <ClearButton onPointerDown={handleReset} />}
        {categories.map(({ label, options }) => (
          <ListItem key={label} nested sx={{ gap: 0.5, mx: 0.5 }} {...slotProps?.listItem}>
            <ListSubheader {...slotProps?.listSubheader}>{label}</ListSubheader>
            <List size="sm" sx={{ gap: 0.25 }}>
              {options.map(({ label, type, value }) => (
                <Item
                  key={value}
                  onPointerDown={() => handleSelect(value)}
                  selected={filterView.includes(value)}
                  type={type}
                  value={value}
                >
                  {label}
                </Item>
              ))}
            </List>
          </ListItem>
        ))}
      </Menu>
    </Dropdown>
  );
};
