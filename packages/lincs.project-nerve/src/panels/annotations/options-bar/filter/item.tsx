import {
  ListItemContent,
  ListItemDecorator,
  MenuItem,
  useTheme,
  type ListItemDecoratorProps,
  type MenuItemProps,
} from '@mui/joy';
import { PointerEventHandler, PropsWithChildren, useState } from 'react';
import type { AnnotationStatus } from '../../../../annotation';
import { Icon, IconProps } from '../../../../icons';
import { EntityTypes, type EntityType } from '../../../../types';

interface Props extends PropsWithChildren {
  onPointerDown?: PointerEventHandler;
  onPointerEnter?: PointerEventHandler;
  onPointerLeave?: PointerEventHandler;
  type: 'entity' | 'status';
  selected: MenuItemProps['selected'];
  value: EntityType | AnnotationStatus;
  slotProps?: {
    root?: Omit<MenuItemProps, 'children'>;
    decorator?: Omit<ListItemDecoratorProps, 'children'>;
    icon?: Omit<IconProps, 'name'>;
  };
}

export const Item = ({
  children,
  onPointerDown,
  onPointerEnter,
  onPointerLeave,
  selected,
  value,
  slotProps,
}: Props) => {
  const theme = useTheme();

  const [hover, setHover] = useState(false);

  return (
    <MenuItem
      onPointerDown={onPointerDown}
      onPointerEnter={(event) => {
        setHover(true);
        onPointerEnter?.(event);
      }}
      onPointerLeave={(event) => {
        setHover(false);
        onPointerLeave?.(event);
      }}
      selected={selected}
      sx={{ borderRadius: 4 }}
      {...slotProps}
    >
      <ListItemDecorator {...slotProps?.decorator}>
        <Icon
          name={value}
          style={{
            color:
              hover && EntityTypes.includes(value as EntityType)
                ? theme.vars.palette[value as EntityType][500]
                : 'inherit',
          }}
        />
      </ListItemDecorator>
      <ListItemContent sx={{ textTransform: 'capitalize' }}>{children}</ListItemContent>
    </MenuItem>
  );
};
