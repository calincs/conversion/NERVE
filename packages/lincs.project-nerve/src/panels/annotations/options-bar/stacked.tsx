import { IconButton, type IconButtonProps } from '@mui/joy';
import { useAtom } from 'jotai';
import { IoLayers } from 'react-icons/io5';
import { stackedAnnotationsAtom } from '../../../jotai/store';

export const Stacked = ({ onPointerDown, ...props }: IconButtonProps) => {
  const [stackedView, setStackedView] = useAtom(stackedAnnotationsAtom);

  return (
    <IconButton
      aria-label="stacked annotations"
      aria-pressed={stackedView ? 'true' : 'false'}
      color="neutral"
      onPointerDown={(event) => {
        setStackedView(!stackedView);
        onPointerDown?.(event);
      }}
      size="sm"
      sx={{
        minWidth: 36,
        ':hover': { backgroundColor: 'background.level3' },
      }}
      variant={stackedView ? 'soft' : 'plain'}
      {...props}
    >
      <IoLayers />
    </IconButton>
  );
};
