import { atom } from 'jotai';

export const panelHeightAtom = atom(0);
panelHeightAtom.debugLabel = 'panel.annotation.height.Atom';

export const optionsComponentHeightAtom = atom(0);
optionsComponentHeightAtom.debugLabel = 'panel.annotation.optionsComponent.height.Atom';

export const footerHeightAtom = atom(0);
footerHeightAtom.debugLabel = 'panel.annotation.footer.height.Atom';

export const cardMaxHeightAtom = atom((get) => {
  return get(panelHeightAtom) - get(optionsComponentHeightAtom) - get(footerHeightAtom);
});
cardMaxHeightAtom.debugLabel = 'panel.card.max-height.Atom';
