'use client';

import { Box, type BoxProps } from '@mui/joy';
import { useAtomValue, useSetAtom } from 'jotai';
import { useLayoutEffect, useRef } from 'react';
import { Collection } from './collection';
import { OptionsBar } from './options-bar';
import { panelHeightAtom } from './store';
import { Footer } from './footer';
import { annotationsAtom } from '../../jotai/store';

export interface Props {
  slotProps?: {
    root?: Omit<BoxProps, 'children'>;
  };
}

export const AnnotationsPanel = ({ slotProps }: Props) => {
  const setPanelHeight = useSetAtom(panelHeightAtom);
  const annotations = useAtomValue(annotationsAtom);

  const ref = useRef<HTMLDivElement | null>(null);

  useLayoutEffect(() => {
    if (!ref.current) return;

    const container = ref.current;
    setPanelHeight(container.offsetHeight);

    const observer = new ResizeObserver(() => {
      if (!ref.current) return;
      const containerHeight = ref.current.offsetHeight;
      setPanelHeight(containerHeight);
    });

    observer.observe(container);

    return () => {
      observer.unobserve(container);
      observer.disconnect();
    };
  }, [setPanelHeight]);

  return (
    <Box
      ref={ref}
      height="100%"
      position="relative"
      px={1}
      py={0.5}
      borderRadius={1}
      {...slotProps?.root}
    >
      <OptionsBar />
      <Collection />
      {annotations.size > 0 && <Footer />}
    </Box>
  );
};
