import { Sheet, Stack, Typography, useColorScheme, type CardProps } from '@mui/joy';
import { Trans, useTranslation } from 'react-i18next';
import { HiOutlineArrowSmRight } from 'react-icons/hi';
import { NerveIcon } from '../../icons/svg/nerve-icon';

export interface Props {
  slotProps?: {
    root?: Omit<CardProps, 'children'>;
  };
}

export const NoAnnotationsCard = ({ slotProps }: Props) => {
  const { mode, systemMode } = useColorScheme();
  const { t } = useTranslation();
  const isDark = mode === 'dark' || (mode === 'system' && systemMode === 'dark');

  return (
    <Sheet
      color="neutral"
      sx={{ width: '100%', p: 2, backgroundColor: 'transparent' }}
      {...slotProps?.root}
    >
      <Typography level="title-md" mb={2}>
        {t('nerve.message.There are currently no entity annotations in this document')}
      </Typography>
      <Stack gap={1}>
        <Stack direction="row" alignItems="stretch" gap={2}>
          <HiOutlineArrowSmRight style={{ minWidth: 16, marginTop: 4 }} />
          <Typography level="body-sm">
            <Trans i18nKey="nerve.message.Click on the button above o run named entity recognition">
              Click on the button NER{' '}
              <NerveIcon
                style={{
                  width: 20,
                  height: 20,
                  marginBottom: -5,
                  marginLeft: 2,
                  marginRight: -4,
                  fill: isDark ? 'white' : 'black',
                  rotate: '-90deg',
                }}
              />{' '}
              to run named entity recognition.
            </Trans>
          </Typography>
        </Stack>
        {/* TODO: Uncomment this when manual entity annotation is implemented */}
        {/* <Stack direction="row" alignItems="stretch" gap={2}>
          <HiOutlineArrowSmRight style={{ minWidth: 16, marginTop: 2 }} />
          <Typography level="body-sm">
            {t('nerve.message.or select some text and click the button')}
          </Typography>
        </Stack> */}
      </Stack>
    </Sheet>
  );
};
