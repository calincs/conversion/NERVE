import { Sheet, Typography } from '@mui/joy';
import { useAtomValue, useSetAtom } from 'jotai';
import { useEffect, useRef } from 'react';
import { annotationsAtom } from '../../../jotai/store';
import { useCollection } from '../collection/useCollection';
import { footerHeightAtom } from '../store';
import { useTranslation } from 'react-i18next';

export const Footer = () => {
  const { t } = useTranslation();
  const { annotationsList, stackedAnnotations } = useCollection();
  const annotations = useAtomValue(annotationsAtom);
  const setFooterHeight = useSetAtom(footerHeightAtom);

  const ref = useRef<HTMLDivElement | null>(null);

  const total = annotationsList.size !== annotations.size;

  useEffect(() => {
    if (ref.current) setFooterHeight(ref.current.getBoundingClientRect().height);
    return () => setFooterHeight(0);
  }, []);

  return (
    <Sheet
      ref={ref}
      invertedColors
      variant="soft"
      sx={{
        display: 'flex',
        width: '100%',
        alignItems: 'center',
        mt: -1.25,
        py: 0.5,
        px: 2,
        borderRadius: 4,
      }}
    >
      <Typography level="body-xs" textTransform="capitalize">
        {t('nerve.common.annotation', { count: annotationsList.size })}: {annotationsList.size}{' '}
        {total && `(${annotations.size})`}
      </Typography>
    </Sheet>
  );
};
