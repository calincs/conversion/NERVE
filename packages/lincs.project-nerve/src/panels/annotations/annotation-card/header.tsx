import {
  Chip,
  CircularProgress,
  IconButton,
  Sheet,
  Stack,
  Typography,
  useColorScheme,
  useTheme,
  type SheetProps,
} from '@mui/joy';
import chroma from 'chroma-js';
import { motion, type Variants } from 'framer-motion';
import { useSetAtom } from 'jotai';
import { PointerEvent, SyntheticEvent, useEffect, useMemo, useRef, useState } from 'react';
import { TbPencil } from 'react-icons/tb';
import { Annotation } from '../../../annotation';
import { Icon } from '../../../icons';
import { annotationsAtom } from '../../../jotai/store';
import { DraftMarker, Menu } from './components';
import { useAnnotation } from './hooks';
import { headerHeightAtom } from './store';

interface HeaderProps extends SheetProps {
  annotation: Annotation;
  isProcessing?: boolean;
  opened?: boolean;
  stacked?: number;
}

export const Header = ({
  annotation,
  isProcessing = false,
  onPointerDown,
  onPointerOver,
  onPointerOut,
  opened = false,
  stacked,
  ...props
}: HeaderProps) => {
  const { palette, vars } = useTheme();
  const { mode, systemMode } = useColorScheme();
  const setHeaderHeightAtom = useSetAtom(headerHeightAtom);
  const setAnnotations = useSetAtom(annotationsAtom);
  const { loadCandidates } = useAnnotation();
  const [hover, setHover] = useState(false);
  const [persistHover, setPersistHover] = useState(false);

  const ref = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    if (!ref.current) return;

    const container = ref.current;
    setHeaderHeightAtom(container.offsetHeight);

    const observer = new ResizeObserver(() => {
      if (!ref.current) return;
      const containerHeight = ref.current.offsetHeight;
      setHeaderHeightAtom(containerHeight);
    });

    observer.observe(container);

    return () => {
      observer.unobserve(container);
      observer.disconnect();
    };
  }, [ref, setHeaderHeightAtom]);

  const handlePointerOver = (event: PointerEvent<HTMLDivElement>) => {
    setHover(true);
    onPointerOver?.(event);
  };

  const handlePointerOut = (event: PointerEvent<HTMLDivElement>) => {
    setHover(false);
    onPointerOut?.(event);
  };

  const handlePersisHover = (value: boolean) => setPersistHover(value);

  const handleUnlockButton = (event: PointerEvent<HTMLAnchorElement>) => {
    event.stopPropagation();
    event.preventDefault();

    annotation.setLocked(false);
    setAnnotations(annotation);
  };

  const handleMenuPointerDown = (
    _event: SyntheticEvent | globalThis.MouseEvent | TouchEvent,
    value?: 'close' | 'open' | 'reclassify',
  ) => {
    handlePersisHover(value === 'open');
    if (value === 'reclassify' && opened) loadCandidates(annotation);
  };

  const isDark = useMemo(() => {
    if (mode === 'dark') return true;
    if (mode === 'system' && systemMode === 'dark') return true;
    return false;
  }, [mode, systemMode]);

  const rootVariants: Variants = useMemo(
    () => ({
      initial: {
        backgroundColor: chroma(palette[annotation.entityType][500]).alpha(0).css(),
      },
      hover: {
        backgroundColor: opened
          ? palette[annotation.entityType].solidBg
          : chroma(palette[annotation.entityType][500]).alpha(0.2).css(),
      },
      opened: {
        backgroundColor: palette[annotation.entityType].solidBg,
      },
    }),
    [palette, opened, annotation.entityType, isDark],
  );

  return (
    <Sheet
      animate={opened ? 'opened' : persistHover || isProcessing ? 'hover' : 'initial'}
      color={annotation.entityType}
      component={motion.div}
      id="annotation-header-sheet"
      invertedColors
      layout
      onPointerDown={onPointerDown}
      onPointerOver={handlePointerOver}
      onPointerOut={handlePointerOut}
      ref={ref}
      sx={{ cursor: 'pointer', backgroundColor: 'transparent' }}
      variant={opened ? 'solid' : 'plain'}
      variants={rootVariants}
      whileHover="hover"
      {...props}
    >
      <Stack
        alignItems="center"
        direction="row"
        id="annotation-header-stack-1"
        justifyContent="space-between"
        minHeight={44}
        px={1}
        py={0.5}
      >
        <Stack alignItems="center" direction="row" gap={1.5}>
          {annotation.isDraft && (
            <DraftMarker
              entityType={annotation.entityType}
              isHover={hover || persistHover}
              isOpen={opened}
            />
          )}
          <Icon
            name={annotation.isDraft ? `${annotation.entityType}Draft` : annotation.entityType}
            size={annotation.isDraft ? 16 : 20}
            style={{
              marginLeft: 6,
              color:
                (hover || persistHover) && !open
                  ? vars.palette[annotation.entityType][500]
                  : vars.palette.text.primary,
            }}
          />
          <Typography
            pt={0.25}
            fontWeight={opened ? 700 : 500}
            sx={{
              color:
                (hover || persistHover) && !open
                  ? vars.palette[annotation.entityType][500]
                  : vars.palette.text.primary,
            }}
          >
            {annotation.quote}
          </Typography>
        </Stack>
        <Stack direction="row" gap={1.5} alignItems="center">
          {stacked && stacked > 1 && !(hover || persistHover) && (
            <Chip size="sm" variant="soft">
              {stacked}
            </Chip>
          )}
          {isProcessing && (
            <CircularProgress color={annotation.entityType} size="sm" variant="plain" />
          )}
          {annotation.locked && !annotation.isDraft && opened && (
            <IconButton
              color="neutral"
              id="annotation-edit"
              onPointerDown={handleUnlockButton}
              sx={{
                ':hover': {
                  backgroundColor: ({ palette }) =>
                    chroma(palette[annotation.entityType][500]).alpha(0.2).css(),
                },
              }}
            >
              <TbPencil />
            </IconButton>
          )}
          {!annotation.locked && (hover || persistHover) && !isProcessing && (
            <Menu
              annotation={annotation}
              onPointerDown={handleMenuPointerDown}
              onPointerLeave={() => handlePersisHover(false)}
            />
          )}
        </Stack>
      </Stack>
    </Sheet>
  );
};
