import { Sheet, type SheetProps } from '@mui/joy';
import { motion, type Variants } from 'framer-motion';
import { useMemo } from 'react';
import type { EntityType } from '../../../../types';

interface Props extends SheetProps {
  entityType: EntityType;
  isHover?: boolean;
  isOpen?: boolean;
}

export const DraftMarker = ({ entityType, isHover, isOpen, ...props }: Props) => {
  const circleVariants: Variants = useMemo(
    () => ({
      hover: {
        rotate: 360,
        transition: { duration: 20, repeat: Infinity, ease: 'linear', type: 'tween' },
      },
      hoverBoder: { scale: 1.1 },
    }),
    [],
  );

  return (
    <Sheet
      component={motion.div}
      variants={circleVariants}
      sx={{
        position: 'absolute',
        height: 28,
        width: 28,
        borderRadius: '50%',
        borderStyle: 'dashed',
        borderWidth: 1,
        backgroundColor: 'transparent',
        borderColor: ({ vars }) => {
          return isHover && !isOpen ? vars.palette[entityType][500] : vars.palette.text.primary;
        },
      }}
      {...props}
    />
  );
};
