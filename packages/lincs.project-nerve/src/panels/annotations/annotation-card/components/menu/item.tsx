import {
  ListItemButton,
  ListItemContent,
  ListItemDecorator,
  MenuItem,
  useTheme,
  type ListItemButtonProps,
  type ListItemContentProps,
  type ListItemDecoratorProps,
  type ListItemProps,
} from '@mui/joy';
import { useRef, useState, type PointerEventHandler, type PropsWithChildren } from 'react';
import { IoChevronForward } from 'react-icons/io5';
import { Icon, type IconName, type IconProps } from '../../../../../icons';
import type { EntityType } from '../../../../../types';
import { SubMenu } from './sub-menu';

interface Props extends PropsWithChildren {
  entityType?: EntityType;
  disabled?: ListItemButtonProps['disabled'];
  icon?: IconName;
  listItemProps?: ListItemProps;
  onPointerDown?: PointerEventHandler;
  onPointerLeave?: PointerEventHandler;
  onPointerOver?: PointerEventHandler;
  submenu?: React.ReactNode;
  slotProps?: {
    root?: Omit<
      ListItemButtonProps,
      'ref' | 'children' | 'onPointerDown' | 'onPointerOver' | 'onPointerOut'
    >;
    decorator?: ListItemDecoratorProps;
    icon?: Omit<IconProps, 'name'>;
    content?: Omit<ListItemContentProps, 'children'>;
  };
}

export const Item = ({
  children,
  disabled,
  entityType,
  icon,
  onPointerDown,
  onPointerLeave,
  onPointerOver,
  submenu,
  slotProps,
}: Props) => {
  const { vars } = useTheme();

  const ref = useRef<HTMLDivElement>(null);

  const [hover, setHover] = useState(false);
  const [open, setOpen] = useState(false);

  const handleOpen = (value: boolean) => {
    setOpen(value);
  };

  const handlePointerEnter: PointerEventHandler<HTMLDivElement> = (event) => {
    setHover(true);
    if (submenu) setOpen(true);
    onPointerOver?.(event);
  };

  const handlePointerLeave: PointerEventHandler<HTMLDivElement> = (event) => {
    setHover(false);
    if (submenu) setOpen(false);
    onPointerLeave?.(event);
  };

  const handlePointerDown: PointerEventHandler<HTMLDivElement> = (event) => {
    if (submenu) setOpen(!open);
    onPointerDown?.(event);
  };

  return (
    <ListItemButton
      ref={ref}
      color={!entityType ? 'neutral' : hover ? entityType : 'neutral'}
      disabled={disabled}
      onPointerDown={handlePointerDown}
      onPointerEnter={handlePointerEnter}
      onPointerLeave={handlePointerLeave}
      slots={{ root: MenuItem }}
      sx={{
        backgroundColor: disabled ? vars.palette.neutral.plainHoverBg : 'inherit',
        borderRadius: 4,
        textTransform: 'capitalize',
      }}
      variant="soft"
      {...slotProps?.root}
    >
      <ListItemDecorator {...slotProps?.decorator}>
        {icon && (
          <Icon
            name={icon}
            style={{
              color: entityType
                ? disabled
                  ? vars.palette.neutral.softDisabledColor
                  : vars.palette[entityType].plainColor
                : 'inherit',
            }}
            {...slotProps?.icon}
          />
        )}
      </ListItemDecorator>
      <ListItemContent {...slotProps?.content}>{children}</ListItemContent>
      {submenu && (
        <>
          <IoChevronForward />
          <SubMenu
            anchorEl={ref.current}
            onPointerLeave={() => onPointerLeave}
            open={open}
            setOpen={handleOpen}
          >
            {submenu}
          </SubMenu>
        </>
      )}
    </ListItemButton>
  );
};
