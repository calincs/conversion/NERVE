import { Popper, type PopperOwnProps } from '@mui/base';
import { MenuList, type MenuListProps } from '@mui/joy';
import { PointerEventHandler, type PropsWithChildren } from 'react';

interface Props extends PropsWithChildren {
  anchorEl: PopperOwnProps['anchorEl'];
  modifiers?: PopperOwnProps['modifiers'];
  onPointerLeave?: PointerEventHandler;
  open: boolean;
  placement?: PopperOwnProps['placement'];
  setOpen: (value: boolean) => void;
  slotProps?: {
    root?: Omit<
      PopperOwnProps,
      'children' | 'anchorEl' | 'modifiers' | 'onPointerLeave' | 'open' | 'placement'
    >;
    menuList?: Omit<MenuListProps, 'children'>;
  };
}

export const SubMenu = ({
  anchorEl,
  children,
  modifiers = [{ name: 'offset', options: { offset: [0, -1] } }],
  onPointerLeave,
  open,
  placement = 'right-start',
  setOpen,
  slotProps,
}: Props) => {
  return (
    <Popper
      anchorEl={anchorEl}
      modifiers={modifiers}
      onPointerLeave={(event) => {
        setOpen(false);
        onPointerLeave?.(event);
      }}
      open={open}
      placement={placement}
      role={undefined}
      style={{ zIndex: 1000 }}
      {...slotProps?.root}
    >
      <MenuList size="sm" sx={{ px: 0.5 }} {...slotProps?.menuList}>
        {children}
      </MenuList>
    </Popper>
  );
};
