import { ClickAwayListener, Popper, type PopperOwnProps } from '@mui/base';
import {
  Box,
  Divider,
  IconButton,
  MenuList,
  type BoxProps,
  type IconButtonProps,
  type MenuListProps,
} from '@mui/joy';
import chroma from 'chroma-js';
import { useAtomValue } from 'jotai';
import {
  useRef,
  useState,
  type PointerEvent,
  type PointerEventHandler,
  type SyntheticEvent,
} from 'react';
import { useTranslation } from 'react-i18next';
import { Annotation } from '../../../../../annotation';
import { Icon, type IconProps } from '../../../../../icons';
import { stackedAnnotationsAtom } from '../../../../../jotai/store';
import { EntityTypes, type EntityType } from '../../../../../types';
import type { Affect } from '../../candidates';
import { useVetting } from '../../hooks/useVetting';
import { Item } from './item';

interface Props {
  annotation: Annotation;
  disabled?: boolean;
  onPointerDown?: (
    event: SyntheticEvent | globalThis.MouseEvent | TouchEvent | PointerEvent,
    value?: 'close' | 'open' | 'reclassify',
  ) => void;
  onPointerLeave?: PointerEventHandler;
  slotPtops?: {
    root?: Omit<BoxProps, 'children' | 'onPointerDown'>;
    iconButton?: Omit<IconButtonProps, 'ref' | 'disabled' | 'onPointerDown'>;
    icon?: Omit<IconProps, 'name'>;
    popper?: Omit<PopperOwnProps, 'children' | 'anchorEl' | 'onPointerLeave' | 'open'>;
    menuList?: Omit<MenuListProps, 'children'>;
  };
}

export const Menu = ({ annotation, disabled, onPointerDown, onPointerLeave, slotPtops }: Props) => {
  const { t } = useTranslation();

  const { reject, reclassify, relink } = useVetting(annotation);

  const stackedView = useAtomValue(stackedAnnotationsAtom);

  const buttonRef = useRef<HTMLButtonElement>(null);

  const [open, setOpen] = useState(false);

  const handlePointerLeave = (event: PointerEvent<HTMLDivElement>) => {
    setOpen(false);
    onPointerLeave?.(event);
  };

  const handleMenuPointerDown = (event: PointerEvent<HTMLDivElement>) => {
    event.stopPropagation();
    event.preventDefault();
  };

  const handleClickAway = (event: globalThis.MouseEvent | TouchEvent) => {
    setOpen(false);
    onPointerDown?.(event, 'close');
  };

  const handleDropdownPointerDown = (event: PointerEvent<HTMLButtonElement>) => {
    onPointerDown?.(event, open ? 'close' : 'open');
    setOpen(!open);
  };

  const handleReclassify = async (
    event: PointerEvent<Element>,
    value: EntityType,
    affect: Affect,
  ) => {
    await reclassify(value, affect);
    onPointerDown?.(event, 'reclassify');
  };

  return (
    <ClickAwayListener onClickAway={handleClickAway}>
      <Box onPointerDown={handleMenuPointerDown} {...slotPtops?.root}>
        <IconButton
          aria-controls={'annotation-menu'}
          aria-expanded={open ? 'true' : undefined}
          aria-haspopup="true"
          disabled={disabled}
          color="neutral"
          id="annotation-menu"
          onPointerDown={handleDropdownPointerDown}
          ref={buttonRef}
          sx={{
            ':hover': {
              backgroundColor: ({ palette }) =>
                chroma(palette[annotation.entityType][500]).alpha(0.2).css(),
            },
          }}
          {...slotPtops?.iconButton}
        >
          <Icon name="moreMenu" {...slotPtops?.icon} />
        </IconButton>
        <Popper
          anchorEl={buttonRef.current}
          id="composition-menu"
          modifiers={[{ name: 'offset', options: { offset: [0, 4] } }]}
          onPointerLeave={handlePointerLeave}
          open={open}
          placement="right-start"
          style={{ zIndex: 1000 }}
          role={undefined}
          {...slotPtops?.popper}
        >
          <MenuList size="sm" sx={{ px: 0.5 }} {...slotPtops?.menuList}>
            {!annotation.isDraft && (
              <Item icon="change" onPointerDown={() => relink('current')}>
                {stackedView ? t('nerve.common.relink current') : t('nerve.common.relink')}
              </Item>
            )}
            <Item
              icon="reclassify"
              submenu={EntityTypes.map((value) => (
                <Item
                  key={value}
                  disabled={annotation.entityType === value}
                  entityType={value}
                  icon={value}
                  onPointerDown={(event) => handleReclassify(event, value, 'current')}
                >
                  {t(`nerve.entity.${value}`)}
                </Item>
              ))}
            >
              {stackedView ? t('nerve.common.reclassify current') : t('nerve.common.reclassify')}
            </Item>
            <Item icon="remove" onPointerDown={() => reject('current')}>
              {stackedView ? t('nerve.common.remove current') : t('nerve.common.remove')}
            </Item>
            {stackedView && (
              <>
                <Divider sx={{ my: 0.5 }} />
                {!annotation.isDraft && (
                  <Item icon="change" onPointerDown={() => relink('all')}>
                    {t('nerve.common.relink all')}
                  </Item>
                )}
                <Item
                  icon="reclassify"
                  submenu={EntityTypes.map((value) => (
                    <Item
                      key={value}
                      disabled={annotation.entityType === value}
                      entityType={value}
                      icon={value}
                      onPointerDown={(event) => handleReclassify(event, value, 'all')}
                    >
                      {t(`nerve.entity.${value}`)}
                    </Item>
                  ))}
                >
                  {t('nerve.common.reclassify all')}
                </Item>
                <Item icon="remove" onPointerDown={() => reject('all')}>
                  {t('nerve.common.remove all')}
                </Item>
              </>
            )}
          </MenuList>
        </Popper>
      </Box>
    </ClickAwayListener>
  );
};
