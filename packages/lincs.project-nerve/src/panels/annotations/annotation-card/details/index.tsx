import { List, Sheet, type ListProps, type SheetProps } from '@mui/joy';
import chroma from 'chroma-js';
import { motion } from 'framer-motion';
import { useTranslation } from 'react-i18next';
import { AiOutlineSafetyCertificate } from 'react-icons/ai';
import { GiTargeting } from 'react-icons/gi';
import { SiArtifacthub } from 'react-icons/si';
import { Annotation } from '../../../../annotation';
import { AddPropertyButton, DropDown, Label, Property, Uri } from './components';
import { useDetails } from './useDetails';
import { useDetailsOptions } from './useDetailsOptions';

export interface Props {
  annotation: Annotation;
  slotProps?: {
    root?: Omit<SheetProps, 'children'>;
    list?: Omit<ListProps, 'children'>;
  };
}

export const Details = ({ annotation, slotProps }: Props) => {
  const { t } = useTranslation();

  const { authorityName, entityType, entityUri, locked } = annotation;

  const {
    handleAddProperty,
    handleCertaintyChange,
    handleModeExistenceChange,
    handlePrecisionChange,
    showCertaintyField,
    showModeExistenceField,
    showPrecisionField,
  } = useDetails(annotation);

  const { optionsCertainty, optionsModeExistence, optionsPrecision } = useDetailsOptions();

  return (
    <Sheet
      component={motion.div}
      layout
      invertedColors
      sx={{
        backgroundColor: ({ palette }) => chroma(palette[entityType][500]).alpha(0.2).css(),
      }}
      {...slotProps?.root}
    >
      <List size="sm" sx={{ '--List-gap': '4px' }} {...slotProps?.list}>
        <Label authorityName={authorityName} entityType={entityType} uri={entityUri}>
          {annotation.bodyLabel}
        </Label>
        <Uri entityType={entityType}>{entityUri}</Uri>
        {Annotation.allowsAttribute(entityType, 'modeExistence') && (
          <>
            {showModeExistenceField ? (
              <Property
                label={t('nerve.entity.mode of existence')}
                startDecorator={<SiArtifacthub style={{ marginRight: 8 }} />}
              >
                <DropDown
                  entityType={entityType}
                  disabled={locked}
                  onChange={handleModeExistenceChange}
                  options={optionsModeExistence}
                  removable
                  value={annotation.modeExistence}
                />
              </Property>
            ) : !locked ? (
              <AddPropertyButton
                entityType={entityType}
                onPointerDown={() => handleAddProperty('modeExistence')}
              >
                {t('nerve.entity.mode of existence')}
              </AddPropertyButton>
            ) : null}
          </>
        )}
        {Annotation.allowsAttribute(entityType, 'certainty') && (
          <>
            {showCertaintyField ? (
              <Property
                label={t('nerve.entity.certainty')}
                startDecorator={<AiOutlineSafetyCertificate style={{ marginRight: 8 }} />}
              >
                <DropDown
                  entityType={entityType}
                  disabled={locked}
                  onChange={handleCertaintyChange}
                  options={optionsCertainty}
                  removable
                  value={annotation.certainty}
                />
              </Property>
            ) : !locked ? (
              <AddPropertyButton
                entityType={entityType}
                onPointerDown={() => handleAddProperty('certainty')}
              >
                {t('nerve.entity.certainty')}
              </AddPropertyButton>
            ) : null}
          </>
        )}
        {Annotation.allowsAttribute(entityType, 'precision') && (
          <>
            {showPrecisionField ? (
              <Property
                label={t('nerve.entity.precision')}
                startDecorator={<GiTargeting style={{ marginRight: 8 }} />}
              >
                <DropDown
                  entityType={entityType}
                  disabled={locked}
                  onChange={handlePrecisionChange}
                  options={optionsPrecision}
                  removable
                  value={annotation.precision}
                />
              </Property>
            ) : !locked ? (
              <AddPropertyButton
                entityType={entityType}
                onPointerDown={() => handleAddProperty('precision')}
              >
                {t('nerve.entity.precision')}
              </AddPropertyButton>
            ) : null}
          </>
        )}
      </List>
    </Sheet>
  );
};
