import { Option, Select, selectClasses, type SelectProps } from '@mui/joy';
import chroma from 'chroma-js';
import { useState } from 'react';
import type { IconBaseProps } from 'react-icons';
import { GoChevronDown } from 'react-icons/go';
import type { EntityType } from '../../../../../types';

export interface DropDownProps<T> {
  disabled?: boolean;
  entityType: EntityType;
  onChange?: (value?: T) => void;
  options: { label: string; value: T }[];
  removable?: boolean;
  value?: T;
  slopProps?: {
    root?: Omit<SelectProps<{}, false>, 'children' | 'defaultValue' | 'onChange'>;
    indicatorIcon?: IconBaseProps;
  };
}

export const DropDown = <T,>({
  disabled,
  entityType,
  onChange,
  options,
  removable,
  value,
  slopProps,
}: DropDownProps<T>) => {
  const [currentValue, setCurrentValue] = useState<T | undefined>(value);

  const handleChange = (_value: T | '_blank') => {
    if (_value === '_blank') {
      setCurrentValue(undefined);
      onChange?.();
      return;
    }
    setCurrentValue(_value);
    onChange?.(_value);
  };

  return (
    <Select
      defaultValue={currentValue}
      indicator={<GoChevronDown {...slopProps?.indicatorIcon} />}
      onChange={(_event, value) => value && handleChange(value as T)}
      placeholder="Choose one…"
      size="sm"
      slotProps={{
        button: { sx: { justifyContent: 'flex-end', textTransform: 'capitalize' } },
      }}
      sx={{
        backgroundColor: 'transparent',
        ':hover': {
          backgroundColor: ({ palette }) => chroma(palette[entityType][500]).alpha(0.1).css(),
        },
        pointerEvents: disabled ? 'none' : 'auto',
        [`& .${selectClasses.indicator}`]: {
          display: disabled ? 'none' : 'flex',
          transition: '0.2s',
          [`&.${selectClasses.expanded}`]: {
            transform: 'rotate(-180deg)',
          },
        },
      }}
      variant="plain"
      {...slopProps?.root}
    >
      {removable && <Option sx={{ mx: 0.5, my: 0.25, borderRadius: 4 }} value="_blank" />}
      {options.map(({ label, value }) => (
        <Option
          key={`${value}`}
          sx={{ mx: 0.5, my: 0.25, borderRadius: 4, textTransform: 'capitalize' }}
          value={value}
        >
          {label}
        </Option>
      ))}
    </Select>
  );
};
