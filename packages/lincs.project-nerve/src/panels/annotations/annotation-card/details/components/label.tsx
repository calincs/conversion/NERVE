import {
  Button,
  ListItem,
  Typography,
  type ButtonProps,
  type ListItemProps,
  type TypographyProps,
} from '@mui/joy';
import type { PropsWithChildren } from 'react';
import type { EntityType } from '../../../../../types';

export interface LabelProps extends PropsWithChildren {
  authorityName: string;
  entityType: EntityType;
  uri: string;
  slotProps?: {
    root?: Omit<ListItemProps, 'children'>;
    typography?: Omit<TypographyProps, 'children'>;
    button?: Omit<ButtonProps, 'children' | 'href' | 'target'>;
  };
}

export const Label = ({ authorityName, children, entityType, uri, slotProps }: LabelProps) => (
  <ListItem sx={{ justifyContent: 'space-between' }} {...slotProps?.root}>
    <Typography level="title-sm" {...slotProps?.typography}>
      {children}
    </Typography>
    <Button
      color={entityType}
      component="a"
      href={uri}
      size="sm"
      target="_blank"
      variant="outlined"
      {...slotProps?.button}
    >
      {authorityName}
    </Button>
  </ListItem>
);
