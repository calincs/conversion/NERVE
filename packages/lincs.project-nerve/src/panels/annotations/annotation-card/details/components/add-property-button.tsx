import {
  ListItem,
  ListItemButton,
  ListItemContent,
  ListItemDecorator,
  type ListItemButtonProps,
  type ListItemContentProps,
  type ListItemDecoratorProps,
  type ListItemProps,
} from '@mui/joy';
import chroma from 'chroma-js';
import type { PointerEventHandler, PropsWithChildren } from 'react';
import type { IconBaseProps } from 'react-icons';
import { IoAddCircleOutline } from 'react-icons/io5';
import type { EntityType } from '../../../../../types';

export interface Props extends PropsWithChildren {
  entityType: EntityType;
  onPointerDown?: PointerEventHandler;
  slotProps?: {
    root?: Omit<ListItemProps, 'children'>;
    button?: Omit<ListItemButtonProps, 'onPointerDown'>;
    decorator?: ListItemDecoratorProps;
    icon?: IconBaseProps;
    content?: Omit<ListItemContentProps, 'children'>;
  };
}

export const AddPropertyButton = ({ children, entityType, onPointerDown, slotProps }: Props) => (
  <ListItem sx={{ mx: 0.5, borderRadius: 4 }} {...slotProps?.root}>
    <ListItemButton
      onPointerDown={onPointerDown}
      sx={{
        minHeight: 38,
        px: 0.5,
        py: 0.5,
        gap: 0.25,
        borderRadius: 4,
        fontWeight: 300,
        fontStyle: 'italic',
        '*::first-letter': { textTransform: 'uppercase' },
        ':hover': {
          backgroundColor: ({ palette }) =>
            `${chroma(palette[entityType][500]).alpha(0.1).css()} !important`,
        },
      }}
    >
      <ListItemDecorator {...slotProps?.decorator}>
        <IoAddCircleOutline {...slotProps?.icon} />
      </ListItemDecorator>
      <ListItemContent {...slotProps?.content}>{children as Capitalize<string>}</ListItemContent>
    </ListItemButton>
  </ListItem>
);
