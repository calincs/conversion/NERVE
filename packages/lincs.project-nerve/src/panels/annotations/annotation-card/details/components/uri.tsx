import {
  Link,
  ListItem,
  Typography,
  type LinkProps,
  type ListItemProps,
  type TypographyProps,
} from '@mui/joy';
import { RiLinkM } from 'react-icons/ri';
import type { IconProps } from '../../../../../icons';
import type { EntityType } from '../../../../../types';

export interface UriProps {
  children: string;
  entityType: EntityType;
  slotProps?: {
    root?: Omit<ListItemProps, 'children'>;
    typogrsaphy?: Omit<TypographyProps, 'children'>;
    icon?: Omit<IconProps, 'name'>;
    link?: Omit<LinkProps, 'children' | 'component' | 'href' | 'target'>;
  };
}

export const Uri = ({ children, entityType, slotProps }: UriProps) => {
  return (
    <ListItem sx={{ justifyContent: 'space-between' }} {...slotProps?.root}>
      <Typography
        level="title-sm"
        startDecorator={<RiLinkM style={{ marginRight: 8 }} size={16} {...slotProps?.icon} />}
        {...slotProps?.typogrsaphy}
      >
        URI
      </Typography>
      <Link
        color={entityType}
        component="a"
        href={children}
        target="_blank"
        level="body-sm"
        sx={{
          display: 'block',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          whiteSpace: 'nowrap',
        }}
        {...slotProps?.link}
      >
        {children}
      </Link>
    </ListItem>
  );
};
