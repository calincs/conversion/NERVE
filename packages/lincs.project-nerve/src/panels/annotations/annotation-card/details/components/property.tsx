import { ListItem, Typography, type ListItemProps, type TypographyProps } from '@mui/joy';
import type { PropsWithChildren } from 'react';

export interface PropertyProps extends PropsWithChildren {
  label: string;
  startDecorator?: TypographyProps['startDecorator'];
  slotProps?: {
    root?: Omit<ListItemProps, 'children'>;
    label?: Omit<TypographyProps, 'children' | 'startDecorator'>;
  };
}

export const Property = ({ children, label, startDecorator, slotProps }: PropertyProps) => (
  <ListItem sx={{ justifyContent: 'space-between' }} {...slotProps?.root}>
    <Typography level="title-sm" startDecorator={startDecorator} {...slotProps?.label}>
      {label}
    </Typography>
    {children}
  </ListItem>
);
