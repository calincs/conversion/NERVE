import type { Certainty, ModeExistence, Precision } from '@lincs.project/webannotation-schema';
import { useTranslation } from 'react-i18next';

interface Option<T> {
  label: string;
  value: T;
}

export const useDetailsOptions = () => {
  const { t } = useTranslation();

  const optionsModeExistence: Option<ModeExistence>[] = [
    { label: t('nerve.common.real'), value: 'edit:modeReal' },
    { label: t('nerve.common.fictional'), value: 'edit:modeFictional' },
    { label: t('nerve.common.fictionalized'), value: 'edit:modeFictionalized' },
  ];

  const optionsCertainty: Option<Certainty>[] = [
    { label: t('nerve.common.high'), value: 'edit:certaintyHigh' },
    { label: t('nerve.common.medium'), value: 'edit:certaintyMedium' },
    { label: t('nerve.common.low'), value: 'edit:certaintyLow' },
    { label: t('nerve.common.unknown'), value: 'edit:certaintyUnknown' },
  ];

  const optionsPrecision: Option<Precision>[] = [
    { label: t('nerve.common.high'), value: 'edit:precisionHigh' },
    { label: t('nerve.common.medium'), value: 'edit:precisionMedium' },
    { label: t('nerve.common.low'), value: 'edit:precisionLow' },
    { label: t('nerve.common.unknown'), value: 'edit:precisionUnknown' },
  ];
  return {
    optionsCertainty,
    optionsModeExistence,
    optionsPrecision,
  };
};
