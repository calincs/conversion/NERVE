import type { Certainty, ModeExistence, Precision } from '@lincs.project/webannotation-schema';
import { useSetAtom } from 'jotai';
import { useState } from 'react';
import { Annotation } from '../../../../annotation';
import { documentHasChangedAtom } from '../../../../jotai/store';
import {
  getCertaintyValue,
  getModeExistenceValue,
  getPrecisionValue,
} from '../../../../schema-mapping';
import { getTagByAttribute } from '../../../../viewer/helper';
import { nanoid } from 'nanoid';

export const useDetails = (annotation: Annotation) => {
  const [showCertaintyField, setShowCertaintyField] = useState(!!annotation.certainty);
  const [showPrecisionField, setShowPrecisionField] = useState(!!annotation.precision);
  const [showModeExistenceField, setShowModeExistenceField] = useState(!!annotation.modeExistence);

  const setDocumentHasChanged = useSetAtom(documentHasChangedAtom);

  const handleModeExistenceChange = (value?: ModeExistence) => {
    if (!value) setShowModeExistenceField(false);

    annotation.setModeExistence(value);

    const tag = getTagByAttribute('data-nerve-wa-uuid', annotation.uuid);
    if (!tag) return;

    value
      ? tag.setAttribute('type', getModeExistenceValue(value, 'tei'))
      : tag.removeAttribute('type');
    setDocumentHasChanged(nanoid(11));
  };

  const handleCertaintyChange = (value?: Certainty) => {
    if (!value) setShowCertaintyField(false);

    annotation.setCertainty(value);

    const tag = getTagByAttribute('data-nerve-wa-uuid', annotation.uuid);
    if (!tag) return;

    value ? tag.setAttribute('cert', getCertaintyValue(value, 'tei')) : tag.removeAttribute('cert');
    setDocumentHasChanged(nanoid(11));
  };

  const handlePrecisionChange = (value?: Precision) => {
    if (!value) setShowPrecisionField(false);

    annotation.setPrecision(value);

    const tag = getTagByAttribute('data-nerve-wa-uuid', annotation.uuid);
    if (!tag) return;

    value
      ? tag.setAttribute('precision', getPrecisionValue(value, 'tei'))
      : tag.removeAttribute('precision');
    setDocumentHasChanged(nanoid(11));
  };

  const handleAddProperty = (value: 'certainty' | 'precision' | 'modeExistence') => {
    if (value === 'certainty') setShowCertaintyField(true);
    if (value === 'precision') setShowPrecisionField(true);
    if (value === 'modeExistence') setShowModeExistenceField(true);
  };
  return {
    handleAddProperty,
    handleCertaintyChange,
    handleModeExistenceChange,
    handlePrecisionChange,
    showCertaintyField,
    showModeExistenceField,
    showPrecisionField,
  };
};
