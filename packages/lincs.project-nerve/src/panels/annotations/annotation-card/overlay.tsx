import { Box } from '@mui/joy';
import { motion } from 'framer-motion';

interface OverlayProps {
  onPointerDown: () => void;
  opened: boolean;
}

export const Overlay = ({ onPointerDown, opened }: OverlayProps) => (
  <Box
    onPointerDown={onPointerDown}
    component={motion.div}
    initial={false}
    animate={{ opacity: opened ? 1 : 0 }}
    sx={{
      position: 'fixed',
      zIndex: 1,
      width: '100%',
      maxWidth: 400,
      height: '100%',
      mt: -0.75,
      backdropFilter: 'blur(2px)',
      willChange: 'opacity',
      pointerEvents: opened ? 'auto' : 'none',
    }}
  />
);
