import { atom } from 'jotai';

export const headerHeightAtom = atom(0);
headerHeightAtom.debugLabel = 'panel.annotation.header.height.Atom';
