import { Button, IconButton, Sheet, Stack } from '@mui/joy';
import chroma from 'chroma-js';
import { useSetAtom } from 'jotai';
import { useEffect, useMemo, useRef, useState } from 'react';
import { IoChevronBack, IoChevronForward } from 'react-icons/io5';
import { EntityType } from '../../../types';
import { optionsComponentHeightAtom } from '../store';

interface NavigationProps {
  count: number;
  total?: number;
  current?: number;
  entityType: EntityType;
  onChange: (value: number) => void;
}

export const Navigation = ({
  count,
  current = 1,
  entityType,
  onChange,
  total = 30,
}: NavigationProps) => {
  const setViewOptionsPanelHeight = useSetAtom(optionsComponentHeightAtom);

  const [selected, setSelected] = useState(current);
  const ref = useRef<HTMLDivElement | null>(null);

  const numVisible = 7;

  const visiableRange = useMemo(() => {
    const arr = Array.from({ length: numVisible }, (_, i) => {
      return current - (numVisible - 1) / 2 + i;
    });
    return arr;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [total, current]);

  // const handleChange = (_event: ChangeEvent<unknown>, value: number) => {
  //   setSelected(value);
  //   onChange(value);
  // };

  const handleItemPointerDown = (value: number) => {
    setSelected(value);
    onChange(value);
  };

  useEffect(() => {
    if (ref.current) {
      setViewOptionsPanelHeight(ref.current.getBoundingClientRect().height);
    }
  }, [setViewOptionsPanelHeight]);

  return (
    <Sheet color={entityType} invertedColors variant="solid">
      <Stack
        ref={ref}
        direction="row"
        px={1}
        pt={0.5}
        sx={{
          backgroundColor: ({ palette }) =>
            palette.mode === 'dark'
              ? palette[entityType][800]
              : chroma(palette[entityType][500]).alpha(0.2).css(),
        }}
      >
        {/* <Pagination
          count={count}
          onChange={handleChange}
          page={selected}
          shape="rounded"
          siblingCount={3}
          size="small"
        /> */}
        <IconButton
          disabled={selected === 1}
          onPointerDown={() => handleItemPointerDown(selected - 1)}
          size="sm"
          sx={{ minWidth: 24 }}
        >
          <IoChevronBack />
        </IconButton>
        <Button
          onPointerDown={() => handleItemPointerDown(1)}
          size="sm"
          variant={selected === 1 ? 'soft' : 'plain'}
          sx={{ minWidth: 24, px: 1 }}
        >
          1
        </Button>
        ...
        <Stack direction="row" sx={{ maxWidth: 320, overflow: 'auto' }}>
          {visiableRange.map((value) =>
            value === 1 ? null : (
              <Button
                key={value}
                onPointerDown={() => handleItemPointerDown(value)}
                size="sm"
                variant={value === selected ? 'soft' : 'plain'}
                sx={{ minWidth: 24, px: 1 }}
              >
                {value}
              </Button>
            ),
          )}
        </Stack>
        ...
        <Button
          onPointerDown={() => handleItemPointerDown(total)}
          size="sm"
          variant={selected === total ? 'soft' : 'plain'}
          sx={{ minWidth: 24, px: 1 }}
        >
          {total}
        </Button>
        <IconButton
          disabled={selected === total}
          onPointerDown={() => handleItemPointerDown(selected + 1)}
          size="sm"
          sx={{ minWidth: 24 }}
        >
          <IoChevronForward />
        </IconButton>
      </Stack>
    </Sheet>
  );
};
