import { ColorPaletteProp, Typography } from '@mui/joy';
import { useSetAtom } from 'jotai';
import { useResetAtom } from 'jotai/utils';
import { useTranslation } from 'react-i18next';
import { Annotation } from '../../../../annotation';
import { useAnnotations } from '../../../../hooks';
import {
  alertDialogAtom,
  alertDialogIsProcessingAtom,
  setAlertDialogAtom,
} from '../../../../jotai/store';
import type { EntityType } from '../../../../types';
import { type Affect } from '../candidates';

interface OpenDialogProps {
  actionLabel: string;
  color?: ColorPaletteProp;
  body: React.ReactNode;
  entityType: EntityType;
  onConfirm: (value?: EntityType) => Promise<void>;
  title: string;
}

export const useVetting = (annotation: Annotation) => {
  const {
    approveAnnotation,
    approveAnnotationsByTarget,
    reclassifyAnnotationByUuid,
    reclassifyAnnotationsByTarget,
    relinkAnnotationByUuid,
    relinkAnnotationsByTarget,
    removeAnnotationByUuid,
    removeAnnotationsByTarget,
  } = useAnnotations();
  const resetAlertDialog = useResetAtom(alertDialogAtom);
  const setAlertDialog = useSetAtom(setAlertDialogAtom);
  const setAlertDialogIsProcessing = useSetAtom(alertDialogIsProcessingAtom);
  const { t } = useTranslation();

  const openDialog = async ({
    actionLabel,
    color = 'primary',
    body,
    entityType,
    onConfirm,
    title,
  }: OpenDialogProps) => {
    return new Promise((resolve) => {
      const handleConfirm = async () => {
        setAlertDialogIsProcessing(true);
        await onConfirm(entityType);
        setAlertDialogIsProcessing(false);
        closeAlertDialog();
        resolve('confirm');
      };

      const handleClose = async () => {
        closeAlertDialog();
        resolve('cancel');
      };

      setAlertDialog({
        actions: [
          {
            canShowLoading: true,
            color,
            label: actionLabel,
            onPointerDown: handleConfirm,
            variant: 'soft',
          },
          { label: t('nerve.common.cancel'), onPointerDown: handleClose },
        ],
        children: body,
        onClose: handleClose,
        severity: 'warning',
        title,
      });
    });
  };

  const approve = async (bodyId: string, affect: Affect = 'current') => {
    const bodyCandidate = annotation.getBodyCandidateById(bodyId);
    if (!bodyCandidate) return;

    if (affect === 'all') {
      const action = t('nerve.common.approve');

      const message = t('nerve.message.Are you sure you want to approve all instances of');

      const body = (
        <Typography>
          {message}
          <Typography fontWeight={700}>{annotation.quote}</Typography>?
        </Typography>
      );

      await openDialog({
        actionLabel: action,
        color: 'warning',
        body,
        entityType: annotation.entityType,
        onConfirm: async () => {
          await approveAnnotationsByTarget(annotation.quote, bodyCandidate, {
            entityType: annotation.entityType,
            isDraft: annotation.isDraft,
          });
        },
        title: action,
      });

      return;
    }

    await approveAnnotation(annotation, bodyCandidate);
  };

  const reject = async (affect: Affect = 'current') => {
    const action = t('nerve.common.remove');

    const message =
      affect === 'all'
        ? t('nerve.message.Are you sure you want to remove all instances of')
        : t('nerve.message.Are you sure you want to remove');

    const body = (
      <Typography>
        {message}
        <Typography
          color={annotation.entityType}
          sx={{ borderRadius: 4, mx: 0.5, textTransform: 'capitalize' }}
          variant="outlined"
        >
          {t(`nerve.entity.${annotation.entityType}`)}
        </Typography>
        <Typography fontWeight={700}>{annotation.quote}</Typography>?
      </Typography>
    );

    await openDialog({
      actionLabel: action,
      color: 'warning',
      body,
      entityType: annotation.entityType,
      onConfirm: async () => {
        affect === 'all'
          ? await removeAnnotationsByTarget(annotation.quote, {
              entityType: annotation.entityType,
              isDraft: annotation.isDraft,
            })
          : await removeAnnotationByUuid(annotation.uuid);
      },
      title: action,
    });
  };

  const relink = async (affect: Affect = 'current') => {
    const action = t('nerve.common.relink');

    const message =
      affect === 'all'
        ? t('nerve.message.Are you sure you want to relink all instances of')
        : t('nerve.message.Are you sure you want to relink');

    const body = (
      <Typography>
        {message}
        <Typography fontWeight={700}>{` ${annotation.quote} `}</Typography>?
      </Typography>
    );

    await openDialog({
      actionLabel: action,
      body,
      entityType: annotation.entityType,
      onConfirm: async () => {
        affect === 'all'
          ? await relinkAnnotationsByTarget(annotation.quote, {
              entityType: annotation.entityType,
              isDraft: annotation.isDraft,
            })
          : await relinkAnnotationByUuid(annotation.uuid);
      },
      title: action,
    });
  };

  const reclassify = async (newEntityType: EntityType, affect: Affect = 'current') => {
    const action = t('nerve.common.reclassify');

    const message =
      affect === 'all'
        ? t('nerve.message.Are you sure you want to reclassify all instances of')
        : t('nerve.message.Are you sure you want to reclassify');

    const body = (
      <Typography>
        {message}
        <Typography fontWeight={700}>{` ${annotation.quote} `}</Typography>
        {t('nerve.common.as')}
        <Typography
          color={newEntityType}
          sx={{ borderRadius: 4, mx: 0.5, textTransform: 'capitalize' }}
          variant="outlined"
        >
          {t(`nerve.entity.${newEntityType}`)}
        </Typography>
        ?
      </Typography>
    );

    await openDialog({
      actionLabel: action,
      body,
      entityType: annotation.entityType,
      onConfirm: async () => {
        affect === 'all'
          ? await reclassifyAnnotationsByTarget(annotation.quote, newEntityType, {
              entityType: annotation.entityType,
              isDraft: annotation.isDraft,
            })
          : await reclassifyAnnotationByUuid(annotation.uuid, newEntityType);
      },
      title: action,
    });
  };

  const closeAlertDialog = () => {
    resetAlertDialog();
  };

  return {
    approve,
    reclassify,
    relink,
    reject,
  };
};
