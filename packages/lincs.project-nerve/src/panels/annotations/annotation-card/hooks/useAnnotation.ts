import { useAtom } from 'jotai';
import { useResetAtom } from 'jotai/utils';
import { useState } from 'react';
import { Annotation, getEntityCandidates } from '../../../../annotation';
import { annotationsAtom, selectedAnnotationUuidAtom } from '../../../../jotai/store';
import { removeTagAttribute, setTagAttribute } from '../../../../viewer/helper';

export const useAnnotation = () => {
  const [annotations, setAnnotations] = useAtom(annotationsAtom);
  const [selectedAnnotationUuid, setSelectedAnnotationUuid] = useAtom(selectedAnnotationUuidAtom);
  const resetSelectedAnnotationId = useResetAtom(selectedAnnotationUuidAtom);

  const [isProcessing, setIsProcessing] = useState(false);

  const selectTagByUuid = (uuid: string) => {
    //prevent select the same annotation
    if (selectedAnnotationUuid === uuid) return;

    // deselect any selected instance in the document
    if (selectedAnnotationUuid) deselectTagByUuid(selectedAnnotationUuid);

    //select
    setSelectedAnnotationUuid(uuid);

    //select in the document
    const tag = setTagAttribute(uuid, 'data-nerve-selected', 'true');

    //TODO: Select Element
    if (tag) {
      // * scrollIntoView
      // scrollIntoView is a better alternative,
      // but Goolge Chome has a bug and cannot handle multiple `scrollIntoView`
      // from differnt element simultaeusly (both on the document and on the sidebar)
      tag.scrollIntoView({ behavior: 'smooth' });

      // Alternative
      // const value = tag.getBoundingClientRect().top;
      // tag.ownerDocument.scrollingElement?.scrollTo({ top: value, behavior: 'smooth' });
    }
  };

  const loadCandidates = async (annotation: Annotation) => {
    setIsProcessing(true);

    const bodyCandidates = await getEntityCandidates(
      annotation.query ?? annotation.quote,
      annotation.entityType,
    );

    if (!bodyCandidates) {
      setIsProcessing(false);
      return;
    }

    annotation.bodyCandidates = bodyCandidates;
    setAnnotations(annotation);
    setIsProcessing(false);
  };

  // const loadCandidates = async (annotation: Annotation) => {
  //   const services = await db.entityLookup
  //     .where({ entityType: annotation.entityType })
  //     .filter((service) => !service.disabled && service.active)
  //     .sortBy('order');

  //   const servicesNames = services
  //     .filter(({ lincsApiCode }) => lincsApiCode !== undefined)
  //     .flatMap(({ lincsApiCode }) => lincsApiCode)
  //     .map((code) => code ?? '')
  //     .filter((code) => code !== '');

  //   const query = annotation.query ?? annotation.quote;

  //   const { data, isLoading, error } = useQuery<ReconcileResult | undefined>({
  //     queryKey: [query, ...servicesNames],
  //     queryFn: async () => await reconcile(query, servicesNames),
  //   });

  //   const bodyCandidates = await getEntityCandidates(
  //     annotation.query ?? annotation.quote,
  //     annotation.entityType,
  //   );

  //   if (!bodyCandidates) {
  //     setIsProcessing(false);
  //     return;
  //   }

  //   annotation.bodyCandidates = bodyCandidates;
  //   setAnnotations(annotation);
  //   setIsProcessing(false);

  // };

  const deselectTagByUuid = (uuid: string) => {
    removeTagAttribute(uuid, 'data-nerve-selected');
    const currentAnnotation = annotations.get(uuid);
    if (!currentAnnotation?.isDraft) currentAnnotation?.setLocked(true);
    resetSelectedAnnotationId();
  };

  return {
    deselectTagByUuid,
    isProcessing,
    loadCandidates,
    selectTagByUuid,
  };
};
