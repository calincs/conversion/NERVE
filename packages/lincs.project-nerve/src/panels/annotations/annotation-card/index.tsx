import { Box, Stack } from '@mui/joy';
import { AnimatePresence, Variants, motion } from 'framer-motion';
import { useAtomValue } from 'jotai';
import { useEffect, useMemo, useState } from 'react';
import { selectedAnnotationUuidAtom, stackedAnnotationsAtom } from '../../../jotai/store';
import { Header } from './header';
// import { Navigation } from './navigation';
import { Annotation } from '../../../annotation';
import { removeTagAttribute, scrollToElement, setTagAttribute } from '../../../viewer/helper';
import { cardMaxHeightAtom } from '../store';
import { Candidates } from './candidates';
import { Details } from './details';
import { useAnnotation } from './hooks';
import { Overlay } from './overlay';

interface AnnotationProps {
  annotation: Annotation;
  collection?: Annotation[];
}

export const AnnotationCard = ({ annotation, collection }: AnnotationProps) => {
  const selectedAnnotationUuid = useAtomValue(selectedAnnotationUuidAtom);
  const stackedView = useAtomValue(stackedAnnotationsAtom);
  const maxCardHeight = useAtomValue(cardMaxHeightAtom);
  const { deselectTagByUuid, isProcessing, loadCandidates, selectTagByUuid } = useAnnotation();

  const [currentInstance, setCurrentInstance] = useState<Annotation>(annotation);

  useEffect(() => {
    setCurrentInstance(annotation);
    if (
      selectedAnnotationUuid === annotation.uuid &&
      annotation.isDraft &&
      !annotation.bodyCandidates
    ) {
      loadCandidates(annotation);
    }
  }, [annotation, loadCandidates, selectedAnnotationUuid]);

  useEffect(() => {
    setCurrentInstance(annotation);
  }, [annotation.isDraft, annotation, annotation.locked]);

  const opened = useMemo(
    () => selectedAnnotationUuid === currentInstance.uuid,
    [selectedAnnotationUuid, currentInstance],
  );

  const count = useMemo(() => collection?.length ?? 1, [collection?.length]);

  const handleHeaderPointerOver = () => {
    setTagAttribute(currentInstance.uuid, 'data-nerve-action', 'hover');
  };

  const handleHeaderPointerOut = () => {
    removeTagAttribute(currentInstance.uuid, 'data-nerve-action');
  };

  const handleHeaderPointerDown = async () => {
    if (opened) {
      deselectTagByUuid(currentInstance.uuid);
      return;
    }

    selectTagByUuid(currentInstance.uuid);
    scrollToElement(currentInstance.uuid);
  };

  const handleCandidatesClose = () => {
    deselectTagByUuid(currentInstance.uuid);
  };

  // const stackedNavIndex = useMemo(() => {
  //   const index = collection?.findIndex((item) => item.uuid === currentInstance.uuid);
  //   if (!index) return 1;
  //   return index + 1;
  // }, [collection, currentInstance.uuid]);

  // const handleNavigatiOnChange = (current: number) => {
  //   const currentAnnotation = collection?.[current - 1];
  //   if (!currentAnnotation) return;

  //   setCurrentInstance(currentAnnotation);

  //   selectTagById(currentAnnotation.uuid);
  //   scrollToElement(currentAnnotation.uuid);
  // };

  const variantions: Variants = {
    initial: {
      maxHeight: 44,
      width: '100%',
      // transition: { type: 'spring', stiffness: 300, damping: 35 },
    },
    opened: {
      maxHeight: maxCardHeight,
      width: currentInstance.isDraft ? 'calc(100% - 20px)' : '100%',
      // transition: { type: 'spring', stiffness: 200, damping: 30 },
      transition: {
        type: 'tween',
      },
    },
  };

  return (
    <>
      {opened && currentInstance.isDraft && (
        <>
          <Overlay onPointerDown={handleHeaderPointerDown} opened={currentInstance.isDraft} />
          <Box height={44} />
        </>
      )}
      <Stack
        component={motion.div}
        overflow="hidden"
        borderRadius={1}
        boxShadow={({ vars }) => (opened ? vars.shadow.md : 0)}
        variants={variantions}
        initial="initial"
        animate={opened ? 'opened' : 'initial'}
        // layoutId={currentInstance.uuid}
        // layout
        layoutScroll
        sx={{
          // position: opened && currentInstance.isDraft ? 'fixed' : 'relative',
          position: opened && currentInstance.isDraft ? 'absolute' : 'relative',
          top: 0,
          borderRadius: 4,
          // bgcolor: opened
          //   ? palette.mode === 'dark'
          //     ? palette.grey[800]
          //     : palette.background.paper
          //   : 'transparent',
          // scrollMarginTop: '50px',
          zIndex: opened ? 1 : 'inherit',
        }}
      >
        <Header
          annotation={currentInstance}
          isProcessing={isProcessing}
          onPointerDown={handleHeaderPointerDown}
          onPointerOver={handleHeaderPointerOver}
          onPointerOut={handleHeaderPointerOut}
          opened={opened}
          stacked={stackedView ? count : undefined}
        />
        <AnimatePresence>
          {opened && (
            <Stack component={motion.div} layout="preserve-aspect">
              {/* {stackedView && (
                <Navigation
                  count={count}
                  current={stackedNavIndex}
                  onChange={handleNavigatiOnChange}
                  entityType={entityType}
                />
              )} */}
              {currentInstance.isDraft ? (
                <Candidates annotation={currentInstance} onClose={handleCandidatesClose} />
              ) : (
                <Details annotation={currentInstance} />
              )}
            </Stack>
          )}
        </AnimatePresence>
      </Stack>
    </>
  );
};
