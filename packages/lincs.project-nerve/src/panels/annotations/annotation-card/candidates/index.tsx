import type { Body } from '@lincs.project/webannotation-schema';
import { Stack, type StackProps } from '@mui/joy';
import { motion } from 'framer-motion';
import { useAtom, useAtomValue } from 'jotai';
import { useEffect, useRef, useState } from 'react';
import { Annotation } from '../../../../annotation';
import { BodyCandidates, QueryField } from '../../../../components';
import { annotationsAtom, stackedAnnotationsAtom } from '../../../../jotai/store';
import { cardMaxHeightAtom } from '../../store';
import { useAnnotation, useVetting } from '../hooks';
import { headerHeightAtom } from '../store';
import { Footer } from './footer';

export type Affect = 'current' | 'all';

interface Props extends Omit<StackProps, 'children'> {
  annotation: Annotation;
  onClose: () => void;
}

export const Candidates = ({ annotation, onClose, ...props }: Props) => {
  const [annotations, setAnnotations] = useAtom(annotationsAtom);
  const stackedView = useAtomValue(stackedAnnotationsAtom);
  const maxCardHeight = useAtomValue(cardMaxHeightAtom);
  const annotationsHeaderHeight = useAtomValue(headerHeightAtom);

  const { approve, reject } = useVetting(annotation);

  const [maxHeight, setMaxHeight] = useState<number>(maxCardHeight);
  const [selectedBody, setSelectedBody] = useState<Body | undefined>(undefined);
  const [isProcessingQuery, setIsProcessingQuery] = useState(false);
  const [isProccesingAction, setIsProccesingAction] = useState<'approve' | 'reject' | undefined>(
    undefined,
  );

  const { loadCandidates } = useAnnotation();

  const queryFieldRef = useRef<HTMLDivElement | null>(null);
  const footerRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    const footerHeight = footerRef.current?.offsetHeight ?? 0;
    const queryHeight = queryFieldRef.current?.offsetHeight ?? 0;
    let height = maxCardHeight - annotationsHeaderHeight - queryHeight - footerHeight;
    if (stackedView) height = height - 44; //TODO
    setMaxHeight(height);
  }, [annotationsHeaderHeight, maxCardHeight, stackedView]);

  const handleQuery = async (query: string) => {
    setIsProcessingQuery(true);

    annotation.query = query;
    annotations.set(annotation.uuid, annotation);
    setAnnotations(annotation);

    await loadCandidates(annotation);
    setIsProcessingQuery(false);
  };

  const handleRejectAction = async () => {
    setIsProccesingAction('reject');
    await reject(stackedView ? 'all' : 'current');
    setIsProccesingAction(undefined);
  };

  const handleApproveAction = async () => {
    if (!selectedBody) return;
    setIsProccesingAction('approve');
    await approve(selectedBody.id, stackedView ? 'all' : 'current');
    setIsProccesingAction(undefined);
  };

  return (
    <Stack component={motion.div} layout {...props}>
      {annotation && (
        <>
          <QueryField
            disabled={isProcessingQuery}
            entityType={annotation.entityType}
            onQuery={handleQuery}
            ref={queryFieldRef}
            selection={annotation.query ?? annotation.quote}
          />
          <BodyCandidates
            annotation={annotation}
            disabled={isProcessingQuery}
            onCandidateDoubleClick={handleApproveAction}
            maxHeight={maxHeight}
            menuPosition="top"
            setSelectedBody={setSelectedBody}
          />
          <Footer
            annotation={annotation}
            disabled={isProcessingQuery}
            isProccesing={isProccesingAction}
            onApprove={handleApproveAction}
            onClose={onClose}
            onReject={handleRejectAction}
            ref={footerRef}
            selectedBody={selectedBody}
          />
        </>
      )}
    </Stack>
  );
};
