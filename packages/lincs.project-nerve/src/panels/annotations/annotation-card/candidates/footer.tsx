/* eslint-disable @typescript-eslint/prefer-nullish-coalescing */
import type { Body } from '@lincs.project/webannotation-schema';
import {
  Button,
  IconButton,
  Sheet,
  Stack,
  type ButtonProps,
  type IconButtonProps,
  type SheetProps,
  type StackProps,
} from '@mui/joy';
import chroma from 'chroma-js';
import { useAtomValue } from 'jotai';
import { forwardRef } from 'react';
import { useTranslation } from 'react-i18next';
import { Annotation } from '../../../../annotation';
import { Icon } from '../../../../icons';
import { stackedAnnotationsAtom } from '../../../../jotai/store';
import { Menu } from '../components';

interface Props {
  annotation: Annotation;
  disabled?: boolean;
  isProccesing?: 'approve' | 'reject';
  onApprove: () => void;
  onClose: () => void;
  onReject: () => void;
  selectedBody?: Body;
  slotProps?: {
    root?: Omit<SheetProps, 'children' | 'ref'>;
    stack?: Omit<StackProps, 'children'>;
    closeButton?: Omit<IconButtonProps, 'disabled' | 'onPointerDown'>;
    RejectButton?: Omit<ButtonProps, 'children' | 'disabled' | 'loading' | 'onPointerDown'>;
    approveButton?: Omit<ButtonProps, 'children' | 'disabled' | 'loading' | 'onPointerDown'>;
  };
}

export const Footer = forwardRef<HTMLDivElement, Props>(function Footer(
  {
    annotation,
    disabled,
    isProccesing,
    onApprove,
    onClose,
    onReject,
    selectedBody,
    slotProps,
  }: Props,
  ref,
) {
  const stackedView = useAtomValue(stackedAnnotationsAtom);
  const { t } = useTranslation();

  return (
    <Sheet invertedColors ref={ref} variant="soft" {...slotProps?.root}>
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        gap={1}
        p={0.75}
        {...slotProps?.stack}
      >
        <IconButton disabled={!!isProccesing} onPointerDown={onClose} {...slotProps?.closeButton}>
          <Icon name="chevronUp" />
        </IconButton>
        <Button
          color="neutral"
          disabled={disabled || isProccesing === 'approve'}
          loading={isProccesing === 'reject'}
          onPointerDown={onReject}
          startDecorator={<Icon name="clear" />}
          sx={{ width: '100%', textTransform: 'capitalize' }}
          variant="plain"
        >
          {stackedView ? t('nerve.common.all') : ''}
        </Button>
        <Button
          color={annotation.entityType}
          disabled={disabled || isProccesing === 'reject'}
          loading={isProccesing === 'approve'}
          onPointerDown={onApprove}
          startDecorator={<Icon name="approve" />}
          sx={{
            width: '100%',
            color: ({ vars }) => vars.palette[annotation.entityType][500],
            borderColor: ({ vars }) => vars.palette[annotation.entityType][500],
            textTransform: 'capitalize',
            ':hover': {
              backgroundColor: ({ palette }) =>
                chroma(palette[annotation.entityType][500]).alpha(0.2).css(),
              borderColor: ({ vars }) => vars.palette[annotation.entityType][700],
            },
          }}
          variant={!selectedBody?.id ? 'plain' : 'outlined'}
        >
          {stackedView ? t('nerve.common.all') : ''}
        </Button>
        <Menu annotation={annotation} disabled={disabled} />
      </Stack>
    </Sheet>
  );
});
