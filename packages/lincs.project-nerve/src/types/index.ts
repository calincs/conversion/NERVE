import { WebAnnotation } from '@lincs.project/webannotation-schema';
import z from 'zod';

export const NervePrpsSchema = z.object({
  annotations: z.array(WebAnnotation).optional(),
  document: z.object({
    content: z.string(),
    url: z.string().url().optional(),
  }),
  preferences: z
    .object({
      darkTheme: z.boolean().optional(),
      locale: z.string().optional(),
    })
    .optional(),
  user: z.object({
    name: z.string(),
    url: z.string(),
  }),
  onDocumentOnchange: z.function().args(z.boolean()).optional(),
});
export type NerveProps = z.infer<typeof NervePrpsSchema>;

export type ErrorType = 'info' | 'warning' | 'error';

export interface IError {
  type?: ErrorType;
  message: string;
}

export interface User {
  name: string;
  url: string;
}

export const AuthorityTypes = [
  'DBpedia',
  'Geonames',
  'Getty',
  'LGPN',
  'Lincs',
  'VIAF',
  'Wikidata',
] as const;
export type AuthorityType = (typeof AuthorityTypes)[number];

export const EntityTypes = [
  'person',
  'place',
  'organization',
  'work',
  'event',
  'physicalThing',
  'conceptualObject',
  'citation',
] as const;

export type EntityType = (typeof EntityTypes)[number];

export type SortView = 'linear' | 'alphabetically';

export interface AuthorityEntityLincs {
  lincsApiCode: string | string[];
}

export interface AuthorityEntityCustom {
  request: (query: string) => { description?: string; label: string; uri: string };
}

export type AuthorityEntity = (AuthorityEntityLincs | AuthorityEntityCustom) & {
  disabled?: boolean;
  order?: number;
};

export type AuthorityEntityTypeServiceM = Map<EntityType, AuthorityEntity>;

export interface Authority {
  id: string;
  name: string;
  disabled?: boolean;
  entityType: AuthorityEntityTypeServiceM;
}

export interface AuthorityService
  extends Partial<AuthorityEntityLincs>,
    Partial<AuthorityEntityCustom> {
  id: string;
  authorityId: string;
  entityType: EntityType;
  name: string;
  active: boolean;
  order: number;
  disabled?: boolean;
}

export type LookupSetting = Map<EntityType, AuthorityService[]>;

// *****

export interface AnnotationCollectionModel {
  '@context': string[];
  id: string;
  type: 'AnnotationCollection';
  label: string;
  first: string | AnnotationPage;
  last?: string;
  total?: number;
}

export interface AnnotationPage {
  '@context'?: string[];
  id: string;
  type: 'AnnotationPage';
  items: WebAnnotation[];
  next?: string;
  prev?: string;
  partOf?: string;
  startIndex: number;
}

export type MimeType =
  | 'application/xml'
  | 'application/tei+xml'
  | 'text/xml'
  | 'text/html'
  | 'text/plain'
  | (string & {});

export interface DocumentMetadata {
  language?: string;
  mimetype: MimeType;
  schemaFamily?: 'tei';
  title?: string;
  url?: string;
}
