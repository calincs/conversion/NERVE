import { nanoid } from 'nanoid';
import {
  EntityTypes,
  type Authority,
  type AuthorityService,
  type EntityType,
  type LookupSetting,
} from '../types';

const authorities: Authority[] = [
  {
    id: 'dbpedia',
    name: 'DBPedia',
    entityType: new Map([
      ['person', { lincsApiCode: 'DBpedia-Person', order: 4 }],
      ['place', { lincsApiCode: 'DBpedia-Place', order: 5 }],
      ['organization', { lincsApiCode: 'DBpedia-Organisation', order: 4 }],
      ['work', { lincsApiCode: 'DBpedia-Work', order: 4 }],
      ['physicalThing', { lincsApiCode: 'DBpedia-All', order: 2 }],
      ['conceptualObject', { lincsApiCode: 'DBpedia-All', order: 2 }],
      ['event', { lincsApiCode: 'DBpedia-Event', order: 1 }],
      ['citation', { lincsApiCode: 'DBpedia-Work', order: 2 }],
    ]),
  },
  {
    id: 'getty',
    name: 'Getty',
    entityType: new Map([
      ['person', { lincsApiCode: 'Getty-ULAN', order: 1 }],
      ['place', { lincsApiCode: 'Getty-TGN', order: 1 }],
      ['organization', { lincsApiCode: 'Getty-All', order: 1 }],
      ['work', { lincsApiCode: ['Getty-AAT', 'Getty-All'], order: 1 }],
      ['physicalThing', { lincsApiCode: ['Getty-AAT', 'Getty-All'], order: 0 }],
      ['conceptualObject', { lincsApiCode: ['Getty-AAT', 'Getty-All'], order: 0 }],
      ['event', { lincsApiCode: 'Getty-All', order: 3 }],
      ['citation', { lincsApiCode: ['Getty-AAT', 'Getty-All'], order: 4 }],
    ]),
  },
  {
    id: 'geonames',
    name: 'Geonames',
    entityType: new Map([['place', { lincsApiCode: 'Geonames', order: 0 }]]),
  },
  {
    id: 'lincs',
    name: 'Lincs',
    entityType: new Map([
      ['person', { lincsApiCode: 'LINCS', order: 3 }],
      ['place', { lincsApiCode: 'LINCS', order: 3 }],
      ['organization', { lincsApiCode: 'LINCS', order: 3 }],
      ['work', { lincsApiCode: 'LINCS', order: 3 }],
      ['physicalThing', { lincsApiCode: 'LINCS', order: 3 }],
      ['conceptualObject', { lincsApiCode: 'LINCS', order: 3 }],
      ['event', { lincsApiCode: 'LINCS', order: 2 }],
      ['citation', { lincsApiCode: 'LINCS', order: 3 }],
    ]),
  },
  {
    id: 'viaf',
    name: 'VIAF',
    entityType: new Map([
      ['person', { lincsApiCode: 'VIAF-Personal', order: 0 }],
      ['place', { lincsApiCode: 'VIAF-Geographic', order: 4 }],
      ['organization', { lincsApiCode: 'VIAF-Corporate', order: 0 }],
      [
        'work',
        { lincsApiCode: ['VIAF-Bibliographic', 'VIAF-Expressions', 'VIAF-Works'], order: 0 },
      ],
      ['physicalThing', { lincsApiCode: ['VIAF-Expressions', 'VIAF-Works'], order: 4 }],
      ['conceptualObject', { lincsApiCode: ['VIAF-Expressions', 'VIAF-Works'], order: 4 }],
      [
        'citation',
        { lincsApiCode: ['VIAF-Bibliographic', 'VIAF-Expressions', 'VIAF-Works'], order: 0 },
      ],
    ]),
  },
  {
    id: 'wikidata',
    name: 'Wikidata',
    entityType: new Map([
      ['person', { lincsApiCode: 'Wikidata', order: 2 }],
      ['place', { lincsApiCode: 'Wikidata', order: 2 }],
      ['organization', { lincsApiCode: 'Wikidata', order: 2 }],
      ['work', { lincsApiCode: 'Wikidata', order: 2 }],
      ['physicalThing', { lincsApiCode: 'Wikidata', order: 1 }],
      ['conceptualObject', { lincsApiCode: 'Wikidata', order: 1 }],
      ['event', { lincsApiCode: 'Wikidata', order: 0 }],
      ['citation', { lincsApiCode: 'Wikidata', order: 1 }],
    ]),
  },
];

export const getAuthorityById = (id: string) => authorities.find((auth) => auth.id == id);

const getAuthorityServices = (entityType: EntityType) => {
  const services: AuthorityService[] = [];

  authorities.forEach((authority) => {
    const entityTypeService = authority.entityType.get(entityType);
    if (!entityTypeService) return;

    const order = entityTypeService.order ?? services.length;

    services.push({
      ...entityTypeService,
      id: nanoid(11),
      authorityId: authority.id,
      name: authority.name,
      entityType,
      active: true,
      order,
    });
  });

  return services;
};

export const initialLookupSettings: LookupSetting = new Map(
  EntityTypes.map((type) => [type, getAuthorityServices(type)]),
);
