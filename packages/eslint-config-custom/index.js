module.exports = {
  extends: [
    'next/core-web-vitals',
    'turbo',
    //* For more relaxed TS rules, comment the next line.
    // 'plugin:@typescript-eslint/recommended-type-checked',
    'plugin:@typescript-eslint/stylistic-type-checked',
    'plugin:@tanstack/eslint-plugin-query/recommended',
    'prettier',
  ],
  settings: {
    react: { version: 'detect' },
  },
  ignorePatterns: ['**/coverage/**/*.*', '**/dist/**/*.*'],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: ['./tsconfig.json'],
  },
  plugins: ['@typescript-eslint', '@tanstack/query'],
  rules: {
    '@next/next/no-html-link-for-pages': 'off',
    '@typescript-eslint/ban-ts-comment': 1,
    '@typescript-eslint/no-explicit-any': 1,
    '@typescript-eslint/require-await': 0,
    '@tanstack/query/exhaustive-deps': 'error',
    '@tanstack/query/stable-query-client': 'error',
  },
  overrides: [
    {
      files: ['**/*.js'],
      extends: ['next/core-web-vitals', 'prettier'],
    },
    {
      files: ['**/*.md'],
      processor: 'markdown/markdown',
      plugins: ['markdown'],
      extends: ['eslint:recommended', 'plugin:markdown/recommended', 'prettier'],
    },
    {
      files: ['**/*.md/*.ts'],
      rules: { strict: 'off' },
    },
    {
      env: { node: true },
      files: ['**/*.yml', '**/*.yaml'],
      extends: ['eslint:recommended', 'plugin:yml/standard', 'plugin:yml/prettier', 'prettier'],
      rules: { 'yml/no-empty-mapping-value': 0 },
    },
  ],
};
