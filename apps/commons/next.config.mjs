import withMDX from '@next/mdx';
import createNextIntlPlugin from 'next-intl/plugin';
import remarkDirective from 'remark-directive';
import remarkDirectiveRehype from 'remark-directive-rehype';

const withNextIntl = createNextIntlPlugin();

/** @type {import('next').NextConfig} */
const nextConfig = {
  eslint: {
    ignoreDuringBuilds: true,
  },
  experimental: {
    missingSuspenseWithCSRBailout: false,
    optimizePackageImports: ['date-fns', '@mui/material', 'react-icons/*'],
  },
  output: 'standalone',
  transpilePackages: ['@lincs.project/nerve', 'jotai-devtools'],
  // Override the default webpack configuration
  webpack: (config) => {
    // Ignore node-specific modules when bundling for the browser
    // See https://webpack.js.org/configuration/resolve/#resolvealias
    config.resolve.alias = {
      ...config.resolve.alias,
      sharp$: false,
      // 'onnxruntime-node$': false,
    };
    return config;
  },
};

export default withMDX({
  options: {
    providerImportSource: '@mdx-js/react',
    remarkPlugins: [remarkDirective, remarkDirectiveRehype],
  },
})(withNextIntl(nextConfig));
