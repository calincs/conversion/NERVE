import { BroadcastChannel } from 'broadcast-channel';
import { secondsToMilliseconds } from 'date-fns';

export type Channel =
  | 'keycloak-account-manager'
  | 'keycloak-link-account'
  | 'keycloak-login-callback';

export interface Message {
  success: boolean;
  error?: string;
  codeVerifier?: string;
  [x: string]: unknown;
}

export interface LinkAccountMessage extends Message {
  isRefresh?: boolean;
  provider?: string;
}

const _TIME_OUT = secondsToMilliseconds(30); //MAX TIME to callback the origin

interface Options {
  timeout?: number;
}

/**
 * The `openAuxWindowChannel` function opens a new window, creates a broadcast channel, and returns a promise that
 * resolves with the response received on the broadcast channel or rejects with a timeout error.
 * @param {Channel} channel - The `channel` parameter is a string that represents the name of the
 * broadcast channel. It is used to establish communication between the main window and the auxiliary
 * window. Both windows should use the same channel name to communicate with each other.
 * @param {string} url - The `url` parameter is a string that represents the URL of the auxiliary
 * window that you want to open.
 * @param {Options} options - The `options` parameter is an object that contains additional
 * configuration options for the function. It has a default value of `{ timeout: _TIME_OUT }`, where
 * `_TIME_OUT` is a variable that holds the default timeout value (in milliseconds).
 * @returns The function `openAuxWindowChannel` returns a Promise that resolves to a value of type `T`.
 */
export const openAuxWindowChannel = <T = unknown>(
  channel: Channel,
  url: string,
  options: Options = { timeout: _TIME_OUT },
): Promise<T> => {
  const { timeout } = options;

  const broadcastChannel = new BroadcastChannel<T>(channel);

  window.open(url);

  return new Promise((resolve, reject) => {
    const timer = setTimeout(() => {
      void broadcastChannel.close();
      clearTimeout(timer);
      reject('timeout');
    }, timeout);

    broadcastChannel.onmessage = async (response: T) => {
      void broadcastChannel.close();
      clearTimeout(timer);
      return resolve(response);
    };
  });
};
