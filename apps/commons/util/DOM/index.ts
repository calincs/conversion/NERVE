export const isValidXml = (string: string) => {
  const doc = new DOMParser().parseFromString(string, 'application/xml');
  const parsererror = doc.querySelector('parsererror');
  return !parsererror;
};

export const hasAnnotationsContainerTag = (content: string) => {
  const doc = new DOMParser().parseFromString(content, 'application/xml');
  if (!doc) return false;
  const container = doc.querySelector('xenoData');
  if (!container) return false;
  return true;
};

export const getAnnotationsContainerUrl = (content: string) => {
  const doc = new DOMParser().parseFromString(content, 'application/xml');
  if (!doc) return;
  const container = doc.querySelector('xenoData');
  if (!container) return;
  return container.textContent ?? undefined;
};
