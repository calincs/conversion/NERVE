'use client';

import { MdxComponents } from '@/mdx-components';
import { MDXComponents, MDXProps } from 'mdx/types';

interface Props {
  contents: Record<string, (props: MDXProps) => React.JSX.Element>;
  locale?: string;
  mdxComponentOverride?: MDXComponents;
}

export const ContainerCompiledMdxContent = ({
  contents,
  locale = 'en',
  mdxComponentOverride: mdxComponentsOverride,
}: Props) => {
  const components = MdxComponents(mdxComponentsOverride);
  const Content = contents[locale];
  return <Content components={components} />;
};
