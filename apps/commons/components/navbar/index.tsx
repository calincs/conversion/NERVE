'use client';

import { Link, usePathname } from '@/navigation';
import { Button, Stack, type StackProps } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { Logo, type LogoProps } from '../logo';

interface NavBarProps {
  logoDisabled?: boolean;
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    logo?: LogoProps;
  };
}

export const NavBar = ({ logoDisabled, slotProps }: NavBarProps) => {
  const pathName = usePathname();
  const t = useTranslations();

  return (
    <Stack direction="row" alignItems="center" gap={2} {...slotProps?.root}>
      {!logoDisabled && (
        <Link href="/">
          <Logo height={32} variation="simplified" {...slotProps?.logo} />
        </Link>
      )}
      <Stack direction="row" alignItems="center" gap={1}>
        <Link href="/about">
          <Button
            color={pathName === '/about' ? 'primary' : 'neutral'}
            size="sm"
            sx={{ textTransform: 'capitalize' }}
            variant={pathName === '/about' ? 'soft' : 'plain'}
          >
            {t('common.about')}
          </Button>
        </Link>
        <Link href="/privacy">
          <Button
            color={pathName === '/privacy' ? 'primary' : 'neutral'}
            size="sm"
            sx={{ textTransform: 'capitalize' }}
            variant={pathName === '/privacy' ? 'soft' : 'plain'}
          >
            {t('common.privacy')}
          </Button>
        </Link>
      </Stack>
    </Stack>
  );
};
