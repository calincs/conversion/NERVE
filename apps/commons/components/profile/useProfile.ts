import { nerveAtoms } from '@/app/[locale]/edit/nerve/store';
import { deleteDb } from '@/db';
import { useAtomValue } from 'jotai';
import { signOut } from 'next-auth/react';
import { useLocale } from 'next-intl';
import * as CookieConsent from 'vanilla-cookieconsent';
import { resetCookieConsent } from '../cookie-consent';

export const useProfile = () => {
  const nerveActions = useAtomValue(nerveAtoms.nerveActions);
  const locale = useLocale();

  const handlePrivacyClick = () => {
    CookieConsent.showPreferences();
  };

  const handleSignout = async () => {
    await deleteDb();

    localStorage.clear();
    sessionStorage.clear();

    signOut({ redirectTo: `/${locale}` });
    resetCookieConsent();
  };

  const handleOpenNerveSettings = async () => {
    nerveActions?.openSettingsPanel();
  };

  return {
    handleOpenNerveSettings,
    handlePrivacyClick,
    handleSignout,
    nerveActions,
  };
};
