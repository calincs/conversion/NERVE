'use client';

import { ListItem, Typography, type ListItemProps, type TypographyProps } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { useState, type PointerEventHandler } from 'react';
import type { IconBaseProps } from 'react-icons';
import { MdFingerprint } from 'react-icons/md';
import { Selector } from './selector';

interface IdentityProps {
  onPointerOver?: PointerEventHandler;
  onPointerOut?: PointerEventHandler;
  slotProps?: {
    root?: Omit<ListItemProps, 'children'>;
    typography?: Omit<TypographyProps, 'children'>;
    icon?: IconBaseProps;
  };
}

export const Identity = ({ onPointerOver, onPointerOut, slotProps }: IdentityProps) => {
  const t = useTranslations();
  // const prefIDProvider = useLiveQuery(() => db.userPreferences.get('identityProvider'));
  const [hover, setHover] = useState(false);

  return (
    <ListItem
      sx={{ alignItems: 'center', justifyContent: 'space-between', gap: 1, px: 1.5 }}
      {...slotProps?.root}
      onPointerOver={(event) => {
        setHover(true);
        onPointerOver?.(event);
        slotProps?.root?.onPointerOver?.(event);
      }}
      onPointerOut={(event) => {
        setHover(false);
        onPointerOut?.(event);
        slotProps?.root?.onPointerOut?.(event);
      }}
    >
      <Typography
        level="body-sm"
        startDecorator={<MdFingerprint style={{ marginRight: 14 }} {...slotProps?.icon} />}
        sx={{ color: ({ palette }) => (hover ? palette.text.secondary : palette.text.tertiary) }}
        {...slotProps?.typography}
      >
        {t('common.identity')}
      </Typography>
      <Selector hover={hover} />
    </ListItem>
  );
};
