import { db } from '@/db';
import { useIdentityProvider } from '@/hooks';
import { useLiveQuery } from 'dexie-react-hooks';
import { useState } from 'react';

export const useSelector = () => {
  const { isProviderLinked, isAccessTokenExpired, linkProvider, refreshAccessToken } =
    useIdentityProvider();

  const prefIDProvider = useLiveQuery(() => db.userPreferences.get('identityProvider'));

  const [isProcessing, setProcessing] = useState(false);

  const handleChange = async (value: string | null) => {
    if (!value) return;

    setProcessing(true);

    const providerId = value;
    const isLinked = await isProviderLinked(providerId);

    if (isLinked) {
      if (await isAccessTokenExpired(providerId)) {
        const refreshed = await refreshAccessToken(providerId);
        if (!refreshed) return setProcessing(false);
      }
    }

    if (prefIDProvider?.value === providerId) return setProcessing(false);

    if (!isLinked) {
      const linked = await linkProvider(providerId);
      if (!linked) return setProcessing(false);
    }

    await db.userPreferences.put({ key: 'identityProvider', value: providerId });
    setProcessing(false);
  };

  return {
    isProcessing,
    handleChange,
    prefIDProvider,
  };
};
