'use client';

import { ProviderIcon, type ProviderIconProps } from '@/components/icons';
import { useLoadIdentityProviders } from '@/hooks';
import { usePathname } from '@/navigation';
import { CircularProgress, Select, type SelectProps } from '@mui/joy';
import { useSession } from 'next-auth/react';
import { Provider } from './provider';
import { useSelector } from './useSelector';

interface Props extends Omit<SelectProps<string, false>, 'value'> {
  hover?: boolean;
  iconProps?: ProviderIconProps;
}

export const Selector = ({ hover, iconProps, ...props }: Props) => {
  const pathname = usePathname();
  const { data: session } = useSession();

  const { providers, isLoading, error } = useLoadIdentityProviders();
  const { isProcessing, handleChange, prefIDProvider } = useSelector();

  if (isLoading === undefined || !prefIDProvider) {
    return <CircularProgress size="sm" />;
  }

  return (
    <Select
      disabled={!!error || isLoading || pathname === '/edit' || isProcessing}
      name="identity-provider"
      size="sm"
      startDecorator={
        <ProviderIcon providerId={prefIDProvider.value} style={{ marginRight: 2 }} {...iconProps} />
      }
      sx={{
        backgroundColor: hover ? undefined : 'transparent',
        '& .MuiSelect-button': { textTransform: 'capitalize' },
      }}
      value={prefIDProvider.value}
      variant={hover ? 'soft' : 'plain'}
      {...props}
      onChange={(event, newValue) => {
        handleChange(newValue);
        props?.onChange?.(event, newValue);
      }}
    >
      {providers?.map(({ enabled = false, providerId }) => {
        if (!providerId) return null;
        return (
          <Provider
            key={providerId}
            disabled={!enabled}
            linked={session?.user.linkedAccounts?.some(
              ({ identityProvider }) => identityProvider === providerId,
            )}
            selected={prefIDProvider.value === providerId}
            providerId={providerId}
          />
        );
      })}
    </Select>
  );
};
