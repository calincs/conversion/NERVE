'use client';

import {
  ProviderIcon,
  ProviderStatusIcon,
  type ProviderIconProps,
  type ProviderStatusIconProps,
} from '@/components/icons';
import { useIdentityProvider } from '@/hooks';
import { Option, Typography, type OptionProps, type TypographyProps } from '@mui/joy';
import { memo, useEffect, useState } from 'react';

interface Props {
  disabled?: OptionProps['disabled'];
  linked?: boolean;
  providerId: string;
  selected?: boolean;
  slotProps?: {
    root?: Omit<OptionProps, 'children' | 'label' | 'value' | 'disabled'>;
    typography?: Omit<TypographyProps, 'children'>;
    providerIcon?: ProviderIconProps;
    statusIcon?: ProviderStatusIconProps;
  };
}

export const Provider = memo(function Provider({
  disabled,
  linked,
  providerId,
  selected,
  slotProps,
}: Props) {
  const { isAccessTokenExpired } = useIdentityProvider();

  const [tokenExpired, setTokenExpired] = useState(false);

  useEffect(() => {
    void checkTokens();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const checkTokens = async () => {
    const isExpired = await isAccessTokenExpired(providerId);
    setTokenExpired(isExpired);
  };

  return (
    <Option
      key={providerId}
      disabled={disabled}
      label={providerId}
      value={providerId}
      sx={{
        justifyContent: 'space-between',
        minWidth: 160,
        mx: 0.5,
        borderRadius: 'sm',
        textTransform: 'capitalize',
      }}
      {...slotProps?.root}
    >
      <Typography
        level="body-sm"
        startDecorator={
          <ProviderIcon
            providerId={providerId}
            style={{ marginRight: 2 }}
            {...slotProps?.providerIcon}
          />
        }
        sx={{ color: ({ palette }) => palette.text.secondary }}
        {...slotProps?.typography}
      >
        {providerId}
      </Typography>
      <ProviderStatusIcon
        status={selected ? 'selected' : !linked ? 'unlinked' : tokenExpired ? 'expired' : undefined}
        {...slotProps?.statusIcon}
      />
    </Option>
  );
});
