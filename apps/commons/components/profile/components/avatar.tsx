'use client';

import { ProviderIcon, type ProviderIconProps } from '@/components/icons';
import { db } from '@/db';
import {
  Avatar as AvatarJoy,
  Badge,
  type AvatarProps as AvatarJoyProps,
  type BadgeProps,
} from '@mui/joy';
import { useLiveQuery } from 'dexie-react-hooks';
import { motion } from 'framer-motion';
import { useSession } from 'next-auth/react';

export interface AvatarProps {
  slotProps?: {
    badge?: Omit<BadgeProps, 'children'>;
    avatar?: Omit<AvatarJoyProps, 'src' | 'alt' | 'children'>;
    providerIcon?: Omit<ProviderIconProps, 'providerId'>;
  };
}

export const Avatar = ({ slotProps }: AvatarProps) => {
  const { data: session } = useSession();
  const image = session?.user.image ?? undefined;
  const name = session?.user.name;
  const username = session?.user.username;

  const prefIDProvider = useLiveQuery(() => db.userPreferences.get('identityProvider'));

  return (
    <Badge
      anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      badgeContent={
        <ProviderIcon providerId={prefIDProvider?.value} size={18} {...slotProps?.providerIcon} />
      }
      badgeInset="10%"
      component={motion.div}
      sx={{ '--Badge-paddingX': '0px', '--Badge-ring': '1px' }}
      variant="plain"
      initial={{ scale: 0 }}
      animate={{ scale: 1 }}
      {...slotProps?.badge}
    >
      <AvatarJoy
        alt={name ?? username}
        color="primary"
        size="sm"
        src={image}
        {...slotProps?.avatar}
      />
    </Badge>
  );
};
