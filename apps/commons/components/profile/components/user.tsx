'use client';

import {
  Avatar,
  Box,
  IconButton,
  Link,
  Stack,
  Typography,
  type IconButtonProps,
  type LinkProps,
  type StackProps,
  type TypographyProps,
} from '@mui/joy';
import { motion, type Variants } from 'framer-motion';
import { useSession } from 'next-auth/react';
import { useState } from 'react';
import { IconBaseProps } from 'react-icons';
import { RxReload } from 'react-icons/rx';
import { useUser } from '../../../hooks/useUser';

export interface UserProps {
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    column?: Omit<StackProps, 'children' | 'direction'>;
    row?: Omit<StackProps, 'children' | 'direction'>;
    name?: Omit<TypographyProps, 'children'>;
    manageAcountLink?: Omit<LinkProps, 'children' | 'onPointerDown'>;
    refresh?: {
      button?: Omit<IconButtonProps, 'children' | 'onPointerDown' | 'disabled'>;
      icon?: IconBaseProps;
    };
  };
}

export const User = ({ slotProps }: UserProps = {}) => {
  const { data: session } = useSession();
  const user = session?.user;

  const { manageAccount, refreshAccount } = useUser();

  const [refreshing, setRefreshing] = useState(false);

  const handleManageAccountClick = () => manageAccount();

  const handleManageRefreshClick = async () => {
    setRefreshing(true);
    await refreshAccount();
    setRefreshing(false);
  };

  const variants: Variants = {
    default: { rotate: 0 },
    animate: {
      rotate: 360,
      transition: { ease: 'linear', duration: 1, repeat: Infinity },
    },
  };

  return (
    <Stack direction="row" p={1} gap={1} {...slotProps?.root}>
      <Avatar
        alt={user?.name ?? user?.username}
        color="primary"
        size="sm"
        src={user?.image ?? undefined}
      />
      <Stack {...slotProps?.column}>
        <Typography>{user?.name}</Typography>
        <Stack direction="row" alignItems="center" gap={1} {...slotProps?.row}>
          <Link
            color="neutral"
            level="body-xs"
            onPointerDown={handleManageAccountClick}
            underline="none"
            variant="plain"
            {...slotProps?.manageAcountLink}
          >
            {user?.username}
          </Link>
          <IconButton
            disabled={refreshing}
            onPointerDown={handleManageRefreshClick}
            size="sm"
            sx={{ minWidth: 12, minHeight: 12 }}
            {...slotProps?.refresh?.button}
          >
            <Box
              display="flex"
              component={motion.div}
              variants={variants}
              initial="default"
              animate={refreshing ? 'animate' : 'default'}
            >
              <RxReload style={{ width: 8, height: 8 }} {...slotProps?.refresh?.icon} />
            </Box>
          </IconButton>
        </Stack>
      </Stack>
    </Stack>
  );
};
