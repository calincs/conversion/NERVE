'use client';

import { ThemeSwitcher } from '@/components/ui';
import { ListItem, Typography, type ListItemProps, type TypographyProps } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { useState } from 'react';
import type { IconBaseProps } from 'react-icons';
import { ImBrightnessContrast } from 'react-icons/im';

export interface ThemeProps {
  slotProps?: {
    root?: Omit<ListItemProps, 'children'>;
    typography?: Omit<TypographyProps, 'children'>;
    icon?: IconBaseProps;
  };
}

export const Theme = ({ slotProps }: ThemeProps) => {
  const t = useTranslations();
  const [hover, setHover] = useState(false);
  return (
    <ListItem
      sx={{ alignItems: 'center', justifyContent: 'space-between', gap: 1, px: 1.5 }}
      {...slotProps?.root}
      onPointerOver={(event) => {
        setHover(true);
        slotProps?.root?.onPointerOver?.(event);
      }}
      onPointerOut={(event) => {
        setHover(false);
        slotProps?.root?.onPointerOut?.(event);
      }}
    >
      <Typography
        level="body-sm"
        startDecorator={<ImBrightnessContrast style={{ marginRight: 14 }} {...slotProps?.icon} />}
        sx={{ color: ({ palette }) => (hover ? palette.text.secondary : palette.text.tertiary) }}
        {...slotProps?.typography}
      >
        {t('Theme.theme')}
      </Typography>
      <ThemeSwitcher />
    </ListItem>
  );
};
