'use client';

import { getTsrLincsAuth } from '@/auth/lincs/auth-api';
import { usePathname } from '@/navigation';
import { ClickAwayListener } from '@mui/base/ClickAwayListener';
import {
  Divider,
  Dropdown,
  IconButton,
  List,
  Menu,
  MenuButton,
  type MenuButtonProps,
  type MenuProps,
} from '@mui/joy';
import { QueryClient } from '@tanstack/react-query';
import { useSession } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import { useState } from 'react';
import { MdOutlinePrivacyTip } from 'react-icons/md';
import { PiSignOutBold } from 'react-icons/pi';
import { TbSettings2 } from 'react-icons/tb';
import { MenuItemButton } from '../ui';
import { Identity, Language, Theme, User } from './components';
import { Avatar, type AvatarProps } from './components/avatar';
import { useProfile } from './useProfile';
import { useAtomValue } from 'jotai';
import { lincsAuthApiUrlAtom } from '@/jotai/store';

export interface ProfileProps {
  slotProps?: {
    avatar?: AvatarProps;
    menuButton?: MenuButtonProps;
    menu?: MenuProps;
  };
}

const queryClient = new QueryClient();

export const Profile = ({ slotProps }: ProfileProps) => {
  const pathname = usePathname();
  const { data: session } = useSession();
  const t = useTranslations();

  const { handlePrivacyClick, handleOpenNerveSettings, handleSignout, nerveActions } = useProfile();

  const lincsAuthApiUr = useAtomValue(lincsAuthApiUrlAtom);

  const [open, setOpen] = useState(false);

  const prefetch = async () => {
    await queryClient.prefetchQuery({
      queryKey: ['providers'],
      queryFn: () => getTsrLincsAuth(lincsAuthApiUr).providers.getAll.query(),
    });
  };

  return (
    <Dropdown
      open={open}
      onOpenChange={(_event, isOpen) => {
        if (isOpen) setOpen(isOpen);
      }}
    >
      {session && (
        <MenuButton
          slots={{ root: IconButton }}
          sx={{ ':hover': { backgroundColor: 'transparent' } }}
          variant="plain"
          {...slotProps?.menuButton}
          onPointerOver={(event) => {
            prefetch();
            slotProps?.menuButton?.onPointerOver?.(event);
          }}
        >
          <Avatar {...slotProps?.avatar} />
        </MenuButton>
      )}
      <ClickAwayListener onClickAway={() => setOpen(false)}>
        <Menu placement="bottom-end" sx={{ minWidth: 280 }} variant="soft" {...slotProps?.menu}>
          <User />
          <Divider sx={{ mb: 0.5 }} />
          <List sx={{ gap: 1, py: 1 }}>
            {!nerveActions && <Identity />}
            <MenuItemButton Icon={MdOutlinePrivacyTip} onPointerDown={handlePrivacyClick}>
              {t('common.privacy settings')}
            </MenuItemButton>
            <Theme />
            <Language />
            {pathname === '/edit' && (
              <MenuItemButton Icon={TbSettings2} onPointerDown={handleOpenNerveSettings}>
                {t('common.settings')}
              </MenuItemButton>
            )}
          </List>
          <Divider sx={{ mb: 0.5 }} />
          <MenuItemButton Icon={PiSignOutBold} onPointerDown={handleSignout}>
            {t('common.signOut')}
          </MenuItemButton>
        </Menu>
      </ClickAwayListener>
    </Dropdown>
  );
};
