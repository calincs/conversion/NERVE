'use client';

import { Grid, Sheet, type SheetProps } from '@mui/joy';

interface TopBarProps {
  center?: React.ReactNode;
  left?: React.ReactNode;
  right?: React.ReactNode;
  variant?: 'fixed' | 'float';
  slotProps?: {
    root?: Omit<SheetProps, 'children'>;
  };
}

export const TopBar = ({ center, left, right, variant = 'fixed', slotProps }: TopBarProps) => (
  <Sheet
    sx={{
      position: variant === 'float' ? 'absolute' : 'inherit',
      zIndex: 1000,
      width: '100vw',
      backgroundColor: 'transparent',
    }}
    {...slotProps?.root}
  >
    <Grid
      container
      p={1}
      sx={{ flexGrow: 1 }}
      columns={3}
      justifyContent="space-evenly"
      alignItems="center"
    >
      <Grid xs pl={1}>
        {left}
      </Grid>
      <Grid xs display="flex" justifyContent="center">
        {center}
      </Grid>
      <Grid xs display="flex" justifyContent="flex-end">
        {right}
      </Grid>
    </Grid>
  </Sheet>
);
