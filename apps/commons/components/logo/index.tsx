'use client';

import { useColorScheme } from '@mui/joy';
import { getImageProps, type ImageProps } from 'next/image';
import { getDimensions, getVariationDimensions } from './helper';

export type Direction = 'horizontal' | 'vertical';
export type Variation = 'full' | 'simplified' | 'icon';

export interface LogoProps {
  height?: number;
  width?: number;
  direction?: Direction;
  responsive?: boolean;
  style?: React.CSSProperties;
  variation?: Variation;
}

export const Logo = ({
  height,
  width,
  direction = 'horizontal',
  responsive,
  style,
  variation = 'simplified',
}: LogoProps) => {
  const { mode } = useColorScheme();

  const variationDimension = getVariationDimensions(direction, variation);
  const dimensions = getDimensions(variationDimension, { width, height });

  const common: Omit<ImageProps, 'src'> = {
    alt: 'NERVE',
    width: dimensions.width,
    height: dimensions.height,
    priority: true,
    style: {
      width: responsive ? '100%' : 'inherit',
      height: responsive ? 'auto' : 'inherit',
      ...style,
    },
  };

  const {
    props: { srcSet: dark },
  } = getImageProps({ ...common, src: `/logo/webp/nerve_${direction}_${variation}_dark.webp` });

  const {
    props: { srcSet: light, ...rest },
  } = getImageProps({ ...common, src: `/logo/webp/nerve_${direction}_${variation}_light.webp` });

  return (
    <picture style={{ display: 'inline-flex', alignItems: 'center' }}>
      {mode === 'dark' ? (
        <source srcSet={dark} />
      ) : mode === 'light' ? (
        <source srcSet={light} />
      ) : (
        <>
          <source media="(prefers-color-scheme: dark)" srcSet={dark} />
          <source media="(prefers-color-scheme: light)" srcSet={light} />
        </>
      )}
      <img {...rest} />
    </picture>
  );
};
