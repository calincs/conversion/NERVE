import type { Direction, Variation } from './index';

interface Dimensions {
  height: number;
  width: number;
}

export const getDimensions = (
  dimension: Dimensions,
  { width, height }: Partial<Dimensions>,
): Dimensions => {
  if (width) {
    const ratio = dimension.height / dimension.width;
    return { width, height: width * ratio };
  }

  if (height) {
    const ratio = dimension.width / dimension.height;
    return { width: height * ratio, height };
  }

  return dimension;
};

export const getVariationDimensions = (direction: Direction, variation: Variation): Dimensions => {
  if (direction === 'vertical') {
    if (variation === 'icon') return { width: 250, height: 200 };
    if (variation === 'simplified') return { width: 320, height: 360 };
    return { width: 320, height: 400 };
  }
  if (variation === 'icon') return { width: 200, height: 250 };
  if (variation === 'simplified') return { width: 500, height: 160 };
  return { width: 500, height: 200 };
};
