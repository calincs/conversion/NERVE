'use client';

import { usePathname } from '@/navigation';
import { ClickAwayListener } from '@mui/base/ClickAwayListener';
import {
  Dropdown,
  IconButton,
  List,
  Menu,
  MenuButton,
  type ListProps,
  type MenuButtonProps,
  type MenuProps,
} from '@mui/joy';
import { useTranslations } from 'next-intl';
import { useState } from 'react';
import { MdOutlinePrivacyTip } from 'react-icons/md';
import { TbSettings, TbSettings2 } from 'react-icons/tb';
import { MenuItemButton } from '../ui';
import { Language, Theme } from './components';
import { useProfile } from './useProfile';

export interface ProfileAnonymousProps {
  slotProps?: {
    menuButton?: MenuButtonProps;
    menu?: Omit<MenuProps, 'children'>;
    list?: Omit<ListProps, 'children'>;
  };
}

export const ProfileAnonymous = ({ slotProps }: ProfileAnonymousProps) => {
  const pathname = usePathname();
  const t = useTranslations();

  const { handlePrivacyClick, handleOpenNerveSettings } = useProfile();

  const [open, setOpen] = useState(false);

  return (
    <Dropdown
      open={open}
      onOpenChange={(_event, isOpen) => {
        if (isOpen) setOpen(isOpen);
      }}
    >
      <MenuButton
        slots={{ root: IconButton }}
        sx={{ ':hover': { backgroundColor: 'transparent' } }}
        variant="plain"
        {...slotProps?.menuButton}
      >
        <TbSettings />
      </MenuButton>

      <ClickAwayListener onClickAway={() => setOpen(false)}>
        <Menu placement="bottom-end" sx={{ minWidth: 280 }} variant="soft" {...slotProps?.menu}>
          <List sx={{ gap: 1, py: 1 }} {...slotProps?.list}>
            <MenuItemButton Icon={MdOutlinePrivacyTip} onPointerDown={handlePrivacyClick}>
              {t('common.privacy settings')}
            </MenuItemButton>
            <Theme />
            <Language />
            {pathname === '/edit' && (
              <MenuItemButton Icon={TbSettings2} onPointerDown={handleOpenNerveSettings}>
                {t('common.settings')}
              </MenuItemButton>
            )}
          </List>
        </Menu>
      </ClickAwayListener>
    </Dropdown>
  );
};
