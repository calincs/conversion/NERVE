import { nerveAtoms } from '@/app/[locale]/edit/nerve/store';
import { useAtomValue } from 'jotai';
import * as CookieConsent from 'vanilla-cookieconsent';

export const useProfile = () => {
  const nerveActions = useAtomValue(nerveAtoms.nerveActions);

  const handlePrivacyClick = () => {
    CookieConsent.showPreferences();
  };

  const handleOpenNerveSettings = async () => {
    nerveActions?.openSettingsPanel();
  };

  return {
    handleOpenNerveSettings,
    handlePrivacyClick,
    nerveActions,
  };
};
