'use client';

import {
  Button,
  CircularProgress,
  Divider,
  Modal,
  Sheet,
  Stack,
  ToggleButtonGroup,
  type SheetProps,
} from '@mui/joy';
import { motion, type Variants } from 'framer-motion';
import { useSession } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import { useEffect, useState } from 'react';
import { Main, type ViewType } from './main';
import { SidePanel } from './sidemenu';
import { useFilePanel } from './useFilePanel';

export interface FilePanelProps {
  slotProps?: {
    root?: Omit<SheetProps, 'children'>;
  };
}

export const FilePanel = ({ slotProps }: FilePanelProps) => {
  const { status: sessionStatus } = useSession();
  const t = useTranslations();

  const { isProcessingPermalink, recent, samples } = useFilePanel();

  const showRecent = sessionStatus === 'authenticated' && recent && recent?.length > 0;

  const [selectedView, setSelectedView] = useState<ViewType>(showRecent ? 'recent' : 'sample');

  useEffect(() => {
    setSelectedView(showRecent ? 'recent' : 'sample');
  }, [showRecent]);

  const variants: Variants = {
    initial: { opacity: 0 },
    default: { opacity: 1 },
  };

  return (
    <Sheet
      component={motion.div}
      sx={{
        display: { xs: 'none', sm: 'flex' },
        width: { sm: 600, md: 800, lg: 1000 },
        minHeight: { sm: 320, lg: showRecent ? 400 : 320 },
        overflow: 'hidden',
        backgroundColor: 'transparent',
        transition: 'all 0.4s ease-in',
      }}
      variants={variants}
      initial="initial"
      animate={recent === undefined ? 'initial' : 'default'}
      {...slotProps?.root}
    >
      <SidePanel />
      <Sheet sx={{ p: 1, width: { sm: 360, md: 560, lg: 760 }, backgroundColor: 'transparent' }}>
        <Stack gap={1}>
          <ToggleButtonGroup
            onChange={(_event, newValue) => newValue && setSelectedView(newValue)}
            size="sm"
            spacing={1}
            sx={{ justifyContent: 'center' }}
            value={selectedView}
            variant="plain"
          >
            {showRecent && (
              <Button value="recent" variant={selectedView === 'recent' ? 'soft' : 'plain'}>
                {t('FilePanel.recent')}
              </Button>
            )}
            <Button value="sample" variant={selectedView === 'sample' ? 'soft' : 'plain'}>
              {t('FilePanel.sample')}
            </Button>
          </ToggleButtonGroup>
          <Divider />
          <Main
            collection={selectedView === 'recent' ? recent : samples}
            selectedView={selectedView}
          />
        </Stack>
      </Sheet>
      <Modal
        open={isProcessingPermalink}
        sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
      >
        <CircularProgress />
      </Modal>
    </Sheet>
  );
};
