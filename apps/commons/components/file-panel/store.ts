import { Resource } from '@/types';
import { atom } from 'jotai';
import { atomWithReset } from 'jotai/utils';
import { DialogSource, Props as StorageDialogProps } from '../dialogs/storage-dialog';

export const storageServiceDialogAtom = atomWithReset<StorageDialogProps>({
  open: false,
});
storageServiceDialogAtom.debugLabel = 'StorageServiceDialog.Atom';

export const openLoadStorageServiceDialogAtom = atom(
  null,
  (_get, set, update: { resource?: Resource | string; source: DialogSource }) => {
    set(storageServiceDialogAtom, {
      open: true,
      type: 'load',
      onClose: () => set(storageServiceDialogAtom, { open: false }),
      ...update,
    });
  },
);

export const openSaveStorageServiceDialogAtom = atom(
  null,
  (
    _get,
    set,
    update: {
      resource?: Resource;
      source: Extract<DialogSource, 'cloud'>;
      _onClose?: (resource?: Resource) => void;
    },
  ) => {
    const { _onClose } = update;
    set(storageServiceDialogAtom, {
      open: true,
      type: 'save',
      onClose: (resource) => {
        set(storageServiceDialogAtom, { open: false });
        _onClose?.(resource);
      },
      ...update,
    });
  },
);

export const filePanelAtoms = {
  storageServiceDialog: storageServiceDialogAtom,
  loadStorageServiceDialog: openLoadStorageServiceDialogAtom,
  saveStorageServiceDialog: openSaveStorageServiceDialogAtom,
};
