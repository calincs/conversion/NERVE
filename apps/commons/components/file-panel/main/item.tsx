'use client';

import { db } from '@/db';
import { useOpenResource } from '@/hooks';
import { LocalDbDocument } from '@/types';
import {
  IconButton,
  ListItem,
  ListItemButton,
  Stack,
  Typography,
  useColorScheme,
  type ListItemButtonProps,
  type ListItemProps,
} from '@mui/joy';
import { formatDistanceToNow } from 'date-fns';
import { AnimatePresence, motion } from 'framer-motion';
import { AiOutlineClockCircle } from 'react-icons/ai';
import { BiMinusCircle } from 'react-icons/bi';
import { Details } from './details';

export interface ItemProps {
  data: LocalDbDocument;
  isSample?: boolean;
  selected?: ListItemButtonProps['selected'];
  onSelect: (id: string) => void;
  slotProps?: {
    root?: Omit<ListItemProps, 'children'>;
    button?: Omit<ListItemButtonProps, 'children' | 'onSelect'>;
  };
}

export const Item = ({ data, isSample, selected, onSelect, slotProps }: ItemProps) => {
  const { mode, systemMode } = useColorScheme();

  const { openResource } = useOpenResource();

  const isDark = mode === 'dark' || (mode === 'system' && systemMode === 'dark');

  const { resource, id, lastOpenedAt } = data;
  const filename = 'filename' in resource ? resource.filename : `untitled-${id}`;
  const lastDate =
    lastOpenedAt &&
    formatDistanceToNow(new Date(lastOpenedAt), {
      includeSeconds: true,
      addSuffix: true,
    });

  const handleSelected = () => {
    if (!selected) onSelect(id);
  };

  const handleOpen = async () => {
    if (!selected) onSelect(id);
    openResource(resource);
  };

  const handleRemove = async () => await db.recentDocuments.delete(id);

  return (
    <ListItem
      color={selected ? 'primary' : 'neutral'}
      variant={selected ? 'outlined' : 'plain'}
      component={motion.li}
      sx={{
        mx: selected ? 0 : 1,
        borderRadius: 4,
        overflow: 'hidden',
      }}
      layout="preserve-aspect"
      animate={{ height: 'auto', opacity: 1 }}
      exit={{ height: 0, opacity: 0 }}
      {...slotProps?.root}
    >
      <ListItemButton
        color={isDark ? 'primary' : 'neutral'}
        selected={selected}
        {...slotProps?.button}
        onPointerDown={(event) => {
          handleSelected();
          slotProps?.button?.onPointerDown?.(event);
        }}
        onDoubleClick={(event) => {
          handleOpen();
          slotProps?.button?.onDoubleClick?.(event);
        }}
      >
        <Stack sx={{ p: 1 }} width="100%" gap={0.5}>
          <Stack direction="row" gap={2} alignItems="center" justifyContent="space-between">
            <Stack>
              <Typography level="title-sm">{filename}</Typography>
              {lastDate && (
                <Stack direction="row" alignItems="center" gap={0.25}>
                  <AiOutlineClockCircle size={10} />
                  <Typography level="body-xs">{lastDate}</Typography>
                </Stack>
              )}
            </Stack>
            {selected && !isSample && (
              <IconButton onPointerDown={handleRemove} size="sm" sx={{ borderRadius: '50%' }}>
                <BiMinusCircle />
              </IconButton>
            )}
          </Stack>
          <AnimatePresence>{selected && !isSample && <Details document={data} />}</AnimatePresence>
        </Stack>
      </ListItemButton>
    </ListItem>
  );
};
