'use client';

import { ProviderIcon } from '@/components/icons';
import { LocalDbDocument } from '@/types';
import { Box, Stack, type BoxProps, type StackProps } from '@mui/joy';
import { MdLanguage, MdOutlineComputer } from 'react-icons/md';

interface Props extends React.PropsWithChildren {
  resource: LocalDbDocument['resource'];
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    box?: Omit<BoxProps, 'children'>;
  };
}

export const Header = ({ children, resource, slotProps }: Props) => {
  return (
    <Stack
      direction="row"
      alignItems="flex-start"
      justifyContent="space-between"
      gap={0.5}
      {...slotProps?.root}
    >
      {/* <Typography fontWeight={700} level="body-xs" sx={{ textTransform: 'capitalize' }}>
        {children}
      </Typography> */}
      <Box height={16} width={16} {...slotProps?.box}>
        {resource.storageSource === 'cloud' ? (
          <ProviderIcon providerId={resource.provider} />
        ) : resource.storageSource === 'url' ? (
          <MdLanguage />
        ) : (
          <MdOutlineComputer />
        )}
      </Box>
    </Stack>
  );
};
