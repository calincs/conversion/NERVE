import { type LocalDbDocument } from '@/types';
import { Typography, type TypographyProps } from '@mui/joy';

export interface SourceInfoProps {
  resource: LocalDbDocument['resource'];
  slotProps?: {
    root?: Omit<TypographyProps, 'children'>;
    owner?: Omit<TypographyProps, 'children'>;
    repo?: Omit<TypographyProps, 'children'>;
  };
}

export const SourceInfo = ({ resource, slotProps }: SourceInfoProps) => {
  return (
    <Typography level="body-xs" pt="1px" sx={{ whiteSpace: 'nowrap' }} {...slotProps?.root}>
      {resource.storageSource === 'cloud' ? (
        <>
          <Typography mr={0.5} variant="soft" {...slotProps?.owner}>
            {resource.owner}
          </Typography>
          <Typography mr={0.5} variant="soft" {...slotProps?.repo}>
            {resource.repo}
          </Typography>
          {resource.path !== '' && `${resource.path}/`}
          {resource.filename}
        </>
      ) : resource.storageSource === 'url' ? (
        resource.url
      ) : (
        'local storage'
      )}
    </Typography>
  );
};
