'use client';

import type { LocalDbDocument } from '@/types';
import { Divider, Stack, type DividerProps, type StackProps } from '@mui/joy';
import { motion } from 'framer-motion';
import { useTranslations } from 'next-intl';
import { Header } from './header';
import { SourceInfo } from './sourceInfo';

export interface DetailsProps {
  document: LocalDbDocument;
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    divider?: DividerProps;
    stack?: Omit<StackProps, 'children'>;
  };
}

export const Details = ({ document: localDocument, slotProps }: DetailsProps) => {
  const { annotations, resource } = localDocument;

  const t = useTranslations();

  return (
    <Stack
      component={motion.div}
      gap={1}
      initial={{ height: 0 }}
      animate={{ height: 'auto' }}
      exit={{ height: 0 }}
      overflow="hidden"
      {...slotProps?.root}
    >
      <Divider {...slotProps?.divider} />
      <Stack direction="row" gap={0.5}>
        <Stack {...slotProps?.stack}>
          <Header resource={resource}>{t('common.document')}</Header>
          {typeof annotations === 'object' && annotations.source && (
            <Header resource={annotations.source}>{t('common.annotations')}</Header>
          )}
        </Stack>
        <Stack overflow="scroll">
          {<SourceInfo resource={resource} />}
          {typeof annotations === 'object' && annotations.source && (
            <SourceInfo resource={annotations.source} />
          )}
        </Stack>
      </Stack>
    </Stack>
  );
};
