'use client';

import { RecentDocument } from '@/db';
import { Stack, type StackProps } from '@mui/joy';
import { AnimatePresence } from 'framer-motion';
import { useSession } from 'next-auth/react';
import { useState } from 'react';
import { Item } from './item';

export type ViewType = 'recent' | 'sample';

export interface MainProps {
  collection: RecentDocument[];
  selectedView: ViewType;
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
  };
}

export const Main = ({ collection, selectedView, slotProps }: MainProps) => {
  const { status: sessionStatus } = useSession();
  const [selected, setSelected] = useState<string | null>(null);

  return (
    <Stack
      direction={selectedView === 'sample' ? 'row' : 'column'}
      flexWrap={selectedView === 'sample' ? 'wrap' : 'nowrap'}
      alignContent="flex-start"
      alignItems={selectedView === 'sample' ? 'flex-start' : 'stretch'}
      height={{
        sm: sessionStatus === 'authenticated' ? 500 : 430,
        md: sessionStatus === 'authenticated' ? 500 : 400,
        lg: sessionStatus === 'authenticated' ? 500 : 325,
      }}
      overflow="auto"
      py={1}
      gap={1}
      {...slotProps?.root}
    >
      <AnimatePresence>
        {collection.map((document) => (
          <Item
            key={document.id}
            data={{ ...document }}
            isSample={selectedView === 'sample'}
            onSelect={(id: string) => setSelected(selected !== id ? id : null)}
            selected={selected === document.id}
          />
        ))}
      </AnimatePresence>
    </Stack>
  );
};
