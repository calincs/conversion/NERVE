'use client';

import { db, type RecentDocument } from '@/db';
import { useOpenResource, usePermalink } from '@/hooks';
import { loadCollection } from '@/server-actions';
import { useLiveQuery } from 'dexie-react-hooks';
import { useSetAtom } from 'jotai';
import { nanoid } from 'nanoid';
import { useSession } from 'next-auth/react';
import { useSearchParams } from 'next/navigation';
import { useEffect, useState } from 'react';
import { filePanelAtoms } from './store';

export const useFilePanel = () => {
  const searchParams = useSearchParams();
  const { status: sessionStatus } = useSession();

  const { openResource } = useOpenResource();
  const { getResourceFromPermalink } = usePermalink();

  const loadStorageServiceDialog = useSetAtom(filePanelAtoms.loadStorageServiceDialog);

  const recent = useLiveQuery(
    () => db.recentDocuments.toCollection().reverse().sortBy('lastOpenedAt'),
    [],
    [],
  );

  const [isProcessingPermalink, setIsProcessingPermalink] = useState(false);
  const [samples, setSamples] = useState<RecentDocument[]>([]);

  useEffect(() => {
    loadSamples();
  }, []);

  useEffect(() => {
    searchParams.size !== 0 && processPermalink();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sessionStatus]);

  const loadSamples = async () => {
    const samples = await loadCollection('samples');
    const sampleCollection = samples.map((sample) => ({
      id: nanoid(),
      resource: { ...sample, isSample: true },
    }));
    setSamples(sampleCollection);
  };

  const processPermalink = async () => {
    setIsProcessingPermalink(true);
    const resource = await getResourceFromPermalink();
    setIsProcessingPermalink(false);

    if (!resource) return;

    if (resource.storageSource === 'url') {
      openResource(resource);
      return;
    }

    if (resource.storageSource === 'cloud') {
      if (resource.filename) {
        openResource(resource);
        return;
      }

      if (sessionStatus === 'authenticated') {
        loadStorageServiceDialog({ source: 'cloud', resource });
      }

      return;
    }
  };

  return {
    isProcessingPermalink,
    recent,
    samples,
  };
};
