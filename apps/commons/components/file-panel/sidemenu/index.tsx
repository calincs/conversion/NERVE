import {
  MenuList,
  Stack,
  Typography,
  type MenuListProps,
  type StackProps,
  type TypographyProps,
} from '@mui/joy';
import { useSetAtom } from 'jotai';
import { useSession } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import { BsCloud } from 'react-icons/bs';
import { GoPaste } from 'react-icons/go';
import { MdComputer, MdLanguage } from 'react-icons/md';
import { filePanelAtoms } from '../store';
import { Item } from './item';

export interface SidePanel {
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    menuList?: Omit<MenuListProps, 'children'>;
    title?: Omit<TypographyProps, 'children'>;
  };
}

export const SidePanel = ({ slotProps }: SidePanel) => {
  const { data: session } = useSession();
  const t = useTranslations();

  const loadStorageServiceDialog = useSetAtom(filePanelAtoms.loadStorageServiceDialog);

  return (
    <Stack width={240} gap={3} py={2} px={2} {...slotProps?.root}>
      <Typography
        level="title-lg"
        letterSpacing={3}
        ml={6.5}
        textTransform="uppercase"
        {...slotProps?.title}
      >
        {t('FilePanel.open')}
      </Typography>
      <MenuList
        sx={{ backgroundColor: 'transparent', gap: 0.25 }}
        variant="plain"
        {...slotProps?.menuList}
      >
        <Item
          decorator={<BsCloud />}
          disabled={!session}
          onPointerDown={() => {
            loadStorageServiceDialog({ source: 'cloud' });
          }}
          tooltip={
            !session
              ? t('FilePanel.You must sign in to open and save documents from the cloud')
              : undefined
          }
        >
          {t('FilePanel.From the Cloud')}
        </Item>
        <Item
          decorator={<MdLanguage />}
          onPointerDown={() => loadStorageServiceDialog({ source: 'url' })}
        >
          {t('FilePanel.From URL')}
        </Item>
        <Item
          decorator={<MdComputer />}
          onPointerDown={() => loadStorageServiceDialog({ source: 'local' })}
        >
          {t('FilePanel.From your device')}
        </Item>
        <Item
          decorator={<GoPaste />}
          onPointerDown={() => loadStorageServiceDialog({ source: 'paste' })}
        >
          {t('FilePanel.Paste')}
        </Item>
      </MenuList>
    </Stack>
  );
};
