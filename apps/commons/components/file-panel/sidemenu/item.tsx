import {
  ListItemDecorator,
  MenuItem,
  Tooltip,
  Typography,
  type MenuItemProps,
  type TooltipProps,
} from '@mui/joy';

export interface Props extends MenuItemProps {
  decorator?: React.ReactNode;
  tooltip?: TooltipProps['title'];
  slotProps?: {
    root?: never;
    tooltip?: Omit<TooltipProps, 'title'>;
  };
}

export const Item = ({ children, decorator, tooltip, slotProps, ...props }: Props) => (
  <Tooltip
    arrow
    placement="top"
    size="sm"
    title={
      tooltip && (
        <Typography level="body-xs" maxWidth={200}>
          {tooltip}
        </Typography>
      )
    }
    variant="soft"
    {...slotProps?.tooltip}
  >
    <span>
      <MenuItem
        sx={{
          borderRadius: 'sm',
          '[data-mui-color-scheme="dark"] &': {
            ':hover': {
              backgroundColor: 'primary.800',
            },
          },
        }}
        {...props}
      >
        {decorator && <ListItemDecorator>{decorator}</ListItemDecorator>}
        {children}
      </MenuItem>
    </span>
  </Tooltip>
);
