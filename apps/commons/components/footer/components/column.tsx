import { Stack, Typography, type StackProps, type TypographyProps } from '@mui/joy';

interface ColumnProps extends React.PropsWithChildren {
  heading: TypographyProps['children'];
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    typography?: Omit<TypographyProps, 'children'>;
  };
}

export const Column = ({ children, heading, slotProps }: ColumnProps) => (
  <Stack alignItems="flex-start" gap={0.5} {...slotProps?.root}>
    <Typography level="title-sm" fontWeight={700} pb={1} {...slotProps?.typography}>
      {heading}
    </Typography>
    {children}
  </Stack>
);
