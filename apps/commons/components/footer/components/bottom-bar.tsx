import { LocaleSwitcher, ThemeSwitcher } from '@/components/ui';
import { Grid, type GridProps, Link } from '@mui/joy';
import { GrCreativeCommons } from 'react-icons/gr';

export interface BottomBarProps {
  slotProps?: {
    root?: Omit<GridProps, 'children'>;
  };
}

export const BottomBar = ({ slotProps }: BottomBarProps) => (
  <Grid
    container
    width="100%"
    sx={{ flexGrow: 1 }}
    columns={3}
    justifyContent="space-evenly"
    {...slotProps?.root}
  >
    <Grid xs>
      <Link
        color="neutral"
        flexGrow={1}
        href="https://creativecommons.org/"
        level="body-xs"
        px={1}
        startDecorator={<GrCreativeCommons />}
        target="_blank"
      >
        Creative Commons
      </Link>
    </Grid>
    <Grid xs display="flex" justifyContent="center">
      <ThemeSwitcher bordered />
    </Grid>
    <Grid xs display="flex" justifyContent="flex-end">
      <LocaleSwitcher backgroundColor="transparent" />
    </Grid>
  </Grid>
);
