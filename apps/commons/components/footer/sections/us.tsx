import { Logo, type LogoProps } from '@/components/logo';
import { Link, Stack, Typography, type StackProps } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { AiOutlineMail } from 'react-icons/ai';
import { RiTwitterXFill } from 'react-icons/ri';
import { SlSocialYoutube } from 'react-icons/sl';

export interface UsProps {
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    stack?: Omit<StackProps, 'children'>;
    logo?: LogoProps;
  };
}

export const Us = ({ slotProps }: UsProps) => {
  const t = useTranslations();
  return (
    <Stack justifyContent="space-between" height="100%" spacing={2} {...slotProps?.root}>
      <Stack spacing={1} alignItems={{ xs: 'center', sm: 'flex-start' }} {...slotProps?.stack}>
        <Logo height={32} {...slotProps?.logo} />
        <Typography level="title-sm" sx={{ mt: '0px !important' }}>
          {t('meta.description')}
        </Typography>
        <Stack
          direction={{ xs: 'row', sm: 'column' }}
          pt={{ xs: 2, sm: 1 }}
          spacing={{ xs: 1.5, sm: 1 }}
          alignItems={{ xs: 'center', sm: 'flex-start' }}
        >
          <Link color="neutral" href="mailto:lincs@uoguelph.ca" level="body-xs" underline="hover">
            <Stack direction="row" alignItems="center" gap={1}>
              <AiOutlineMail />
              {t('common.emailUs')}
            </Stack>
          </Link>
          <Link
            color="neutral"
            href="https://x.com/cwrcproject"
            target="_blank"
            level="body-xs"
            underline="hover"
          >
            <Stack direction="row" alignItems="center" gap={1}>
              <RiTwitterXFill />
              Twitter
            </Stack>
          </Link>
          <Link
            color="neutral"
            href="https://www.youtube.com/channel/UCuLMD1dLD6_AAMeXlyZgzQQ"
            level="body-xs"
            target="_blank"
            underline="hover"
          >
            <Stack direction="row" alignItems="center" gap={1}>
              <SlSocialYoutube />
              YouTube
            </Stack>
          </Link>
        </Stack>
      </Stack>
    </Stack>
  );
};
