import { Link } from '@mui/joy';

export const MoreTools = () => (
  <>
    <Link color="neutral" href="https://lincsproject.ca" level="body-xs" target="_blank">
      LINCS Project
    </Link>
    <Link color="neutral" href="https://www.leaf-vre.org" level="body-xs" target="_blank">
      LEAF-VRE
    </Link>
    <Link color="neutral" href="https://dtoc.leaf-vre.org" level="body-xs" target="_blank">
      DToC
    </Link>
    <Link color="neutral" href="https://leaf-writer.leaf-vre.org" level="body-xs" target="_blank">
      LEAF-Writer
    </Link>
    <Link color="neutral" disabled level="body-xs" target="_blank">
      VERSD
    </Link>
  </>
);
