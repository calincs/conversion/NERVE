'use client';

import { Link } from '@/navigation';
import { Link as JoyLink } from '@mui/joy';
import { useTranslations } from 'next-intl';
import * as CookieConsent from 'vanilla-cookieconsent';

export const Privacy = () => {
  const t = useTranslations();
  return (
    <>
      <Link href="/privacy">
        <JoyLink color="neutral" component="span" level="body-xs">
          {t('Footer.privacy policy')}
        </JoyLink>
      </Link>
      <JoyLink
        color="neutral"
        href="#"
        level="body-xs"
        onPointerDown={() => CookieConsent.showPreferences()}
      >
        {t('Footer.cookie preferences')}
      </JoyLink>
    </>
  );
};
