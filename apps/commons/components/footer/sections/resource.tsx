import { Link } from '@/navigation';
import { Link as JoyLink } from '@mui/joy';
import { useTranslations } from 'next-intl';

export const Resource = () => {
  const t = useTranslations();
  return (
    <>
      <Link href="/about" style={{ textDecoration: 'none', lineHeight: '1rem' }}>
        <JoyLink
          color="neutral"
          component="span"
          level="body-xs"
          sx={{ textTransform: 'capitalize' }}
          underline="hover"
        >
          {t('common.about')}
        </JoyLink>
      </Link>
      <JoyLink color="neutral" disabled level="body-xs">
        {t('Footer.documentation')}
      </JoyLink>
      <JoyLink
        color="neutral"
        href="https://gitlab.com/calincs/conversion/lincs-webannotation"
        level="body-xs"
        target="_blank"
      >
        Web Annotation Model
      </JoyLink>
      <JoyLink
        color="neutral"
        href="https://gitlab.com/calincs/conversion/NERVE/-/blob/main/apps/commons/CHANGELOG.md"
        level="body-xs"
        target="_blank"
      >
        {t('Footer.changelog')}
      </JoyLink>{' '}
      <JoyLink
        color="neutral"
        href="https://gitlab.com/calincs/conversion/NERVE/-/issues/new"
        level="body-xs"
        target="_blank"
      >
        {t('Footer.report bugs')}
      </JoyLink>
      <JoyLink
        color="neutral"
        href="https://gitlab.com/calincs/conversion/NERVE"
        level="body-xs"
        target="_blank"
      >
        GitLab Repository
      </JoyLink>
    </>
  );
};
