import { Link } from '@mui/joy';
import { useTranslations } from 'next-intl';

export const Sponsors = () => {
  const t = useTranslations();
  return (
    <>
      <Link color="neutral" href="https://www.innovation.ca/" level="body-xs" target="_blank">
        {t('about.sponsors.cfi')}
      </Link>
    </>
  );
};
