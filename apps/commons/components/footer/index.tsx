import { Grid, Sheet, Stack, type GridProps, type SheetProps, type StackProps } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { BottomBar } from './components/bottom-bar';
import { Column } from './components/column';
import { MoreTools, Privacy, Resource, Us } from './sections';

export interface FooterProps {
  slotProps?: {
    root?: Omit<SheetProps, 'children'>;
    stack?: Omit<StackProps, 'children'>;
    grid?: Omit<GridProps, 'children'>;
  };
}

export const Footer = ({ slotProps }: FooterProps) => {
  const t = useTranslations();

  return (
    <Sheet sx={{ display: 'flex', justifyContent: 'center', p: 2 }} {...slotProps?.root}>
      <Stack maxWidth={1000} width="100%" alignItems="center" gap={2} {...slotProps?.stack}>
        <Grid
          container
          width="100%"
          spacing={{ xs: 2, sm: 3, md: 2 }}
          sx={{ flexGrow: 1 }}
          py={4}
          pb={7}
          {...slotProps?.grid}
        >
          <Grid xs={12} sm={3} md={6} mb={{ xs: 2, sm: 0 }}>
            <Us />
          </Grid>
          <Grid xs={4} sm={3} md={2}>
            <Column heading={t('Footer.resources')}>
              <Resource />
            </Column>
          </Grid>
          <Grid xs={4} sm={3} md={2}>
            <Column heading={t('common.privacy')}>
              <Privacy />
            </Column>
          </Grid>
          <Grid xs={4} sm={3} md={2}>
            <Column heading={t('Footer.more tools')}>
              <MoreTools />
            </Column>
          </Grid>
        </Grid>
        <BottomBar />
      </Stack>
    </Sheet>
  );
};
