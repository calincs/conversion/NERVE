'use client';

import { IconButton, Tooltip, type IconButtonProps, type TooltipProps } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { MdOutlinePrivacyTip } from 'react-icons/md';
import * as CookieConsent from 'vanilla-cookieconsent';

export interface CookiesPreferenceButtonProps {
  slotProps?: {
    root?: Omit<IconButtonProps, 'onPointerDown'>;
    tooltip?: Omit<TooltipProps, 'children' | 'title'>;
  };
}

export const CookiesPreferenceButton = ({ slotProps }: CookiesPreferenceButtonProps) => {
  const t = useTranslations();

  return (
    <Tooltip size="sm" title={t('Footer.cookie preferences')} {...slotProps?.tooltip}>
      <IconButton
        onPointerDown={() => CookieConsent.showPreferences()}
        size="sm"
        {...slotProps?.root}
      >
        <MdOutlinePrivacyTip />
      </IconButton>
    </Tooltip>
  );
};
