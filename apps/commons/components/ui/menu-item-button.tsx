'use client';

import { MenuItem, Typography, type MenuItemProps, type TypographyProps } from '@mui/joy';
import { useState } from 'react';
import type { IconType } from 'react-icons';

export interface MenuItemButtonProps extends MenuItemProps {
  Icon?: IconType;
  typographyProps?: Omit<TypographyProps, 'children'>;
}

export const MenuItemButton = ({
  children,
  Icon,
  onPointerOver,
  onPointerOut,
  ...props
}: MenuItemButtonProps) => {
  const [hover, setHover] = useState(false);

  return (
    <MenuItem
      onPointerEnter={(event) => {
        setHover(true);
        onPointerOver?.(event);
      }}
      onPointerLeave={(event) => {
        setHover(false);
        onPointerOut?.(event);
      }}
      sx={{
        mx: 0.5,
        px: 0.9,
        gap: 0.5,
        borderRadius: 4,
        textTransform: 'capitalize',
      }}
      {...props}
      variant={props.disabled ? 'plain' : 'soft'}
    >
      <Typography
        level="body-sm"
        startDecorator={Icon && <Icon style={{ marginRight: 14 }} />}
        sx={{ color: hover ? 'text.secondary' : 'text.tertiary' }}
        {...props.typographyProps}
      >
        {children}
      </Typography>
    </MenuItem>
  );
};
