'use client';

import { locales } from '@/i18n';
import { usePathname, useRouter } from '@/navigation';
import { Option, Select, type SelectProps } from '@mui/joy';
import { useLocale, useTranslations } from 'next-intl';
import { CSSProperties, useTransition } from 'react';

interface Props extends Omit<SelectProps<string, false>, 'children'> {
  backgroundColor?: CSSProperties['backgroundColor'];
}

export const LocaleSwitcher = ({ backgroundColor, ...props }: Props) => {
  const locale = useLocale();
  const pathname = usePathname();
  const router = useRouter();
  const t = useTranslations();

  const [isPending, startTransition] = useTransition();

  const onSelectChange = (nextLocale: typeof locale) => {
    startTransition(() => {
      router.replace(pathname, { locale: nextLocale as (typeof locales)[number] });
    });
  };

  return (
    <Select
      disabled={isPending || pathname === '/edit'}
      name="locale"
      size="sm"
      sx={{ backgroundColor, boxShadow: 'none' }}
      value={locale}
      variant="soft"
      {...props}
      onChange={(_event, newValue) => {
        if (newValue) onSelectChange(newValue);
        props?.onChange?.(_event, newValue);
      }}
    >
      {locales.map((localeOption) => (
        <Option key={localeOption} sx={{ mx: 0.5, borderRadius: 'sm' }} value={localeOption}>
          {t('LocaleSwitcher.locale', { locale: localeOption })}
        </Option>
      ))}
    </Select>
  );
};
