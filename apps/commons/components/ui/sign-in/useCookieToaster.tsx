'use client';

import { Button, Stack } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { closeSnackbar, enqueueSnackbar } from 'notistack';
import { useEffect, useState } from 'react';
import * as CookieConsent from 'vanilla-cookieconsent';

export const useCookieToaster = () => {
  const t = useTranslations();

  const [consentToAuthenticate, setConsentToAuthenticate] = useState(
    CookieConsent.acceptedService('identiyProviders', 'authentication'),
  );

  const handleChangeConsent = () => {
    const consentToAuthentication = CookieConsent.acceptedService(
      'identiyProviders',
      'authentication',
    );
    if (!consentToAuthentication) return;
    setConsentToAuthenticate(CookieConsent.acceptedService('identiyProviders', 'authentication'));
  };

  useEffect(() => {
    window.addEventListener('cc:onConsent', handleChangeConsent, { once: true });
    window.addEventListener('cc:onChange', handleChangeConsent, { once: true });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const showToaster = () => {
    const id = enqueueSnackbar(t('messages.you must accept cookies before sign in'), {
      autoHideDuration: 5000,
      action: (
        <Stack direction="row" gap={1}>
          <Button
            size="sm"
            onPointerDown={() => {
              closeSnackbar(id);
              CookieConsent.showPreferences();
            }}
            variant="solid"
          >
            {t('messages.Review your cookies preferences')}
          </Button>
        </Stack>
      ),
    });
  };

  return { consentToAuthenticate, showToaster };
};
