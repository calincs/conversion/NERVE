'use client';

import { Button, type ButtonProps } from '@mui/joy';
import 'client-only';
import { signIn } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import { PointerEventHandler } from 'react';
import { useCookieToaster } from './useCookieToaster';

interface SignInProps extends Omit<ButtonProps, 'children'> {
  signInCallbackUrl?: string;
}

export const SignIn = ({ onPointerDown, signInCallbackUrl, ...props }: SignInProps) => {
  const t = useTranslations();

  const { consentToAuthenticate, showToaster } = useCookieToaster();

  const handlePointerDown: PointerEventHandler<HTMLButtonElement> = (event) => {
    if (!consentToAuthenticate) return showToaster();

    void signIn('keycloak', { callbackUrl: signInCallbackUrl }, { prompt: 'login' });
    onPointerDown?.(event);
  };

  return (
    <Button onPointerDown={handlePointerDown} size="lg" variant="solid" {...props}>
      {t('common.signIn')}
    </Button>
  );
};
