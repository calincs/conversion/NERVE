'use client';

import { acceptedService } from 'vanilla-cookieconsent';
import 'vanilla-cookieconsent/dist/cookieconsent.css';

declare const window: Window & { dataLayer: Record<string, unknown>[] };

export const gtag: Gtag.Gtag = function () {
  // eslint-disable-next-line prefer-rest-params
  window.dataLayer.push(arguments as unknown as Record<string, unknown>);
};

//* See strategy here:
// - https://github.com/orestbida/cookieconsent/discussions/523
// - https://developers.google.com/tag-platform/security/guides/consent?consentmode=basic#gtag.js

export const listenForConsent = (setLoadScript: (value: boolean) => void) => {
  if (window._ccRun) return;

  window.dataLayer = window.dataLayer || [];

  gtag('consent', 'default', {
    ad_storage: 'denied',
    ad_user_data: 'denied',
    ad_personalization: 'denied',
    analytics_storage: 'denied',
    functionality_storage: 'denied',
    personalization_storage: 'denied',
    security_storage: 'denied',
    wait_for_update: 500,
  });

  setLoadScript(true);

  gtag('js', new Date());
  gtag('config', process.env.NEXT_PUBLIC_GA_ID);
};

export const updateGtagConsent = (value?: boolean) => {
  value = value ?? acceptedService('googleAnalytics', 'analytics');
  const consent = value ? 'granted' : 'denied';

  gtag('consent', 'update', {
    // ad_storage: consent,
    // ad_user_data: consent,
    // ad_personalization: consent,
    analytics_storage: consent,
    functionality_storage: consent,
    // personalization_storage: consent,
    security_storage: consent,
  });

  gtag('event', 'consent_analytics', { value: consent });

  window.dataLayer.push({ event: 'cookie_consent_update' });
};
