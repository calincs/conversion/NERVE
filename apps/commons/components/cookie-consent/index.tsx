'use client';

import { useColorScheme } from '@mui/joy/styles';
import Script from 'next/script';
import { useEffect, useState } from 'react';
import * as CookieConsent from 'vanilla-cookieconsent';
import 'vanilla-cookieconsent/dist/cookieconsent.css';
import getConfig from './config';
import { listenForConsent, updateGtagConsent } from './gtag-utils';

declare global {
  interface Window {
    _ccRun: boolean;
  }
}

const updateCookieConsent = () => {
  CookieConsent.showPreferences();
};

const resetCookieConsent = () => {
  CookieConsent.eraseCookies(/^([a-zA-Z0-9._\-]+)\s*/);
  CookieConsent.eraseCookies('ar_debug', '/', '.www.google-analytics.com');
  updateGtagConsent(false);
  CookieConsent.reset(true);
};

const CookieConsentComponent = () => {
  const [loadScript, setLoadScript] = useState(false);
  const { mode, systemMode } = useColorScheme();
  useEffect(() => {
    listenForConsent(setLoadScript);
    CookieConsent.run(getConfig());
  }, []);

  useEffect(() => {
    mode === 'dark' || (mode === 'system' && systemMode === 'dark')
      ? document.documentElement.classList.add('cc--darkmode')
      : document.documentElement.classList.remove('cc--darkmode');
  }, [mode, systemMode]);

  return (
    <>
      {loadScript && (
        <>
          <Script
            async
            // type="text/plain"
            data-category="analytics"
            src={`https://www.googletagmanager.com/gtag/js?id=${process.env.NEXT_PUBLIC_GA_ID}`}
            strategy="lazyOnload"
          />
        </>
      )}
    </>
  );
};

export { CookieConsentComponent, resetCookieConsent, updateCookieConsent };
