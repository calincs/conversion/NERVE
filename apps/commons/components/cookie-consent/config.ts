import { acceptedService, type CookieConsentConfig } from 'vanilla-cookieconsent';
import { updateGtagConsent } from './gtag-utils';

const getConfig = () => {
  const config: CookieConsentConfig = {
    // root: 'body',
    // autoShow: true,
    disablePageInteraction: true,
    // hideFromBots: true,
    // mode: 'opt-in',
    // revision: 0,

    cookie: {
      // name: 'cc_cookie',
      // domain: location.hostname,
      // path: '/',
      // sameSite: "Lax",
      // expiresAfterDays: 365,
    },

    /**
     * Callback functions
     */
    // onFirstConsent: ({ cookie }) => {
    //   console.log('onFirstConsent fired', cookie);
    // },

    onConsent: ({ cookie }) => {
      // console.log('onConsent fired!', cookie);
      updateGtagConsent(acceptedService('googleAnalytics', 'analytics'));
    },

    onChange: ({ changedCategories, changedServices }) => {
      // console.log('onChange fired!', changedCategories, changedServices);
      if (changedCategories.includes('analytics')) {
        updateGtagConsent(acceptedService('googleAnalytics', 'analytics'));
      }
    },

    // onModalReady: ({ modalName }) => {
    //   console.log('ready:', modalName);
    // },

    // onModalShow: ({ modalName }) => {
    //   console.log('visible:', modalName);
    // },

    // onModalHide: ({ modalName }) => {
    //   console.log('hidden:', modalName);
    // },

    // https://cookieconsent.orestbida.com/reference/configuration-reference.html#guioptions
    guiOptions: {
      consentModal: {
        layout: 'bar',
        position: 'bottom',
        equalWeightButtons: true,
        flipButtons: true,
      },
      preferencesModal: {
        layout: 'box',
        equalWeightButtons: true,
        flipButtons: false,
      },
    },

    categories: {
      necessary: {
        readOnly: true, // this category cannot be disabled
        services: {
          cookiesConsent: {
            label: 'Cookies Consent',
            // eslint-disable-next-line @typescript-eslint/no-empty-function
            onAccept: () => {},
            // eslint-disable-next-line @typescript-eslint/no-empty-function
            onReject: () => {},
            cookies: [
              { name: 'cc_cookies' },
              { name: 'NEXT_LOCALE' },
              { name: 'authjs.callback-url' },
              { name: 'authjs.csrf-token' },
            ],
          },
        },
      },
      authentication: {
        enabled: false,
        autoClear: {
          cookies: [
            //from Next Auth
            { name: /^authjs.session.token/ },
            //from github
            { name: /^_octo/ }, //
            { name: 'color_mode', domain: '.github.com' },
            { name: 'dotcom_user', domain: '.github.com' },
            { name: 'fileTreeExpanded', domain: '.github.com' },
            { name: 'logged_in', domain: '.github.com' },
            { name: 'tz', domain: '.github.com' },
          ],
          reloadPage: true,
        },
        services: {
          identiyProviders: {
            label: 'Identiy Providers',
            // eslint-disable-next-line @typescript-eslint/no-empty-function
            onAccept: () => {},
            // eslint-disable-next-line @typescript-eslint/no-empty-function
            onReject: () => {},
            cookies: [{ name: /^authjs./ }], // regex: match all cookies starting with '_authjs'
          },
        },
      },
      analytics: {
        autoClear: {
          cookies: [
            { name: /^_ga/ }, // regex: match all cookies starting with '_ga'
            { name: 'ar_debug', domain: '.www.google-analytics.com' },
          ],
        },
        // https://cookieconsent.orestbida.com/reference/configuration-reference.html#category-services
        services: {
          googleAnalytics: {
            label: 'Google Analytics',
            // eslint-disable-next-line @typescript-eslint/no-empty-function
            onAccept: () => {
              // console.log('accept analytics');
            },
            // eslint-disable-next-line @typescript-eslint/no-empty-function
            onReject: () => {
              // console.log('reject analytics');
            },
            cookies: [
              { name: /^_ga/ }, // regex: match all cookies starting with '_ga'
              { name: 'ar_debug', domain: '.www.google-analytics.com' },
            ],
          },
        },
      },
    },

    language: {
      default: 'en',
      autoDetect: 'document',
      translations: {
        en: {
          consentModal: {
            label: 'Cookie Consent',
            title: 'We use cookies',
            description: `
              This website protects your privacy.
              We and selected third parties use cookies or similar technologies for technical purposes and, with your consent, for analytics. Denying consent may make some features unavailable. You can freely give, deny, or withdraw your consent at any time.  
              Use the 'Accept all' button to consent to the use of such technologies. Use the 'Reject all' button or close this notice to continue without accepting.`,
            acceptAllBtn: 'Accept all',
            acceptNecessaryBtn: 'Reject all',
            showPreferencesBtn: 'Lean more and customize',
            closeIconLabel: 'Reject all and close modal',
            // revisionMessage: 'string',
            footer: `<a href="/privacy">Privacy Policy</a>`,
          },
          preferencesModal: {
            title: 'Manage cookie preferences',
            acceptAllBtn: 'Accept all',
            acceptNecessaryBtn: 'Reject all',
            savePreferencesBtn: 'Accept current selection',
            closeIconLabel: 'Close modal',
            serviceCounterLabel: 'Service|Services',
            sections: [
              {
                title: 'Your Privacy Choices',
                description: `In this panel you can express your consent preferences related to the processing of your personal informationmto help us achieve the features and activities described below. You may review and change expressed choices at any time by resurfacing this panel via the the link at the top bar or on your pofile panel.
                <br/><br/>
                To deny your consent to the specific processing activities described below, switch the toggles to off or use the “Reject all” button and confirm you want to save your choices. Please be aware that denying consent for a particular purpose may make related features unavailable. For more details relative to cookies and other sensitive data, please read the full. Privacy Policy.`,
              },
              {
                title: 'Strictly Necessary',
                description:
                  'These cookies are essential for the proper functioning of the website and cannot be disabled.',
                linkedCategory: 'necessary',
                cookieTable: {
                  caption: 'Cookie table',
                  headers: {
                    name: 'Cookie',
                    domain: 'Domain',
                    duration: 'Duration',
                    description: 'Description',
                  },
                  body: [
                    {
                      name: 'cc_cookies',
                      domain: location.hostname,
                      duration: '180 days',
                      description: 'Stores cookies consent preferences.',
                    },
                    {
                      name: 'NEXT_LOCALE',
                      domain: location.hostname,
                      duration: '365 days',
                      description: 'Stores preferred language.',
                    },
                    {
                      name: 'authjs.callback-url',
                      domain: location.hostname,
                      duration: 'session',
                      description: 'Stores the website base url',
                    },
                    {
                      name: 'authjs.csrf-token',
                      domain: location.hostname,
                      duration: 'session',
                      description:
                        'Stores token used to prevent Cross-Site Request Forgery (CSRF) attacks.',
                    },
                  ],
                },
              },
              {
                title: 'Authentication',
                description: `Necessary cookies used by NERVE Commons to authenticate you. It allows for basic functionalities, which include inserting your preferred indentity informartion (e.g., your Github / GitLab account or ORCID) in Web Annotations you created. These cookies also enable loading and saving files fromt and to your GitHub/GitLab repositories.`,
                linkedCategory: 'authentication',
                cookieTable: {
                  caption: 'Cookie table',
                  headers: {
                    name: 'Cookie',
                    domain: 'Domain',
                    duration: 'Duration',
                    description: 'Description',
                  },
                  body: [
                    {
                      name: 'authjs.session.token.*',
                      domain: location.hostname,
                      duration: '3.5 hours',
                      description:
                        'Stores your profile and session information, which includes information provided by your Github, GitLab, or ORCID account, susch as your name, email address, and other information.',
                    },
                    {
                      name: '*',
                      domain: '.github.com',
                      duration: 'vary: session - 1 year',
                      description: 'Used by GitHub to store user settings and preferences.',
                    },
                  ],
                },
              },
              {
                title: 'Performance and Analytics',
                description:
                  'These cookies collect information about how you use our website. It might include the title of the document you are editing on NERVE. All of the data is anonymized and cannot be used to identify you.',
                linkedCategory: 'analytics',
                cookieTable: {
                  caption: 'Cookie table',
                  headers: {
                    name: 'Cookie',
                    domain: 'Domain',
                    duration: 'Duration',
                    desc: 'Description',
                  },
                  body: [
                    {
                      name: '_ga',
                      domain: location.hostname,
                      duration: '400 days',
                      desc: 'Used by Google Analytics to distinguish users and collect data on the number of times a user has visited the website as well as dates for the first and most recent visit.',
                    },
                    {
                      name: '_ga_*',
                      domain: location.hostname,
                      duration: '400 days',
                      desc: 'Used by Google Analytics to persist session state and collect data on the number of times a user has visited the website as well as dates for the first and most recent visit.',
                    },
                    {
                      name: 'ar_debug',
                      domain: '.www.google-analytics.com',
                      duration: '90 days',
                      desc: 'Used by Google Analytics for debug purposes.',
                    },
                  ],
                },
              },
              {
                title: 'More information',
                description:
                  'For any queries in relation to my policy on cookies and your choices, please <a href="#contact-page">contact us</a>',
              },
            ],
          },
        },
      },
    },
  };

  return config;
};

export default getConfig;
