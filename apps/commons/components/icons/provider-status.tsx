import type { IconBaseProps } from 'react-icons';
import { BiRefresh, BiSolidBadgeCheck } from 'react-icons/bi';
import { MdOutlineAddLink } from 'react-icons/md';

export interface ProviderStatusIconProps extends IconBaseProps {
  status?: 'selected' | 'unlinked' | 'expired';
}

export const ProviderStatusIcon = ({ status, ...props }: ProviderStatusIconProps) => {
  if (status === 'selected') return <BiSolidBadgeCheck {...props} />;
  if (status === 'unlinked') return <MdOutlineAddLink {...props} />;
  if (status === 'expired') return <BiRefresh {...props} />;
  return null;
};
