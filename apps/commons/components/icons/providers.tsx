import type { IconBaseProps } from 'react-icons';
import { AiFillGithub, AiFillGitlab } from 'react-icons/ai';
import { MdOutlineComputer } from 'react-icons/md';
import { SiOrcid } from 'react-icons/si';

export interface ProviderIconProps extends IconBaseProps {
  providerId?: string;
}

export const ProviderIcon = ({ providerId, ...props }: ProviderIconProps) => {
  if (providerId === 'github') return <AiFillGithub {...props} />;
  if (providerId === 'gitlab') return <AiFillGitlab {...props} />;
  if (providerId === 'orcid') return <SiOrcid {...props} />;
  return <MdOutlineComputer {...props} />;
};
