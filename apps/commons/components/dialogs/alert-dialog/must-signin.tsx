import { SignIn } from '@/components/ui/sign-in';
import { atoms } from '@/jotai/store';
import {
  Button,
  DialogActions,
  DialogContent,
  type ButtonProps,
  type DialogActionsProps,
  type DialogContentProps,
} from '@mui/joy';
import { useSetAtom } from 'jotai';
import { RESET } from 'jotai/utils';
import { useTranslations } from 'next-intl';

interface MustSignInProps extends React.PropsWithChildren {
  slotProps?: {
    content: Omit<DialogContentProps, 'children'>;
    actions?: Omit<DialogActionsProps, 'children'>;
    button?: ButtonProps;
  };
}

export const MustSignIn = ({ children, slotProps }: MustSignInProps) => {
  const t = useTranslations();

  const setAlertDialog = useSetAtom(atoms.alertDialog);

  return (
    <>
      <DialogContent sx={{ pt: 0.5 }} {...slotProps?.content}>
        {children}
      </DialogContent>
      <DialogActions {...slotProps?.actions}>
        <SignIn size="md" />
        <Button
          color="neutral"
          sx={{ textTransform: 'capitalize' }}
          variant="plain"
          {...slotProps?.button}
          onPointerDown={(event) => {
            setAlertDialog(RESET);
            slotProps?.button?.onPointerDown?.(event);
          }}
        >
          {t('common.cancel')}
        </Button>
      </DialogActions>
    </>
  );
};
