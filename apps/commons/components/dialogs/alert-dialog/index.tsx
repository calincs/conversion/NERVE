import { atoms } from '@/jotai/store';
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Modal,
  ModalDialog,
  type ButtonProps,
  type DialogActionsProps,
  type DialogContentProps,
  type DialogTitleProps,
  type DividerProps,
  type ModalDialogProps,
  type ModalProps,
} from '@mui/joy';
import { useSetAtom } from 'jotai';
import { RESET } from 'jotai/utils';
import { useTranslations } from 'next-intl';
import { IoWarningOutline } from 'react-icons/io5';
import { RiErrorWarningLine } from 'react-icons/ri';

export interface AlertDialogProps extends React.PropsWithChildren {
  actions?: React.ReactNode;
  header?: string;
  headerIcon?: React.ReactNode;
  message?: React.ReactNode;
  onClose?: ModalProps['onClose'];
  severity?: 'error' | 'warning';
  open?: ModalProps['open'];
  slotProps?: {
    modal?: Omit<ModalProps, 'open' | 'children' | 'onClose'>;
    dialog?: Omit<ModalDialogProps, 'children' | 'role'>;
    dialogTitle?: Omit<DialogTitleProps, 'children'>;
    divider?: DividerProps;
    dialogContent?: Omit<DialogContentProps, 'children'>;
    dialogActions?: Omit<DialogActionsProps, 'children'>;
    closeButton?: Omit<ButtonProps, 'children'>;
  };
}

export const AlertDialog = ({
  actions,
  children,
  header,
  headerIcon,
  open = false,
  onClose,
  message,
  severity,
  slotProps,
}: AlertDialogProps) => {
  const t = useTranslations();
  const setAlertDialog = useSetAtom(atoms.alertDialog);

  return (
    <Modal
      onClose={(event) => {
        setAlertDialog(RESET);
        onClose?.(event, 'backdropClick');
      }}
      open={open}
      {...slotProps?.modal}
    >
      <ModalDialog role="alertdialog" variant="plain" size="sm" {...slotProps?.dialog}>
        {header && (
          <DialogTitle sx={{ alignItems: 'center' }} {...slotProps?.dialogTitle}>
            {headerIcon}
            {severity && severity === 'error' ? <RiErrorWarningLine /> : <IoWarningOutline />}
            {header}
          </DialogTitle>
        )}
        <Divider sx={slotProps?.divider} />
        {children ?? (
          <>
            <DialogContent sx={{ pt: 0.5 }} {...slotProps?.dialogContent}>
              {message}
            </DialogContent>
            {actions ?? (
              <DialogActions
                sx={{ alignItems: 'flex-end' }}
                orientation="vertical"
                {...slotProps?.dialogActions}
              >
                <Button
                  size="sm"
                  sx={{ textTransform: 'capitalize' }}
                  {...slotProps?.closeButton}
                  onPointerDown={(event) => {
                    onClose?.(event, 'closeClick');
                    setAlertDialog(RESET);
                    slotProps?.closeButton?.onPointerDown?.(event);
                  }}
                >
                  {t('common.close')}
                </Button>
              </DialogActions>
            )}
          </>
        )}
      </ModalDialog>
    </Modal>
  );
};
