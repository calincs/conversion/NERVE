import { db } from '@/db';
import { useOpenResource, usePermalink } from '@/hooks';
import { atoms } from '@/jotai/store';
import { usePathname } from '@/navigation';
import type { Resource } from '@/types';
import { isValidXml } from '@/util/DOM';
import type { Resource as LWStorageServiceResource } from '@cwrc/leafwriter-storage-service';
import { CircularProgress, Modal, type ModalProps } from '@mui/joy';
import { useLiveQuery } from 'dexie-react-hooks';
import { useSetAtom } from 'jotai';
import { useResetAtom } from 'jotai/utils';
import { useSession } from 'next-auth/react';
import { useLocale, useTranslations } from 'next-intl';
import { Suspense, lazy } from 'react';
import { filePanelAtoms } from '../file-panel/store';

const StorageDialogService = lazy(() =>
  import('@cwrc/leafwriter-storage-service/dialog').then((module) => ({
    default: module.StorageDialog,
  })),
);

export type DialogType = 'load' | 'save';
export type DialogSource = 'cloud' | 'local' | 'paste' | 'url';

interface SlotProps {
  slotProps?: {
    fallbackModal?: Omit<ModalProps, 'open'>;
  };
}

type StorageDialogClose = SlotProps & { open: false };
type StorageDialogOpen = SlotProps &
  ({
    open: true;
    resource?: Resource | string;
    onChange?: (resource?: Resource) => void;
    onClose?: (resource?: Resource) => void;
  } & (
    | {
        type: Extract<DialogType, 'load'>;
        source: DialogSource;
      }
    | {
        type: Extract<DialogType, 'save'>;
        source?: Extract<DialogSource, 'cloud'>;
      }
  ));

export type Props = StorageDialogClose | StorageDialogOpen;

export const StorageDialog = ({ slotProps, ...props }: Props) => {
  const locale = useLocale();
  const pathname = usePathname();
  const { data: session } = useSession();
  const t = useTranslations();

  const setResourceGlobal = useSetAtom(atoms.resource);
  const resetServiceDialogAtom = useResetAtom(filePanelAtoms.storageServiceDialog);

  const { setPermalink, resetPermalink } = usePermalink();
  const { openResource } = useOpenResource();

  const prefStorageProvider = useLiveQuery(() => db.userPreferences.get('storageProvider'));

  //***** */

  if (props.open === false) return null;

  const { onChange, onClose, open, resource, source, type } = props;

  const userProviders = session?.user.linkedAccounts?.map(({ access_token, identityProvider }) => ({
    access_token: access_token,
    name: identityProvider,
  }));

  const handleChange = (res?: LWStorageServiceResource) => {
    onChange?.(res as Resource);

    if (pathname !== '/') return;
    setPermalink(res as Resource);
  };

  const handleLoad = async (res: LWStorageServiceResource) => {
    if (!res.content) return;
    loadResource(res);
  };

  const loadResource = async (res: LWStorageServiceResource) => {
    onClose?.(res as Resource);
    await openResource(res as Resource);
  };

  const handleSave = (res: LWStorageServiceResource) => {
    setResourceGlobal(res as Resource);
    setPermalink(res as Resource);
    onClose?.(res as Resource);
  };

  const close = () => {
    onClose?.();
    resetServiceDialogAtom();
    if (pathname !== '/') return;
    if (type === 'load') resetPermalink();
  };

  const clickAway = () => {
    onClose?.();
    resetServiceDialogAtom();
    if (pathname !== '/') return;
    resetPermalink();
  };

  const validXML = (content: string) => {
    const isContentValid = isValidXml(content);
    return isContentValid
      ? { valid: true }
      : { valid: false, error: t('error.xml not well-formed') };
  };

  return (
    <Suspense
      fallback={
        <Modal
          open={true}
          sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
          {...slotProps?.fallbackModal}
        >
          <CircularProgress />
        </Modal>
      }
    >
      <StorageDialogService
        config={{
          allowedMimeTypes: ['application/xml'],
          defaultCommitMessage: 'Updated via Nerve Commons',
          locale,
          providers: userProviders,
          preferProvider: prefStorageProvider?.value,
          validate: validXML,
        }}
        onBackdropClick={type === 'load' ? clickAway : undefined}
        onCancel={close}
        onChange={handleChange}
        onLoad={handleLoad}
        onSave={handleSave}
        open={open}
        resource={resource}
        source={source}
        type={type}
      />
    </Suspense>
  );
};
