'use client';

import { MustSignIn } from '@/components/dialogs/alert-dialog/must-signin';
import { atoms } from '@/jotai/store';
import { usePathname, useRouter } from '@/navigation';
import { Error } from '@/types';
import { Stack, Typography } from '@mui/joy';
import { useSetAtom } from 'jotai';
import { useResetAtom } from 'jotai/utils';
import { useTranslations } from 'next-intl';

export const usePemalinkDialogs = () => {
  const pathname = usePathname();
  const router = useRouter();
  const t = useTranslations();

  const resetResource = useResetAtom(atoms.resource);
  const setAlertDialog = useSetAtom(atoms.alertDialog);

  const handleInvalidPermalink = (error?: Error) => {
    const title = error?.title ?? t('common.error');
    const severity = error?.type === 'info' ? undefined : error?.type;
    const message = error?.message ?? `${t('messages.Nerve could not parse the permalink')}.`;

    setAlertDialog({
      header: title,
      severity,
      message: (
        <>
          <Typography level="body-sm">{message}</Typography>
          <Typography level="body-sm">{t('FilePanel.warning.check URL structure')}.</Typography>
        </>
      ),
      onClose: () => router.push(`/`),
    });
  };
  const handleInvalidProvider = (provider: string) => {
    setAlertDialog({
      header: t('FilePanel.warning.provider error'),
      severity: 'error',
      message: (
        <Stack gap={1}>
          <Typography level="body-sm">
            {t.rich('FilePanel.warning.storage provider invalid', {
              provider: (chunks) => (
                <Typography borderRadius="xs" color="danger" fontWeight={700} variant="soft">
                  {provider}
                </Typography>
              ),
            })}
            .
          </Typography>
          <Typography level="body-sm">{t('FilePanel.warning.check URL structure')}.</Typography>
        </Stack>
      ),
      onClose: () => router.push(`/`),
    });
  };

  const handleInvalidUrl = () => {
    setAlertDialog({
      header: t('messages.url invalid'),
      severity: 'error',
      message: (
        <Typography level="body-sm">{t('FilePanel.warning.check URL structure')}.</Typography>
      ),
      onClose: () => router.push(`/`),
    });
  };

  const handleMissingProviderAttributes = (data: Record<string, string | undefined>) => {
    const attributes = [...Object.entries(data)];
    setAlertDialog({
      header: t('messages.permalink missing attribute'),
      severity: 'error',
      message: (
        <Stack gap={1}>
          <Typography level="body-sm">{t('FilePanel.warning.check URL structure')}.</Typography>
          {attributes
            .filter(([key, value]) => key !== 'path' && value !== '')
            .map(([key, value]) => (
              <Typography key={key} color={value ? 'neutral' : 'danger'} level="body-sm">
                {key}:
                <Typography
                  borderRadius="xs"
                  color={value ? 'neutral' : 'danger'}
                  component="code"
                  fontFamily="monospace"
                  mx={1}
                  noWrap
                  px={1}
                  variant="soft"
                >
                  {value ?? 'null'}
                </Typography>
              </Typography>
            ))}
        </Stack>
      ),
      onClose: () => router.push(`/`),
    });
  };

  const handleProviderNotLinked = (provider: string) => {
    setAlertDialog({
      header: t('FilePanel.warning.provider error'),
      severity: 'warning',
      message: (
        <Stack gap={1}>
          <Typography level="body-sm">
            {t.rich('FilePanel.warning.storage provider not linked', {
              provider: (chunks) => (
                <Typography borderRadius="xs" color="warning" fontWeight={700} variant="soft">
                  {provider}
                </Typography>
              ),
            })}
            .
          </Typography>
          <Typography level="body-sm">{t('messages.check your profile')}.</Typography>
        </Stack>
      ),
      onClose: () => router.push(`/`),
    });
  };

  const handleResourceNotFound = (url: string) => {
    setAlertDialog({
      header: t('common.error'),
      severity: 'error',
      message: (
        <Stack gap={1}>
          <Typography level="body-sm">{`${t('messages.resource not found')}.`}</Typography>
          <Typography
            borderRadius="xs"
            component="code"
            fontFamily="monospace"
            level="body-sm"
            variant="soft"
          >
            {url}
          </Typography>
          <Typography level="body-sm">{t('FilePanel.warning.check URL structure')}.</Typography>
        </Stack>
      ),
      onClose: () => router.push(`/`),
    });
  };

  const handleResourceNotSupported = (url: string) => {
    setAlertDialog({
      header: t('common.error'),
      severity: 'error',
      message: (
        <Stack gap={1}>
          <Typography level="body-sm">{`${t('messages.resource not supported')}.`}</Typography>
          <Typography
            borderRadius="xs"
            component="code"
            fontFamily="monospace"
            level="body-sm"
            variant="soft"
          >
            {url}
          </Typography>
        </Stack>
      ),
      onClose: () => router.push(`/`),
    });
  };

  const handleUserUnauthenticated = () => {
    setAlertDialog({
      header: t('common.signin required'),
      severity: 'warning',
      children: <MustSignIn>{t('messages.you must sign in to open this resource')}.</MustSignIn>,
      onClose: () => {
        resetResource();
        router.push(pathname);
      },
    });
  };

  return {
    handleInvalidPermalink,
    handleInvalidProvider,
    handleInvalidUrl,
    handleMissingProviderAttributes,
    handleProviderNotLinked,
    handleResourceNotFound,
    handleResourceNotSupported,
    handleUserUnauthenticated,
  };
};
