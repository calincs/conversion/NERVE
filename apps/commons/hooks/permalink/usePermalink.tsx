'use client';

import { atoms } from '@/jotai/store';
import { usePathname, useRouter } from '@/navigation';
import type { Error, Resource } from '@/types';
import { useResetAtom } from 'jotai/utils';
import { useSession } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import { useSearchParams } from 'next/navigation';
import queryString from 'query-string';
import z from 'zod';
import { useIdentityProvider } from '../useIdentityProvider';
import { usePemalinkDialogs } from './usePemalinkDialogs';

export type Permalink = { resource: Resource; valid: true } | ({ valid: false } & Error);

export const usePermalink = () => {
  const pathname = usePathname();
  const router = useRouter();
  const searchParams = useSearchParams();
  const { status: sessionStatus } = useSession();
  const t = useTranslations();

  const { isProviderLinked, providerHasStorage } = useIdentityProvider();
  const {
    handleInvalidPermalink,
    handleInvalidProvider,
    handleInvalidUrl,
    handleProviderNotLinked,
    handleUserUnauthenticated,
  } = usePemalinkDialogs();

  const resetResource = useResetAtom(atoms.resource);

  const convertToSearchParams = (resource: Resource) => {
    const { storageSource } = resource;

    if (storageSource === 'local' || storageSource === 'paste') {
      return queryString.stringify(
        { local: true, filename: resource.filename },
        { skipEmptyString: true, skipNull: true, sort: false },
      );
    }

    if (storageSource === 'url') {
      return queryString.stringify(
        { url: resource.url, filename: resource.filename },
        { skipEmptyString: true, skipNull: true, sort: false },
      );
    }

    const { provider, owner, ownerType, repo, path, filename, url } = resource;

    return queryString.stringify(
      { provider, owner, ownerType, repo, path, filename, url: provider ? undefined : url },
      { skipEmptyString: true, skipNull: true, sort: false },
    );
  };

  const getResourceFromPermalink = async (): Promise<Resource | undefined> => {
    if (searchParams.size === 0) return;
    // if (sessionStatus === 'loading') return;

    const permalink = parsePermalink();
    if (!permalink) {
      resetPermalink();
      return;
    }

    //* INVALID
    if (!permalink.valid) {
      handleInvalidPermalink(permalink);
      return;
    }

    if (permalink.resource.storageSource === 'url') {
      const urlSchema = z.string().url();
      const isValid = urlSchema.safeParse(permalink.resource.url).success;
      if (!isValid) {
        handleInvalidUrl();
        return;
      }
    }

    if (permalink.resource.storageSource === 'cloud') {
      //* STORAGE PROVIDER NOT SUPORTED
      const provider = permalink.resource.provider;

      //* INVALID PROVIDER
      const hasStorage = await providerHasStorage(provider);
      if (!hasStorage) {
        handleInvalidProvider(provider);
        return;
      }

      //* UNAUTHENTICATED
      if (pathname === '/' && !permalink.resource.filename && sessionStatus === 'unauthenticated') {
        handleUserUnauthenticated();
        return;
      }

      //* PROVIDER NOT LINKED
      if (
        pathname === '/' &&
        !permalink.resource.filename &&
        sessionStatus === 'authenticated' &&
        !isProviderLinked(provider)
      ) {
        handleProviderNotLinked(provider);
        return;
      }
    }

    return permalink.resource;
  };

  const parsePermalink = (query?: URLSearchParams): Permalink | undefined => {
    if (!query) query = searchParams;
    if (query.size === 0) return;

    // * It refers to a local resource
    const isLocal = query.get('local');
    if (isLocal) {
      return {
        valid: false,
        type: 'error',
        message: `${t('messages.Nerve cannot directly open resouces from local devices')}.`,
      };
    }

    //* It refers to a url
    const url = query.get('url');
    if (url) {
      const isSample = query.get('sample');

      return {
        valid: true,
        resource: {
          format: 'xml',
          url,
          storageSource: 'url',
          isSample: !!isSample,
        },
      };
    }

    //* if it refers to a repo
    const provider = query.get('provider');
    const owner = query.get('owner');
    if (provider && owner) {
      return {
        valid: true,
        resource: {
          format: 'xml',
          storageSource: 'cloud',
          provider,
          owner,
          ownerType: query.get('ownerType') ?? 'user',
          repo: query.get('repo') ?? undefined,
          branch: query.get('branch') ?? 'main',
          path: query.get('path') ?? undefined,
          filename: query.get('filename') ?? undefined,
        },
      };
    }

    return {
      valid: false,
      type: 'error',
      message: `${t('messages.Nerve could not parse the permalink')}.`,
    };
  };

  const resetPermalink = () => {
    resetResource();
    router.push(pathname);
  };

  const setPermalink = (value?: Resource | string) => {
    if (!value || value === '/') {
      resetPermalink();
      return;
    }

    const query = typeof value == 'string' ? value : convertToSearchParams(value);
    const path = pathname.endsWith('/') ? pathname.slice(0, -1) : pathname;
    router.push(`${path}?${query}`);
  };

  return {
    convertToSearchParams,
    getResourceFromPermalink,
    parsePermalink,
    resetPermalink,
    setPermalink,
  };
};
