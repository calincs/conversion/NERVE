import { db } from '@/db';
import { useSession } from 'next-auth/react';
import { useEffect } from 'react';

export const usePreferences = () => {
  const { data: session } = useSession();

  const initiate = async () => {
    setupPrefIdentityProvider();
    setupPrefStorageProvider();
  };

  const setupPrefIdentityProvider = async () => {
    if (!session?.user.identityProvider || !session?.user.linkedAccounts) return;

    const prefIdentityProvider = await db.userPreferences.get('identityProvider');
    const isLinkedAccount = session.user.linkedAccounts.some(
      ({ identityProvider }) => identityProvider === prefIdentityProvider?.value,
    );

    if (!prefIdentityProvider || !isLinkedAccount) {
      await db.userPreferences.put({
        key: 'identityProvider',
        value: session.user.identityProvider,
      });
    }
  };

  const setupPrefStorageProvider = async () => {
    if (!session?.user.identityProvider || !session?.user.linkedAccounts) return;

    const prefStorageProvider = await db.userPreferences.get('storageProvider');
    const isLinkedAccount = session.user.linkedAccounts.some(
      ({ identityProvider }) => identityProvider === prefStorageProvider?.value,
    );

    if (!prefStorageProvider || !isLinkedAccount) {
      await db.userPreferences.put({
        key: 'storageProvider',
        value: session.user.identityProvider,
      });
    }
  };

  useEffect(() => {
    if (session) initiate();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [session]);
};
