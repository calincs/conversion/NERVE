import { getTsrLincsAuth } from '@/auth/lincs/auth-api';
import { lincsAuthApiUrlAtom } from '@/jotai/store';
import { useAtomValue } from 'jotai';

export const useLoadIdentityProviders = () => {
  const lincsAuthApiUr = useAtomValue(lincsAuthApiUrlAtom);

  const { data, isLoading, error } = getTsrLincsAuth(lincsAuthApiUr).providers.getAll.useQuery({
    queryKey: ['providers'],
  });

  if (Array.isArray(data?.body)) {
    data?.body?.sort((a, b) => {
      const idA = a.providerId?.toUpperCase() ?? '';
      const idB = b.providerId?.toUpperCase() ?? '';
      if (idA < idB) return -1;
      if (idA > idB) return 1;
      return 0;
    });
  }

  return {
    providers: data?.body,
    isLoading,
    error,
  };
};
