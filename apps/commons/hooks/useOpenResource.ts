import { store, atoms } from '@/jotai/store';
import { usePathname, useRouter } from '@/navigation';
import type { CloudResource, PasteResource, Resource, UploadResource, UrlResource } from '@/types';
import { nanoid } from 'nanoid';
import { useLocale } from 'next-intl';
import queryString from 'query-string';

export const useOpenResource = () => {
  const locale = useLocale();
  const pathname = usePathname();
  const router = useRouter();

  const openResource = async (resource: Resource) => {
    if (resource.storageSource === 'local' || resource.storageSource === 'paste') {
      openLocalResource(resource);
      return;
    }

    if (resource.storageSource === 'url') {
      openResourceFromUrl(resource);
      return;
    }

    openResourceFromProvider(resource);
  };

  const openLocalResource = async (resource: UploadResource | PasteResource) => {
    const id = nanoid(11);
    store.set(atoms.resource, resource);

    const params = queryString.stringify(
      {
        local: true,
        id,
        filename: resource.filename,
      },
      { skipEmptyString: true, skipNull: true, sort: false },
    );

    open(`/edit?${params}`);
  };

  const openResourceFromUrl = async (resource: UrlResource) => {
    store.set(atoms.resource, resource);

    const params = queryString.stringify(
      { url: resource.url, filename: resource.filename, sample: resource.isSample },
      { skipEmptyString: true, skipNull: true },
    );

    open(`/edit?${params}`);
  };

  const openResourceFromProvider = async (resource: CloudResource) => {
    store.set(atoms.resource, resource);

    const { provider, owner, ownerType, repo, branch, path, filename, writePermission } = resource;

    const params = queryString.stringify(
      { provider, owner, ownerType, repo, branch, path, filename },
      { skipEmptyString: true, skipNull: true, sort: false },
    );

    const route = writePermission === false ? 'view' : 'edit';

    open(`/${route}?${params}`);
  };

  const open = (route: string) => {
    //*  Open new tab if it is not the homepage
    if (pathname !== '/') {
      const url = `${window.location.origin}/${locale}${route}`;
      window.open(url, '_blank');
      return;
    }

    //* Open in the same tab
    router.push(pathname + route);
  };

  return {
    openResource,
  };
};
