'use client';

import { getTsrLincsAuth } from '@/auth/lincs/auth-api';
import type { LinkedAccount } from '@/auth/lincs/types';
import { getLinkAccountUrl } from '@/server-actions';
import { delay } from '@/util/general';
import type { LinkAccountMessage } from '@/util/http/broadcast-channel';
import { openAuxWindowChannel } from '@/util/http/broadcast-channel';
import { add, fromUnixTime, isBefore, secondsToMilliseconds } from 'date-fns';
import { useSession } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import { enqueueSnackbar } from 'notistack';
import randomstring from 'randomstring';
import { useLoadIdentityProviders } from './useLoadIdentityProviders';
import { useAtomValue } from 'jotai';
import { lincsAuthApiUrlAtom } from '@/jotai/store';

const TOKEN_EXPIRES_IN = 5; // minutes

/**
 * The `useIdentityProvider` function provides a set of utility functions for managing linked accounts,
 * including retrieving access tokens, refreshing tokens, and linking accounts with external identity providers.
 * @returns The `useIdentityProvider` function returns an object with a set of utility functions for managing linked accounts
 */
export const useIdentityProvider = () => {
  const { data: session, update } = useSession();
  const t = useTranslations();
  const { providers, isLoading: isProvidersLoading } = useLoadIdentityProviders();

  const lincsAuthApiUr = useAtomValue(lincsAuthApiUrlAtom);

  /**
   * The function `getAccessToken` retrieves the access token from a linked account provider.
   * @param {string} providerId - The `providerId` parameter is a string that represents the ID of a provider.
   * @returns The access token from the provider's token object.
   */
  const getAccessToken = async (providerId: string) => {
    const provider = await getProvider(providerId);
    if (!provider) return;
    if (!provider.token) return;

    return provider.token.access_token;
  };

  /**
   * The function `getLinkedAccount` retrieves a linked account based on the provided provider ID.
   * @param {string} providerId - A string representing the identity provider ID.
   * @returns The function `getLinkedAccount` returns the provider object that matches the given `providerId`
   */
  const getProvider = async (providerId: string) => {
    const linkedAccounts = await getProviders();
    if (!linkedAccounts) return;

    const provider = linkedAccounts.find((account) => account.identityProvider === providerId);
    return provider;
  };

  /**
   * The function `getLinkedAccounts` retrieves the linked accounts from a JSON Web Token (JWT) if it exists.
   * @returns Returns an array with the user's `linkedAccounts`.
   */
  const getProviders = async () => {
    if (!session) return;
    return session.user.linkedAccounts;
  };

  /**
   * The function `hasStorageProviderLinked` checks if a storage provider is linked to the user's account.
   * @returns A boolean value indicating whether a storage provider is linked to the user's account.
   */
  const hasStorageProviderLinked = async () => {
    if (!isProvidersLoading) await delay(1000);
    return providers?.some((provider) => {
      if (!provider.providerId || !provider.hasStorage) return;
      return session?.user.linkedAccounts
        ?.map(({ identityProvider }) => identityProvider)
        .includes(provider.providerId);
    });
  };

  /**
   * Checks if a provider with the given providerId has storage.
   * @param {string} providerId - The ID of the provider to check.
   * @return {boolean | undefined} - True if the provider has storage, false otherwise, or undefined if providers is undefined.
   */
  const providerHasStorage = async (providerId: string): Promise<boolean | undefined> => {
    let _providers = providers;
    if (!isProvidersLoading || !_providers) {
      const { body, status } = await getTsrLincsAuth(lincsAuthApiUr).providers.getAll.query();
      if (status === 500) return;
      _providers = body;
    }

    return !!_providers.find((provider) => provider.providerId === providerId)?.hasStorage;
  };

  /**
   * The function `isAccessTokenExpired` checks if a provider's access token has expired.
   * @param {string | LinkedAccount} provider - The `provider` parameter can be either a string or a
   * `LinkedAccount` object. If it is a string, it represents the ID of a linked account. If it is a
   * `LinkedAccount` object, it contains information about the linked account, including the access token
   * expiration time.
   * @returns a boolean value indicating whether the IDP token is expired or not.
   */
  const isAccessTokenExpired = async (provider: string | LinkedAccount) => {
    if (typeof provider === 'string') {
      const _provider = await getProvider(provider);
      if (!_provider) return false;
      provider = _provider;
    }

    const accessTokenExpiration = provider.token?.accessTokenExpiration;
    if (!accessTokenExpiration) return false;

    const willExpiredIn = add(new Date(), { minutes: TOKEN_EXPIRES_IN });
    const isExpired = isBefore(fromUnixTime(accessTokenExpiration), willExpiredIn);

    return isExpired;
  };

  /**
   * The function checks if a specific provider is linked to the user's account.
   * @param {string} provider - The `provider` parameter is a string that represents the identity
   * provider that we want to check if it is linked to the user's account.
   * @returns A boolean value.
   */
  const isProviderLinked = (provider: string) => {
    return session?.user.linkedAccounts?.some(
      ({ identityProvider }) => identityProvider === provider,
    );
  };

  /**
   * The `triggerPromptLinkProvider` function is used to link a user account with a specific external identity provider by generating a codeVerifier and opening an auxiliary window to complete the linking process.
   *
   * @param {string} providerId - The ID of the provider for which the account is being linked.
   * @param {object} options - An optional object that can contain the following property:
   *   - `isRefresh` {boolean} - If the call has been made to refresh the identity provider tokens.
   * @returns {Promise<LinkAccountMessage>} A promise that resolves to a `LinkAccountMessage` object with properties `success`, `error`, and `codeVerifier`.
   */
  const triggerPromptLinkProvider = async (
    providerId: string,
    options?: { isRefresh?: boolean },
  ) => {
    const codeVerifier = randomstring.generate();

    const url = await getLinkAccountUrl({
      providerId,
      isRefresh: options?.isRefresh,
      codeVerifier,
    });

    if (!url) {
      return {
        success: false,
        message: 'url not found',
      };
    }

    const message = await openAuxWindowChannel<LinkAccountMessage>('keycloak-link-account', url, {
      timeout: secondsToMilliseconds(30),
    }).catch((error: string) => {
      return { success: false, error: error } as LinkAccountMessage;
    });

    if (codeVerifier !== message.codeVerifier) {
      return {
        success: false,
        error: 'codeVerifier invalid',
      } satisfies LinkAccountMessage;
    }

    return message;
  };

  /**
   * The function `linkProvider` is an asynchronous function that links an account with a provider and
   * displays a success message if the linking is successful.
   * @returns A boolean value indicating whether the account linking was successful or not.
   */
  const linkProvider = async (provider: string) => {
    const response = await triggerPromptLinkProvider(provider);

    if (!response.success) {
      if (response.error === 'timeout') return;
      const message = response.error ? response.error : t('common.Something whent wrong');
      enqueueSnackbar(message, { variant: 'error' });
      return;
    }

    await update({ reason: 'linkAccount' });
    enqueueSnackbar(`${provider} ${t('user.linked')}`);

    return response.success;
  };

  /**
   * The function `refreshToken` refreshes IDP tokens and displays a success message if the refresh is successful.
   * @returns A boolean value indicating whether the IDP tokens were successfully refreshed.
   */
  const refreshAccessToken = async (provider: string) => {
    const response = await triggerPromptLinkProvider(provider, { isRefresh: true });

    if (!response.success) {
      if (response.error === 'timeout') return;
      const message = response.error ? response.error : t('common.Something whent wrong');
      enqueueSnackbar(message, { variant: 'error' });
      return;
    }

    await update({ reason: 'refreshIDPTokens' });
    enqueueSnackbar(`${provider} ${t('user.tokens refreshed')}`);

    return response.success;
  };

  /**
   * The function `refreshAllTokens` refreshes all IDP tokens and displays a notification for each provider.
   * @returns If `providers` is falsy (e.g. `null`, `undefined`, `false`, or an empty array), then
   * nothing is being returned. Otherwise, an array of objects is being returned.
   */
  const refreshAllAccessTokens = async () => {
    const linkedAccounts = await getProviders();
    if (!linkedAccounts) return;

    const providers = [];

    for (const account of linkedAccounts) {
      const isExpired = await isAccessTokenExpired(account);
      if (!isExpired) {
        providers.push({ provider: account.identityProvider, tokenValid: true });
        return;
      }

      const refreshedToken = await triggerPromptLinkProvider(account.identityProvider);
      providers.push({
        provider: account.identityProvider,
        tokenValid: refreshedToken.success,
        refreshed: refreshedToken.success,
        message: refreshedToken.message,
      });
    }

    for (const provider of providers) {
      if (!(typeof provider.message === 'string')) continue;
      enqueueSnackbar(provider.message, { variant: provider.refreshed ? 'default' : 'error' });
    }

    await update({ reason: 'refreshIDPTokens' });
  };

  return {
    getAccessToken,
    getProvider,
    getProviders,
    hasStorageProviderLinked,
    isAccessTokenExpired,
    isProviderLinked,
    isProvidersLoading,
    linkProvider,
    providers,
    providerHasStorage,
    refreshAccessToken,
    refreshAllAccessTokens,
  };
};
