'use client';

import { getManageAccountUrl } from '@/server-actions';
import type * as SiteBroadcastChannel from '@/util/http/broadcast-channel';
import { openAuxWindowChannel } from '@/util/http/broadcast-channel';
import { minutesToMilliseconds } from 'date-fns';
import { useSession } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import { enqueueSnackbar } from 'notistack';
import randomstring from 'randomstring';

/**
 * The `useUser` function is a custom hook that provides functionality for managing a user's account, including
 * generating a URL for account management, opening a window to manage the account, and updating the account.
 * @returns The `useUser` hook returns an object with two functions: `manageAccount` and `refreshAccount`.
 */
export const useUser = () => {
  const { data: session, update } = useSession();
  const t = useTranslations();

  /**
   * The `manageAccount` function is responsible for opening a window to manage a user's account,
   * verifying the code verifier, handling response errors, and updating the account if successful.
   * @returns The function `manageAccount` returns a boolean value indicating whether the account was
   * successfully updated or not.
   */
  const manageAccount = async () => {
    const codeVerifier = randomstring.generate();
    const accountMangementUrl = await getManageAccountUrl(codeVerifier);
    if (!accountMangementUrl) return;

    const response = await openAuxWindowChannel<SiteBroadcastChannel.Message>(
      'keycloak-account-manager',
      accountMangementUrl,
      { timeout: minutesToMilliseconds(5) },
    ).catch((error: string) => {
      return { success: false, error: error } as SiteBroadcastChannel.Message;
    });

    if (codeVerifier !== response.codeVerifier) {
      return {
        success: false,
        error: `${t('common.error')}: ${t('error.codeVerifier invalid')}`,
      } as SiteBroadcastChannel.Message;
    }

    if (!response.success) {
      if (response.error === 'timeout') return;
      const message = response.error ? response.error : t('common.Something whent wrong');
      enqueueSnackbar(message, { variant: 'error' });
      return;
    }

    await update({ reason: 'accountUpdated' });
    enqueueSnackbar(t('user.Account updated'));

    return response.success;
  };

  /**
   * The function `refreshAccount` updates the account and displays a notification.
   */
  const refreshAccount = async () => {
    await update({ reason: 'accountUpdated' });
    enqueueSnackbar(t('user.Account updated'));
  };

  return {
    manageAccount,
    refreshAccount,
    user: session?.user,
  };
};
