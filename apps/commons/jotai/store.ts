import type { AlertDialogProps } from '@/components/dialogs/alert-dialog';
import type { Resource, User } from '@/types';
import { atom, createStore } from 'jotai';
import { atomWithReset, RESET } from 'jotai/utils';

export const lincsAuthApiUrlAtom = atom(process.env.LINCS_AUTH_API_URL);
lincsAuthApiUrlAtom.debugLabel = 'lincsAuthApiUrl.Atom';

export const userAtom = atom<User | undefined>(undefined);
userAtom.debugLabel = 'user.Atom';

export const resourceAtom = atomWithReset<Resource | undefined>(undefined);
resourceAtom.debugLabel = 'resource.Atom';

const alertDialogPropsAtom = atomWithReset<AlertDialogProps>({ open: false });
alertDialogPropsAtom.debugLabel = 'alertDialogProps.Atom';
export const alertDialogAtom = atom(
  (get) => get(alertDialogPropsAtom),
  (_get, set, newValue: Omit<AlertDialogProps, 'open'> | typeof RESET) => {
    set(alertDialogPropsAtom, newValue === RESET ? RESET : { ...newValue, open: true });
  },
);
alertDialogAtom.debugLabel = 'alertDialog.Atom';

export const atoms = {
  user: userAtom,
  resource: resourceAtom,
  alertDialog: alertDialogAtom,
};

export const store = createStore();
