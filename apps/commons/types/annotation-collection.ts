import { z } from 'zod';

export const AnnotationsPageSchema = z.object({
  '@context': z.literal('http://www.w3.org/ns/anno.jsonld'),
  id: z.string(),
  type: z.literal('AnnotationPage'),
  partOf: z.string().optional(),
  items: z.array(z.unknown()),
  next: z.string().optional(),
  previous: z.string().optional(),
  startIndex: z.number().nonnegative(),
});

export type AnnotationsPage = z.infer<typeof AnnotationsPageSchema>;

export const AnnotationCollectionSchema = z.object({
  '@context': z.literal('http://www.w3.org/ns/anno.jsonld'),
  id: z.string(),
  type: z.literal('AnnotationCollection'),
  label: z.union([z.string(), z.array(z.string())]).optional(),
  total: z.number().nonnegative().optional(),
  target: z.union([z.string().uuid(), z.string().url()]),
  first: z.union([z.string(), AnnotationsPageSchema]),
  last: z.string().optional(),
});

export type AnnotationCollection = z.infer<typeof AnnotationCollectionSchema>;
