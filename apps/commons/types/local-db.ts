import z from 'zod';
import { AnnotationCollectionSchema } from './annotation-collection';
import { CloudResourceSchema, UrlResourceSchema } from './resource';

const LocalDbAnnotationSchema = AnnotationCollectionSchema.merge(
  z.object({
    source: z.union([UrlResourceSchema, CloudResourceSchema]).optional(),
  }),
);

export const LocalDbDocumentSchema = z.object({
  id: z.union([z.string().url(), z.string().nanoid()]),
  resource: z.union([UrlResourceSchema, CloudResourceSchema]),
  annotations: z.union([z.string().url(), LocalDbAnnotationSchema]).optional(),
  lastOpenedAt: z.date().optional(),
});

export type LocalDbDocument = z.infer<typeof LocalDbDocumentSchema>;
