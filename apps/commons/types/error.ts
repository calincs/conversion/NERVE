export const ErrorTypes = ['info', 'warning', 'error'] as const;
export type ErrorType = (typeof ErrorTypes)[number];

export interface Error {
  message: string;
  title?: string;
  type: ErrorType;
}
