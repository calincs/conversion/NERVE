import z from 'zod';

export const userSchema = z.object({
  name: z.string(),
  url: z.string().url(),
});

export type User = z.infer<typeof userSchema>;

export const userPreferencesSchema = z.object({
  identityProvider: z
    .object({
      key: z.literal('identityProvider'),
      value: z.string(),
    })
    .optional(),
  storageProvider: z
    .object({
      key: z.literal('storageProvider'),
      value: z.string(),
    })
    .optional(),
});

export type UserPreferences = z.infer<typeof userPreferencesSchema>;
