export * from './annotation-collection';
export * from './error';
import z from 'zod';

export const formatTypes = ['xml', 'json', 'plaintext'] as const;
export const FormatTypesSchema = z.enum(formatTypes);
export type FormatType = z.infer<typeof FormatTypesSchema>;

export const storageSourceTypes = ['cloud', 'local', 'paste', 'url'] as const;
export const StorageSourceTypesSchema = z.enum(storageSourceTypes);
export type StorageSourceType = z.infer<typeof StorageSourceTypesSchema>;

export const ResurceBaseSchema = z.object({
  format: FormatTypesSchema,
  storageSource: StorageSourceTypesSchema,
  filename: z.string().optional(),
  content: z.string().optional(),
  title: z.string().optional(),
  screenshot: z.string().optional(),
  schemaName: z.string().optional(),
});

const PasteResourceSchema = ResurceBaseSchema.merge(
  z.object({ storageSource: StorageSourceTypesSchema.extract(['paste']), content: z.string() }),
);
export type PasteResource = z.infer<typeof PasteResourceSchema>;

const UploadResourceSchema = ResurceBaseSchema.merge(
  z.object({ storageSource: StorageSourceTypesSchema.extract(['local']), filename: z.string() }),
);
export type UploadResource = z.infer<typeof UploadResourceSchema>;

export const UrlResourceSchema = ResurceBaseSchema.merge(
  z.object({
    isSample: z.boolean().optional(),
    storageSource: StorageSourceTypesSchema.extract(['url']),
    url: z.string().url(),
  }),
);
export type UrlResource = z.infer<typeof UrlResourceSchema>;

export const CloudResourceSchema = ResurceBaseSchema.merge(
  z.object({
    storageSource: StorageSourceTypesSchema.extract(['cloud']),
    provider: z.string(), //z.union([z.literal('github'), z.literal('gitlab')]),
    ownerType: z.string(), //z.union([z.literal('user'), z.literal('org')]),
    owner: z.string(),
    repo: z.string().optional(),
    branch: z.string().optional(),
    path: z.string().optional(),
    hash: z.string().optional(),
    url: z.string().url().optional(),
    writePermission: z.boolean().optional(),
  }),
);
export type CloudResource = z.infer<typeof CloudResourceSchema>;

export const ResourceSchema = z.union([
  PasteResourceSchema,
  UploadResourceSchema,
  UrlResourceSchema,
  CloudResourceSchema,
]);
export type Resource = z.infer<typeof ResourceSchema>;
