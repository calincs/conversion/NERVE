import { LocalDbDocument, UserPreferences } from '@/types';
import Dexie, { Table } from 'dexie';

export interface RecentDocument extends LocalDbDocument {
  id: NonNullable<LocalDbDocument['id']>;
}

export class DexieDB extends Dexie {
  userPreferences!: Table<{ key: keyof UserPreferences; value: string }>;
  recentDocuments!: Table<RecentDocument, string>;

  constructor() {
    super('NERVE-Commons');
    this.version(1).stores({
      userPreferences: 'key', // '&' means 'unique'
      recentDocuments: '&id, resource.url', // '&' means 'unique'
    });
  }
}

export const db = new DexieDB();

export const clearCache = async () => {
  await db.recentDocuments
    .clear()
    .catch(() => new Error('Clear `recentDocuments` table: Something went wrong.'));
};

export const deleteDb = async () => {
  await Dexie.delete('NERVE');

  await db.delete().catch(() => new Error('Something went wrong.'));
};
