'use client';

import { usePreferences } from '@/hooks/usePreferences';
import { lincsAuthApiUrlAtom, store } from '@/jotai/store';
import { Provider } from 'jotai';
import { DevTools } from 'jotai-devtools';
import 'jotai-devtools/styles.css';

interface Props extends React.PropsWithChildren {
  lincsAuthApiUrl: string;
}

export const JotaiProvider = ({ children, lincsAuthApiUrl }: Props) => {
  store.set(lincsAuthApiUrlAtom, lincsAuthApiUrl);
  usePreferences();
  return (
    <Provider store={store}>
      {children} <DevTools theme="dark" />
    </Provider>
  );
};
