'use client';

import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
// import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { getTsrLincsAuth } from '../auth/lincs/auth-api';
import { useAtomValue } from 'jotai';
import { lincsAuthApiUrlAtom } from '@/jotai/store';

const queryClient = new QueryClient();

export const ReactQueryProvider = ({ children }: React.PropsWithChildren) => {
  const lincsAuthApiUrl = useAtomValue(lincsAuthApiUrlAtom);
  const tsrLincsAuth = getTsrLincsAuth(lincsAuthApiUrl);
  return (
    <QueryClientProvider client={queryClient}>
      <tsrLincsAuth.ReactQueryProvider>{children}</tsrLincsAuth.ReactQueryProvider>
      {/* <ReactQueryDevtools buttonPosition="top-left" position="left" /> */}
    </QueryClientProvider>
  );
};
