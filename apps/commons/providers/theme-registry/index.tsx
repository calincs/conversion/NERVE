'use client';

import { theme } from '@lincs.project/nerve';
import { GlobalStyles } from '@mui/joy';
import CssBaseline from '@mui/joy/CssBaseline';
import InitColorSchemeScript from '@mui/joy/InitColorSchemeScript';
import { CssVarsProvider as JoyCssVarsProvider } from '@mui/joy/styles';
import {
  THEME_ID as MATERIAL_THEME_ID,
  Experimental_CssVarsProvider as MaterialCssVarsProvider,
  experimental_extendTheme as materialExtendTheme,
} from '@mui/material/styles';
import * as React from 'react';

const materialTheme = materialExtendTheme();

export const ThemeRegistry = ({ children }: { children: React.ReactNode }) => {
  return (
    <>
      <InitColorSchemeScript defaultMode="system" />
      <MaterialCssVarsProvider theme={{ [MATERIAL_THEME_ID]: materialTheme }}>
        <JoyCssVarsProvider defaultMode="system" theme={theme}>
          <CssBaseline />
          <GlobalStyles
            styles={{
              // The {selector} is the CSS selector to target the icon.
              // We recommend using a class over a tag if possible.
              '{selector}': {
                color: 'var(--Icon-color)',
                margin: 'var(--Icon-margin)',
                fontSize: 'var(--Icon-fontSize, 20px)',
                width: '1em',
                height: '1em',
              },
            }}
          />
          {children}
        </JoyCssVarsProvider>
      </MaterialCssVarsProvider>
    </>
  );
};
