import { extendTheme } from '@mui/joy/styles';

//* https://uxdesign.cc/defining-colors-in-your-design-system-828148e6210a
// * https://colorbox.io/

export const theme = extendTheme({
  colorSchemes: {
    light: {
      palette: {
        primary: {
          '50': '#c4efff',
          '100': '#a8e5fb',
          '200': '#8cdbf7',
          '300': '#72d1f3',
          '400': '#59c7ee',
          '500': '#38b8e6',
          '600': '#1fa8d9',
          '700': '#0c8ebd',
          '800': '#00516e',
          '900': '#0d2933',
          plainColor: 'var(--joy-palette-primary-900)',
          plainHoverBg: 'var(--joy-palette-primary-500)',
          solidBg: 'var(--joy-palette-primary-700)',
          solidActiveBg: 'var(--joy-palette-primary-900)',
          solidHoverBg: 'var(--joy-palette-primary-800)',
          solidDisabledBg: 'var(--joy-palette-primary-50)',
          solidDisabledColor: 'var(--joy-palette-neutral-400)',
          outlinedColor: 'var(--joy-palette-primary-800)',
          outlinedBorder: 'var(--joy-palette-primary-700)',
          outlinedHoverBg: 'var(--joy-palette-primary-50)',
          softColor: 'var(--joy-palette-primary-800)',
          softBg: 'var(--joy-palette-primary-200)',
          softHoverBg: 'var(--joy-palette-primary-300)',
          softActiveBg: 'var(--joy-palette-primary-400)',
        },
      },
    },
    dark: {
      palette: {
        primary: {
          solidBg: 'var(--joy-palette-primary-400)',
        },
      },
    },
  },
});
