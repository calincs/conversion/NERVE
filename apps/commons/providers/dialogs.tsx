'use client';

import { AlertDialog } from '@/components/dialogs/alert-dialog';
import { StorageDialog } from '@/components/dialogs/storage-dialog';
import { filePanelAtoms } from '@/components/file-panel/store';
import { atoms } from '@/jotai/store';
import { useAtomValue } from 'jotai';

export const DialogProvider = ({ children }: { children: React.ReactNode }) => {
  const alertDialog = useAtomValue(atoms.alertDialog);
  const storageServiceDialog = useAtomValue(filePanelAtoms.storageServiceDialog);
  return (
    <>
      {children}
      <StorageDialog {...storageServiceDialog} />
      <AlertDialog {...alertDialog} />
    </>
  );
};
