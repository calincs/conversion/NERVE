'use client';

import { useTheme } from '@mui/joy';
import { SnackbarProvider as NotistackProvider } from 'notistack';
import * as React from 'react';

export const SnackbarProvider = ({ children }: { children: React.ReactNode }) => {
  const theme = useTheme();
  return (
    <NotistackProvider
      anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      autoHideDuration={4000}
      style={{
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.background.surface,
      }}
    >
      {children}
    </NotistackProvider>
  );
};
