import createIntlMiddleware from 'next-intl/middleware';
import { NextRequest, userAgent } from 'next/server';
import { locales } from './i18n';

export const config = {
  // Skip all paths that should not be internationalized. This example skips the
  // folders "api", "_next" and all files with an extension (e.g. favicon.ico)
  // matcher: ['/((?!api|_next|css|images|fonts|.*\\..*).*)'],
  matcher: ['/', '/(en|fr)/:path*', '/((?!api|_next|.*\\..*).*)'],
};

// export default async function middleware(request: NextRequest) {
//   //* Resposive: Get user viewport
//   const { device } = userAgent(request);
//   const viewport = device.type === 'mobile' ? 'mobile' : 'desktop';

//   //pass data to searchParams
//   if (viewport === 'mobile') request.nextUrl.searchParams.set('viewport', viewport);

//   //* Local Create and call the next-intl middleware
//   const handleI18nRouting = createIntlMiddleware({
//     locales,
//     defaultLocale: 'en',
//     // localePrefix: 'as-needed',
//   });
//   const response = handleI18nRouting(request);

//   return response;
// }

const intlMiddleware = createIntlMiddleware({
  locales,
  defaultLocale: 'en',
  // localePrefix: 'as-needed',
});

export default function middleware(request: NextRequest) {
  //* Resposive: Get user viewport
  const { device } = userAgent(request);
  const viewport = device.type === 'mobile' ? 'mobile' : 'desktop';

  //pass data to searchParams
  if (viewport === 'mobile') request.nextUrl.searchParams.set('viewport', viewport);

  // Matchers

  const { pathname } = request.nextUrl;

  const shouldHandle =
    pathname === '/' ||
    new RegExp(`^/(${locales.join('|')})(/.*)?$`).test(request.nextUrl.pathname);
  if (!shouldHandle) return;

  return intlMiddleware(request);
}
