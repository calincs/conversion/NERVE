module.exports = {
  root: true,
  extends: ['custom'],
  ignorePatterns: ['README.md', '.eslintrc.js', 'next.config.mjs'],
  rules: {
    // Consistently import navigation APIs from `@/navigation`
    'no-restricted-imports': [
      'error',
      {
        name: 'next/link',
        message: 'Please import from `@/navigation` instead.',
      },
      {
        name: 'next/navigation',
        importNames: ['redirect', 'permanentRedirect', 'useRouter', 'usePathname'],
        message: 'Please import from `@/navigation` instead.',
      },
    ],
  },
};
