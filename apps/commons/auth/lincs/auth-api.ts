import { contract } from '@lincs.project/auth-api-contract';
import { initTsrReactQuery } from '@ts-rest/react-query/v5';

export const getTsrLincsAuth = (baseUrl: string) => {
  return initTsrReactQuery(contract.v1, {
    baseUrl,
    baseHeaders: {},
  });
};
