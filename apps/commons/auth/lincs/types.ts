import { contract } from '@lincs.project/auth-api-contract';
import { type ClientInferResponseBody } from '@ts-rest/core';

export type LinkedAccounts = ClientInferResponseBody<
  typeof contract.v1.users.getLinkedAccounts,
  200
>;
export type LinkedAccountInstance = LinkedAccounts[0];

export interface IdentityProviderToken {
  access_token: string;
  accessTokenExpiration?: number;
  scope?: string;
  token_type: string;
  expires_in?: number;
  refresh_token?: string;
  created_at?: number;
}

export interface LinkedAccount extends Omit<LinkedAccountInstance, 'identityProvider'> {
  access_token: string;
  accessTokenExpires?: number;
  identityProvider: string;
  userId?: string;
  userName?: string;
  token?: IdentityProviderToken;
}
