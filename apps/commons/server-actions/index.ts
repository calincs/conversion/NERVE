'use server';

import { auth } from '@/auth';
import type { UrlResource } from '@/types';
import crypto from 'crypto';
import fs from 'fs/promises';
import jsonwebtoken from 'jsonwebtoken';
import { nanoid } from 'nanoid';
import { getLocale } from 'next-intl/server';
import queryString from 'query-string';

export const getManageAccountUrl = async (codeVerifier: string) => {
  'use server';

  const { KEYCLOAK_BASE_URL, KEYCLOAK_ID, KEYCLOAK_REALM, HOST_URL } = process.env;

  const locale = await getLocale();

  const referrer_uri = queryString.stringifyUrl({
    url: `${HOST_URL}/${locale}/manage-account`,
    query: { codeVerifier },
  });

  const url = queryString.stringifyUrl({
    url: `${KEYCLOAK_BASE_URL}/realms/${KEYCLOAK_REALM}/account`,
    query: {
      referrer: KEYCLOAK_ID,
      referrer_uri,
    },
  });

  return url;
};

interface getLinkAccountUrlProps {
  codeVerifier: string;
  isRefresh?: boolean;
  providerId: string;
}

export const getLinkAccountUrl = async ({
  codeVerifier,
  isRefresh,
  providerId,
}: getLinkAccountUrlProps) => {
  'use server';

  const { KEYCLOAK_BASE_URL, KEYCLOAK_REALM, HOST_URL } = process.env;

  const locale = await getLocale();

  const session = await auth();

  const accessToken = session?.access_token;
  if (!accessToken) return;

  const redirect_uri = queryString.stringifyUrl(
    {
      url: `${HOST_URL}/${locale}/link-accounts`,
      query: { providerId, isRefresh, codeVerifier },
    },
    { skipNull: true },
  );

  try {
    const token = jsonwebtoken.decode(accessToken);
    if (!token || typeof token === 'string') return;

    const userSession = token.session_state as string; // token.getSessionState() or "user session id"
    const client_id = token.azp as string;
    const nonce = nanoid();
    const input = nonce + userSession + client_id + providerId;
    const hash = crypto.createHash('sha256').update(input, 'utf-8').digest().toString('base64url');

    const url = queryString.stringifyUrl({
      url: `${KEYCLOAK_BASE_URL}/realms/${KEYCLOAK_REALM}/broker/${providerId}/link`,
      query: {
        nonce,
        hash,
        client_id,
        redirect_uri,
      },
    });

    return url;
  } catch (error) {
    console.error(error);
    return;
  }
};

export const loadCollection = async (collection: 'samples') => {
  'use server';
  const file = await fs.readFile(`${process.cwd()}/public/content/${collection}.json`, 'utf8');
  const documents = JSON.parse(file);
  return documents as UrlResource[];
};

export const toHashHex = async (string: string) => {
  'use server';
  const data = new TextEncoder().encode(string);
  const hashBuffer = await crypto.subtle.digest('SHA-256', data);
  const hashArray = Array.from(new Uint8Array(hashBuffer));
  const hashHex = hashArray.map((bytes) => bytes.toString(16).padStart(2, '0')).join('');
  return hashHex;
};
