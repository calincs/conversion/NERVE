import { Link, Stack, Typography } from '@mui/joy';
import type { MDXComponents } from 'mdx/types';
import Image, { type ImageProps } from 'next/image';

export const MdxComponents = (override?: MDXComponents): MDXComponents => {
  return {
    wrapper: ({ children }) => (
      <Stack p={{ xs: 2, sm: 0 }} spacing={4}>
        {children}
      </Stack>
    ),
    section: ({ children }) => (
      <Stack direction={{ xs: 'column', sm: 'row' }} gap={{ xs: 2, sm: 6 }}>
        {children}
      </Stack>
    ),
    h1: ({ children }) => (
      <Typography
        level="h1"
        sx={{
          '[data-mui-color-scheme="dark"] &': { color: 'primary.300' },
        }}
      >
        {children}
      </Typography>
    ),
    h2: ({ children }) => (
      <Typography
        level="h2"
        sx={{
          '[data-mui-color-scheme="dark"] &': { color: 'primary.300' },
        }}
      >
        {children}
      </Typography>
    ),
    h3: ({ children }) => (
      <Typography
        level="h3"
        sx={{
          '[data-mui-color-scheme="dark"] &': { color: 'primary.300' },
        }}
      >
        {children}
      </Typography>
    ),
    h4: ({ children }) => (
      <Typography
        level="h4"
        sx={{
          '[data-mui-color-scheme="dark"] &': { color: 'primary.300' },
        }}
      >
        {children}
      </Typography>
    ),
    p: ({ children }) => (
      <Typography level="body-sm" pb={2}>
        {children}
      </Typography>
    ),

    a: ({ children, ...props }) => (
      <Link
        color="neutral"
        rel="noreferrer"
        target="_blank"
        underline="always"
        {...(props as HTMLLinkElement['attributes'])}
      >
        {children}
      </Link>
    ),
    img: ({ alt, ...props }) => (
      <Image
        sizes="100vw"
        style={{ width: '100%', height: 'auto' }}
        {...(props as ImageProps)}
        alt={alt ?? ''}
      />
    ),
    code: ({ children }) => (
      <Typography
        level="body-sm"
        component="code"
        sx={{
          px: 0.5,
          borderRadius: 1,
          borderWidth: 1,
          borderStyle: 'solid',
          borderColor: 'grey.400',
          backgroundColor: 'color-mix(in srgb, var(--mui-palette-grey-200) 50%, transparent)',
          fontFamily: 'monospace',
          '[data-mui-color-scheme="dark"] &': {
            borderColor: 'grey.600',
            backgroundColor: 'color-mix(in srgb, var(--mui-palette-grey-800) 50%, transparent)',
          },
        }}
      >
        {children}
      </Typography>
    ),
    abbr: ({ children, title }) => (
      <Typography component="abbr" title={title}>
        {children}
      </Typography>
    ),
    ...override,
  };
};
