declare namespace NodeJS {
  export interface ProcessEnv {
    AUTH_SECRET: string;
    AUTH_URL: string;
    HOST_URL: string;
    KEYCLOAK_BASE_URL: string;
    KEYCLOAK_ID: string;
    KEYCLOAK_REALM: string;
    KEYCLOAK_SECRET: string;
    LINCS_AUTH_API_URL: string;
    NEXT_PUBLIC_GA_ID: string;
  }
}
