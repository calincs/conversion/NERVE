import { Logo } from '@/components';
import { Button, Divider, Sheet, Stack, Typography } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { Link } from '../../navigation';

export default function NotFound() {
  const t = useTranslations();
  return (
    <Sheet
      component="main"
      sx={{
        height: '100vh',
        width: '100vw',
        py: { xs: 6, sm: 12 },
        background: 'linear-gradient(180deg, rgba(45,156,219,0) 0%, rgba(13,103,142,0.4) 100%)',
      }}
    >
      <Stack alignItems="center" gap={5} justifyContent="center" sx={{ height: '50vh' }}>
        <Stack>
          <Logo direction="vertical" variation="simplified" />
          <Typography
            letterSpacing="8px"
            level="h2"
            textAlign="center"
            textTransform="uppercase"
            sx={{ textDecoration: 'line-through', textDecorationStyle: 'dotted' }}
          >
            racking
          </Typography>
        </Stack>
        <Stack alignItems="center" gap={2}>
          <Stack direction="row" gap={2}>
            <Typography level="h3" textTransform="uppercase">
              404
            </Typography>
            <Divider orientation="vertical" />
            <Typography fontWeight={200} level="h3">
              {t('NotFoundPage.This page could not be found')}
            </Typography>
          </Stack>
          <Typography level="body-sm" textAlign="center">
            {t('NotFoundPage.description')}
          </Typography>
        </Stack>
        <Link href="/" passHref>
          <Button variant="outlined">{t('NotFoundPage.Go to Homepage')}</Button>
        </Link>
      </Stack>
    </Sheet>
  );
}
