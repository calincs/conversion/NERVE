import { isValidXml } from '@/util/DOM';

export type InitError =
  | { reason: 'invalid-url' }
  | { reason: 'resource-not-found' | 'resource-not-supported'; data: string }
  | { reason: 'missing-provider-attributes'; data: Record<string, string | undefined> };

const userIdProviderBaseUrl = new Map<string, string>([
  ['github', 'https://github.com'],
  ['gitlab', 'https://gitlab.com'],
  ['orcid', 'https://orcid.org'],
]);

export const getUserUrl = ({
  provider = '',
  username = '',
}: {
  provider?: string;
  username?: string;
}) => {
  const baseUrl = userIdProviderBaseUrl.get(provider);
  if (!baseUrl || username === '') return 'anonymous';
  return `${baseUrl}/${username}`;
};

export const fetchDocument = async (
  url: string,
): Promise<{ content?: string; error?: InitError }> => {
  try {
    const response = await fetch(new URL(url));

    if (!response.ok) {
      return { error: { reason: 'resource-not-found', data: url } };
    }

    const data = await response.text();
    if (!isValidXml(data)) {
      return { error: { reason: 'resource-not-supported', data: url } };
    }

    return { content: data };
  } catch (error) {
    return { error: { reason: 'invalid-url' } };
  }
};
