'use client';

import { atoms } from '@/jotai/store';
import {
  Badge,
  Box,
  IconButton,
  Stack,
  Tooltip,
  Typography,
  useTheme,
  type BadgeProps,
  type IconButtonProps,
  type StackProps,
  type TooltipProps,
} from '@mui/joy';
import { motion, type Variants } from 'framer-motion';
import { useAtomValue } from 'jotai';
import { useTranslations } from 'next-intl';
import { TbCloud, TbCloudCheck, TbCloudUp } from 'react-icons/tb';
import { useResource } from '../../hooks/useResource';
import { nerveAtoms } from '../../nerve/store';

export interface SaveCloudProps {
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    tooltip?: Omit<TooltipProps, 'children' | 'title'>;
    iconButton?: Omit<IconButtonProps, 'children' | 'disabled'>;
    badge?: Omit<BadgeProps, 'children' | 'invisible'>;
  };
}

export const SaveCloud = ({ slotProps }: SaveCloudProps) => {
  const theme = useTheme();
  const t = useTranslations();

  const { handleSave } = useResource();

  const resource = useAtomValue(atoms.resource);

  const delaySaveTimer = useAtomValue(nerveAtoms.delaySaveTimer);
  const documentHasChanged = useAtomValue(nerveAtoms.documentHasChanged);
  const isSaving = useAtomValue(nerveAtoms.isSaving);
  const nerveActions = useAtomValue(nerveAtoms.nerveActions);

  const onPointerDown = () => {
    if (!nerveActions?.getContentHasChanged() || isSaving) return;
    handleSave();
  };

  const animationProps: Variants = {
    visible: { width: 'auto', opacity: 1 },
    hidden: { width: 0, opacity: 0 },
  };

  return (
    <Stack direction="row" alignItems="center" {...slotProps?.root}>
      <Tooltip
        size="sm"
        title={
          documentHasChanged
            ? t('storage.click to save')
            : isSaving
              ? t('storage.saving')
              : t('storage.all changes salved')
        }
        {...slotProps?.tooltip}
      >
        <IconButton
          aria-label="save"
          disabled={!documentHasChanged || isSaving}
          size="sm"
          sx={{
            mt: -0.125,
            ml: 0.5,
            cursor: documentHasChanged ? 'pointer' : 'default',
          }}
          {...slotProps?.iconButton}
          onPointerDown={(event) => {
            onPointerDown();
            slotProps?.iconButton?.onPointerDown?.(event);
          }}
        >
          <Badge
            badgeInset="14%"
            color="warning"
            invisible={!documentHasChanged || isSaving}
            size="sm"
            {...slotProps?.badge}
          >
            {isSaving ? (
              <TbCloudUp style={{ width: 16, height: 16 }} />
            ) : documentHasChanged ? (
              <TbCloud color={theme.vars.palette.warning[500]} style={{ width: 16, height: 16 }} />
            ) : (
              <TbCloudCheck style={{ width: 16, height: 16 }} />
            )}
          </Badge>
        </IconButton>
      </Tooltip>
      {resource?.storageSource === 'cloud' && (
        <Box
          position="absolute"
          ml={3.5}
          overflow="hidden"
          sx={{ opacity: 0.5 }}
          component={motion.div}
          variants={animationProps}
          initial="visible"
          animate={delaySaveTimer.isRunning() ? 'visible' : 'hidden'}
        >
          <Typography level="body-xs" ml={0.5} whiteSpace="nowrap">
            {t('storage.waiting provider', { provider: 'github' })}
          </Typography>
        </Box>
      )}
    </Stack>
  );
};
