'use client';

import { atoms } from '@/jotai/store';
import {
  Stack,
  Tooltip,
  Typography,
  type StackProps,
  type TooltipProps,
  type TypographyProps,
} from '@mui/joy';
import { useAtomValue } from 'jotai';
import { useTranslations } from 'next-intl';
import { useMemo } from 'react';
import { FullPath } from './full-path';
import { SaveCloud } from './save-cloud';

export interface MetaProps {
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    tooltip?: Omit<TooltipProps, 'children' | 'title'>;
    typography?: Omit<TypographyProps, 'children'>;
  };
}

export const Meta = ({ slotProps }: MetaProps) => {
  const t = useTranslations();

  const resource = useAtomValue(atoms.resource);

  const fullPath = useMemo(() => {
    if (!resource) return;
    if (resource.storageSource === 'local' || resource.storageSource === 'paste') return;

    if (resource.storageSource === 'url') return resource.url;

    const { filename, owner, path, repo } = resource;
    return `${owner}: ${repo}/${path && `${path}/`}${filename}`;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [resource?.filename]);

  return (
    <>
      {resource && (
        <Stack
          direction="row"
          flex={1}
          flexShrink={1}
          justifyContent="center"
          alignItems="center"
          minWidth={0}
          {...slotProps?.root}
        >
          <Tooltip
            enterDelay={1000}
            size="sm"
            title={
              fullPath && (
                <FullPath
                  provider={resource.storageSource === 'cloud' ? resource.provider : undefined}
                  url={
                    resource.storageSource === 'cloud' || resource.storageSource === 'url'
                      ? resource.url
                      : undefined
                  }
                >
                  {fullPath}
                </FullPath>
              )
            }
            variant="soft"
            {...slotProps?.tooltip}
          >
            <Typography
              component="h2"
              ml={1}
              sx={{
                cursor: 'default',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
              }}
              level="title-md"
              {...slotProps?.typography}
            >
              {resource.title ?? resource.filename ?? t('common.untitled')}
            </Typography>
          </Tooltip>
          {resource.storageSource === 'cloud' && <SaveCloud />}
        </Stack>
      )}
    </>
  );
};
