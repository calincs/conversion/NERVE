import { ProviderIcon, type ProviderIconProps } from '@/components/icons';
import {
  IconButton,
  Stack,
  Typography,
  type IconButtonProps,
  type StackProps,
  type TypographyProps,
} from '@mui/joy';
import type { IconBaseProps } from 'react-icons';
import { HiExternalLink } from 'react-icons/hi';

interface FullPathProps extends React.PropsWithChildren {
  provider?: string;
  url?: string;
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    providerIcon?: Omit<ProviderIconProps, 'providerId'>;
    typography?: Omit<TypographyProps, 'children'>;
    iconButton?: Omit<IconButtonProps, 'children'>;
    icon?: IconBaseProps;
  };
}

export const FullPath = ({ children, provider, url, slotProps }: FullPathProps) => {
  return (
    <Stack
      direction="row"
      alignItems="center"
      sx={{ overflow: 'hidden' }}
      spacing={1}
      {...slotProps?.root}
    >
      <ProviderIcon
        providerId={provider}
        style={{ width: 14, height: 14 }}
        {...slotProps?.providerIcon}
      />
      <Typography sx={{ cursor: 'default' }} level="body-xs" {...slotProps?.typography}>
        {children}
      </Typography>
      {url && (
        <IconButton
          size="sm"
          sx={{ minWidth: 20, minHeight: 20, borderRadius: 4 }}
          {...slotProps?.iconButton}
          onPointerDown={(event) => {
            window.open(url, '_blank');
            slotProps?.iconButton?.onPointerDown?.(event);
          }}
        >
          <HiExternalLink style={{ width: 12, height: 12 }} {...slotProps?.icon} />
        </IconButton>
      )}
    </Stack>
  );
};
