import { Logo, type LogoProps } from '@/components/logo';
import { Stack, type StackProps } from '@mui/joy';
import { MainMenu } from '../menu';

export interface NavBarProps {
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    logo?: LogoProps;
  };
}

export const NavBar = ({ slotProps }: NavBarProps) => (
  <Stack direction="row" alignItems="center" gap={1} {...slotProps?.root}>
    <MainMenu />
    <Logo height={32} variation="simplified" {...slotProps?.logo} />
  </Stack>
);
