'use client';

import { MenuItemButton, type MenuItemButtonProps } from '@/components/ui';
import {
  Divider,
  Dropdown,
  IconButton,
  List,
  Menu,
  MenuButton,
  type DividerProps,
  type ListProps,
  type MenuButtonProps,
  type MenuProps,
} from '@mui/joy';
import { useSession } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import { LuMenu } from 'react-icons/lu';
import { useResource } from '../../hooks/useResource';
import { ExportMenu } from './export-menu';

interface MainMenuProps {
  slotProps?: {
    menuButton?: Omit<MenuButtonProps, 'children'>;
    menu?: Omit<MenuProps, 'children'>;
    divider?: DividerProps;
    list?: Omit<ListProps, 'children'>;
    menuItemButton?: Omit<MenuItemButtonProps, 'children'>;
  };
}

export const MainMenu = ({ slotProps }: MainMenuProps) => {
  const { status: sessionStatus } = useSession();
  const t = useTranslations();

  const { handleClose, handleOpenStorageDialog, handleSave } = useResource();

  return (
    <Dropdown>
      <MenuButton slots={{ root: IconButton }} {...slotProps?.menuButton}>
        <LuMenu />
      </MenuButton>
      <Menu
        placement="bottom-end"
        size="sm"
        sx={{ minWidth: 180 }}
        variant="soft"
        {...slotProps?.menu}
      >
        <MenuItemButton
          {...slotProps?.menuItemButton}
          disabled={sessionStatus !== 'authenticated'}
          onPointerDown={(event) => {
            handleOpenStorageDialog();
            slotProps?.menuItemButton?.onPointerDown?.(event);
          }}
        >
          {t('common.open')}...
        </MenuItemButton>
        <Divider sx={{ my: 0.5 }} {...slotProps?.divider} />
        <List sx={{ gap: 1 }} {...slotProps?.list}>
          <MenuItemButton
            disabled={sessionStatus !== 'authenticated'}
            onPointerDown={(event) => {
              handleSave('save');
              slotProps?.menuItemButton?.onPointerDown?.(event);
            }}
          >
            {t('common.save')}
          </MenuItemButton>
          <MenuItemButton
            disabled={sessionStatus !== 'authenticated'}
            onPointerDown={(event) => {
              handleSave('saveAs');
              slotProps?.menuItemButton?.onPointerDown?.(event);
            }}
          >
            {t('common.saveAs')}...
          </MenuItemButton>
          <ExportMenu />
        </List>
        <Divider sx={{ my: 0.5 }} {...slotProps?.divider} />
        <MenuItemButton
          onPointerDown={(event) => {
            handleClose();
            slotProps?.menuItemButton?.onPointerDown?.(event);
          }}
        >
          {t('common.close')}
        </MenuItemButton>
      </Menu>
    </Dropdown>
  );
};
