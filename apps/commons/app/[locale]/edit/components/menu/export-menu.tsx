'use client';

import { MenuItemButton, type MenuItemButtonProps } from '@/components/ui';
import {
  Box,
  ListItemButton,
  ListItemContent,
  Menu,
  type BoxProps,
  type ListItemButtonProps,
  type ListItemContentProps,
  type MenuProps,
} from '@mui/joy';
import { useTranslations } from 'next-intl';
import { useRef, useState } from 'react';
import type { IconBaseProps } from 'react-icons';
import { IoChevronForwardSharp } from 'react-icons/io5';
import { useResource } from '../../hooks/useResource';

interface ExportMenuProps {
  slotProps?: {
    root?: Omit<BoxProps, 'children' | 'ref'>;
    listItemButton?: Omit<ListItemButtonProps, 'children'>;
    menu?: Omit<MenuProps, 'children' | 'anchorEl' | 'open'>;
    content?: Omit<ListItemContentProps, 'children'>;
    chevronIcon?: IconBaseProps;
    menuButon?: Omit<MenuItemButtonProps, 'children'>;
  };
}

export const ExportMenu = ({ slotProps }: ExportMenuProps) => {
  const t = useTranslations();

  const { handleExportAnnotations, handleExport } = useResource();

  const [open, setOpen] = useState(false);

  const ref = useRef<HTMLElement | null>(null);

  return (
    <Box
      ref={ref}
      {...slotProps?.root}
      onPointerEnter={(event) => {
        setOpen(true);
        slotProps?.root?.onPointerEnter?.(event);
      }}
      onPointerLeave={(event) => {
        setOpen(false);
        slotProps?.root?.onPointerLeave?.(event);
      }}
    >
      <ListItemButton
        sx={{ mx: 0.5, px: 0.9, textTransform: 'capitalize', borderRadius: 4 }}
        variant="soft"
        {...slotProps?.listItemButton}
      >
        <ListItemContent {...slotProps?.content}>{t('common.export')}</ListItemContent>
        <IoChevronForwardSharp {...slotProps?.chevronIcon} />
      </ListItemButton>

      <Menu
        anchorEl={ref.current}
        open={open}
        placement="left-start"
        size="sm"
        sx={{ ml: '-4px !important' }}
        variant="soft"
        {...slotProps?.menu}
      >
        <MenuItemButton
          {...slotProps?.menuButon}
          onPointerDown={(event) => {
            handleExport();
            slotProps?.menuButon?.onPointerDown?.(event);
          }}
        >
          {t('common.export xml and annotations')}
        </MenuItemButton>
        <MenuItemButton
          onPointerDown={(event) => {
            handleExport({ includeStandoff: false });
            slotProps?.menuButon?.onPointerDown?.(event);
          }}
        >
          {t('common.xml only')}
        </MenuItemButton>
        <MenuItemButton
          onPointerDown={(event) => {
            handleExportAnnotations();
            slotProps?.menuButon?.onPointerDown?.(event);
          }}
        >
          {t('common.web annotation only')}
        </MenuItemButton>
      </Menu>
    </Box>
  );
};
