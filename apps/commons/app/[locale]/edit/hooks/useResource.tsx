import { filePanelAtoms } from '@/components/file-panel/store';
import { AUTOSAVE_DELAY, AUTOSAVE_ON, SAVE_RETRY_DELAY } from '@/config';
import { db, RecentDocument } from '@/db';
import { atoms } from '@/jotai/store';
import { useRouter } from '@/navigation';
import { Error, Resource } from '@/types';
import { isErrorMessage } from '@/types/assert';
import { log } from '@/util/log';
import { saveDocument } from '@cwrc/leafwriter-storage-service';
import { saveAs as fileSaver } from 'file-saver';
import { useAtom, useAtomValue, useSetAtom } from 'jotai';
import { useSession } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import { enqueueSnackbar } from 'notistack';
import { nerveAtoms } from '../nerve/store';

export const useResource = () => {
  const router = useRouter();
  const { data: session, status: sessionStatus } = useSession();
  const t = useTranslations();

  const [resource, setResource] = useAtom(atoms.resource);

  const loadStorageServiceDialog = useSetAtom(filePanelAtoms.loadStorageServiceDialog);
  const saveStorageServiceDialog = useSetAtom(filePanelAtoms.saveStorageServiceDialog);

  const autosaveTimer = useAtomValue(nerveAtoms.autosaveTimer);
  const delaySaveTimer = useAtomValue(nerveAtoms.delaySaveTimer);
  const setDocumentHasChanged = useSetAtom(nerveAtoms.documentHasChanged);
  const [nerveActions, setNerveActions] = useAtom(nerveAtoms.nerveActions);
  const setIsSaving = useSetAtom(nerveAtoms.isSaving);

  const userProviders = session?.user.linkedAccounts?.map(({ access_token, identityProvider }) => ({
    access_token: access_token,
    name: identityProvider,
  }));

  const handleClose = () => {
    nerveActions?.unload();
    setNerveActions(null);
    router.push('/');
  };

  const handleDocumentHasChanged = (value: boolean) => {
    setDocumentHasChanged(value);

    if (AUTOSAVE_ON) {
      if (!value) {
        autosaveTimer.stop();
        return;
      }

      if (autosaveTimer.isRunning()) autosaveTimer.stop();
      autosaveTimer.start({ countdown: true, startValues: { seconds: AUTOSAVE_DELAY } });
    }
  };

  const handleExportAnnotations = async () => {
    if (!nerveActions || !resource) return;

    const annotations = nerveActions.getAnnotations();
    const blob = new Blob([JSON.stringify(annotations, null, 4)], {
      type: 'application/ld+json',
    });
    const filename = resource.filename
      ? `${resource.filename}_annotations.json`
      : 'annotations.xml';
    fileSaver(blob, filename);
  };

  const handleExport = async ({ includeStandoff = true }: { includeStandoff?: boolean } = {}) => {
    if (!nerveActions || !resource) return;

    const content = nerveActions.getContent({ includeStandoff });
    if (!content) return;

    const filename = resource.filename ?? 'untitled.xml';
    const blob = new Blob([content], { type: 'application/xml' });
    fileSaver(blob, filename);
  };

  const handleOpenStorageDialog = async () => {
    loadStorageServiceDialog({
      source: sessionStatus === 'authenticated' ? 'cloud' : 'local',
    });
  };

  const handleSave = async (action: 'save' | 'saveAs' = 'save') => {
    if (!nerveActions || !resource) return;

    const content = nerveActions.getContent();
    if (!content) return;

    // const screenshot = await nerveActions.getDocumentScreenshot();

    if (action === 'saveAs') {
      saveAs({
        content,
        // screenshot,
      });
      return;
    }

    const saved = await save({
      content,
      // screenshot
    });

    enqueueSnackbar(
      saved.success
        ? t('storage.document saved')
        : `${saved.error.message}. ${t('storage.document not saved')}!`,
      { variant: saved.success ? 'success' : 'warning' },
    );
  };

  const save = async ({
    content,
    screenshot,
  }: {
    content: string;
    screenshot?: string;
  }): Promise<{ success: true } | { success: false; error: Error }> => {
    if (!resource) {
      return {
        success: false,
        error: { type: 'error', message: 'Resource not found' },
      };
    }

    // Check diff document
    if (!nerveActions?.getContentHasChanged()) {
      return { success: true };
    }

    setIsSaving(true);

    // Check provider
    if (resource.storageSource !== 'cloud') {
      const message = t('storage.provider not found');
      log.error(message);
      setIsSaving(false);
      return { success: false, error: { type: 'error', message } };
    }

    const provider = userProviders?.find(({ name }) => name === resource.provider);
    if (!provider) {
      const message = t('storage.provider not found');
      log.error(message);
      setIsSaving(false);
      return { success: false, error: { type: 'error', message } };
    }

    //Prepare resource
    const updatedResource = {
      ...resource,
      content,
      screenshot,
    };

    //* Resquest save
    const response = await saveDocument(provider, updatedResource, true);

    if (isErrorMessage(response)) {
      log.error(response.message);

      if (response.message !== 'conflict') {
        setIsSaving(false);
        return { success: false, error: response };
      }

      if (AUTOSAVE_ON) autosaveTimer.stop();

      delaySaveTimer.stop();
      delaySaveTimer.start({ countdown: true, startValues: { seconds: SAVE_RETRY_DELAY } });

      return { success: false, error: response };
    }

    // Finalize
    setResource(updatedResource);
    localDbUpdate(updatedResource);

    afterSave();

    return { success: true };
  };

  const saveAs = async ({ content, screenshot }: { content: string; screenshot?: string }) => {
    if (!resource) return;

    if (AUTOSAVE_ON) autosaveTimer.pause();

    saveStorageServiceDialog({
      source: 'cloud',
      resource: { ...resource, content },
      _onClose: (_resource) => {
        if (!_resource) {
          if (AUTOSAVE_ON) autosaveTimer.start();
          return;
        }
        setResource({ ..._resource, content, screenshot });
        localDbAdd({ ..._resource, content, screenshot });

        enqueueSnackbar(t('storage.document saved'), { variant: 'success' });

        afterSave();
      },
    });
  };

  const afterSave = async () => {
    nerveActions?.resetContentHasChanged();
    if (AUTOSAVE_ON) {
      autosaveTimer.stop();
      delaySaveTimer.stop();
    }
    setIsSaving(false);
  };

  const localDbAdd = async (resource: Resource) => {
    if (
      sessionStatus !== 'authenticated' ||
      resource.storageSource === 'local' ||
      resource.storageSource === 'paste' ||
      !resource.url ||
      'isSample' in resource
    ) {
      return;
    }

    const lastOpenedAt = new Date();

    const resourceExistsLocally = await db.recentDocuments.get(resource.url);
    if (resourceExistsLocally) {
      await db.recentDocuments.update(resourceExistsLocally.id, { lastOpenedAt });
      return { ...resourceExistsLocally, lastOpenedAt };
    }

    const newRecentDocument: RecentDocument = { id: resource.url, resource, lastOpenedAt };
    await db.recentDocuments.put(newRecentDocument, resource.url);
    return newRecentDocument;
  };

  const localDbUpdate = async (resource: Resource) => {
    if (
      sessionStatus !== 'authenticated' ||
      resource.storageSource === 'local' ||
      resource.storageSource === 'paste' ||
      !resource.url ||
      'isSample' in resource
    ) {
      return;
    }

    const resourceExistsLocally = await db.recentDocuments.get(resource.url);
    localDbAdd(resource);
    if (!resourceExistsLocally) {
      return;
    }

    const affected = await db.recentDocuments.update(resourceExistsLocally.id, {
      lastOpenedAt: new Date(),
    });
    if (affected === 0) {
      await db.recentDocuments.put(resourceExistsLocally, resourceExistsLocally.id);
    }

    return resourceExistsLocally.id;
  };

  return {
    handleClose,
    handleDocumentHasChanged,
    handleExportAnnotations,
    handleExport,
    handleOpenStorageDialog,
    handleSave,
    localDbAdd,
    localDbUpdate,
  };
};
