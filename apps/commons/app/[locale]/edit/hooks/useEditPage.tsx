'use client';

import { db } from '@/db';
import { usePermalink } from '@/hooks';
import { usePemalinkDialogs } from '@/hooks/permalink/usePemalinkDialogs';
import { atoms } from '@/jotai/store';
import { useRouter } from '@/navigation';
import type { CloudResource, Resource, UrlResource } from '@/types';
import { splitPathFilename } from '@/util/string';
import { useAtom } from 'jotai';
import { useSession } from 'next-auth/react';
import { useSearchParams } from 'next/navigation';
import { useEffect, useState } from 'react';
import z from 'zod';
import { fetchDocument, getUserUrl, InitError } from '../helpers';
import { useResource } from './useResource';

export const useEditPage = () => {
  const router = useRouter();
  const searchParams = useSearchParams();
  const { data: session, status: sessionStatus } = useSession();

  const { getResourceFromPermalink } = usePermalink();
  const { localDbAdd } = useResource();

  const [resource, setResource] = useAtom(atoms.resource);
  const [user, setUser] = useAtom(atoms.user);

  const [isReady, setIsReady] = useState(false);

  const {
    handleInvalidPermalink,
    handleInvalidUrl,
    handleResourceNotFound,
    handleResourceNotSupported,
    handleMissingProviderAttributes,
  } = usePemalinkDialogs();

  useEffect(() => {
    init();
  }, []);

  useEffect(() => {
    if (sessionStatus !== 'loading') processUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sessionStatus]);

  const processUser = async () => {
    const prefIdentityProvider = await db.userPreferences.get('identityProvider');

    const name = session?.user?.name ?? 'anonymous';
    const url = getUserUrl({
      provider: prefIdentityProvider?.value,
      username: session?.user?.username,
    });

    setUser({ name, url });
  };

  const init = async () => {
    if (!resource && searchParams.size === 0) {
      router.push(`/`);
      return;
    }

    const currentResource = resource ?? (await getResourceFromPermalink());
    if (!currentResource) return;

    if (currentResource.storageSource !== 'url' && currentResource.storageSource !== 'cloud') {
      setIsReady(true);
      return;
    }

    const source = currentResource.storageSource;

    const { data, error } =
      source === 'url'
        ? await loadFromUrl(currentResource)
        : await loadFromProvider(currentResource);

    if (error) {
      const reason = error.reason;
      if (reason === 'invalid-url') handleInvalidUrl();
      if (reason === 'missing-provider-attributes') handleMissingProviderAttributes(error.data);
      if (reason === 'resource-not-found') handleResourceNotFound(error.data);
      if (reason === 'resource-not-supported') handleResourceNotSupported(error.data);
      return;
    }

    if (!data) {
      handleInvalidUrl();
      return;
    }

    setResource(data);
    await localDbAdd(data);

    setIsReady(true);
  };

  const loadFromUrl = async (
    resource: UrlResource,
  ): Promise<{ data?: Resource; error?: InitError }> => {
    const { content, error } = await fetchDocument(resource.url);
    if (error) return { error };

    const [, extractedFilename] = splitPathFilename(resource.url);
    const filename = resource.filename ?? extractedFilename;

    return { data: { ...resource, filename, content } };
  };

  const loadFromProvider = async (
    resource: CloudResource,
  ): Promise<{ data?: Resource; error?: InitError }> => {
    const { provider, owner, repo, branch, path, filename } = resource;

    if (!provider || !owner || !repo || !filename) {
      return {
        error: {
          reason: 'missing-provider-attributes',
          data: { provider, owner, repo, branch, path, filename },
        },
      };
    }

    const _path = path && path !== '' ? `${path}/` : '';

    if (!resource.url) {
      if (provider === 'github') {
        resource.url = `https://raw.githubusercontent.com/${owner}/${repo}/${branch}/${_path}${filename}`;
      }
      if (provider === 'gitlab') {
        resource.url = `https://gitlab.com/${owner}/${repo}/-/raw/${branch}/${_path}${filename}?ref_type=heads`;
      }
    }

    const isValidUrl = z.string().url().safeParse(resource.url).success;
    if (!isValidUrl || !resource.url) {
      return { error: { reason: 'invalid-url' } };
    }

    const { content, error } = await fetchDocument(resource.url);
    if (error) return { error };

    return { data: { ...resource, content } };
  };

  return {
    isReady,
    user,
  };
};
