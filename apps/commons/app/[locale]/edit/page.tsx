import { auth } from '@/auth';
import { Profile, TopBar } from '@/components';
import { ProfileAnonymous } from '@/components/profile-anonymous';
import { SignIn } from '@/components/ui';
import { locales } from '@/i18n';
import { redirect } from '@/navigation';
import { splitPathFilename } from '@/util/string';
import { Sheet } from '@mui/joy';
import { Metadata } from 'next';
import { unstable_setRequestLocale } from 'next-intl/server';
import { Suspense } from 'react';
import { Meta } from './components/meta';
import { NavBar } from './components/navbar';
import { Main } from './main';

const dynamic = 'force-dynamic';

export async function generateMetadata({
  searchParams,
}: {
  searchParams: Record<string, string | string[] | null>;
}): Promise<Metadata> {
  let filename = Array.isArray(searchParams.filename)
    ? searchParams.filename[0]
    : searchParams.filename;
  const url = Array.isArray(searchParams.url) ? searchParams.url[0] : searchParams.url;

  if (!filename && url) {
    const [, extractedFilename] = splitPathFilename(url);
    filename = extractedFilename;
  }

  return {
    title: filename,
  };
}

export default async function EditPage({
  params: { locale },
  searchParams,
}: {
  params: { locale: (typeof locales)[number] };
  searchParams: Record<string, string | string[] | null>;
}) {
  unstable_setRequestLocale(locale);

  if (searchParams.viewport === 'mobile') {
    redirect('/');
  }

  const session = await auth();

  return (
    <main style={{ height: '100vh', width: '100vw' }}>
      <Sheet
        sx={{
          height: '100vh',
          background: 'linear-gradient(180deg, rgba(45,156,219,0) 0%, rgba(13,103,142,0.4) 100%)',
        }}
      >
        <TopBar
          left={<NavBar />}
          center={<Meta />}
          right={
            session ? (
              <Profile />
            ) : (
              <>
                <SignIn color="neutral" size="sm" variant="outlined" />
                <ProfileAnonymous />
              </>
            )
          }
        />
        <Suspense fallback={'loading...'}>
          <Main />
        </Suspense>
      </Sheet>
    </main>
  );
}
