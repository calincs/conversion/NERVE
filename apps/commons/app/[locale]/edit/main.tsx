'use client';

import { CircularProgress, Modal, type ModalProps } from '@mui/joy';
import { useEditPage } from './hooks/useEditPage';
import { NerveContainer } from './nerve';

export interface MainProps {
  slotProps?: {
    processingModal?: Omit<ModalProps, 'children' | 'open'>;
  };
}

export const Main = ({ slotProps }: MainProps) => {
  const { user, isReady } = useEditPage();

  return (
    <>
      {user && isReady ? (
        <NerveContainer />
      ) : (
        <Modal
          open={true}
          sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
          {...slotProps?.processingModal}
        >
          <CircularProgress />
        </Modal>
      )}
    </>
  );
};
