import { NerveActionsType } from '@lincs.project/nerve';
import { Timer } from 'easytimer.js';
import { atom } from 'jotai';

const autosaveTimer = new Timer();
const timerSaveDelay = new Timer();

export const nerveActionsAtom = atom<NerveActionsType | null>(null);
nerveActionsAtom.debugLabel = 'nerveActions.Atom';

export const isSavingAtom = atom(false);
isSavingAtom.debugLabel = 'isSaving.Atom';

export const documentHasChangedAtom = atom(false);
documentHasChangedAtom.debugLabel = 'documentHasChanged.Atom';

export const autosaveTimerAtom = atom(autosaveTimer);
autosaveTimerAtom.debugLabel = 'autosaveTimer.Atom';

export const delaySaveTimerAtom = atom(timerSaveDelay);
delaySaveTimerAtom.debugLabel = 'timerSaveDelay.Atom';

export const nerveAtoms = {
  autosaveTimer: autosaveTimerAtom,
  delaySaveTimer: delaySaveTimerAtom,
  documentHasChanged: documentHasChangedAtom,
  isSaving: isSavingAtom,
  nerveActions: nerveActionsAtom,
};
