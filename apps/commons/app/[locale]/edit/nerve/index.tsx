'use client';

import { AUTOSAVE_ON } from '@/config';
import { atoms } from '@/jotai/store';
import { Nerve, NerveActionsType } from '@lincs.project/nerve';
import { Stack, useColorScheme, type StackProps } from '@mui/joy';
import { useAtom, useAtomValue } from 'jotai';
import { useLocale } from 'next-intl';
import { useEffect, useMemo, useRef } from 'react';
import { useResource } from '../hooks/useResource';
import { nerveAtoms } from './store';

export interface NerveContainerProps {
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
  };
}

export const NerveContainer = ({ slotProps }: NerveContainerProps) => {
  const { mode, systemMode } = useColorScheme();
  const locale = useLocale();

  const { handleDocumentHasChanged, handleSave } = useResource();

  const user = useAtomValue(atoms.user);
  const resource = useAtomValue(atoms.resource);

  const autosaveTimer = useAtomValue(nerveAtoms.autosaveTimer);
  const delaySaveTimer = useAtomValue(nerveAtoms.delaySaveTimer);
  const [nerveActions, setNerveActions] = useAtom(nerveAtoms.nerveActions);

  const ref = useRef<NerveActionsType>(null);

  useEffect(() => {
    if (nerveActions && AUTOSAVE_ON) {
      autosaveTimer.on('targetAchieved', () => handleSave());
      delaySaveTimer.on('targetAchieved', () => handleSave());
      // autosaveTimer.on('secondsUpdated', (event) => {
      //   console.log('countdown: ', event.detail.timer.getTimeValues().seconds);
      // });
    }

    return () => {
      autosaveTimer.removeAllEventListeners();
      delaySaveTimer.removeAllEventListeners();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [nerveActions]);

  useEffect(() => {
    if (ref.current) {
      setNerveActions(ref.current);
    }
    return () => {
      setNerveActions(null);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ref]);

  const darkTheme = useMemo(() => {
    if (mode === 'dark') return true;
    if (mode === 'system' && systemMode === 'dark') return true;
    return false;
  }, [mode, systemMode]);

  return (
    <Stack sx={{ height: 'calc(100vh - 55px)', overflow: 'hidden' }} {...slotProps?.root}>
      {user && resource?.content && (
        <Nerve
          ref={ref}
          document={{
            content: resource.content,
            url: 'url' in resource ? resource.url : '',
          }}
          preferences={{ darkTheme, locale }}
          user={user}
          onDocumentOnchange={handleDocumentHasChanged}
        />
      )}
    </Stack>
  );
};
