import { CookieConsentComponent } from '@/components/cookie-consent';
import { locales } from '@/i18n';
import { DialogProvider } from '@/providers/dialogs';
import { JotaiProvider } from '@/providers/jotai-provider';
import { NextAuthProvider } from '@/providers/next-auth-provider';
import { ReactQueryProvider } from '@/providers/react-query-provider';
import { SnackbarProvider } from '@/providers/snackbar-provider';
import { ThemeRegistry } from '@/providers/theme-registry';
import { Metadata } from 'next';
import { NextIntlClientProvider, useMessages } from 'next-intl';
import { getTranslations, unstable_setRequestLocale } from 'next-intl/server';
import { Inter } from 'next/font/google';

const dynamic = 'force-dynamic';

const inter = Inter({ subsets: ['latin'] });

export function generateStaticParams() {
  return locales.map((locale) => ({ locale }));
}

interface Props extends React.PropsWithChildren {
  params: {
    locale: (typeof locales)[number];
  };
}

export async function generateMetadata({ params: { locale } }: Props): Promise<Metadata> {
  const t = await getTranslations({ locale });

  const title = t('meta.title');

  return {
    title: {
      template: `${title} | %s`,
      default: title,
    },
    description: t('meta.description'),
    icons: {
      icon: [
        {
          media: '(prefers-color-scheme: light)',
          url: '/images/favicon.svg',
          href: '/images/favicon.svg',
        },
        {
          media: '(prefers-color-scheme: dark)',
          url: '/images/favicon.svg',
          href: '/images/favicon.svg',
        },
      ],
    },

    keywords: ['ner', 'rdf', 'annotation', 'linked data'],
    authors: [
      { name: 'Susan Brown' },
      { name: 'Mihaela Ilovan' },
      { name: 'Luciano Frizzera', url: 'http://luciano.fluxo.art.br' },
    ],
    publisher: 'LINCS Project - Linked Infrastructure for Networkd Cultural Scholarship',
    manifest: '/manifest.json',
    applicationName: 'Nerve Commons',
    openGraph: {
      title: 'Nerve Commons',
      description: t('meta.description'),
      url: 'https://nerve.lincsproject.ca',
      siteName: 'LEAF-Writer Commons',
      images: [
        {
          url: 'https://nerve.lincsproject.ca/images/favicon.svg', // Must be an absolute URL
          width: 800,
          height: 600,
          alt: 'Nerve Commons',
        },
        {
          url: 'https://nerve.lincsproject.ca/favicon.svg', // Must be an absolute URL
          width: 1800,
          height: 1600,
          alt: 'Nerve Commons',
        },
      ],
      locale: 'en',
      type: 'website',
    },
    twitter: {
      card: 'summary_large_image',
      title: 'Nerve Commons',
      description: t('meta.description'),
      site: 'https://nerve.lincsproject.ca',
      creator: '@lincsproject',
      // creatorId: '337814894',
      images: ['https://nerve.lincsproject.ca/images/logo.png'], // Must be an absolute URL //! file is not there
    },
    robots: {
      index: false,
      follow: true,
      nocache: true,
      googleBot: {
        index: true,
        follow: false,
        noimageindex: true,
        'max-video-preview': -1,
        'max-image-preview': 'large',
        'max-snippet': -1,
      },
    },
  };
}

export default function LocaleLayout({ children, params: { locale } }: Props) {
  unstable_setRequestLocale(locale);
  const messages = useMessages();

  return (
    <html data-color-scheme="light" lang={locale} suppressHydrationWarning={true}>
      <body className={inter.className} suppressHydrationWarning={true}>
        <NextAuthProvider>
          <JotaiProvider lincsAuthApiUrl={process.env.LINCS_AUTH_API_URL}>
            <ReactQueryProvider>
              <NextIntlClientProvider messages={messages}>
                <ThemeRegistry>
                  <DialogProvider>
                    <SnackbarProvider>
                      {children}
                      <CookieConsentComponent />
                    </SnackbarProvider>
                  </DialogProvider>
                </ThemeRegistry>
              </NextIntlClientProvider>
            </ReactQueryProvider>
          </JotaiProvider>
        </NextAuthProvider>
      </body>
    </html>
  );
}
