'use client';

import { ContainerCompiledMdxContent } from '@/components/mdx/container-compiled-mdx-content';
import { Box } from '@mui/joy';
import { mdxComponents } from '../components';
import en from './en.mdx';
import fr from './fr.mdx';

interface Props {
  locale?: string;
}

export const CompiledMdxContent = ({ locale = 'en' }: Props) => {
  return (
    <Box sx={{ py: { xs: 3, sm: 6 } }}>
      <ContainerCompiledMdxContent
        contents={{ en, fr }}
        locale={locale}
        mdxComponentOverride={mdxComponents}
      />
    </Box>
  );
};
