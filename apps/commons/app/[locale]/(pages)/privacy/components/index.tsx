import { Link, Typography } from '@mui/joy';
import type { MDXComponents } from 'mdx/types';
import * as CookieConsent from 'vanilla-cookieconsent';

export const mdxComponents: MDXComponents = {
  h1: ({ children }) => (
    <Typography
      level="h1"
      py={10}
      sx={{
        '[data-mui-color-scheme="dark"] &': { color: 'primary.300' },
      }}
      textAlign="center"
    >
      {children}
    </Typography>
  ),
  manage_cookies_preferences: ({ children }) => {
    return (
      <Link
        color="neutral"
        href="#"
        onPointerDown={() => CookieConsent.showPreferences()}
        underline="always"
      >
        {children}
      </Link>
    );
  },
};
