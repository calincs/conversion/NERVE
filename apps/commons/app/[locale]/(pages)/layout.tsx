import { auth } from '@/auth';
import { Footer, NavBar, Profile, TopBar } from '@/components';
import { CookiesPreferenceButton, LocaleSwitcher } from '@/components/ui';
import { locales } from '@/i18n';
import { Sheet, Stack } from '@mui/joy';
import { unstable_setRequestLocale } from 'next-intl/server';

const dynamic = 'force-dynamic';

export default async function AboutLayout({
  children,
  params: { locale },
}: {
  children: React.ReactNode;
  params: {
    locale: (typeof locales)[number];
  };
}) {
  unstable_setRequestLocale(locale);

  const session = await auth();
  return (
    <main style={{ height: '100vh', width: '100vw' }}>
      <TopBar
        left={<NavBar />}
        right={
          session ? (
            <Profile />
          ) : (
            <Stack direction="row" gap={1}>
              {<LocaleSwitcher />}
              {<CookiesPreferenceButton />}
            </Stack>
          )
        }
        variant="float"
      />
      <Sheet
        sx={{
          py: { xs: 6, sm: 12 },
          background: 'linear-gradient(180deg, rgba(45,156,219,0) 0%, rgba(13,103,142,0.4) 100%)',
        }}
      >
        <Sheet sx={{ maxWidth: 1000, m: '0 auto', backgroundColor: 'transparent' }}>
          {children}
        </Sheet>
      </Sheet>
      <Footer />
    </main>
  );
}
