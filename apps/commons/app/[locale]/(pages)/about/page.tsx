import { locales } from '@/i18n';
import { Metadata } from 'next';
import { getTranslations, unstable_setRequestLocale } from 'next-intl/server';
import { CompiledMdxContent } from './content';

const dynamic = 'force-dynamic';

export async function generateMetadata({
  params: { locale },
}: {
  params: { locale: (typeof locales)[number] };
}): Promise<Metadata> {
  const t = await getTranslations({ locale });
  return {
    title: t('common.about'),
  };
}

export default function About({
  params: { locale },
}: {
  params: { locale: (typeof locales)[number] };
}) {
  unstable_setRequestLocale(locale);

  return <CompiledMdxContent {...{ locale }} />;
}
