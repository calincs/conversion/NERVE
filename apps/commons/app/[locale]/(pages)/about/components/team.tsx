import { Grid, type GridProps } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { TeamMember } from './team-member';

export const Team = (props: GridProps) => {
  const t = useTranslations();
  return (
    <Grid container columnSpacing={4} rowSpacing={4} maxWidth={900} sx={{ flexGrow: 1 }} {...props}>
      <Grid>
        <TeamMember
          name="Susan Brown"
          email="sbrown@uoguelph.ca"
          institution={t('about.team.university of guelph')}
          links={[
            { type: 'website', url: 'https://www.uoguelph.ca/~sbrown/' },
            { type: 'orcid', url: 'https://orcid.org/0000-0002-0267-7344' },
            { type: 'gitlab', url: 'https://gitlab.com/SusanIreneBrown' },
            { type: 'github', url: 'https://github.com/SusanBrown' },
          ]}
          positions={[
            { title: t('about.team.project director'), date: `2021 - ${t('about.team.present')}` },
          ]}
        />
      </Grid>
      <Grid>
        <TeamMember
          name="Luciano Frizzera"
          email="lucaju@gmail.com"
          institution={t('about.team.concordia university')}
          links={[
            { type: 'website', url: 'https://lucianofrizzera.com' },
            { type: 'orcid', url: 'https://orcid.org/0000-0001-7244-4178' },
            { type: 'gitlab', url: 'https://gitlab.com/lucaju' },
            { type: 'github', url: 'https://github.com/lucaju' },
          ]}
          positions={[
            {
              title: `${t('about.team.ux designer')} & ${t('about.team.lead developer')}`,
              date: `2021 - ${t('about.team.present')}`,
            },
          ]}
        />
      </Grid>
      <Grid>
        <TeamMember
          name="Mihaela Ilovan"
          email="ilovan@ualberta.ca"
          institution={t('about.team.university of alberta')}
          links={[
            { type: 'gitlab', url: 'https://gitlab.com/ilovan' },
            { type: 'github', url: 'https://github.com/ilovan' },
          ]}
          positions={[
            { title: t('about.team.project manager'), date: `2021 - ${t('about.team.present')}` },
          ]}
        />
      </Grid>
      <Grid>
        <TeamMember
          name="Pieter Botha"
          institution={t('about.team.university of guelph')}
          links={[{ type: 'gitlab', url: 'https://gitlab.com/zacanbot' }]}
          positions={[
            { title: t('about.team.tecnical manager'), date: `2011 - ${t('about.team.present')}` },
          ]}
        />
      </Grid>
      <Grid>
        <TeamMember
          name="Natalie Hervieux"
          institution={t('about.team.university of alberta')}
          links={[{ type: 'gitlab', url: 'https://gitlab.com/nhervieu' }]}
          positions={[
            {
              title: t('about.team.Research IT Analyst'),
              date: `2021 - ${t('about.team.present')}`,
            },
          ]}
        />
      </Grid>
      <Grid>
        <TeamMember
          name="Alliyya Mo"
          institution={t('about.team.university of guelph')}
          links={[
            { type: 'gitlab', url: 'https://gitlab.com/alliyya' },
            { type: 'github', url: 'https://github.com/alliyya' },
          ]}
          positions={[
            {
              title: t('about.team.Data Interface Developer'),
              date: `2021 - ${t('about.team.present')}`,
            },
          ]}
        />
      </Grid>
      <Grid>
        <TeamMember
          name="Jessica Ye"
          institution={t('about.team.university of guelph')}
          links={[{ type: 'gitlab', url: 'https://gitlab.com/Zexi-Jessica' }]}
          positions={[
            { title: t('about.team.Ontology and Metadata Specialist'), date: `2021 - 2024` },
          ]}
        />
      </Grid>
      <Grid>
        <TeamMember
          name="Erin Canning"
          institution={t('about.team.university of guelph')}
          links={[{ type: 'gitlab', url: 'https://gitlab.com/eecanning' }]}
          positions={[{ title: t('about.team.ontology systems'), date: `2021 - 2023` }]}
        />
      </Grid>
      <Grid>
        <TeamMember
          name="Huma Zafar"
          institution={t('about.team.University of Ottawa')}
          links={[{ type: 'gitlab', url: 'https://gitlab.com/hzafar' }]}
          positions={[{ title: t('about.team.Programming Analyst'), date: `2021 - 2022` }]}
        />
      </Grid>
    </Grid>
  );
};
