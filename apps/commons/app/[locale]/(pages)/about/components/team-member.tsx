'use client';

import { Link } from '@/navigation';
import { toHashHex } from '@/server-actions';
import {
  IconButton,
  IconButtonProps,
  Sheet,
  SheetProps,
  Stack,
  Tooltip,
  TooltipProps,
  Typography,
  TypographyProps,
  type StackProps,
} from '@mui/joy';
import { AnimatePresence, motion } from 'framer-motion';
import Image from 'next/image';
import { useEffect, useState } from 'react';
import { AiOutlineMail } from 'react-icons/ai';
import { CgWebsite } from 'react-icons/cg';
import { CiLinkedin } from 'react-icons/ci';
import { LiaOrcid, LiaResearchgate } from 'react-icons/lia';
import { LuInstagram, LuLink2 } from 'react-icons/lu';
import { RiGithubLine, RiGitlabLine, RiTwitterXFill } from 'react-icons/ri';
import { SlSocialYoutube } from 'react-icons/sl';

interface Position {
  date: string;
  title: string;
}

interface link {
  type?: string;
  url: string;
}

export interface Props {
  email?: string;
  gravatarEmail?: string;
  institution?: string;
  name: string;
  links?: link[];
  positions?: Position[];
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    avatar?: Omit<SheetProps, 'children'>;
    stack?: Omit<StackProps, 'children'>;
    name?: Omit<TypographyProps, 'children'>;
    institution?: Omit<TypographyProps, 'children'>;
    positions?: Omit<TypographyProps, 'children'>;
    linksPopover?: {
      root?: Pick<StackProps, 'sx'>;
      tooltip?: Omit<TooltipProps, 'children' | 'title'>;
      iconButton?: Omit<IconButtonProps, 'children'>;
    };
  };
}

export const imageLoader = ({ src, width }: { src: string; width: number }) => {
  return `https://www.gravatar.com/avatar/${src}?d=retro&s=${width}}`;
};

export const TeamMember = ({
  gravatarEmail,
  email,
  institution,
  links,
  name,
  positions,
  slotProps,
}: Props) => {
  const [hover, setHover] = useState(false);
  const [hashGravatar, setHashGravatar] = useState<string | undefined>(undefined);

  const updateHashName = async () => {
    const hash = await toHashHex(gravatarEmail ?? email ?? name);
    setHashGravatar(hash);
  };

  useEffect(() => {
    updateHashName();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Stack
      direction="row"
      gap={0.5}
      {...slotProps?.root}
      onPointerOver={(event) => {
        setHover(true);
        slotProps?.root?.onPointerOver?.(event);
      }}
      onPointerOut={(event) => {
        setHover(false);
        slotProps?.root?.onPointerOut?.(event);
      }}
    >
      <Sheet
        component={motion.div}
        whileHover={{ y: -4 }}
        sx={{
          backgroundColor: 'transparent',
          width: 48,
          height: 48,
          borderRadius: ({ radius }) => radius.sm,
          overflow: 'hidden',
        }}
        {...slotProps?.avatar}
      >
        {hashGravatar && (
          <Image loader={imageLoader} src={hashGravatar} width={48} height={48} alt={name} />
        )}
      </Sheet>
      <Stack px={0.5} position="relative" gap={0.5} {...slotProps?.stack}>
        <Typography fontWeight={700} level="body-sm" {...slotProps?.name}>
          {name}
        </Typography>
        {/* <Typography level="body-xs" {...slotProps?.institution}>
          {institution}
        </Typography> */}
        <Typography level="body-xs" {...slotProps?.positions}>
          {positions?.map((position) => position.title).join(', ')}
        </Typography>
        <AnimatePresence>
          {hover && (
            <Stack
              component={motion.div}
              initial={{ opacity: 0, y: -4 }}
              animate={{ opacity: 1, y: 0 }}
              exit={{ opacity: 0, y: -4 }}
              direction="row"
              gap={0.5}
              alignItems="flex-start"
              position="absolute"
              bottom={-20}
              {...slotProps?.linksPopover?.root}
            >
              {links?.map(({ url, type }) => (
                <Tooltip
                  key={url}
                  enterDelay={1000}
                  size="sm"
                  title={type}
                  {...slotProps?.linksPopover?.tooltip}
                >
                  <Link href={url} target="_blank">
                    <IconButton
                      aria-label={type}
                      size="sm"
                      sx={{ '--IconButton-size': '24px' }}
                      {...slotProps?.linksPopover?.iconButton}
                    >
                      {LinkIcon(type)}
                    </IconButton>
                  </Link>
                </Tooltip>
              ))}
            </Stack>
          )}
        </AnimatePresence>
      </Stack>
    </Stack>
  );
};

const LinkIcon = (type?: string) => {
  if (type?.toLowerCase() === 'website') return <CgWebsite />;
  if (type?.toLowerCase() === 'gitlab') return <RiGitlabLine />;
  if (type?.toLowerCase() === 'github') return <RiGithubLine />;
  if (type?.toLowerCase() === 'twitter' || type?.toLowerCase() === 'x') return <RiTwitterXFill />;
  if (type?.toLowerCase() === 'youtube') return <SlSocialYoutube />;
  if (type?.toLowerCase() === 'email') return <AiOutlineMail />;
  if (type?.toLowerCase() === 'linkedin') return <CiLinkedin />;
  if (type?.toLowerCase() === 'orcid') return <LiaOrcid />;
  if (type?.toLowerCase() === 'researchgate') return <LiaResearchgate />;
  if (type?.toLowerCase() === 'instagram') return <LuInstagram />;

  return <LuLink2 />;
};
