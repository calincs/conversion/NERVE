import { Box, Typography } from '@mui/joy';
import type { MDXComponents } from 'mdx/types';

export const mdxComponents: MDXComponents = {
  h1: ({ children }) => (
    <Typography
      level="h1"
      sx={{
        minWidth: 300,
        maxWidth: 300,
        '[data-mui-color-scheme="dark"] &': { color: 'primary.300' },
      }}
      textAlign={{ xs: 'center', sm: 'right' }}
    >
      {children}
    </Typography>
  ),
  h2: ({ children }) => (
    <Typography
      level="h2"
      sx={{
        minWidth: 300,
        maxWidth: 300,
        '[data-mui-color-scheme="dark"] &': { color: 'primary.400' },
      }}
      textAlign={{ xs: 'center', sm: 'right' }}
    >
      {children}
    </Typography>
  ),
  h3: ({ children }) => (
    <Typography
      level="h3"
      sx={{
        minWidth: 300,
        maxWidth: 300,
        '[data-mui-color-scheme="dark"] &': { color: 'primary.400' },
      }}
      textAlign={{ xs: 'center', sm: 'right' }}
    >
      {children}
    </Typography>
  ),
  h4: ({ children }) => (
    <Typography
      level="h4"
      sx={{
        minWidth: 300,
        maxWidth: 300,
        '[data-mui-color-scheme="dark"] &': { color: 'primary.400' },
      }}
      textAlign={{ xs: 'center', sm: 'right' }}
    >
      {children}
    </Typography>
  ),
  body: ({ children }) => <Box maxWidth={900}>{children}</Box>,
};
