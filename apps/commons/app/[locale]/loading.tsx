import { CircularProgress, Stack } from '@mui/joy';

export default function Loading() {
  return (
    <Stack sx={{ height: '100vh', width: '100vw', alignItems: 'center', justifyContent: 'center' }}>
      <CircularProgress />
    </Stack>
  );
}
