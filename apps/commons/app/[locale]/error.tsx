'use client';

import { Logo } from '@/components/logo';
import { Button, Sheet, Typography } from '@mui/joy';
import { useTranslations } from 'next-intl';

interface Props {
  error: Error;
  reset: () => void;
}

export default function Error({ error, reset }: Props) {
  const t = useTranslations();

  return (
    <Sheet>
      <Logo direction="vertical" variation="simplified" />
      <Typography level="h1">{t('common.error')}</Typography>
      <Typography>{error.message}</Typography>
      <Button onPointerDown={reset}>{t('common.Something whent wrong')}</Button>
    </Sheet>
  );
}
