import { locales } from '@/i18n';
import { Typography } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { unstable_setRequestLocale } from 'next-intl/server';
import { Broadcast } from '../components/broadcast';

const dynamic = 'force-dynamic';

export default function LinkAccounts({
  params: { locale },
  searchParams,
}: {
  params: { locale: (typeof locales)[number] };
  searchParams: Record<string, string | string[] | null>;
}) {
  unstable_setRequestLocale(locale);

  const t = useTranslations();

  const codeVerifier = Array.isArray(searchParams.codeVerifier)
    ? searchParams.codeVerifier[0]
    : searchParams.codeVerifier;

  const error = Array.isArray(searchParams.error) ? searchParams.error[0] : searchParams.error;

  return (
    <>
      <Typography level="h3">{t('ServicePage.Account Manage callback')}</Typography>
      {error && (
        <>
          <Typography level="h4" textTransform="capitalize">
            {t('common.error')}
          </Typography>
          <Typography color="danger" variant="soft">
            {error}
          </Typography>
        </>
      )}
      <Broadcast channel="keycloak-account-manager" {...{ codeVerifier, error }} />
    </>
  );
}
