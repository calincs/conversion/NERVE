'use client';

import type { Channel } from '@/util/http/broadcast-channel';
import { Button, type ButtonProps } from '@mui/joy';
import { BroadcastChannel } from 'broadcast-channel';
import { useTranslations } from 'next-intl';
import { useEffect } from 'react';

interface Props {
  channel: Channel;
  codeVerifier?: string | null;
  error?: string | null;
  isRefresh?: boolean | null;
  provider?: string | null;
  slotProps?: {
    root?: ButtonProps;
  };
}

export const Broadcast = ({ slotProps, ...props }: Props) => {
  const t = useTranslations();

  useEffect(() => {
    const channel = new BroadcastChannel(props.channel);
    const message = { success: !props.error, ...props };
    void channel.postMessage(message);
    window.close();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Button
      {...slotProps?.root}
      onPointerDown={(event) => {
        window.close();
        slotProps?.root?.onPointerDown?.(event);
      }}
      variant="plain"
    >
      {t('ServicePage.Close this window')}
    </Button>
  );
};
