'use client';

import 'client-only';
import { signIn } from 'next-auth/react';
import { useEffect } from 'react';

export const Redirect = ({ callbackUrl }: { callbackUrl: string }) => {
  useEffect(() => {
    void signIn('keycloak', { callbackUrl }, { prompt: 'login' });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <></>;
};
