import { Typography } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { Broadcast } from '../components/broadcast';
import { Redirect } from './redirect';
import { locales } from '@/i18n';
import { unstable_setRequestLocale } from 'next-intl/server';

const dynamic = 'force-dynamic';

export default function LoginProxyCallback({
  params: { locale },
  searchParams,
}: {
  params: { locale: (typeof locales)[number] };
  searchParams: Record<string, string | string[] | null>;
}) {
  unstable_setRequestLocale(locale);

  const t = useTranslations();

  const callbackUrl = Array.isArray(searchParams.callbackUrl)
    ? searchParams.callbackUrl[0]
    : searchParams.callbackUrl;

  if (!!callbackUrl) {
    return (
      <>
        <Typography level="h3">{t('common.Signing in')}...</Typography>
        <Redirect {...{ callbackUrl }} />
      </>
    );
  }

  const codeVerifier = Array.isArray(searchParams.codeVerifier)
    ? searchParams.codeVerifier[0]
    : searchParams.codeVerifier;

  const error = Array.isArray(searchParams.error) ? searchParams.error[0] : searchParams.error;

  return (
    <>
      {error ? (
        <>
          <Typography level="h4" textTransform="capitalize">
            {t('common.error')} {t('common.Signing in')}
          </Typography>
          <Typography color="danger">{error}</Typography>
        </>
      ) : (
        <Typography level="h3">{t('common.Signed in')}...</Typography>
      )}
      <Broadcast channel="keycloak-login-callback" {...{ codeVerifier, error }} />
    </>
  );
}
