import { Footer, Logo } from '@/components';
import { locales } from '@/i18n';
import { Sheet, Stack } from '@mui/joy';
import { unstable_setRequestLocale } from 'next-intl/server';

const dynamic = 'force-dynamic';

export default async function AccountLayout({
  children,
  params: { locale },
}: {
  children: React.ReactNode;
  params: {
    locale: (typeof locales)[number];
  };
}) {
  unstable_setRequestLocale(locale);
  return (
    <>
      <Sheet
        component="main"
        sx={{
          height: '100vh',
          width: '100vw',
          background: 'linear-gradient(180deg, rgba(45,156,219,0) 0%, rgba(13,103,142,0.4) 100%)',
        }}
      >
        <Stack alignItems="center" gap={3} justifyContent="center" sx={{ height: '50vh' }}>
          <Logo variation="simplified" />
          {children}
        </Stack>
      </Sheet>
      <Footer />
    </>
  );
}
