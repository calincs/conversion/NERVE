import { locales } from '@/i18n';
import { Typography } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { unstable_setRequestLocale } from 'next-intl/server';
import { Broadcast } from '../components/broadcast';

const dynamic = 'force-dynamic';

export default function LinkAccounts({
  params: { locale },
  searchParams,
}: {
  params: { locale: (typeof locales)[number] };
  searchParams: Record<string, string | string[] | null>;
}) {
  unstable_setRequestLocale(locale);

  const t = useTranslations();

  const codeVerifier = Array.isArray(searchParams.codeVerifier)
    ? searchParams.codeVerifier[0]
    : searchParams.codeVerifier;

  const error = Array.isArray(searchParams.error) ? searchParams.error[0] : searchParams.error;

  const provider = Array.isArray(searchParams.provider)
    ? searchParams.provider[0]
    : searchParams.provider;

  const isRefresh = Array.isArray(searchParams.isRefresh)
    ? searchParams.isRefresh[0]
    : searchParams.isRefresh;

  return (
    <>
      <Typography level="h3">
        {isRefresh === 'true' ? t('ServicePage.Refresh Tokens') : t('ServicePage.Linked Account')}
      </Typography>
      {error ? (
        <>
          <Typography level="h4" textTransform="capitalize">
            {t('common.error')}
          </Typography>
          <Typography color="danger" variant="soft">
            {error}
          </Typography>
        </>
      ) : (
        <Typography color="success" variant="soft">
          <Typography textTransform="capitalize">{provider}</Typography>
          <Typography>
            {isRefresh ? ` ${t('ServicePage.tokens refreshed')}` : ` ${t('ServicePage.linked')}`}
          </Typography>
        </Typography>
      )}
      <Broadcast
        channel="keycloak-link-account"
        isRefresh={isRefresh === 'true' ? true : false}
        {...{ codeVerifier, error, provider }}
      />
    </>
  );
}
