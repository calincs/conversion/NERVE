import { auth } from '@/auth';
import { Footer, NavBar, Profile, TopBar } from '@/components';
import { FilePanel } from '@/components/file-panel';
import { CookiesPreferenceButton, LocaleSwitcher } from '@/components/ui';
import { locales } from '@/i18n';
import { Sheet, Stack } from '@mui/joy';
import { unstable_setRequestLocale } from 'next-intl/server';
import { CompiledMdxContent as About } from './(pages)/about/content';
import { Header } from './components/header';
import { Suspense } from 'react';

const dynamic = 'force-dynamic';

export default async function Homepage({
  params: { locale },
  searchParams,
}: {
  params: { locale: (typeof locales)[number] };
  searchParams: Record<string, string>;
}) {
  unstable_setRequestLocale(locale);

  const isMobile = searchParams.viewport === 'mobile';

  const session = await auth();

  return (
    <main style={{ height: '100vh', width: '100vw' }}>
      <TopBar
        left={!isMobile ? <NavBar logoDisabled /> : <LocaleSwitcher />}
        right={
          session ? (
            <Profile />
          ) : (
            <Stack direction="row" gap={1}>
              {!isMobile && <LocaleSwitcher />}
              {!isMobile && <CookiesPreferenceButton />}
            </Stack>
          )
        }
        variant="float"
      />
      <Sheet
        id="bg"
        sx={{
          height: '80vh',
          minHeight: 900,
          mx: 'auto',
          p: 8,
          background:
            'radial-gradient(circle at 50% 100%, rgba(13, 103, 142, 0.34) 0%, rgba(45, 156, 219, 0.00) 97%);',
          transition: 'all 0.4s',
        }}
      >
        <Stack alignItems="center" gap={10}>
          <Header isSignedIn={!!session} />
          <Suspense fallback={'loading'}>
            <FilePanel />
          </Suspense>
        </Stack>
      </Sheet>
      {!session && (
        <Sheet variant="soft">
          <About />
        </Sheet>
      )}
      <Footer />
    </main>
  );
}
