import { Logo, type LogoProps } from '@/components/logo';
import { SignIn } from '@/components/ui/sign-in';
import { Stack, Typography, type StackProps } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { Composite } from './composite';

export interface HeaderProps {
  isSignedIn: boolean;
  slotProps?: {
    root?: Omit<StackProps, 'children'>;
    stack?: Omit<StackProps, 'children'>;
    logo?: LogoProps;
  };
}

export const Header = ({ isSignedIn, slotProps }: HeaderProps) => {
  const t = useTranslations();
  return (
    <Stack direction="row" justifyContent="center" alignItems="center" gap={4} {...slotProps?.root}>
      <Stack alignItems="center" gap={2} sx={{ width: '100%' }} {...slotProps?.stack}>
        <Logo variation="full" {...slotProps?.logo} />
        {!isSignedIn && <SignIn variant="outlined" />}
        <Typography
          display={{ xs: 'block', sm: 'none' }}
          letterSpacing="0.05rem"
          level="body-sm"
          textAlign="center"
        >
          {t('mobile.largerScreenMessage')}
        </Typography>
      </Stack>
      {!isSignedIn && <Composite />}
    </Stack>
  );
};
