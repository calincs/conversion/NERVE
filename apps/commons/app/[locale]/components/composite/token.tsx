import { Stack, Typography, type StackProps } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { BiSolidBookAlt, BiSolidQuoteRight } from 'react-icons/bi';
import { BsFillPersonFill } from 'react-icons/bs';
import { FaBoxOpen } from 'react-icons/fa';
import { MdPlace } from 'react-icons/md';
import { PiLightbulbFill } from 'react-icons/pi';
import { RiOrganizationChart } from 'react-icons/ri';
import style from './style.module.css';

type EntityType =
  | 'person'
  | 'place'
  | 'organization'
  | 'work'
  | 'physical thing'
  | 'conceptual object'
  | 'citation';

export interface TokenProps extends StackProps {
  entityType?: EntityType;
  index?: number;
  length: number;
}

const DELAY_INITIAL = 2;
const DELAY_MULTIPLIER = 0.08;

export const Token = ({ entityType, index = 0, length = 30, ...props }: TokenProps) => {
  const t = useTranslations();

  const DELAY = DELAY_INITIAL + index * DELAY_MULTIPLIER;

  return (
    <Stack
      className={style.token}
      direction="row"
      alignItems="center"
      gap={{ md: 0.2, lg: 0.5 }}
      sx={{
        height: { md: 20, lg: 27 },
        width: length,
        py: { md: 0.125, lg: 0.5 },
        px: { md: 0.4, lg: 1 },
        borderRadius: 'sm',
        overflow: 'hidden',
      }}
      style={
        {
          '--background-color': getColor(entityType),
          '--anim-delay': `${DELAY}s`,
        } as React.CSSProperties
      }
      {...props}
    >
      {entityType && (
        <>
          <Stack
            className={style.icon}
            color="white"
            sx={{ transform: { md: 'scale(0.7)', lg: 'scale(1)' } }}
          >
            <Icon type={entityType} />
          </Stack>
          <Stack className={style.label} sx={{ mt: 0.125 }}>
            <Typography
              fontSize={{ md: '0.66rem', lg: '0.75rem' }}
              fontWeight={700}
              level="body-xs"
              sx={{ color: 'white' }}
              textTransform="uppercase"
            >
              {t(`common.${entityType}`)}
            </Typography>
          </Stack>
        </>
      )}
    </Stack>
  );
};

const getColor = (type?: EntityType) => {
  if (type === 'person') return '#2E86DE';
  if (type === 'place') return '#FF9F43';
  if (type === 'organization') return '#B0B97A';
  if (type === 'work') return '#AF46F0';
  if (type === 'physical thing') return '#8395A7';
  if (type === 'conceptual object') return '#FF6B00';
  if (type === 'citation') return '#008040';
  return '#636b7414';
};

const Icon = ({ type }: { type?: EntityType }) => {
  if (type === 'person') return <BsFillPersonFill />;
  if (type === 'place') return <MdPlace />;
  if (type === 'organization') return <RiOrganizationChart />;
  if (type === 'work') return <BiSolidBookAlt />;
  if (type === 'physical thing') return <FaBoxOpen />;
  if (type === 'conceptual object') return <PiLightbulbFill />;
  if (type === 'citation') return <BiSolidQuoteRight />;

  return null;
};
