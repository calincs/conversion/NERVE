import { Sheet, Stack, type SheetProps, type StackProps } from '@mui/joy';
import { Token } from './token';
import { tokens } from './tokens';

export interface CompositeProps {
  slotProps?: {
    root?: Omit<SheetProps, 'children'>;
    stack?: Omit<StackProps, 'children'>;
  };
}

export const Composite = ({ slotProps }: CompositeProps) => {
  return (
    <Sheet
      color="neutral"
      sx={{
        display: { xs: 'none', md: 'flex' },
        width: { md: 390, lg: '100%' },
        height: { md: 288, lg: 382 },
        p: 2,
        overflow: 'hidden',
        borderRadius: 12,
        boxShadow:
          'var(--nerve-shadowRing, 0 0 #000),0px 2px 8px -2px rgba(var(--nerve-shadowChannel, 21 21 21) / var(--nerve-shadowOpacity, 0.08)),0px 20px 24px -4px rgba(var(--nerve-shadowChannel, 21 21 21) / var(--nerve-shadowOpacity, 0.08))',
        transition: 'all 0.4s ease-in',
      }}
      variant="soft"
      {...slotProps?.root}
    >
      <Stack
        direction="row"
        alignContent="flex-start"
        flexWrap="wrap"
        gap={{ md: 0.9, lg: 1.2 }}
        {...slotProps?.stack}
      >
        {tokens.map((token, i) => (
          <Token key={i} index={i} {...token} />
        ))}
      </Stack>
    </Sheet>
  );
};
